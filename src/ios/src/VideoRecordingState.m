//
// Created by Constantin on 14/04/15.
// Copyright (c) 2015 coolshutter. All rights reserved.
//

#import "VideoRecordingState.h"
#import "VideoUtils.h"

@interface VideoRecordingState () {

}


// array of recorded video parts file paths
@property NSMutableArray* videoParts;

@end

@implementation VideoRecordingState

- (id)initWithMaximumTime:(float)seconds
{
    self.videoParts = [NSMutableArray array];
    self.remainingTime = seconds;
    return self;
}


- (void)addVideoPart:(NSString *)filePath withDuration:(float)seconds
{
    [self.videoParts addObject:filePath];
    _remainingTime -= seconds;
    if (_remainingTime < 0) _remainingTime = 0;
    //NSLog(@"Video parts remaining time = %f", _remainingTime);
}

- (int)getPartsCount
{
    return (int)self.videoParts.count;
}


- (void)discardRecordedFiles
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    for (NSString* path in _videoParts)
    {
        NSError* err = nil;
        [fileManager removeItemAtPath:path error:&err];
        if (err != nil)
        {
            //NSLog(@"Error removing temporary file [%@]: %@", path, err);
        }
    }
    [self.videoParts removeAllObjects];
}

- (NSString*)getPartPath:(int)idx
{
    return self.videoParts[idx];
}

- (void)joinVideoToFile:(NSString*)outputFile completion:(void (^)(BOOL success))completion
{
    [VideoUtils joinVideoParts:self.videoParts toFile:outputFile completion:completion];
}

@end