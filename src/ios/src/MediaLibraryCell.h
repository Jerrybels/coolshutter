//
// Created by Constantin on 26/04/15.
// Copyright (c) 2015 coolshutter. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * const MediaLibraryCellIdentifier;


@interface MediaLibraryCell : UICollectionViewCell

@property (strong, nonatomic) UIImageView *imgView;



- (void)setChecked:(BOOL)checked animated:(BOOL)animated;
- (void)setVideoDuration:(double)duration;
- (void)showVideoDurationLabel:(BOOL)show;

@end
