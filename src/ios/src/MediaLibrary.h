//
// Created by Constantin on 26/04/15.
// Copyright (c) 2015 coolshutter. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CameraPluginParams;
@class HMSegmentedControl;
@class UICollectionView;
@class ALAsset;


@protocol MediaLibraryDelegate

- (void)addSelectedItemWithPath:(NSString *)assetURI thumbnailImage:(UIImage *)thumbnail animateFromRect:(CGRect)srcRect isVideo:(BOOL)isVideo assetObject:(ALAsset *)object;

- (void)removeSelectedItemWithPath:(NSString *)assetURI;
- (void)discardAllSelectedItems;
@end

@interface MediaLibrary : NSObject

@property (weak) id<MediaLibraryDelegate> delegate;


- (void)showGallery:(BOOL)show;

- (id)initWithGridView:(UICollectionView *)gridView containerView:(UIView*)container
       filtersControls:(HMSegmentedControl *)filterControls
          pluginParams:(CameraPluginParams*)params;

- (void)deselectItemWithURI:(NSString *)path;

@end