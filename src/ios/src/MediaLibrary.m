//
// Created by Constantin on 26/04/15.
// Copyright (c) 2015 coolshutter. All rights reserved.
//

#import "MediaLibrary.h"
#import "HMSegmentedControl.h"
#import "CameraPluginParams.h"
#import "CameraPluginLocalization.h"
#import "ColorUtils.h"
#import "MediaLibraryCell.h"
#import "MediaGridFlowLayout.h"
#import <AssetsLibrary/AssetsLibrary.h>

UIColor* FILTER_LABEL_TEXT_COLOR_INACTIVE;
UIColor* FILTER_LABEL_TEXT_COLOR_ACTIVE;
UIColor* FILTER_SELECTED_BAR_COLOR;

NSString* FILTER_LABEL_FONT = @"HelveticaNeue-Medium";
#define FILTER_LABEL_TEXT_SIZE  14

typedef NS_ENUM(NSInteger, GalleryFilterType) {
    GalleryFilterPhoto,
    GalleryFilterVideo
};

@interface MediaLibrary ()  <UICollectionViewDataSource, UICollectionViewDelegate>
{
    ALAssetsFilter* _currentAssetFilter;
    GalleryFilterType _currentFilter;
}

@property (weak) UIView* galleryContainer;
@property (weak) UICollectionView* gridView;
@property (weak) HMSegmentedControl *filterControls;
@property (weak) CameraPluginParams *pluginParams;

@property (strong) ALAssetsLibrary *mediaLibrary;
@property (strong) NSMutableArray* assetGroups;
@property (strong) NSMutableArray* buttonIndexToFilterType;

// array of ALAsset
@property (strong) NSMutableArray *mediaItems;
@property (strong) NSMutableSet *selectedItems;

@end

@implementation MediaLibrary

- (id)initWithGridView:(UICollectionView *)gridView containerView:(UIView*)container
       filtersControls:(HMSegmentedControl *)filterControls
          pluginParams:(CameraPluginParams*)params
{
    self = [super init];
    self.galleryContainer = container;
    self.gridView = gridView;
    self.filterControls = filterControls;
    self.pluginParams = params;

    FILTER_LABEL_TEXT_COLOR_INACTIVE = [[UIColor alloc] initWithRGBValue:0xCECFD0];
    FILTER_LABEL_TEXT_COLOR_ACTIVE = [[UIColor alloc] initWithRGBValue:0x777777];
    FILTER_SELECTED_BAR_COLOR   = [[UIColor alloc] initWithRGBValue:0x22AABA];

    self.mediaLibrary = [[ALAssetsLibrary alloc] init];
    self.selectedItems = [NSMutableSet set];

    [self setupGalleryFilterButtons];
    [self setupGalleryGridView];
    return self;
}

- (void)setupGalleryFilterButtons
{
    self.filterControls.type = HMSegmentedControlTypeTextImagesInLine;
    self.filterControls.backgroundColor = [UIColor whiteColor];
    self.filterControls.imagePadding = 7;
    self.filterControls.selectionStyle = HMSegmentedControlSelectionStyleFullWidthStripe;
    self.filterControls.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    self.filterControls.selectionIndicatorHeight = 4;
    self.filterControls.selectionIndicatorColor = FILTER_SELECTED_BAR_COLOR;

    _currentAssetFilter = nil;
    _currentFilter = GalleryFilterVideo;

    self.buttonIndexToFilterType = [NSMutableArray arrayWithCapacity:2];
    NSMutableArray *images = [NSMutableArray arrayWithCapacity:2];
    NSMutableArray *selectedImages = [NSMutableArray arrayWithCapacity:2];
    NSMutableArray *titles = [NSMutableArray arrayWithCapacity:2];
    if (self.pluginParams.photoGalleryAllowed)
    {
        [images addObject:[UIImage imageNamed:@"gallery_button_photo_off.png"]];
        [selectedImages addObject:[UIImage imageNamed:@"gallery_button_photo_on.png"]];
        [titles addObject:self.pluginParams.localization.photosLabel];
        _currentAssetFilter = [ALAssetsFilter allPhotos];
        _currentFilter = GalleryFilterPhoto;
        [self.buttonIndexToFilterType addObject:@(GalleryFilterPhoto)];
    }

    if (self.pluginParams.videoGalleryAllowed)
    {
        [images addObject:[UIImage imageNamed:@"gallery_button_video_off.png"]];
        [selectedImages addObject:[UIImage imageNamed:@"gallery_button_video_on.png"]];
        [titles addObject:self.pluginParams.localization.videosLabel];
        if (_currentAssetFilter == nil) _currentAssetFilter = [ALAssetsFilter allVideos];
        [self.buttonIndexToFilterType addObject:@(GalleryFilterVideo)];
    }

    self.filterControls.sectionImages = images;
    self.filterControls.sectionSelectedImages = selectedImages;
    self.filterControls.sectionTitles = titles;
    self.filterControls.titleTextAttributes = @{
            NSForegroundColorAttributeName: FILTER_LABEL_TEXT_COLOR_INACTIVE,
            NSFontAttributeName: [UIFont fontWithName:FILTER_LABEL_FONT size: FILTER_LABEL_TEXT_SIZE]
    };
    self.filterControls.selectedTitleTextAttributes = @{
            NSForegroundColorAttributeName: FILTER_LABEL_TEXT_COLOR_ACTIVE
    };

    self.filterControls.indexChangeBlock = ^(NSInteger index){
        [self switchFilterTo:index];
    };
}

- (void)switchFilterTo:(NSInteger)index
{
    GalleryFilterType newFilter = (GalleryFilterType) [((NSNumber *) self.buttonIndexToFilterType[(NSUInteger) index]) integerValue];
    if (newFilter == _currentFilter) return;
    _currentFilter = newFilter;

    switch (newFilter)
    {
        case GalleryFilterPhoto:
            _currentAssetFilter = [ALAssetsFilter allPhotos];
            break;

        case GalleryFilterVideo:
            _currentAssetFilter = [ALAssetsFilter allVideos];
            break;
    }

    [self.selectedItems removeAllObjects];

    [self.delegate discardAllSelectedItems];

    [self.gridView setContentOffset:CGPointMake(0,0) animated:NO];
    [self.mediaItems removeAllObjects];
    [self loadMediaItems:_currentAssetFilter];
}

- (void)setupGalleryGridView
{
    self.gridView.backgroundColor = [UIColor colorWithWhite:0.9f alpha:1];
    [self.gridView setCollectionViewLayout:[[MediaGridFlowLayout alloc] init] animated:NO];
    [self.gridView registerClass:[MediaLibraryCell class]
            forCellWithReuseIdentifier:MediaLibraryCellIdentifier];
    self.gridView.allowsSelection = YES;
    self.gridView.dataSource = self;
    self.gridView.delegate = self;
}


- (void)showGallery:(BOOL)show
{
    float newAlpha = show ? 1.0f : 0.0f;
    [UIView animateWithDuration:0.3 animations:^{
        self.galleryContainer.alpha = newAlpha;
    }];
    if (show)
    {
        [self loadAssetGroups];
    }
}

- (void)loadAssetGroups
{
    if (self.assetGroups != nil) return;

    self.assetGroups = [NSMutableArray array];

    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);

    dispatch_async(queue, ^{

        [self.mediaLibrary enumerateGroupsWithTypes:ALAssetsGroupAll usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
            if (group) {
                //NSLog(@"assetGroup: %@", group);
                [self.assetGroups addObject:group];
            }
            else
            {
                [self sortAssetGroups];
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self loadMediaItems:_currentAssetFilter];
                });

            }

        } failureBlock:^(NSError *error) {

            //NSLog(@"error loading asset groups: %@", [error localizedDescription]);
        }];
    });
}

int assetTypeWeight(int assetType)
{
    switch (assetType)
    {
        case ALAssetsGroupPhotoStream:
            return 1;
        case ALAssetsGroupSavedPhotos:
            return 2;
        default:
            return 0;
    }
}

- (void)sortAssetGroups
{
    // we want the camera roll to come first, then - other libraries
    [self.assetGroups sortUsingComparator:^(id obj1, id obj2) {

        ALAssetsGroup* ag1 = obj1, *ag2 = obj2;
        NSNumber* typ1 = [ag1 valueForProperty:ALAssetsGroupPropertyType];
        NSNumber* typ2 = [ag2 valueForProperty:ALAssetsGroupPropertyType];
        int weight1 = assetTypeWeight((int)[typ1 integerValue]);
        int weight2 = assetTypeWeight((int)[typ2 integerValue]);

        if (weight2 > weight1) {
            return (NSComparisonResult)NSOrderedDescending;
        }

        if (weight2 < weight1) {
            return (NSComparisonResult)NSOrderedAscending;
        }
        return (NSComparisonResult)NSOrderedSame;
    }];
}


- (void)loadMediaItems:(ALAssetsFilter*)assetFilter
{
    self.mediaItems = [NSMutableArray array];

    NSString *ver = [[UIDevice currentDevice] systemVersion];
    int iosVer = [ver intValue]; // major OS version
    bool reversePhotoList = iosVer >= 9;

    NSMutableArray *groupItems = [NSMutableArray array];
    for (ALAssetsGroup *assetGroup in self.assetGroups)
    {
        [assetGroup setAssetsFilter:assetFilter];
        // enumerateAssetsUsingBlock is a sync call!
        [assetGroup enumerateAssetsUsingBlock:^(ALAsset *asset, NSUInteger index, BOOL *stop) {
            //NSLog(@"New asset found: %@", asset);
            if (asset)
            {
                [groupItems addObject:asset];
            }
            else
            {
                // in iOS 9+, photo list is reversed, so PhotoAssets are also returned in reversed order
                if (reversePhotoList)
                {
                    [self.mediaItems addObjectsFromArray:[[groupItems reverseObjectEnumerator] allObjects]];
                }
                else
                {
                    [self.mediaItems addObjectsFromArray:groupItems];
                }
            }
        }];
        [groupItems removeAllObjects];
    }

    ////NSLog(@"photo loading done");
    [self.gridView reloadData];
}


#pragma mark -
#pragma mark UICollectionViewDelegate

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section
{
    return self.mediaItems.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath
{

    MediaLibraryCell *cell = [cv dequeueReusableCellWithReuseIdentifier:MediaLibraryCellIdentifier
                                                                forIndexPath:indexPath];

    ALAsset *asset     = (ALAsset *)self.mediaItems[(NSUInteger) indexPath.row];
    CGImageRef thumb = [asset aspectRatioThumbnail];
    UIImage *thumbnail = [UIImage imageWithCGImage:thumb];

    cell.imgView.image = thumbnail;

    BOOL checked = [self.selectedItems containsObject:asset];
    [cell setChecked:checked animated:NO];

    NSString* assetType = [asset valueForProperty:ALAssetPropertyType];
    BOOL isVideo = [assetType compare:ALAssetTypeVideo] == NSOrderedSame;
    if (isVideo)
    {
        NSNumber* videoDuration = [asset valueForProperty:ALAssetPropertyDuration];
        double duration = videoDuration.doubleValue;
        cell.videoDuration = duration;
    }
    [cell showVideoDurationLabel:isVideo];

    return cell;
}

- (BOOL)canAddMorePhotos
{
    if (self.selectedItems.count >= self.pluginParams.maxPhotos)
    {
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
                                                     message:self.pluginParams.localization.maxPhotosText
                                                    delegate:nil
                                           cancelButtonTitle:self.pluginParams.localization.alertOkText
                                           otherButtonTitles:nil];
        [av show];
        return NO;
    }
    return YES;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    BOOL isVideo = (_currentFilter == GalleryFilterVideo);
    ALAsset *asset = self.mediaItems[(NSUInteger) indexPath.row];
    BOOL checked = [self.selectedItems containsObject:asset];
    if (checked) [self.selectedItems removeObject:asset];
    else
    {
        if (isVideo) [self deselectAllItems];
        else
        {
            if (![self canAddMorePhotos]) return;
        }
        [self.selectedItems addObject:asset];
    }

    MediaLibraryCell * cell = (MediaLibraryCell *) [collectionView cellForItemAtIndexPath:indexPath];
    [cell setChecked:!checked animated:YES];

    NSURL * assetURL = [asset valueForProperty:ALAssetPropertyAssetURL];
    if (!checked)
    {
        // remove previous video thumb
        if (isVideo) [self.delegate discardAllSelectedItems];

        CGRect cellFrame = cell.frame;
        cellFrame.origin.y -= collectionView.contentOffset.y;
        cellFrame.origin.y += self.gridView.frame.origin.y + self.galleryContainer.frame.origin.y;
        [self.delegate addSelectedItemWithPath:[assetURL absoluteString]
                                thumbnailImage:[UIImage imageWithCGImage:asset.thumbnail]
                               animateFromRect:cellFrame
                                       isVideo:isVideo
                                   assetObject:asset];
    }
    else
    {
        [self.delegate removeSelectedItemWithPath:[assetURL absoluteString]];
    }

    ////NSLog(@"didSelectItemAtIndexPath: %@, cell = %@", indexPath, cell);
    ////NSLog(@"assetURL = %@", assetURL);
}

- (void)deselectAllItems
{
    NSArray *visibleItems = self.gridView.indexPathsForVisibleItems;
    for (NSIndexPath *idx in visibleItems)
    {
        ALAsset* asset = self.mediaItems[(NSUInteger) idx.row];
        if ([self.selectedItems containsObject:asset])
        {
            MediaLibraryCell *cell = (MediaLibraryCell *) [self.gridView cellForItemAtIndexPath:idx];
            [cell setChecked:NO animated:YES];
        }
    }
    [self.selectedItems removeAllObjects];
}

- (ALAsset*)findItemByURI:(NSString*)uri
{
    for (ALAsset* item in self.selectedItems)
    {
        NSURL * assetURL = [item valueForProperty:ALAssetPropertyAssetURL];
        NSString* uriStr = [assetURL absoluteString];
        if ([uriStr compare:uri] == NSOrderedSame) return item;
    }
    return nil;
}

- (void)deselectItemWithURI:(NSString *)uri
{
    ALAsset* item = [self findItemByURI:uri];
    if (item)
    {
        [self.selectedItems removeObject:item];
        NSArray *visibleItems = self.gridView.indexPathsForVisibleItems;
        for (NSIndexPath *idx in visibleItems)
        {
            ALAsset* asset = self.mediaItems[(NSUInteger) idx.row];
            if (asset == item)
            {
                MediaLibraryCell *cell = (MediaLibraryCell *) [self.gridView cellForItemAtIndexPath:idx];
                [cell setChecked:NO animated:YES];
                return;
            }
        }
    }
}

@end