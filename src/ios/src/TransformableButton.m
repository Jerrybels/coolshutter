//
// Created by Constantin on 25/04/15.
// Copyright (c) 2015 coolshutter. All rights reserved.
//

#import "TransformableButton.h"


@implementation TransformableButton {
    float _rotation;
    float _scale;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    _rotation = 0;
    _scale = 1;
    return self;
}

- (instancetype)init {
    self = [super init];
    _rotation = 0;
    _scale = 1;
    return self;
}

static double degreesToRadians(double angle) {
    return ((angle) / 180.0 * M_PI);
}

- (void)updateTransform {
    CGAffineTransform tr = CGAffineTransformMakeRotation(_rotation);
    self.transform = CGAffineTransformScale(tr, _scale, _scale);
}

- (void)setRotation:(float)degrees {
    _rotation = degreesToRadians(degrees);
    [self updateTransform];
}

- (void)setScale:(float)scale {
    _scale = scale;
    [self updateTransform];
}


@end