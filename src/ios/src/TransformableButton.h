//
// Created by Constantin on 25/04/15.
// Copyright (c) 2015 coolshutter. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface TransformableButton : UIButton

- (void)setRotation:(float)degrees;
- (void)setScale:(float)scale;

@end