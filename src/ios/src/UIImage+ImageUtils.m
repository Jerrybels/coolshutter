//
// Created by Constantin on 13/04/15.
// Copyright (c) 2015 coolshutter. All rights reserved.
//

#import "UIImage+ImageUtils.h"


@implementation UIImage (ImageUtils)

+ (UIImage *) screenshotFromView:(UIView *)view cropRect:(CGRect)rect
{
//    float scale = view.layer.contentsScale;
//    CGFloat width = rect.size.width * scale;
//    CGFloat height = rect.size.height * scale;

    UIGraphicsBeginImageContextWithOptions(view.bounds.size, YES, 0.0);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *fullImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    UIGraphicsBeginImageContext(rect.size);
    [fullImage drawAtPoint:CGPointMake(-rect.origin.x, -rect.origin.y)];
    UIImage *croppedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return croppedImage;
}

- (UIImage *)imageByCroppingImageToSize:(CGSize)targetSize
{
    // not equivalent to image.size (which depends on the imageOrientation)!
    double refWidth = CGImageGetWidth(self.CGImage);
    double refHeight = CGImageGetHeight(self.CGImage);

    CGFloat cropScale = MIN(refWidth / targetSize.width, refHeight / targetSize.height);
    CGFloat cropW = targetSize.width * cropScale;
    CGFloat cropH = targetSize.height * cropScale;


    double x = (refWidth - cropW) / 2.0;
    double y = (refHeight - cropH) / 2.0;

    CGRect cropRect = CGRectMake(x, y, cropW, cropH);
    CGImageRef imageRef = CGImageCreateWithImageInRect(self.CGImage, cropRect);

    UIImage *cropped = [UIImage imageWithCGImage:imageRef scale:self.scale orientation:self.imageOrientation];
    CGImageRelease(imageRef);


    CGFloat scale = MAX(
            targetSize.width/cropped.size.width,
            targetSize.height/cropped.size.height);

    CGFloat width = cropped.size.width * scale;
    CGFloat height = cropped.size.height * scale;

    CGRect rr = CGRectMake( 0, 0, width, height);

    UIGraphicsBeginImageContextWithOptions(targetSize, NO, 0);
    [cropped drawInRect:rr];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (UIImage *)imageByFittingImageToSize:(CGSize)targetSize
{
    // not equivalent to image.size (which depends on the imageOrientation)!
    double refWidth = CGImageGetWidth(self.CGImage);
    double refHeight = CGImageGetHeight(self.CGImage);

    CGFloat fitScale = MAX(refWidth / targetSize.width, refHeight / targetSize.height);
    CGFloat fitW = refWidth / fitScale;
    CGFloat fitH = refHeight / fitScale;

    CGRect rr = CGRectMake( 0, 0, fitW, fitH);

    CGSize realSize = CGSizeMake(fitW, fitH);
    UIGraphicsBeginImageContextWithOptions(realSize, NO, 0);
    [self drawInRect:rr];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

static double calculateConstrainedScale(CGSize src, CGSize max, CGSize min)
{
    double maxScaleW = max.width > 0 ? (double)max.width/ src.width : 1E10f;
    double maxScaleH = max.height > 0 ? (double)max.height / src.height : 1E10f;

    double minScaleW = min.width > 0 ? (double)min.width / src.width : 0;
    double minScaleH = min.height > 0 ? (double)min.height / src.height : 0;
    double maxScale = MIN(maxScaleW, maxScaleH);
    if (maxScale > 0.9e10f) maxScale = 0; // avoid large scale if maxW and maxH are 0
    float minScale = MAX(minScaleW, minScaleH);
    return MAX(maxScale, minScale);

}

- (UIImage*)imageByScalingWithSizeConstraintsMax:(CGSize)maxSize andMin:(CGSize)minSize
{
    UIImage* sourceImage = self;
    UIImage* newImage = nil;
    CGSize imageSize = sourceImage.size;
    double width = imageSize.width;
    double height = imageSize.height;
    double scaleFactor = calculateConstrainedScale(imageSize, maxSize, minSize);

    CGSize scaledSize = CGSizeMake(roundf(width * scaleFactor), roundf(height * scaleFactor));

    UIGraphicsBeginImageContext(scaledSize); // this will resize

    [sourceImage drawInRect:CGRectMake(0, 0, scaledSize.width, scaledSize.height)];

    newImage = UIGraphicsGetImageFromCurrentImageContext();
    if (newImage == nil) {
        NSLog(@"could not scale image");
    }

    // pop the context to get back to the default
    UIGraphicsEndImageContext();
    return newImage;
}

- (UIImage*)imageByScalingNotCroppingForSizeOld:(CGSize)targetSize
{
    UIImage* sourceImage = self;
    UIImage* newImage = nil;
    CGSize imageSize = sourceImage.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    CGFloat targetWidth = targetSize.width;
    CGFloat targetHeight = targetSize.height;
    CGFloat scaleFactor = 0.0;
    CGSize scaledSize = targetSize;

    if (CGSizeEqualToSize(imageSize, targetSize) == NO)
    {
        CGFloat widthFactor = targetWidth / width;
        CGFloat heightFactor = targetHeight / height;

        // opposite comparison to imageByScalingAndCroppingForSize in order to contain the image within the given bounds
        if (widthFactor > heightFactor)
        {
            scaleFactor = heightFactor; // scale to fit height
        }
        else
        {
            scaleFactor = widthFactor; // scale to fit width
        }
        scaledSize = CGSizeMake(MIN(width * scaleFactor, targetWidth), MIN(height * scaleFactor, targetHeight));
    }

    // If the pixels are floats, it causes a white line in iOS8 and probably other versions too
    scaledSize.width = (int)scaledSize.width;
    scaledSize.height = (int)scaledSize.height;

    UIGraphicsBeginImageContext(scaledSize); // this will resize

    [sourceImage drawInRect:CGRectMake(0, 0, scaledSize.width, scaledSize.height)];

    newImage = UIGraphicsGetImageFromCurrentImageContext();
    if (newImage == nil) {
        NSLog(@"could not scale image");
    }

    // pop the context to get back to the default
    UIGraphicsEndImageContext();
    return newImage;
}

- (UIImage*)imageRotatedByDegrees:(int)degrees
{
    double rotation_radians = degrees * M_PI / 180.0;

    CGRect myRect = CGRectMake(0, 0, self.size.width, self.size.height);
    myRect = CGRectApplyAffineTransform(myRect, CGAffineTransformMakeRotation(rotation_radians));
    myRect.size.width = floor(myRect.size.width);
    myRect.size.height = floor(myRect.size.height);

    UIGraphicsBeginImageContext(myRect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();

    // Rotate around the center point
    CGContextTranslateCTM(context, myRect.size.width / 2.0, myRect.size.height / 2.0);
    CGContextRotateCTM(context, rotation_radians);

    CGContextScaleCTM(context, 1.0, -1.0);
    double halfW = self.size.width / 2;
    double halfH = self.size.height / 2;
    CGContextDrawImage(context, CGRectMake(-halfW, -halfH, self.size.width, self.size.height), [self CGImage]);

    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}




@end