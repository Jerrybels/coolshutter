//
// Created by Constantin on 25/04/15.
// Copyright (c) 2015 coolshutter. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CameraPluginParams;
@class VideoRecordingState;
@class SelectedItemInfo;
@class UIView;

typedef void (^ CompletionBlockType)(BOOL cancelled, NSDictionary* result);


@interface PluginResult : NSObject

@property (strong) VideoRecordingState *videoParts;
// array of SelectedItemInfo
@property (strong) NSMutableArray *selectedItems;
@property (strong) CameraPluginParams *pluginParams;

-(instancetype)initWithPluginParams:(CameraPluginParams *)params;

- (SelectedItemInfo*)findItemWithPath:(NSString*)path;

// remove recorded videos/taken images from the temporary folder
+ (void)cleanup;

- (void)beginVideoRecording:(NSString*)pathPart;
- (void)discardRecordedVideo;
- (void)deleteCurrentItemFiles;
- (void)deleteItemAndFile:(SelectedItemInfo *)info;

// time remaining for recording (in seconds)
- (float)getVideoTimeRemaining;

- (BOOL)needToTrimLibraryVideo;

- (void)prepareResult:(UIView*)parentViewForWaitIndicator
           completion:(CompletionBlockType)completion;

- (void)onFileSavedStateChanged;


@end