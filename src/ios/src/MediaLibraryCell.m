//
// Created by Constantin on 26/04/15.
// Copyright (c) 2015 coolshutter. All rights reserved.
//

#import "MediaLibraryCell.h"
#import "LittleThings.h"
#import "ColorUtils.h"

#define CHECK_MARK_SPACING 4.0f

NSString * const MediaLibraryCellIdentifier = @"thumbnailCell";

NSString* DURATION_LABEL_FONT = @"HelveticaNeue-Light";
#define DURATION_LABEL_TEXT_SIZE  13
#define DURATION_LABEL_TEXT_COLOR [UIColor whiteColor]
#define DURATION_LABEL_BG_COLOR [UIColor colorWithWhite:0 alpha:0.5f]
#define TINT_VIEW_COLOR [UIColor colorWithRGBAValue:0x24ABBC50]

@interface MediaLibraryCell ()

@property (strong, nonatomic) UIImageView *itemCheck;
@property (strong, nonatomic) UILabel* durationLabel;
@property (strong, nonatomic) UIView* tintView;

@end

@implementation MediaLibraryCell

@synthesize imgView;

- (id)initWithFrame:(CGRect)frame
{

    self = [super initWithFrame:frame];

    if (self) {

        self.imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, 0.0, frame.size.width, frame.size.height)];
        self.imgView.contentMode = UIViewContentModeScaleAspectFill;
        self.imgView.clipsToBounds = YES;

        [self.contentView addSubview:self.imgView];

        self.itemCheck = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"gallery_select_mark.png"]];
        CGRect checkFrame = self.itemCheck.bounds;
        checkFrame.origin.x = frame.size.width - checkFrame.size.width - CHECK_MARK_SPACING;
        checkFrame.origin.y = frame.size.height - checkFrame.size.height - CHECK_MARK_SPACING;
        self.itemCheck.frame = checkFrame;
        self.itemCheck.alpha = 0;

        [self.contentView addSubview:self.itemCheck];

        self.durationLabel = [[UILabel alloc] init];
        self.durationLabel.font = [UIFont fontWithName:DURATION_LABEL_FONT size:DURATION_LABEL_TEXT_SIZE];
        self.durationLabel.textColor = DURATION_LABEL_TEXT_COLOR;
        self.durationLabel.backgroundColor = DURATION_LABEL_BG_COLOR;
        self.durationLabel.textAlignment = NSTextAlignmentCenter;
        self.durationLabel.hidden = YES;
        [self addSubview:self.durationLabel];

        self.tintView = [[UIView alloc] initWithFrame:self.imgView.bounds];
        [self.imgView addSubview:self.tintView];
        self.tintView.backgroundColor = TINT_VIEW_COLOR;
        self.tintView.alpha = 0;
    }

    return self;
}

- (void)setChecked:(BOOL)checked animated:(BOOL)animated
{
    float newAlpha = checked ? 1.0f : 0.0f;
    if (self.itemCheck.alpha == newAlpha) return;

    if (animated)
    {
        [UIView animateWithDuration:0.3 animations:^{
            self.itemCheck.alpha = newAlpha;
            self.tintView.alpha = newAlpha;
        }];
    }
    else
    {
        self.itemCheck.alpha = newAlpha;
        self.tintView.alpha = newAlpha;
    }
}

- (void)showVideoDurationLabel:(BOOL)show
{
    self.durationLabel.hidden = !show;
}

- (void)setVideoDuration:(double)duration
{
    self.durationLabel.text = [LittleThings formatTime:(int)duration];
    [self.durationLabel sizeToFit];
    CGRect fr = self.durationLabel.bounds;
    float padding = 4;
    fr.origin.y = self.frame.size.height - fr.size.height - 2*padding;
    fr.size.width += 2*padding;
    fr.size.height += 2*padding;
    self.durationLabel.frame = fr;
}


@end

