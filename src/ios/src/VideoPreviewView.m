
#import "VideoPreviewView.h"
#import <AVFoundation/AVFoundation.h>

@implementation VideoPreviewView

+ (Class)layerClass
{
	return [AVCaptureVideoPreviewLayer class];
}

- (AVCaptureSession *)session
{
	return [(AVCaptureVideoPreviewLayer *)[self layer] session];
}

- (void)setSession:(AVCaptureSession *)session
{
    AVCaptureVideoPreviewLayer* previewLayer = (AVCaptureVideoPreviewLayer *)[self layer];
	[previewLayer setSession:session];
    [previewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
}

@end
