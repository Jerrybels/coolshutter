//
// Created by Constantin on 09/04/15.
// Copyright (c) 2015 coolshutter. All rights reserved.
//

#import "NSDictionary+OptionalJSONFields.h"


@implementation NSDictionary (OptionalJSONFields)

- (NSUInteger)optUIntValue:(NSString*)key defValue:(NSUInteger)val
{
    NSNumber* res = [self valueForKey:key];
    return res == nil ? val : res.unsignedIntegerValue;
}

- (BOOL)optBoolValue:(NSString *)key defValue:(BOOL)value {
    NSNumber* res = [self valueForKey:key];
    return res == nil ? value : res.boolValue;
}

- (NSString *)optStringValue:(NSString *)key defValue:(NSString *)value {
    NSString* res = [self valueForKey:key];
    return res == nil ? value : res;
}

@end