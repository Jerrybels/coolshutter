//
// Created by Constantin on 26/04/15.
// Copyright (c) 2015 coolshutter. All rights reserved.
//

#import "LittleThings.h"


@implementation LittleThings
{

}
+ (NSString *)formatTime:(int)seconds
{
    int mm = seconds / 60;
    int ss = seconds % 60;
    return [NSString stringWithFormat:@"%d:%02d", mm, ss];
}

+ (BOOL)moveFileFrom:(NSString*)srcName to:(NSString *)dstName
{
    NSFileManager *mgr = [NSFileManager defaultManager];
    NSError *err = nil;
    [mgr moveItemAtPath:srcName toPath:dstName error:&err];
    if (err)
    {
        NSLog(@"Error while moving file: %@", err);
    }

    return err == nil;
}

+ (int)rotationDegreesFromImageOrientationFlag:(UIImageOrientation)orientation
{
    switch (orientation)
    {
        // default orientation
        case UIImageOrientationUp:
        // as above but image mirrored along other axis. horizontal flip
        case UIImageOrientationUpMirrored:
        default:
            return 0;

        // 180 deg rotation
        case UIImageOrientationDown:
            // horizontal flip
        case UIImageOrientationDownMirrored:
            return 180;

        // 90 deg CCW
        case UIImageOrientationLeft:
            // vertical flip
        case UIImageOrientationLeftMirrored:
            return 270;

        // 90 deg CW
        case UIImageOrientationRight:
        // vertical flip
        case UIImageOrientationRightMirrored:
            return 90;
    }

}

+ (UIImageOrientation)imageOrientationFlagFromRotationDegrees:(int)degrees
{
    while (degrees < 0) degrees += 360;
    switch (degrees % 360)
    {
        default:
        case 0:
            return UIImageOrientationUp;

        case 180:
            return UIImageOrientationDown;

            // 90 deg CCW
        case 270:
            return UIImageOrientationLeft;

            // 90 deg CW
        case 90:
            return UIImageOrientationRight;
    }

}


+ (int)rotationDegreesFromAssetOrientationFlag:(ALAssetOrientation)orientation
{
    switch (orientation)
    {
        case ALAssetOrientationUp:
            return 0;// default orientation
        case ALAssetOrientationDown:
            return 180; // 180 deg rotation
        case ALAssetOrientationLeft:
            return 270; // 90 deg CCW
        case ALAssetOrientationRight:
            return 90;  // 90 deg CW
        case ALAssetOrientationUpMirrored:
            return 0; // as above but image mirrored along other axis. horizontal flip
        case ALAssetOrientationDownMirrored:
            return 180; // horizontal flip
        case ALAssetOrientationLeftMirrored:
            return 270;// vertical flip
        case ALAssetOrientationRightMirrored:
            return 90; // vertical flip

    }
    return 0;
}

+ (BOOL)isBeforeiOS8
{
    return (NSFoundationVersionNumber <= NSFoundationVersionNumber_iOS_7_1);
}
@end