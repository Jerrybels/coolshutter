//
//  CoolShutterViewController.h
//  CoolShutterApp
//
//  Created by Constantin on 08/04/15.
//  Copyright (c) 2015 coolshutter. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CameraPluginParams;

typedef void (^PickerCompletionBlock)(BOOL cancelled, NSDictionary* result);

@interface CoolShutterViewController : UIViewController

- (id)initWithParameters:(CameraPluginParams*)params completion:(PickerCompletionBlock)completion;

@end
