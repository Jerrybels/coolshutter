//
// Created by Constantin on 26/04/15.
// Copyright (c) 2015 coolshutter. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ALAsset;

@interface VideoUtils : NSObject

+ (UIImage*)loadVideoThumbnail:(NSString*)videoPath;
+ (UIImage*)loadVideoThumbnailForURL:(NSURL*)videoURL;

+ (void)joinVideoParts:(NSArray*)srcPartsPaths toFile:(NSString*)outputFile completion:(void (^)(BOOL success))completion;

+ (void)exportVideoFileToLibrary:(NSString*)filePath completion:(void (^)(void))completion;
+ (void)exportPhotoToLibrary:(UIImage*)image completion:(void (^)(void))completion;

+ (void)trimVideo:(NSString*)srcPath
           toFile:(NSString*)dstPath
         fromTime:(double)start
         duration:(double)duration
        minWidth:(int)minW
        maxWidth:(int)maxW
       minHeight:(int)minH
       maxHeight:(int)maxH
        progress:(void(^)(float progress))progressBlock
        completion:(void (^)(BOOL success))completion;

+ (NSString*)findBestVideoCapturePresetMinWidth:(int)minW maxWidth:(int)maxW minHeight:(int)minH maxHeight:(int)maxH;
+ (NSString*)findBestVideoExportPresetMinWidth:(int)minW maxWidth:(int)maxW minHeight:(int)minH maxHeight:(int)maxH;

+ (void)transcodeVideoAsset:(ALAsset *)asset
                toFile:(NSString *)dstPath
              minWidth:(int)minW
              maxWidth:(int)maxW
             minHeight:(int)minH
             maxHeight:(int)maxH
                   progress:(void(^)(float progress))progressBlock
            completion:(void (^)(BOOL success))completion;

+ (void)transcodeVideoFile:(NSString *)srcPath
                    toFile:(NSString *)dstPath
                  minWidth:(int)minW
                  maxWidth:(int)maxW
                 minHeight:(int)minH
                 maxHeight:(int)maxH
                  progress:(void(^)(float progress))progressBlock
                completion:(void (^)(BOOL success))completion;

@end