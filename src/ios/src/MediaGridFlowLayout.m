//
// Created by Constantin on 26/04/15.
// Copyright (c) 2015 coolshutter. All rights reserved.
//

#import "MediaGridFlowLayout.h"

#define ITEM_SPACING 2.0f
#define BOTTOM_BAR_HEIGHT 95.0f
#define TOP_SPACING 8.0f

@implementation MediaGridFlowLayout

- (id)init
{

    self = [super init];

    CGRect screenFrame = UIScreen.mainScreen.bounds;
    float scrW = CGRectGetWidth(screenFrame);
    float scrH = CGRectGetHeight(screenFrame);
    if (scrW > scrH) scrW = scrH;
    float itemSz = floor((scrW - 4*ITEM_SPACING) / 3);
    float spacing = (scrW - 3*itemSz) / 4;

    if (self) {

        self.itemSize = CGSizeMake(itemSz, itemSz);
        self.minimumInteritemSpacing = spacing;
        self.footerReferenceSize = CGSizeMake(100, BOTTOM_BAR_HEIGHT + spacing);
        self.headerReferenceSize = CGSizeMake(100, TOP_SPACING);
        self.minimumLineSpacing = spacing;
    }

    return self;
}

@end
