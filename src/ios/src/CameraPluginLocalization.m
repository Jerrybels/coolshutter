//
// Created by Constantin on 7/04/15.
// Copyright (c) 2015 coolshutter. All rights reserved.
//

#import "CameraPluginLocalization.h"
#import "NSDictionary+OptionalJSONFields.h"


@implementation CameraPluginLocalization {

}

- (id)init {
    self = [super init];
    self.maxPhotosText = @"You have reached the maximum number of photos!";

    self.deletePhotoText = @"Are you sure that you want to delete this photo?";

    self.deleteVideoText = @"Discard recorded video?";

    self.moveFromPhotosText = @"Are you sure that you want to move away and lose the pictures you took?";

    self.moveFromVideosText = @"Are you sure that you want to move away and lose the video you took?";


    self.maxVideoText = @"Maximum video length reached!";
    self.maxVideoTimeCutWarning = @"This video is too long. Only the first %d seconds will be used.";

    self.alertCancelText = @"Cancel"; // = no
    self.alertConfirmText = @"OK";    //  = yes
    self.alertOkText = @"OK";         //  = OK

    self.photoLabel = @"PHOTO";
    self.galleryLabel = @"GALLERY";
    self.videoLabel = @"VIDEO";
    self.videosLabel = @"VIDEOS";
    self.photosLabel = @"PHOTOS";

    return self;
}

- (id)initWithJSON:(NSDictionary*)json {
    if (json == nil) return [self init];

    // parse JSON object

    self.maxPhotosText = [json optStringValue:@"maxPhotosText" defValue:self.maxPhotosText];
    self.deletePhotoText = [json optStringValue:@"deletePhotoText" defValue:self.deletePhotoText];
    self.deleteVideoText =      [json optStringValue:@"deleteVideoText" defValue:self.deletePhotoText];
    self.moveFromPhotosText = [json optStringValue:@"moveFromPhotosText" defValue:self.moveFromPhotosText];
    self.moveFromVideosText = [json optStringValue:@"moveFromVideosText" defValue:self.moveFromVideosText];
    self.maxVideoText = [json optStringValue:@"maxVideoText" defValue:self.maxVideoText];
    self.maxVideoTimeCutWarning = [json optStringValue:@"videoCutWarning" defValue:self.maxVideoTimeCutWarning];
    self.alertCancelText = [json optStringValue:@"alertCancelText" defValue:self.alertCancelText];
    self.alertConfirmText = [json optStringValue:@"alertConfirmText" defValue:self.alertConfirmText];
    self.alertOkText = [json optStringValue:@"alertOkText" defValue:self.alertOkText];

    self.photoLabel = [json optStringValue:@"photoLabel" defValue:self.photoLabel];
    self.photosLabel = [json optStringValue:@"photosLabel" defValue:self.photosLabel];
    self.videoLabel = [json optStringValue:@"videoLabel" defValue:self.videoLabel];
    self.videosLabel = [json optStringValue:@"videosLabel" defValue:self.videosLabel];
    self.galleryLabel = [json optStringValue:@"galleryLabel" defValue:self.galleryLabel];

    return self;
}

@end