//
// Created by Constantin on 12/04/15.
// Copyright (c) 2015 coolshutter. All rights reserved.
//

#import "ThumbsBar.h"
#import "SelectedItemInfo.h"
#import "ColorUtils.h"
#import "PluginResult.h"
#import "UIView+TransformationHelper.h"
#import "UIView+AutoLayout.h"
#import "UIView+ObjectTagAdditions.h"


const float THUMBS_HOR_SPACING = 8;

@interface ThumbsBar ()
{
    float _thumbSize;
}

@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UIView *leftEdge;
@property (nonatomic, strong) UIView *rightEdge;

@property UIColor* fadeColor;

@end

@implementation ThumbsBar {

}
- (id)initWithScroller:(UIScrollView *)scrollView leftEdge:(UIView *)leftEdge rightEdge:(UIView *)rightEdge
                                                 fadeColor:(UIColor*)bkColor
{
    self = [super init];
    self.scrollView = scrollView;
    self.leftEdge = leftEdge;
    self.rightEdge = rightEdge;
    _thumbSize = scrollView.bounds.size.height;
    self.fadeColor = bkColor;
    [self setupFadingEdgeGradients];
    [self.scrollView setBounces:NO];
    return self;
}

- (CAGradientLayer *)createGradientLayerForEdge:(BOOL)left
{
    CAGradientLayer *mask = [CAGradientLayer layer];

    UIColor* clr = self.fadeColor;
    CGColorRef outerColor = [UIColor colorWithRed:clr.red green:clr.green blue:clr.blue alpha: left ? 1.0f : 0.0f].CGColor;
    CGColorRef innerColor = [UIColor colorWithRed:clr.red green:clr.green blue:clr.blue alpha: left ? 0.0f : 1.0f].CGColor;

    mask.colors = @[(__bridge id) outerColor, (__bridge id) innerColor];
    mask.startPoint = CGPointMake(0, 0.5);
    mask.endPoint = CGPointMake(1, 0.5);

    return mask;
}

- (void)setupFadingEdgeGradients {

    CAGradientLayer *leftMask = [self createGradientLayerForEdge:YES];
    CAGradientLayer *rightMask = [self createGradientLayerForEdge:NO];
    leftMask.frame = _leftEdge.bounds;
    rightMask.frame = _rightEdge.bounds;
    [_leftEdge.layer addSublayer:leftMask];
    [_rightEdge.layer addSublayer:rightMask];

    self.scrollView.delegate = self;

    [self scrollViewDidScroll:self.scrollView];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat edgeW = _leftEdge.bounds.size.width;
    // not layed out yet?
    if (edgeW == 0) return;

    CGFloat offset = scrollView.contentOffset.x;
    CGFloat scrollW = scrollView.frame.size.width;
    CGFloat contentW = scrollView.contentSize.width;


    CGFloat leftA = fminf(offset / edgeW, 1.0);
    CGFloat rightA = fmaxf(fminf((contentW - scrollW - offset) / edgeW, 1.0), 0);

    _leftEdge.alpha = leftA;
    _rightEdge.alpha = rightA;
}


- (void)addThumbWithImage:(UIImage *)image sii:(SelectedItemInfo*)item
{
    int thumbsCount = (int)self.pluginResult.selectedItems.count;
    float xOffset = thumbsCount * (_thumbSize + THUMBS_HOR_SPACING);
    item.imgView = [[UIImageView alloc] initWithFrame:CGRectMake(xOffset, 0, _thumbSize, _thumbSize)];
    item.imgView.alpha = 0;
    item.imgView.userInteractionEnabled = YES;
    UIButton* btn = [UIButton buttonWithType:UIButtonTypeCustom];
    //[item.imgView addSubview:btn];

    [btn setImage:[UIImage imageNamed:@"delete_thumbnail.png"] forState:UIControlStateNormal];
    btn.userInteractionEnabled = YES;
    btn.objectTag = item;
    [btn addTarget:self action:@selector(onThumbnailTap:) forControlEvents:UIControlEventTouchUpInside];
    btn.frame = item.imgView.bounds;
    [item.imgView addSubview:btn];
    [btn pinToSuperviewEdges:JRTViewPinAllEdges inset:0];

    //item.imgView.contentMode = UIViewContentModeScaleAspectFill;
    item.imgView.contentMode = UIViewContentModeCenter;
    item.imgView.image = image;
    [self.scrollView addSubview:item.imgView];

    [self.pluginResult.selectedItems addObject:item];

    CGSize sz = self.scrollView.contentSize;
    sz.width = xOffset + _thumbSize;
    self.scrollView.contentSize = sz;

    CGSize scrollSz = self.scrollView.bounds.size;

    if (sz.width > scrollSz.width)
    {
        CGPoint rightOffset = CGPointMake(sz.width - scrollSz.width, 0);
        [self.scrollView setContentOffset:rightOffset animated:YES];
    }
    else
    {
        [self scrollViewDidScroll:self.scrollView];
    }
}

- (void)onThumbnailTap:(id)sender
{
    SelectedItemInfo * sii = ((UIView*)sender).objectTag;
    [self.delegate onThumbnailTap:sii];
}


- (CGRect)lastThumbRect
{
    CGFloat x = _scrollView.frame.origin.x;
    CGFloat y = _scrollView.frame.origin.y;

    SelectedItemInfo* last = [self.pluginResult.selectedItems lastObject];
    CGFloat offset = last.imgView.frame.origin.x;
    CGFloat scrollW = self.scrollView.frame.size.width;

    if (offset + _thumbSize > scrollW)
    {
        offset = scrollW - _thumbSize;
    }

    return CGRectMake(x + offset, y, _thumbSize, _thumbSize);
}

- (void)rotateThumbsToAngle:(int)angle {
    for (SelectedItemInfo * sii in self.pluginResult.selectedItems)
    {
        int thumbAngle = angle - sii.deviceOrientation;
        [sii.imgView setRotationDegrees:thumbAngle];
    }
}

- (void)deleteThumb:(SelectedItemInfo *)info
{
    UIImageView* img = info.imgView;
    [self deleteThumbView:img withDelay:0 requestRelayout:YES];
}

- (void)deleteThumbView:(UIImageView *)img withDelay:(CGFloat)delay requestRelayout:(BOOL)relayout
{
    CGRect oldFrame = img.frame;
    float left = oldFrame.origin.x;
    float right = left + oldFrame.size.width;

    float xOffset = self.scrollView.contentOffset.x;
    float scrollW = self.scrollView.bounds.size.width;
    BOOL visible = ((right > xOffset) && (left < xOffset + scrollW));

    [img removeFromSuperview];

    if (visible)
    {
        // animate thumbnail removal
        UIView* topBar = self.scrollView.superview;
        left -=  xOffset;
        CGRect scrFrame = self.scrollView.frame;
        CGRect fr = CGRectMake(left + scrFrame.origin.x,
                scrFrame.origin.y + oldFrame.origin.y,
                oldFrame.size.width, oldFrame.size.height);
        img.frame = fr;
        [topBar addSubview:img];
        fr.origin.y = -_thumbSize;
        [UIView animateWithDuration:0.3 delay:delay options:UIViewAnimationOptionCurveEaseIn animations:^{
            img.frame = fr;
        } completion:^(BOOL completed) {
            [img removeFromSuperview];
            if (relayout)
            {
                [UIView animateWithDuration:0.3 animations:^{
                    [self relayoutThumbs];
                }];
            }
        }];
    }
    else
    {
        if (relayout)
        {
            [UIView animateWithDuration:0.3 delay:delay options:0 animations:^{
                [self relayoutThumbs];
            } completion:nil];
        }
    }

}

- (void)relayoutThumbs
{
    float x = 0;
    float spacing = (_thumbSize + THUMBS_HOR_SPACING);
    for (SelectedItemInfo *sii in self.pluginResult.selectedItems)
    {
        UIView* v = sii.imgView;
        CGRect fr = v.frame;
        fr.origin.x = x;
        v.frame = fr;
        x += spacing;
    }
    if (x > 0) x -= spacing;
    self.scrollView.contentSize = CGSizeMake(x, _thumbSize);
}

- (void)deleteAllThumbs
{
    int count = (int)_pluginResult.selectedItems.count;
    float delay = 0;
    for (int ii = count - 1; ii >= 0; ii--)
    {
        SelectedItemInfo *sii = _pluginResult.selectedItems[ii];
        [self deleteThumbView:sii.imgView withDelay:delay requestRelayout:(ii == 0)];
        delay += 0.05;
    }
}
@end