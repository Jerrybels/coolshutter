//
// Created by Constantin on 13/04/15.
// Copyright (c) 2015 coolshutter. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface FileUtils : NSObject

+ (NSString*)getTempFilePathWithExtension:(NSString*)extension;
+ (void)deleteAllTemporaryFiles;

@end