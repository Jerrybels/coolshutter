//
// Created by Constantin on 03/05/15.
// Copyright (c) 2015 coolshutter. All rights reserved.
//

#import "CoolShutterPlugin.h"
#import "PluginResult.h"
#import "CoolShutterViewController.h"
#import "CameraPluginParams.h"


@implementation CoolShutterPlugin
{
}

- (void)pickItems:(CDVInvokedUrlCommand *)command
{
    NSString* jsonStr = [command argumentAtIndex:0 withDefault:nil];
    NSData* json = [jsonStr dataUsingEncoding:NSUTF8StringEncoding];
    CameraPluginParams *pluginParams = [[CameraPluginParams alloc] initWithJSON:json];
    CoolShutterPlugin __weak *weakSelf = self;
    CoolShutterViewController* picker = [[CoolShutterViewController alloc] initWithParameters:pluginParams
            completion:^ (BOOL cancelled, NSDictionary* result) {

                CDVPluginResult* pluginResult;
                if (cancelled)
                {
                    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"Cancelled by user"];
                }
                else
                {
                    pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:result];
                }
                [weakSelf.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
            }];

    [self.viewController presentViewController:picker animated:YES completion:nil];
}

- (void)cleanup:(CDVInvokedUrlCommand *)command
{
    NSLog(@"cleanup()");
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [PluginResult cleanup];
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    });
}

@end