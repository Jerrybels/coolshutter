//
// Created by Constantin on 14/04/15.
// Copyright (c) 2015 coolshutter. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface VideoRecordingState : NSObject

@property float remainingTime;
@property (readonly, getter=getPartsCount) int partsCount;

- (id)initWithMaximumTime:(float)seconds;
- (void)addVideoPart:(NSString*)filePath withDuration:(float)seconds;
- (void)discardRecordedFiles;

- (NSString*)getPartPath:(int)idx;

- (void)joinVideoToFile:(NSString*)outputFile completion:(void (^)(BOOL success))completion;

@end