//
// Created by Constantin on 13/04/15.
// Copyright (c) 2015 coolshutter. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (ImageUtils)

+ (UIImage *) screenshotFromView:(UIView *)view cropRect:(CGRect)rect;

- (UIImage *)imageByCroppingImageToSize:(CGSize)size;
- (UIImage *)imageByFittingImageToSize:(CGSize)targetSize;
//- (UIImage*)imageByScalingNotCroppingForSize:(CGSize)targetSize;
- (UIImage*)imageByScalingWithSizeConstraintsMax:(CGSize)maxSize andMin:(CGSize)minSize;
- (UIImage*)imageRotatedByDegrees:(int)degrees;

@end