//
// Created by Constantin on 25/04/15.
// Copyright (c) 2015 coolshutter. All rights reserved.
//

#import "PluginResult.h"
#import "CameraPluginParams.h"
#import "VideoRecordingState.h"
#import "SelectedItemInfo.h"
#import "MBProgressHUD.h"
#import "UIImage+ImageUtils.h"
#import "Base64.h"
#import "VideoUtils.h"
#import "FileUtils.h"

#define RESULT_ITEMS  @"items"


@interface PluginResult() {
    float _progressUnitsTotal;
    float _progressUnitsDone;
}

@property (copy) CompletionBlockType prepareResultCompletionBlock;
@property (atomic) BOOL prepareResultsCycleRunning;
@property MBProgressHUD* progressHUD;

@end

@implementation PluginResult

+ (void)cleanup
{
    [FileUtils deleteAllTemporaryFiles];
}

- (instancetype)initWithPluginParams:(CameraPluginParams *)params {
    self = [super init];
    self.pluginParams = params;
    self.selectedItems = [NSMutableArray array];
    return self;
}

- (void)updateProgress
{
    int percent = (int)round(100.0 * _progressUnitsDone / _progressUnitsTotal);
    if (percent < 0) percent = 0;
    if (percent > 100) percent = 100;
    self.progressHUD.labelText = [NSString stringWithFormat:@"%d%%", percent];
}

- (SelectedItemInfo *)findItemWithPath:(NSString *)path
{
    for (SelectedItemInfo *sii in self.selectedItems)
    {
        if ([sii.itemFilePath compare:path] == NSOrderedSame) return sii;
    }
    return nil;
}

- (void)beginVideoRecording:(NSString*)partPath
{
    if (self.videoParts == nil)
    {
        self.videoParts = [[VideoRecordingState alloc] initWithMaximumTime:(int)self.pluginParams.maxVideoTime];
    }

    //NSLog(@"VideoParts: startedVideoRecording. Current parts = %@", self.videoParts);
}

- (void)discardRecordedVideo
{
    if (self.videoParts != nil)
    {
        [self.videoParts discardRecordedFiles];
        self.videoParts = nil;
    }
}

- (void)deleteCurrentItemFiles
{
    [self discardRecordedVideo];
    for (SelectedItemInfo *sii in self.selectedItems)
    {
        NSFileManager *mgr = [NSFileManager defaultManager];
        NSError* err = nil;
        [mgr removeItemAtPath:sii.itemFilePath error:&err];
        if (err != nil)
        {
            //NSLog(@"Error deleting item: %@", err);
        }
    }

}

- (float)getVideoTimeRemaining
{
    if (self.videoParts == nil) return (int)self.pluginParams.maxVideoTime;

    return self.videoParts.remainingTime;
}


- (void)deleteItemAndFile:(SelectedItemInfo *)info
{
    [self.selectedItems removeObject:info];
    NSFileManager *mgr = [NSFileManager defaultManager];
    NSError* err = nil;
    [mgr removeItemAtPath:info.itemFilePath error:&err];
    if (err != nil)
    {
        //NSLog(@"Error deleting item: %@", err);
    }
}

// call this method to check if all pending background operations are finished
- (BOOL)areAllResultsProcessed
{
    //NSLog(@"areAllResultsProcessed");
    // Data that should be processed for result (if requested in plugin params):
    // 1. Video parts joined
    if (self.videoParts) {
        //NSLog(@"NO - waiting video parts join");
        return NO;
    }

    // 2. All items have their data saved to disk
    // 3. Thumbnails generated for all files (if requested)
    // 4. Taken pictures/video exported to Assets Library (if requested)
    for (SelectedItemInfo *sii in self.selectedItems)
    {
        if (![self isItemProcessed:sii]) {
            //NSLog(@"NO - item %@ not ready", sii.itemFilePath);
            return NO;
        }
    }

    return YES;
}

- (BOOL)isItemProcessed:(SelectedItemInfo*)sii
{
    if (!sii.fileDataSaved) {
        //NSLog(@"Item data not saved yet");
        return NO;
    }
    if (_pluginParams.generateThumbnailsRequested && !sii.thumbData && !sii.thumbFile) {
        //NSLog(@"Item thumbnail is not generated");
        return NO;
    }
    if (_pluginParams.saveToGallery && !sii.isFromLibrary && !sii.exportedToLibrary) {
        //NSLog(@"Item is not exported to library yet");
        return NO;
    }

    if (sii.postProcessingFlags != 0) return NO;

    return YES;
}

- (void)prepareResultAsync
{
    self.prepareResultsCycleRunning = YES;
    __weak PluginResult *weakSelf = self;
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0);
    dispatch_async(queue, ^ {
        //NSLog(@"prepareResultAsync started");
        BOOL needToRepeatCycle = NO;

        for (SelectedItemInfo *sii in weakSelf.selectedItems)
        {
            if ([weakSelf isItemProcessed:sii]) continue;
            if (!sii.fileDataSaved) {
                needToRepeatCycle = YES;
                continue;
            }
            // if export is started, then thumbnail is also generated, so we don't have to check it too
            if (sii.exportStarted) continue;

            if (sii.isVideo)
            {
                [weakSelf processVideo:sii];
            }
            else
            {
                [weakSelf processImage:sii];
                _progressUnitsDone += 1;
                [weakSelf updateProgress];
            }
        }

        if (needToRepeatCycle)
        {
            dispatch_async(dispatch_get_main_queue(), ^ {
                [weakSelf prepareResultAsync];
            });
        }
        else
        {
            [weakSelf checkIfAllIsProcessed];
        }

        weakSelf.prepareResultsCycleRunning = NO;
    });
}

- (void)checkIfAllIsProcessed
{
    // if there is no completion block, then we are not waiting for pending operations to complete
    if (self.prepareResultCompletionBlock == nil) return;

    if ([self areAllResultsProcessed])
    {
        [self buildResultJSON];
    }
}

- (void)onFileSavedStateChanged
{
    // if there is no completion block, then we are not waiting for pending operations to complete
    if (self.prepareResultCompletionBlock == nil) return;

    if (!self.prepareResultsCycleRunning)
    {
        [self prepareResultAsync];
    }
}

- (void)processImage:(SelectedItemInfo *)sii
{
    UIImage* img;
    if (sii.isFromLibrary)
    {
        ALAssetRepresentation* representation = [sii.assetObject defaultRepresentation];
        CGImageRef cgImage = [representation fullResolutionImage];
        img = [UIImage imageWithCGImage:cgImage scale:representation.scale orientation:(UIImageOrientation) representation.orientation];

        [self downscalePhotoItem:sii srcImage:img];
    }
    else
    {
        // local file
        img = [UIImage imageWithContentsOfFile:sii.itemFilePath];
    }

    [self generateThumbnail:img selectedItemInfo:sii];

    if (_pluginParams.saveToGallery && !sii.isFromLibrary && !sii.exportedToLibrary)
    {
        __weak PluginResult* weakSelf = self;
        sii.exportStarted = YES;
        [VideoUtils exportPhotoToLibrary:img completion:^{
            sii.exportedToLibrary = YES;
            [weakSelf checkIfAllIsProcessed];
        }];
    }
}

- (void)downscalePhotoItem:(SelectedItemInfo *)sii srcImage:(UIImage *)image
{
    CameraPluginParams *params = self.pluginParams;
    int maxW = (int)params.maxPhotoWidth;
    int maxH = (int)params.maxPhotoHeight;
    int minW = (int)params.minPhotoWidth;
    int minH = (int)params.minPhotoHeight;

    sii.postProcessingFlags &= ~PostProcessResizePhoto;

    //return originalData;
    if (maxW <= 0) return;

    if (sii.deviceOrientation % 180 != 0)
    {
        // swap width and height for rotated images
        int tmp = maxW;
        maxW = maxH;
        maxH = tmp;

        tmp = minW;
        minW = minH;
        minH = tmp;
    }
    if ((image.size.width <= maxW) && (image.size.height <= maxH))
    {
        return;
    }
    UIImage* resized = [image imageByScalingWithSizeConstraintsMax:CGSizeMake(maxW, maxH) andMin:CGSizeMake(minW, minH)];
    if (sii.deviceOrientation != 0) resized = [resized imageRotatedByDegrees: -sii.deviceOrientation];
    NSData* data = UIImageJPEGRepresentation(resized, (CGFloat) ((double)_pluginParams.quality/100.0));

    // save thumbnail to file
    NSString* downscaledFile = [FileUtils getTempFilePathWithExtension:@"jpg"];
    [data writeToFile:downscaledFile atomically:NO];
    sii.itemFilePath = downscaledFile;
    sii.isFromLibrary = NO;
    sii.exportedToLibrary = YES;
}


- (void)processVideo:(SelectedItemInfo*)sii
{
    UIImage* snap;
    if (sii.isFromLibrary)
    {
        snap = [VideoUtils loadVideoThumbnailForURL:[NSURL URLWithString:sii.itemFilePath]];
    }
    else
    {
        snap = [VideoUtils loadVideoThumbnail:sii.itemFilePath];
    }

    [self generateThumbnail:snap selectedItemInfo:sii];

    if (_pluginParams.saveToGallery && !sii.isFromLibrary && !sii.exportStarted && !sii.exportedToLibrary)
    {
        PluginResult __weak *weakSelf = self;
        sii.exportStarted = YES;
        [VideoUtils exportVideoFileToLibrary:sii.itemFilePath completion:^ {
            sii.exportedToLibrary = YES;
            [weakSelf checkIfAllIsProcessed];
        }];
    }
}

- (void)generateThumbnail:(UIImage *)image selectedItemInfo:(SelectedItemInfo*)sii
{
    if (_pluginParams.generateThumbnailsRequested)
    {
        int maxW = (int)_pluginParams.maxThumbnailWidth;
        int maxH = (int)_pluginParams.maxThumbnailHeight;
        int minW = (int)_pluginParams.minThumbnailWidth;
        int minH = (int)_pluginParams.minThumbnailHeight;
        if (sii.deviceOrientation % 180 != 0)
        {
            // swap width and height for rotated images
            int tmp = maxW;
            maxW = maxH;
            maxH = tmp;

            tmp = minW;
            minW = minH;
            minH = tmp;
        }
        UIImage* thumbnail = [image imageByScalingWithSizeConstraintsMax:CGSizeMake(maxW, maxH) andMin:CGSizeMake(minW, minH)];
        if (sii.deviceOrientation != 0) thumbnail = [thumbnail imageRotatedByDegrees: -sii.deviceOrientation];
        NSData* data = UIImageJPEGRepresentation(thumbnail, (CGFloat) ((double)_pluginParams.quality/100.0));
        if (_pluginParams.thumbnailsAsFiles)
        {
            // save thumbnail to file
            NSString* thumbFileName = [FileUtils getTempFilePathWithExtension:@"jpg"];
            [data writeToFile:thumbFileName atomically:NO];
            sii.thumbFile = thumbFileName;
        }
        else
        {
            sii.thumbData = [@"data:image/jpeg;base64," stringByAppendingString:[data base64EncodedString]];
        }
        sii.thumbWidth = (int) thumbnail.size.width;
        sii.thumbHeight = (int) thumbnail.size.height;
    }

}

- (void)setupConversionFlags
{
    _progressUnitsTotal = self.selectedItems.count;
    _progressUnitsDone = 0;
    for (SelectedItemInfo *sii in self.selectedItems)
    {
        if ([self needToTrimLibraryVideo:sii]) sii.postProcessingFlags |= PostProcessTrimVideo;
        if ([self needToTranscodeVideo:sii]) sii.postProcessingFlags |= PostProcessTranscodeVideo;

        BOOL needResize = !sii.isVideo && sii.isFromLibrary && _pluginParams.downscalePhotosFromGallery;
        if (needResize) sii.postProcessingFlags |= PostProcessResizePhoto;
    }
}

- (void)preprocessVideo:(CompletionBlockType)completion
{
    if (self.selectedItems.count > 0)
    {
        SelectedItemInfo *sii = self.selectedItems[0];
        if (sii.postProcessingFlags & PostProcessTrimVideo)
        {
            [self trimSelectedVideoToMaxDuration:sii progress:^(float progress) {
                _progressUnitsDone = progress;
                [self updateProgress];
            } completion:^ {
                [self preprocessVideo:completion];
            }];
            return;
        }

        if (sii.postProcessingFlags & PostProcessTranscodeVideo)
        {
            [self transcodeVideo:sii  progress:^(float progress) {
                _progressUnitsDone = progress;
                [self updateProgress];
            } completion:^{
                [self preprocessVideo:completion];
            }];
            return;
        }
    }

    if (self.videoParts)
    {
        SelectedItemInfo *sii = self.selectedItems[0];

        PluginResult __weak *weakSelf = self;

        [self.videoParts joinVideoToFile:sii.itemFilePath completion:^(BOOL success) {
            weakSelf.videoParts = nil;
            sii.fileDataSaved = YES;

            [weakSelf prepareResultAsync];

        }];
    }
    else
    {
        [self prepareResultAsync];
    }
}

- (void)prepareResult:(UIView*)parentViewForWaitIndicator
           completion:(CompletionBlockType)completion
{
    self.prepareResultCompletionBlock = completion;
    self.progressHUD = [MBProgressHUD showHUDAddedTo:parentViewForWaitIndicator animated:YES];

    [self setupConversionFlags];

    [self preprocessVideo:completion];
}


- (void)buildResultJSON
{
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    NSMutableArray *items = [NSMutableArray array];

    for (SelectedItemInfo * sii in _selectedItems)
    {
        NSMutableDictionary *entry = [NSMutableDictionary dictionary];
        if (sii.thumbData != nil || sii.thumbFile != nil)
        {
            if (sii.thumbData) [entry setValue:sii.thumbData forKey:@"thumbnailData"];
            if (sii.thumbFile) {
                NSString* thumbPath = [[NSURL fileURLWithPath:sii.thumbFile isDirectory:NO] absoluteString];
                [entry setValue:thumbPath forKey:@"thumbnailUri"];
            }
            [entry setValue:@(sii.thumbWidth) forKey:@"thumbWidth"];
            [entry setValue:@(sii.thumbHeight) forKey:@"thumbHeight"];
        }
        NSString* itemUri;
        if (sii.isFromLibrary) itemUri = sii.itemFilePath;
        else
        {
            itemUri = [[NSURL fileURLWithPath:sii.itemFilePath isDirectory:NO] absoluteString];
        }
        [entry setValue:itemUri forKey:@"fileUri"];
        [entry setValue:@(sii.nativeOrientation + sii.deviceOrientation) forKey:@"orientation"];
        [items addObject:entry];
    }
    [dict setValue:items forKey:RESULT_ITEMS];

    //NSLog(@"Plugin result = %@", dict);
    self.progressHUD = nil;
    dispatch_async(dispatch_get_main_queue(), ^{
        //[MBProgressHUD hideHUDForView:parentViewForWaitIndicator animated:YES];
        self.prepareResultCompletionBlock(NO, dict);
    });
}

- (BOOL)needToTrimLibraryVideo
{
    if (self.selectedItems.count <= 0) return NO;
    SelectedItemInfo *sii = self.selectedItems[0];
    return [self needToTrimLibraryVideo:sii];
}

- (BOOL)needToTrimLibraryVideo:(SelectedItemInfo *)sii
{
    if (!sii.isVideo || !sii.isFromLibrary) return NO;

    NSNumber* dur = [sii.assetObject valueForProperty:ALAssetPropertyDuration];
    double duration = [dur doubleValue];
    return duration > self.pluginParams.maxVideoTime;
}

- (BOOL)needToTranscodeVideo:(SelectedItemInfo *)sii
{
//    if (self.selectedItems.count <= 0) return NO;
//    SelectedItemInfo *sii = self.selectedItems[0];
    if (!sii.isVideo || !sii.isFromLibrary) return NO;

    if (!_pluginParams.downscaleVideosFromGallery) return NO;
    int maxW = (int)_pluginParams.maxVideoWidth;
    int maxH = (int)_pluginParams.maxVideoHeight;

    ALAssetRepresentation *representation = [sii.assetObject defaultRepresentation];
    CGSize vidSize = [representation dimensions];
    if ((vidSize.width <= maxW) && (vidSize.height <= maxH))
    {
        return NO;
    }

    return YES;
}

- (void)trimSelectedVideoToMaxDuration:(SelectedItemInfo*)sii
                              progress:(void(^)(float progress))progressBlock
                            completion:(void (^)(void))completion
{
    NSString* dstFile = [FileUtils getTempFilePathWithExtension:@"mov"];
    int minW = (int)_pluginParams.minVideoWidth;
    int maxW = (int)_pluginParams.maxVideoWidth;
    int minH = (int)_pluginParams.minVideoHeight;
    int maxH = (int)_pluginParams.maxVideoHeight;

    [VideoUtils trimVideo:sii.itemFilePath
                   toFile:dstFile
                 fromTime:0
                 duration:self.pluginParams.maxVideoTime
            minWidth:minW maxWidth:maxW minHeight:minH maxHeight:maxH
                 progress:progressBlock
    completion:^ (BOOL success) {
        //NSLog(@"Video cut complete, success = %d",success);
        sii.postProcessingFlags &= ~PostProcessTrimVideo;
        sii.postProcessingFlags &= ~PostProcessTranscodeVideo;
        sii.itemFilePath = dstFile;
        sii.isFromLibrary = NO;
        sii.exportedToLibrary = YES;
        sii.assetObject = nil;
        completion();
    }];
}

- (void)transcodeVideo:(SelectedItemInfo*)sii
              progress:(void(^)(float progress))progressBlock
            completion:(void (^)(void))completion
{
    NSString* dstFile = [FileUtils getTempFilePathWithExtension:@"mov"];
    int minW = (int)_pluginParams.minVideoWidth;
    int maxW = (int)_pluginParams.maxVideoWidth;
    int minH = (int)_pluginParams.minVideoHeight;
    int maxH = (int)_pluginParams.maxVideoHeight;

    [VideoUtils transcodeVideoFile:sii.itemFilePath toFile:dstFile
                          minWidth:minW maxWidth:maxW minHeight:minH maxHeight:maxH
                          progress:progressBlock
                        completion:^ (BOOL success) {
                            sii.postProcessingFlags &= ~PostProcessTranscodeVideo;
                            if (success)
                            {
                                sii.itemFilePath = dstFile;
                                sii.isFromLibrary = NO;
                                sii.exportedToLibrary = YES;
                                sii.assetObject = nil;
                            }
                            completion();
                        }];
}

@end