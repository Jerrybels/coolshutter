//
// Created by Constantin on 7/04/15.
// Copyright (c) 2015 coolshutter. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface CameraPluginLocalization : NSObject

// maxPhotosText : the message that appears when the user try to take more than the
// limited number of pictures. Default : “You have reached the limit number of photos!”. (String)
@property NSString* maxPhotosText;

// deletePhotoText : the message that appears when the user clicks on one of
// the thumbnails in order to delete a picture.
// Default : “Are you sure that you want to delete this photo ?”. (String)
@property NSString* deletePhotoText;

// deleteVideoText : the message to be displayed when user taps on the recorded video thumbnail
@property NSString* deleteVideoText;

// moveFromPhotosText : the message that appears when the user tries to switch out from
// photo mode and has pictures waiting for upload.
// Default : “Are you sure that you want to move away and lose the pictures you took ?”. (String)
@property NSString* moveFromPhotosText;

// moveFromVideosText : the message that appears when the user tries to switch out
// from video mode and has a video waiting for upload.
// Default : “Are you sure that you want to move away and lose the video you took ?”. (String)
@property NSString* moveFromVideosText;

// maxVideoText : the message that appears when maxVideoTime is reached.
// Default : “Maximum video length reached !”. (String)
@property NSString* maxVideoText;
@property NSString* maxVideoTimeCutWarning;

// Text for alerts buttons
@property NSString* alertCancelText;
@property NSString* alertConfirmText;
@property NSString* alertOkText;

// Mode buttons text
@property NSString* photoLabel;
@property NSString* galleryLabel;
@property NSString* videoLabel;
@property NSString* videosLabel;
@property NSString* photosLabel;


- (id)initWithJSON:(NSDictionary*)json;

@end