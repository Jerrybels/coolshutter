//
// Created by Constantin on 27/04/15.
// Copyright (c) 2015 coolshutter. All rights reserved.
//

#import "UIView+ObjectTagAdditions.h"
#import <objc/runtime.h>


static char const * const ObjectTagKey = "ObjectTag";

@implementation UIView (ObjectTagAdditions)

@dynamic objectTag;

- (id)objectTag
{
    return objc_getAssociatedObject(self, ObjectTagKey);
}

- (void)setObjectTag:(id)newObjectTag
{
    objc_setAssociatedObject(self, ObjectTagKey, newObjectTag, OBJC_ASSOCIATION_ASSIGN);
}

@end