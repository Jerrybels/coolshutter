//
// Created by Constantin on 26/04/15.
// Copyright (c) 2015 coolshutter. All rights reserved.
//

#import "VideoUtils.h"
#import "LittleThings.h"
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/ALAssetRepresentation.h>


@implementation VideoUtils
{

}

+ (UIImage*)loadVideoThumbnailForURL:(NSURL*)videoURL
{
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:videoURL options:nil];
    //NSLog(@"Generate video thumbnail for asset %@", asset);
    //NSLog(@"Asset playable = %d, exportable = %d, readable = %d", asset.playable, asset.exportable, asset.readable);
    //NSLog(@"Asset duration = %f", CMTimeGetSeconds(asset.duration));

    AVAssetImageGenerator *generator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    generator.appliesPreferredTrackTransform = YES;

    NSError *err = nil;
    CMTime time = CMTimeMake(0, 1);
    CGImageRef imgRef = [generator copyCGImageAtTime:time actualTime:nil error:&err];

    if (err != nil) {
        NSLog(@"Error generating video thumbnail: err==%@", err);
        return nil;
    }

    UIImage* finalImage = [[UIImage alloc] initWithCGImage:imgRef];
    CGImageRelease(imgRef);
    return finalImage;
}

+ (UIImage*)loadVideoThumbnail:(NSString*)videoPath
{
    return [VideoUtils loadVideoThumbnailForURL:[NSURL fileURLWithPath:videoPath isDirectory:NO]];
}

+ (void)joinVideoParts:(NSArray*)srcPartsPaths toFile:(NSString*)outputFile completion:(void (^)(BOOL success))completion
{
    //NSLog(@"Join video parts: %@", srcPartsPaths);

    if (srcPartsPaths.count == 1)
    {
        BOOL success = [LittleThings moveFileFrom:srcPartsPaths[0] to:outputFile];
        completion(success);
        return;
    }

    NSMutableArray *srcAssets = [NSMutableArray arrayWithCapacity:srcPartsPaths.count];
    for (NSString* path in srcPartsPaths)
    {
        AVAsset *asset = [AVAsset assetWithURL:[NSURL fileURLWithPath:path isDirectory:NO]];
        [srcAssets addObject:asset];
    }

    //setup composition and track
    AVMutableComposition *composition = [[AVMutableComposition alloc]init];

    AVMutableCompositionTrack *outVideoTrack = [composition addMutableTrackWithMediaType:AVMediaTypeVideo preferredTrackID:kCMPersistentTrackID_Invalid];
    AVMutableCompositionTrack *outAudioTrack = [composition addMutableTrackWithMediaType:AVMediaTypeAudio preferredTrackID:kCMPersistentTrackID_Invalid];

    //add assets to track
    CMTime curTime = kCMTimeZero;
    BOOL transformSet = NO;
    for (AVAsset* asset in srcAssets)
    {
        //NSLog(@"Adding asset %@", asset);
        //NSLog(@"Asset playable = %d, exportable = %d, readable = %d", asset.playable, asset.exportable, asset.readable);
        //NSLog(@"Asset duration = %f", CMTimeGetSeconds(asset.duration));

        CMTimeRange range = CMTimeRangeMake(kCMTimeZero, asset.duration);

        NSArray *tracks = [asset tracksWithMediaType:AVMediaTypeVideo];
        if (tracks.count > 0)
        {
            AVAssetTrack* vTrack = tracks[0];
            [outVideoTrack insertTimeRange:range ofTrack:vTrack atTime:curTime error:nil];
            if (!transformSet) {
                outVideoTrack.preferredTransform = vTrack.preferredTransform;
                transformSet = YES;
            }
        }
        tracks = [asset tracksWithMediaType:AVMediaTypeAudio];
        if (tracks.count > 0)
        {
            AVAssetTrack* aTrack = tracks[0];
            [outAudioTrack insertTimeRange:range ofTrack:aTrack atTime:curTime error:nil];
        }

        curTime = CMTimeAdd(curTime, asset.duration);
    }

    AVAssetExportSession *exporter = [[AVAssetExportSession alloc] initWithAsset:composition presetName:AVAssetExportPresetPassthrough];

    ////NSLog(@"%@", exporter.supportedFileTypes);
    exporter.outputURL=[NSURL fileURLWithPath:outputFile];

    exporter.outputFileType = AVFileTypeMPEG4;
    [exporter exportAsynchronouslyWithCompletionHandler:^{
        dispatch_async(dispatch_get_main_queue(), ^{
            //NSLog(@"Video join complete");
            //NSLog(@"Session status = %li", (long)exporter.status);
            //NSLog(@"Exporter error = %@", exporter.error);
            BOOL success = exporter.status == AVAssetExportSessionStatusCompleted;
            if (completion != nil) completion(success);
        });
    }];
}


+ (void)exportVideoFileToLibrary:(NSString*)filePath completion:(void (^)(void))completion
{
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
    NSURL* fileURL = [NSURL URLWithString:filePath];
    if ([library videoAtPathIsCompatibleWithSavedPhotosAlbum:fileURL]) {
        [library writeVideoAtPathToSavedPhotosAlbum:fileURL
                                    completionBlock:^(NSURL *assetURL, NSError *error){
                                        dispatch_async(dispatch_get_main_queue(), ^{
                                            if (completion) completion();
                                        });

                                    }];
    }

}

+ (void)exportPhotoToLibrary:(UIImage*)image completion:(void (^)(void))completion
{
    ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];

    [library writeImageToSavedPhotosAlbum:[image CGImage]
                              orientation:(ALAssetOrientation)[image imageOrientation]
						  completionBlock:^(NSURL *assetURL, NSError *error){
							  completion();
							}];
}

+ (void)trimVideo:(NSString*)srcPath
           toFile:(NSString*)dstPath
         fromTime:(double)start
         duration:(double)duration
        minWidth:(int)minW
        maxWidth:(int)maxW
       minHeight:(int)minH
       maxHeight:(int)maxH
        progress:(void(^)(float progress))progressBlock
        completion:(void (^)(BOOL success))completion
{
    int64_t startMilliseconds = (int64_t) (start * 1000);
    int64_t endMilliseconds = (int64_t) ((start + duration) * 1000);

    NSString *bestPreset = [VideoUtils findBestVideoExportPresetMinWidth:minW maxWidth:maxW minHeight:minH maxHeight:maxH];

    AVURLAsset *videoAsset = [AVURLAsset URLAssetWithURL:[NSURL URLWithString:srcPath] options:nil];

    // see if it's possible to export at the requested quality
    NSArray *compatiblePresets = [AVAssetExportSession exportPresetsCompatibleWithAsset:videoAsset];

    if (!bestPreset || ![compatiblePresets containsObject:bestPreset])
    {
        bestPreset = AVAssetExportPresetPassthrough;
    }

    NSLog(@"Trimming video with preset %@", bestPreset);
    AVAssetExportSession *exportSession = [[AVAssetExportSession alloc] initWithAsset:videoAsset presetName:bestPreset];
    exportSession.outputURL = [NSURL fileURLWithPath:dstPath];
    exportSession.outputFileType = AVFileTypeQuickTimeMovie;
    CMTimeRange timeRange = CMTimeRangeMake(CMTimeMake(startMilliseconds, 1000), CMTimeMake(endMilliseconds - startMilliseconds, 1000));
    exportSession.timeRange = timeRange;

    [exportSession exportAsynchronouslyWithCompletionHandler:^{
        switch (exportSession.status) {
            case AVAssetExportSessionStatusCompleted:
                NSLog(@"Export successfull");
                completion(YES);
                break;

            case AVAssetExportSessionStatusFailed:
                NSLog(@"Export failed:%@",exportSession.error);
                completion(NO);
                break;

            case AVAssetExportSessionStatusCancelled:
                NSLog(@"Canceled:%@", exportSession.error);
                completion(NO);
                break;

            default:
                NSLog(@"Export in progress, status = %ld", (long)exportSession.status);
                break;
        }
    }];

    [VideoUtils monitorExportSessionProgress:exportSession progress:progressBlock];
}

+ (void)monitorExportSessionProgress:(AVAssetExportSession*)session progress:(void(^)(float progress))progressBlock
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, NSEC_PER_MSEC * 200), dispatch_get_main_queue(), ^{
        progressBlock(session.progress);
        if (session.status < AVAssetExportSessionStatusCompleted)
        {
            [VideoUtils monitorExportSessionProgress:session progress:progressBlock];
        }
    });
}

+ (NSString*)findBestVideoCapturePresetMinWidth:(int)minW maxWidth:(int)maxW minHeight:(int)minH maxHeight:(int)maxH
{
    if (maxW <= 0) return AVCaptureSessionPresetHigh;

    struct PresetInfo
    {
        int width;
        int height;
    };
    static struct PresetInfo presetInfo[] = {
            { 352, 288 },
            { 640, 480 },
            { 1280, 720 },
            { 1920, 1080 },
            { 0, 0 },
    };
    NSString* const presetList[] = {
            AVCaptureSessionPreset352x288,
            AVCaptureSessionPreset640x480,
            AVCaptureSessionPreset1280x720,
            AVCaptureSessionPreset1920x1080
    };

    for (int ii = 0; presetInfo[ii].width > 0; ii++)
    {
        int w = presetInfo[ii].width;
        int h = presetInfo[ii].height;
        if ((w >= minW) && (w <= maxW) && (h >= minH) && (h <= maxH)) return presetList[ii];
    }

    // None of the available resolutions is inside the requested range
    if ((minW > 1920) && (minH > 1080)) return AVCaptureSessionPresetHigh;

    int reqW = (maxW + minW) / 2;
    int reqH = (maxH + minH) / 2;

    int minDist = 1000000000;
    int bestPreset = -1;
    for (int ii = 0; presetInfo[ii].width > 0; ii++)
    {
        int w = presetInfo[ii].width;
        int h = presetInfo[ii].height;
        int dist = abs(reqW * reqH - w * h);
        if (dist < minDist)
        {
            minDist = dist;
            bestPreset = ii;
        }
    }

    return bestPreset >= 0 ? presetList[bestPreset] : AVCaptureSessionPresetHigh;
}

+ (NSString*)findBestVideoExportPresetMinWidth:(int)minW maxWidth:(int)maxW minHeight:(int)minH maxHeight:(int)maxH
{
    if (maxW <= 0) return nil;

    struct PresetInfo
    {
        int width;
        int height;
    };
    static struct PresetInfo presetInfo[] = {
            { 640, 480 },
            { 960, 540 },
            { 1280, 720 },
            { 1920, 1080 },
            { 0, 0 },
    };
    NSString* const presetList[] = {
            AVAssetExportPreset640x480,
            AVAssetExportPreset960x540,
            AVAssetExportPreset1280x720,
            AVAssetExportPreset1920x1080
    };

    for (int ii = 0; presetInfo[ii].width > 0; ii++)
    {
        int w = presetInfo[ii].width;
        int h = presetInfo[ii].height;
        if ((w >= minW) && (w <= maxW) && (h >= minH) && (h <= maxH)) return presetList[ii];
    }

    // None of the available resolutions is inside the requested range
    if ((minW > 1920) && (minH > 1080)) return nil;

    int reqW = (maxW + minW) / 2;
    int reqH = (maxH + minH) / 2;

    int minDist = 1000000000;
    int bestPreset = -1;
    for (int ii = 0; presetInfo[ii].width > 0; ii++)
    {
        int w = presetInfo[ii].width;
        int h = presetInfo[ii].height;
        int dist = abs(reqW * reqH - w * h);
        if (dist < minDist)
        {
            minDist = dist;
            bestPreset = ii;
        }
    }

    return bestPreset >= 0 ? presetList[bestPreset] : nil;
}

+ (void)transcodeVideoAsset:(ALAsset *)asset toFile:(NSString *)dstPath
                   minWidth:(int)minW
                   maxWidth:(int)maxW
                  minHeight:(int)minH
                  maxHeight:(int)maxH
                   progress:(void(^)(float progress))progressBlock
                 completion:(void (^)(BOOL success))completion
{
    ALAssetRepresentation *representation = [asset defaultRepresentation];

    NSURL *url = [representation url];
    AVAsset *avAsset = [AVURLAsset URLAssetWithURL:url options:nil];

    [VideoUtils transcodeAVAsset:avAsset toFile:dstPath minWidth:minW
                        maxWidth:maxW minHeight:minH maxHeight:maxH
                        progress:progressBlock
                      completion:completion];

}

+ (void)transcodeVideoFile:(NSString *)srcPath toFile:(NSString *)dstPath
                  minWidth:(int)minW
                  maxWidth:(int)maxW
                 minHeight:(int)minH
                 maxHeight:(int)maxH
                  progress:(void(^)(float progress))progressBlock
                completion:(void (^)(BOOL success))completion
{
    AVAsset *asset = [AVAsset assetWithURL:[NSURL URLWithString:srcPath]];
    [VideoUtils transcodeAVAsset:asset toFile:dstPath
                        minWidth:minW maxWidth:maxW minHeight:minH maxHeight:maxH
                        progress:progressBlock
                      completion:completion];
}

+ (void)transcodeAVAsset:(AVAsset *)asset toFile:(NSString *)dstPath
                   minWidth:(int)minW
                   maxWidth:(int)maxW
                  minHeight:(int)minH
                  maxHeight:(int)maxH
                progress:(void(^)(float progress))progressBlock
              completion:(void (^)(BOOL success))completion
{
    NSString *bestPreset = [VideoUtils findBestVideoExportPresetMinWidth:minW maxWidth:maxW minHeight:minH maxHeight:maxH];
    if (!bestPreset)
    {
        completion(NO);
        return;
    }

    // see if it's possible to export at the requested quality
    NSArray *compatiblePresets = [AVAssetExportSession exportPresetsCompatibleWithAsset:asset];

    if ([compatiblePresets containsObject:bestPreset])
    {
        // set up the export
        AVAssetExportSession *exportSession = [[AVAssetExportSession alloc] initWithAsset:asset presetName:bestPreset];
        exportSession.outputURL = [NSURL fileURLWithPath:dstPath];
        exportSession.outputFileType = AVFileTypeQuickTimeMovie;

        NSLog(@"Transcoding video to %@", bestPreset);
        // run the export
        [exportSession exportAsynchronouslyWithCompletionHandler:^{
            switch ([exportSession status]) {
                case AVAssetExportSessionStatusFailed:
                    NSLog(@"Error while transcoding video: %@", exportSession.error);
                    completion(NO);
                    break;

                case AVAssetExportSessionStatusCancelled:
                    NSLog(@"Video transcoding cancelled");
                    completion(NO);
                    break;

                case AVAssetExportSessionStatusExporting:
                    NSLog(@"Video transcoding progress %f", exportSession.progress);
                    break;

                default:
                    NSLog(@"Unhandled export status: %ld", (long)exportSession.status);
                    break;

                case AVAssetExportSessionStatusCompleted:
                    NSLog(@"Transcoding complete");
                    completion(YES);
                    break;
            }
        }];
        [VideoUtils monitorExportSessionProgress:exportSession progress:progressBlock];
    }
    else
    {
        NSLog(@"No compatible preset with %@", bestPreset);
        completion(NO);
    }

}



@end