//
// Created by Constantin on 12/04/15.
// Copyright (c) 2015 coolshutter. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (TransformationHelper)

- (void)setRotationDegrees:(double)degrees;

@end