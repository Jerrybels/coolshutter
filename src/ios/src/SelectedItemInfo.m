//
// Created by Constantin on 12/04/15.
// Copyright (c) 2015 coolshutter. All rights reserved.
//

#import "SelectedItemInfo.h"


@implementation SelectedItemInfo {

}

- (instancetype)initFromLibrary:(BOOL)fromLibrary isVideo:(BOOL)isVideo
{
    self = [super init];
    self.isVideo = isVideo;
    self.isFromLibrary = fromLibrary;
    return self;
}

@end