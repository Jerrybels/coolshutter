//
// Created by Constantin on 12/04/15.
// Copyright (c) 2015 coolshutter. All rights reserved.
//

#import "UIView+TransformationHelper.h"


@implementation UIView (TransformationHelper)

static double degreesToRadians(double angle) {
    return ((angle) / 180.0 * M_PI);
}

- (void)setRotationDegrees:(double)degrees {
    self.transform = CGAffineTransformMakeRotation(degreesToRadians(degrees));
}

@end