//
// Created by Constantin on 7/04/15.
// Copyright (c) 2015 coolshutter. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CameraPluginLocalization;

//final private static int FLAG_ALLOW_PHOTO = 1;
//final private static int FLAG_ALLOW_VIDEO = 2;
//final private static int FLAG_ALLOW_GALLERY_PHOTO = 4;
//final private static int FLAG_ALLOW_GALLERY_VIDEO = 8;
//final private static int FLAG_ALLOW_ALL = FLAG_ALLOW_PHOTO | FLAG_ALLOW_VIDEO | FLAG_ALLOW_GALLERY_PHOTO | FLAG_ALLOW_GALLERY_VIDEO;

typedef NS_OPTIONS(NSUInteger, CameraAllowedSources)
{
    FLAG_ALLOW_PHOTO            = 1 << 0,
    FLAG_ALLOW_VIDEO            = 1 << 1,
    FLAG_ALLOW_GALLERY_PHOTO    = 1 << 2,
    FLAG_ALLOW_GALLERY_VIDEO    = 1 << 3,
    FLAG_ALLOW_ALL = FLAG_ALLOW_PHOTO | FLAG_ALLOW_VIDEO | FLAG_ALLOW_GALLERY_PHOTO | FLAG_ALLOW_GALLERY_VIDEO
};

@interface CameraPluginParams : NSObject

@property (readonly) BOOL videoAllowed;
@property (readonly) BOOL photoAllowed;
@property (readonly) BOOL videoGalleryAllowed;
@property (readonly) BOOL photoGalleryAllowed;
@property (readonly) BOOL someGallerySourceAllowed;

@property NSUInteger quality;
@property NSUInteger maxPhotos;
@property NSUInteger maxVideoTime;
@property BOOL saveToGallery;
@property BOOL generateThumbnailsRequested;
@property BOOL thumbnailsAsFiles;
@property NSUInteger maxThumbnailWidth;
@property NSUInteger maxThumbnailHeight;
@property NSUInteger minThumbnailWidth;
@property NSUInteger minThumbnailHeight;

@property NSUInteger maxPhotoWidth;
@property NSUInteger minPhotoWidth;
@property NSUInteger maxPhotoHeight;
@property NSUInteger minPhotoHeight;

@property NSUInteger maxVideoWidth;
@property NSUInteger minVideoWidth;
@property NSUInteger maxVideoHeight;
@property NSUInteger minVideoHeight;

@property BOOL downscalePhotosFromGallery;
@property BOOL downscaleVideosFromGallery;


@property CameraPluginLocalization *localization;


-(id)initWithJSON:(NSData*)json;
-(id)init;

@end