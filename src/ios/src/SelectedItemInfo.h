//
// Created by Constantin on 12/04/15.
// Copyright (c) 2015 coolshutter. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AssetsLibrary/AssetsLibrary.h>

typedef NS_ENUM(NSInteger, ItemPostProcessingFlags) {
    PostProcessTrimVideo = 0x1,
    PostProcessTranscodeVideo = 0x2,
    PostProcessResizePhoto = 0x4
};


@interface SelectedItemInfo : NSObject

// top bar thumbnail view
@property (nonatomic, strong) UIImageView* imgView;

// path to the file
@property (nonatomic, strong) NSString *itemFilePath;

// item data is saved on background thread, so it can become available with some delay
@property (assign) BOOL fileDataSaved;

// base64 encoded JPEG representation of the item thumbnail (if requested in plugin parameters)
@property (nonatomic, strong) NSString* thumbData;
// thumbnail file path (if thumbs are requested as files)
@property (nonatomic, strong) NSString* thumbFile;
// thumbnail width and height
@property (assign) int thumbWidth;
@property (assign) int thumbHeight;

// flag indicating that the item was successfully exported to the phone assets library (if requested in the plugin parameters)
@property (assign) BOOL exportedToLibrary;
@property (assign) BOOL exportStarted;

@property (assign) BOOL isVideo;
// if that item is picked from the phone library or taken with camera (file in tmp folder)
@property (assign) BOOL isFromLibrary;

@property (assign) uint32_t postProcessingFlags;

@property(nonatomic, strong) ALAsset *assetObject;

@property(nonatomic, assign) int nativeOrientation;
@property(nonatomic, assign) int deviceOrientation;

- (instancetype)initFromLibrary:(BOOL)fromLibrary isVideo:(BOOL)isVideo;


@end