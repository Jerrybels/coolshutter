//
//  CoolShutterViewController.m
//  CoolShutterApp
//
//  Created by Constantin on 08/04/15.
//  Copyright (c) 2015 coolshutter. All rights reserved.
//

#import "CoolShutterViewController.h"
#import "CameraPluginParams.h"
#import "VideoPreviewView.h"
#import "UIViewController+UIViewController_FullScreen.h"
#import "CameraPluginLocalization.h"
#import "ColorUtils.h"
#import "UIView+AutoLayout.h"
#import "UIView+TransformationHelper.h"
#import "ThumbsBar.h"
#import "UIImage+ImageUtils.h"
#import "SelectedItemInfo.h"
#import "TransformableButton.h"
#import "CameraAccess.h"
#import "PluginResult.h"
#import "LittleThings.h"
#import "ToastView.h"
#import "VideoRecordingState.h"
#import "FileUtils.h"
#import "VideoUtils.h"
#import "UIAlertView+Blocks.h"
#import "HMSegmentedControl.h"
#import "MediaLibrary.h"

UIColor* MODE_LABEL_TEXT_COLOR_INACTIVE;
UIColor* MODE_LABEL_TEXT_COLOR_ACTIVE;
#define MODE_BUTTON_SIZE 50
#define MODE_LABEL_INSET 10
#define MODE_LABEL_TOP_OFFSET -5
#define MODE_LABEL_HOR_SPACING 6
NSString* MODE_LABEL_FONT = @"HelveticaNeue-Bold";
#define MODE_LABEL_TEXT_SIZE  14
#define MODE_ICON_BOTTOM_OFFSET 12
#define INACTIVE_BUTTON_HOR_OFFSET  MODE_BUTTON_SIZE

const float RECORDING_INDICATOR_TIMER_RES = 0.05;
const float RECORDING_INDICATOR_BLINK_TIME = 0.5;
const int RECORDING_INDICATOR_BLINK_COUNT = (int)(RECORDING_INDICATOR_BLINK_TIME / RECORDING_INDICATOR_TIMER_RES);


typedef NS_ENUM(int, PluginModeType)
{
    PluginModeGallery = 0,
    PluginModePhoto = 1,
    PluginModeVideo = 2,
    PluginModesCount = 3
};

@interface CoolShutterViewController () <CameraAccessDelegate, ThumbsBarDelegate, MediaLibraryDelegate>
{
    BOOL _firstLayoutPass;
    int _currentMode; // PluginModeGallery, PluginModePhoto, PluginModeVideo
    int _currentScreenRotation; // in degrees
    float _progressContainerBottomOffset;
    int _recordingIndicatorBlinkCounter;
    int _screenRotationSavedOnRecordingStart;
    // photo taking is in progress (i.e. user pressed the
    // shutter button, but the photo callback did not fire yet)
    BOOL _photoInProgress;
}

@property (strong, nonatomic) CameraPluginParams* pluginParams;
@property (strong, nonatomic) PluginResult* pluginResult;
@property (strong, nonatomic) IBOutlet UIView* containerView;

@property (strong, nonatomic) IBOutlet UIButton* cancelButton;
@property (strong, nonatomic) IBOutlet UIButton* uploadButton;
@property (strong, nonatomic) IBOutlet UIScrollView* thumbsScroll;
@property (strong, nonatomic) IBOutlet UIView* leftFadingEdge;
@property (strong, nonatomic) IBOutlet UIView* rightFadingEdge;

@property (strong, nonatomic) IBOutlet UIView* bottomBar;
@property (strong, nonatomic) IBOutlet UIView* topBar;

@property (strong, nonatomic) IBOutlet UIView* progressContainer;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint* progressContainerCenterX;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint* progressContainerBottomSpace;

@property (strong, nonatomic) IBOutlet UIProgressView* progressView;
@property (strong, nonatomic) IBOutlet UILabel* remainingTimeLabel;

@property (strong, nonatomic) IBOutlet UIImageView* recordingIndicator;

@property (strong, nonatomic) IBOutlet UIView* galleryContainer;
@property (strong, nonatomic) IBOutlet HMSegmentedControl* galleryFilterControl;
@property (strong, nonatomic) IBOutlet UICollectionView* gridView;


@property TransformableButton* galleryButton;
@property TransformableButton* photoButton;
@property TransformableButton* videoButton;

@property NSArray* activeButtons;
@property NSArray* activeLabels;


@property (strong, nonatomic) IBOutlet UIButton* flashModeButton;
@property (strong, nonatomic) IBOutlet UIButton* switchCameraButton;

@property (strong, nonatomic) IBOutlet VideoPreviewView* cameraPreviewView;

@property (nonatomic, strong) IBOutlet UIView* focusRect;

@property (nonatomic, strong) IBOutlet NSLayoutConstraint* focusRectCenterX;
@property (nonatomic, strong) IBOutlet NSLayoutConstraint* focusRectCenterY;

@property ThumbsBar* thumbsBar;
@property CameraAccess* camera;
@property MediaLibrary *mediaLibrary;

- (void)switchToPhotoMode:(id)sender;
- (void)switchToVideoMode:(id)sender;
- (void)switchToGalleryMode:(id)sender;
- (IBAction)switchCamera:(id)sender;
- (IBAction)selectFlashMode:(id)sender;
- (IBAction)cancelPicker:(id)sender;
- (IBAction)donePicking:(id)sender;
- (IBAction)takePicture:(id)sender;
- (IBAction)recordVideo:(id)sender;



@property(nonatomic, strong) NSMutableArray *activeButtonsConstraints;
@property(nonatomic, strong) NSTimer *recordingIndicatorTimer;

@property (copy) PickerCompletionBlock pickerCompletionBlock;

@property(nonatomic, strong) UITapGestureRecognizer *tapRecognizer;

@end

@implementation CoolShutterViewController
{
}

#pragma mark -
#pragma mark INITIALIZATION

- (id)init {
    ////NSLog(@"CoolShutter::init");
    self = [super init];
    self.pluginParams = [[CameraPluginParams alloc] init];
    [self setupDefaultInstanceVariableValues];
    return self;
}

- (id)initWithParameters:(CameraPluginParams *)params completion:(PickerCompletionBlock)completion {
    //NSLog(@"CoolShutter::initWithParameters");
    self = [self initWithNibName:@"CoolShutterViewController" bundle:nil];
    self.pluginParams = params;
    self.pickerCompletionBlock = completion;
    [self setupDefaultInstanceVariableValues];
    return self;
}

- (void)setupDefaultInstanceVariableValues
{
    _firstLayoutPass = YES;
    _currentScreenRotation = 0;
    self.camera = [[CameraAccess alloc] init];
    self.camera.delegate = self;
    self.pluginResult = [[PluginResult alloc] initWithPluginParams:self.pluginParams];
    self.camera.pluginResult = self.pluginResult;
}

#pragma mark -
#pragma mark LIFECYCLE CALLBACKS

- (void)viewDidLoad {
    //NSLog(@"CoolShutter::viewDidLoad");

    [super viewDidLoad];

    _progressContainerBottomOffset = self.progressContainerBottomSpace.constant;

    MODE_LABEL_TEXT_COLOR_INACTIVE = [[UIColor alloc] initWithRGBValue:0x929292];
    MODE_LABEL_TEXT_COLOR_ACTIVE   = [[UIColor alloc] initWithRGBValue:0x24ABBC];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleOrientationChange:) name:UIDeviceOrientationDidChangeNotification object:nil];

    [self setFullScreenMode];

    [self createModeButtons];
    [self.camera setupCameraPreviewAsync:self.cameraPreviewView
                    withVideoOrientation:self.interfaceOrientation
                andInitialModeSetToVideo:(_currentMode == PluginModeVideo)];

    self.mediaLibrary = [[MediaLibrary alloc] initWithGridView:self.gridView containerView:self.galleryContainer
                                               filtersControls:self.galleryFilterControl
                                                pluginParams:self.pluginParams];
    self.mediaLibrary.delegate = self;

    UISwipeGestureRecognizer* swipeRecognizer = [[UISwipeGestureRecognizer alloc] init];
    swipeRecognizer.direction = UISwipeGestureRecognizerDirectionLeft;
    [swipeRecognizer addTarget:self action:@selector(onSwipeLeft:)];
    [self.containerView addGestureRecognizer:swipeRecognizer];

    swipeRecognizer = [[UISwipeGestureRecognizer alloc] init];
    swipeRecognizer.direction = UISwipeGestureRecognizerDirectionRight;
    [swipeRecognizer addTarget:self action:@selector(onSwipeRight:)];
    [self.containerView addGestureRecognizer:swipeRecognizer];

    self.tapRecognizer = [[UITapGestureRecognizer alloc] init];
    [self.tapRecognizer addTarget:self action:@selector(onTapToFocus:)];
    [self.containerView addGestureRecognizer:self.tapRecognizer];
    
    // force switch to a new mode
    int newMode = _currentMode;
    _currentMode = -1;
    [self doSwitchToMode:newMode];

    [self setupFocusRectContents];
}

- (void)viewWillAppear:(BOOL)animated {
    [self.camera startPreview];

    if (self.thumbsBar == nil)
    {
        self.thumbsBar = [[ThumbsBar alloc] initWithScroller:self.thumbsScroll
                                                    leftEdge:self.leftFadingEdge
                                                   rightEdge:self.rightFadingEdge
                                                   fadeColor:self.topBar.backgroundColor];
        self.thumbsBar.pluginResult = self.pluginResult;
        self.thumbsBar.delegate = self;
    }

}

- (void)viewDidAppear:(BOOL)animated {
    CGPoint progressCenter = self.bottomBar.center;
    progressCenter.y = self.bottomBar.frame.origin.y - self.progressContainer.bounds.size.height;
    self.progressContainer.center = progressCenter;
}

- (void)viewWillDisappear:(BOOL)animated {
    [self restoreFullScreenMode];
}

- (void)viewDidDisappear:(BOOL)animated
{
    //NSLog(@"CoolShutter::viewDidDisappear");
}

- (void)viewDidLayoutSubviews
{
    if (_firstLayoutPass) {
        [self updateModeButtonsAndLabels];
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return NO;
}

- (BOOL)shouldAutorotate {
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL) prefersStatusBarHidden
{
    return YES;
}

- (void)onSwipeLeft:(UIGestureRecognizer*)rec
{
    [self switchModeOnSwipe:1];
}

- (void)onSwipeRight:(UIGestureRecognizer*)rec
{
    [self switchModeOnSwipe:-1];
}

#pragma mark -
#pragma mark FOCUS INDICATOR

- (void)setupFocusRectContents
{
    CGSize sz = self.focusRect.bounds.size;
    CAShapeLayer *shape = [CAShapeLayer layer];
    CGMutablePathRef path = CGPathCreateMutable();
    float segSize = 12;
    CGPathMoveToPoint(path, nil, 0, segSize);
    CGPathAddLineToPoint(path, nil, 0, 0);
    CGPathAddLineToPoint(path, nil, segSize, 0);

    CGPathMoveToPoint(path, nil, sz.width, segSize);
    CGPathAddLineToPoint(path, nil, sz.width, 0);
    CGPathAddLineToPoint(path, nil, sz.width - segSize, 0);

    CGPathMoveToPoint(path, nil, sz.width, sz.height - segSize);
    CGPathAddLineToPoint(path, nil, sz.width, sz.height);
    CGPathAddLineToPoint(path, nil, sz.width - segSize, sz.height);

    CGPathMoveToPoint(path, nil, 0, sz.height - segSize);
    CGPathAddLineToPoint(path, nil, 0, sz.height);
    CGPathAddLineToPoint(path, nil, segSize, sz.height);

    shape.path = path;
    shape.frame = self.focusRect.bounds;
    shape.strokeColor = [UIColor greenColor].CGColor;
    shape.lineWidth = 2;
    shape.fillColor = nil;

    self.focusRect.backgroundColor = [UIColor clearColor];
    [self.focusRect.layer addSublayer:shape];
}

- (void)onTapToFocus:(UIGestureRecognizer*)rec
{
    CGPoint pt = [rec locationInView:self.cameraPreviewView];
    CGPoint center = self.cameraPreviewView.center;
    self.focusRectCenterX.constant = center.x - pt.x;
    self.focusRectCenterY.constant = center.y - pt.y;
    pt = [self.camera convertToPointOfInterestFrom:self.cameraPreviewView.frame coordinates:pt];
    [self.camera doFocusAndExposureAtPoint:pt];
}

- (void)showFocusingAnimation
{
    [self.focusRect layoutIfNeeded];

    // Objective C callback hell :)
    [UIView animateWithDuration:0.0
                          delay:0.0
                        options:UIViewAnimationOptionAllowUserInteraction
                     animations:^{
                         self.focusRect.alpha = 0;
                         self.focusRect.transform = CGAffineTransformMakeScale(2, 2);
                     }
                     completion:^(BOOL finished) {
                         [UIView animateWithDuration:0.5
                                               delay:0
                                             options:UIViewAnimationOptionCurveEaseOut|UIViewAnimationOptionAllowUserInteraction
                                          animations:^{
                             self.focusRect.alpha = 1;
                             self.focusRect.transform = CGAffineTransformIdentity;
                         } completion:^(BOOL success){
                             [UIView animateWithDuration:0.3
                                                   delay:1.5
                                                 options:UIViewAnimationOptionAllowUserInteraction
                                              animations:^ {
                                 self.focusRect.alpha = 0;
                             } completion:nil];
                         }];
                     }
    ];
}

#pragma mark -
#pragma mark CONTROLS SETUP

- (UIButton *)createModeLabel:(NSString *)text type:(NSInteger)buttonMode action:(SEL)action {
    UIButton *lbl = [UIButton buttonWithType:UIButtonTypeCustom];
    lbl.tag = buttonMode;
    lbl.titleLabel.font = [UIFont fontWithName:MODE_LABEL_FONT size:MODE_LABEL_TEXT_SIZE];
    [lbl setTitleColor:MODE_LABEL_TEXT_COLOR_INACTIVE forState:UIControlStateNormal];
    lbl.translatesAutoresizingMaskIntoConstraints = NO;
    [lbl setTitle:text forState:UIControlStateNormal];
    [lbl setContentEdgeInsets:UIEdgeInsetsMake(MODE_LABEL_INSET, MODE_LABEL_INSET, MODE_LABEL_INSET, MODE_LABEL_INSET)];
    lbl.titleLabel.textAlignment = NSTextAlignmentCenter;
    [lbl addTarget:self action:action forControlEvents:UIControlEventTouchUpInside];
    return lbl;
}

- (TransformableButton *)createModeButton:(NSString *)iconName type:(NSInteger)buttonMode action:(SEL)action {
    TransformableButton* btn = [TransformableButton buttonWithType:UIButtonTypeCustom];
    btn.tag = buttonMode;
    btn.translatesAutoresizingMaskIntoConstraints = NO;
    [btn setImage:[UIImage imageNamed:iconName] forState:UIControlStateNormal];
    [btn constrainToWidth:MODE_BUTTON_SIZE];
    [btn constrainToHeight:MODE_BUTTON_SIZE];
    if (action != nil)
    {
        [btn addTarget:self action:action forControlEvents:UIControlEventTouchUpInside];
    }
    return btn;
}

- (void)createModeButtons {
    NSMutableArray *labels = [NSMutableArray arrayWithCapacity:3];
    NSMutableArray *buttons = [NSMutableArray arrayWithCapacity:3];
    self.activeLabels = labels;
    self.activeButtons = buttons;

    PluginModeType newMode = PluginModePhoto;
    if (_pluginParams.someGallerySourceAllowed) {
        UIButton *lbl = [self createModeLabel:_pluginParams.localization.galleryLabel type:PluginModeGallery action:@selector(switchToGalleryMode:)];

        self.galleryButton = [self createModeButton:@"gallery_mode.png" type:PluginModeGallery action:nil];
        self.galleryButton.userInteractionEnabled = NO;
        [labels addObject:lbl];
        [buttons addObject:_galleryButton];
        newMode = PluginModeGallery;
    }
    if (_pluginParams.photoAllowed)
    {
        UIButton* lbl = [self createModeLabel:_pluginParams.localization.photoLabel type:PluginModePhoto  action:@selector(switchToPhotoMode:)];
        self.photoButton = [self createModeButton:@"photo_mode.png" type:PluginModePhoto action:@selector(takePicture:)];
        self.photoButton.userInteractionEnabled = NO;
        [labels addObject:lbl];
        [buttons addObject:_photoButton];
        newMode = PluginModePhoto;
    }
    if (_pluginParams.videoAllowed)
    {
        UIButton* lbl = [self createModeLabel:_pluginParams.localization.videoLabel type:PluginModeVideo  action:@selector(switchToVideoMode:)];
        self.videoButton = [self createModeButton:@"video_mode.png" type:PluginModeVideo action:@selector(recordVideo:)];
        self.videoButton.userInteractionEnabled = NO;
        [labels addObject:lbl];
        [buttons addObject:_videoButton];
        if (newMode != PluginModePhoto) newMode = PluginModeVideo;
    }

    NSMutableArray *buttonsCenterConstraints = [NSMutableArray arrayWithCapacity:3];
    self.activeButtonsConstraints = buttonsCenterConstraints;

    size_t count = labels.count;
    for (unsigned int ii = 0; ii < count; ii++)
    {
        UIButton* lbl = labels[ii];
        [self.containerView addSubview:lbl];
        NSLayoutConstraint *topConstraint = [NSLayoutConstraint constraintWithItem:lbl
                                                                         attribute:NSLayoutAttributeTop
                                                                         relatedBy:NSLayoutRelationEqual
                                                                            toItem:self.bottomBar
                                                                         attribute:NSLayoutAttributeTop
                                                                        multiplier:1 constant:MODE_LABEL_TOP_OFFSET];
        [self.containerView addConstraint:topConstraint];

        UIButton* btn = buttons[ii];
        [self.containerView addSubview:btn];
        NSLayoutConstraint *bottomConstraint = [NSLayoutConstraint constraintWithItem:btn
                                         attribute:NSLayoutAttributeBottom
                                         relatedBy:NSLayoutRelationEqual
                                            toItem:self.bottomBar
                                         attribute:NSLayoutAttributeBottom
                                        multiplier:1 constant:-MODE_ICON_BOTTOM_OFFSET];
        [self.containerView addConstraint:bottomConstraint];

        NSLayoutConstraint *centerConstraint = [NSLayoutConstraint constraintWithItem:btn
                                                                            attribute:NSLayoutAttributeCenterX
                                                                            relatedBy:NSLayoutRelationEqual
                                                                               toItem:self.containerView
                                                                            attribute:NSLayoutAttributeCenterX
                                                                           multiplier:1 constant:0];
        [buttonsCenterConstraints addObject:centerConstraint];
        [self.containerView addConstraint:centerConstraint];

    }

    if ((count & 1) == 1)
    {
        // for odd number of buttons, fix the central button to the center
        unsigned int centerIdx = (unsigned int)count / 2;
        UIButton* center = labels[centerIdx];
        [center centerInView:self.bottomBar onAxis:NSLayoutAttributeCenterX];

        // put other buttons to the left and right of the central one
        if (count == 3)
        {
            UIButton* left = labels[0];
            UIButton* right = labels[2];
            NSLayoutConstraint *leftC = [NSLayoutConstraint constraintWithItem:left
                                                                     attribute:NSLayoutAttributeRight
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:center
                                                                     attribute:NSLayoutAttributeLeft
                                                                    multiplier:1
                                                                      constant:-MODE_LABEL_HOR_SPACING];
            NSLayoutConstraint *rightC = [NSLayoutConstraint constraintWithItem:right
                                                                     attribute:NSLayoutAttributeLeft
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:center
                                                                     attribute:NSLayoutAttributeRight
                                                                    multiplier:1
                                                                      constant:MODE_LABEL_HOR_SPACING];
            [self.containerView addConstraint:leftC];
            [self.containerView addConstraint:rightC];
        }
    }
    else
    {
        [self.containerView spaceViews:labels onAxis:UILayoutConstraintAxisHorizontal];
    }

    _currentMode = newMode;
}

- (void)updateModeButtonsAndLabels
{
    ////NSLog(@"updateModeButtonsAndLabels");
    // change the active label color
    const int count = (int)self.activeLabels.count;
    BOOL rightSide = NO;
    if (!_firstLayoutPass)
    {
        [self.containerView layoutIfNeeded];

        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.3];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    }


    for (unsigned int ii = 0; ii < count; ii++)
    {
        UIButton* lbl = self.activeLabels[ii];
        BOOL active = (lbl.tag == _currentMode);
        [lbl setTitleColor:active ? MODE_LABEL_TEXT_COLOR_ACTIVE : MODE_LABEL_TEXT_COLOR_INACTIVE forState:UIControlStateNormal];
        
        NSLayoutConstraint *centerConstraint = self.activeButtonsConstraints[ii];

        TransformableButton* btn = self.activeButtons[ii];
        if (btn != self.galleryButton) btn.userInteractionEnabled = active;

        if (active)
        {
            centerConstraint.constant = 0;
            rightSide = YES;
        }
        else
        {
            centerConstraint.constant = (rightSide ? INACTIVE_BUTTON_HOR_OFFSET : -INACTIVE_BUTTON_HOR_OFFSET);
        }

        btn.alpha = active ? 1.0f : 0;
        float scale = active ? 1.0f : 0.7f;
        btn.scale = scale;
    }
    if (!_firstLayoutPass) {
        [self.containerView layoutIfNeeded];
        [UIView commitAnimations];
    }
    _firstLayoutPass = NO;
}

- (void)handleOrientationChange:(NSNotification *)notify {
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];

    int newAngle = _currentScreenRotation;

    switch (orientation)
    {
        case UIDeviceOrientationPortrait:
            newAngle = 0;
            break;

        case UIDeviceOrientationPortraitUpsideDown:
            newAngle = 180;
            break;

        case UIDeviceOrientationLandscapeLeft:
            newAngle = 90;
            break;

        case UIDeviceOrientationLandscapeRight:
            newAngle = 270;
            break;
		
		default:
			break;
    }

    if (_currentMode == PluginModeGallery)
    {
        // fix controls orientation in gallery mode
        newAngle = 0;
    }

    if (newAngle == _currentScreenRotation) return;
    _currentScreenRotation = newAngle;
    [self rotateControlsToAngle:newAngle];
}


- (void)rotateControlsToAngle:(int)angle
{
    int absRot = abs(angle);
    ////NSLog(@"device angle = %d", angle);

    bool rotateModeLabels = false;
    float dx = 0;
    float dy = 0;
    if (absRot == 0 || absRot == 180)
    {
        // portrait mode - no need to offset progress bar position
        rotateModeLabels = true;
    }
    else
    {
        // landscape mode, push the bar to the side of the screen
        float barH = self.progressContainer.bounds.size.height;
        dx = (float) round(self.containerView.frame.size.width / 2 - barH - _progressContainerBottomOffset);
        if (angle > 180) dx = -dx;

        // barTop should point to the center of the visible preview frame
        float verticalSpace = self.containerView.frame.size.height - self.topBar.bounds.size.height
                - self.bottomBar.bounds.size.height;
        dy = (float) round(verticalSpace / 2  - barH);
        if ([LittleThings isBeforeiOS8])
        {
            dy = dy + self.bottomBar.bounds.size.height;
        }
        ////NSLog(@"verticalSpace = %f, barH = %f, containerOffset = %f", verticalSpace, barH, _progressContainerBottomOffset);
    }

    ////NSLog(@"progressBar offset = (%f, %f)", dx, dy);

    [self.containerView layoutIfNeeded];
    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^ {
        self.progressContainerCenterX.constant = dx;
        self.progressContainerBottomSpace.constant = _progressContainerBottomOffset + dy;

        [self.progressContainer setRotationDegrees:angle];

        [self.flashModeButton setRotationDegrees:angle];
        [self.switchCameraButton setRotationDegrees:angle];

        self.galleryButton.rotation = angle;
        self.photoButton.rotation = angle;
        self.videoButton.rotation = angle;

        [self.cancelButton setRotationDegrees:angle];
        [self.uploadButton setRotationDegrees:angle];

        if (rotateModeLabels)
        {
            for (UIButton * lbl in self.activeLabels)
            {
                [lbl setRotationDegrees:absRot];
            }
        }

        [self.thumbsBar rotateThumbsToAngle: angle];

        [self.containerView layoutIfNeeded];

    } completion:nil];

}

- (void)showProgressContainer:(BOOL)show
{
    float newAlpha = show ? 1 : 0;
    if (newAlpha == self.progressContainer.alpha) return;

    [UIView animateWithDuration:0.3 animations:^{
        _progressContainer.alpha = newAlpha;
    }];
}

- (void)doSwitchToMode:(int)newMode
{
    if (_currentMode == newMode) return;
    if (_currentMode != PluginModeGallery)
    {
        // delete temporary files for photo or video modes
        [self.pluginResult deleteCurrentItemFiles];
    }
    // remove all thums
    [self discardAllSelectedItems];

    _currentMode = newMode;

    switch (newMode)
    {
        case PluginModeGallery:
            [self.mediaLibrary showGallery:YES];
            [self showProgressContainer:NO];
            [self updateModeButtonsAndLabels];
            break;

        case PluginModePhoto:
            [self showProgressContainer:NO];
            [self updateModeButtonsAndLabels];
            [self.mediaLibrary showGallery:NO];
            [self showCameraAndFlashButtons:YES];
            [self.camera configureForPhotoMode];
            break;

        case PluginModeVideo:
            [self.mediaLibrary showGallery:NO];
            [self showProgressContainer:YES];
            [self updateModeButtonsAndLabels];
            [self updateRecordedVideoTime];
            [self showCameraAndFlashButtons:YES];
            [self.camera configureForVideoMode];
            break;

        default:
            break;
    }

    self.tapRecognizer.enabled = newMode != PluginModeGallery;

    [self updateUploadButtonVisibility];
}

- (void)askUserToDiscardItemsWithSuccessBlock:(void (^)(void))successBlock
{
    BOOL isVideo = (_currentMode == PluginModeVideo);
    // prevent mode switches while recording
    if (isVideo && self.camera.isRecording) return;

    if ((_currentMode == PluginModeGallery) || (self.pluginResult.selectedItems.count == 0))
    {
        successBlock();
        return;
    }

    NSString* alertMessage = isVideo ? self.pluginParams.localization.moveFromVideosText : self.pluginParams.localization.moveFromPhotosText;
    [UIAlertView showWithTitle:nil
                       message:alertMessage
             cancelButtonTitle:self.pluginParams.localization.alertCancelText
             otherButtonTitles:@[self.pluginParams.localization.alertConfirmText]
                      tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                          if (buttonIndex != [alertView cancelButtonIndex]) {
                              successBlock();
                          }
                      }];

}

- (void)switchModeOnSwipe:(int)modeDelta
{
    int newMode = _currentMode + modeDelta;
    if (newMode >= PluginModesCount) return;
    if (newMode < 0) return;
    [self askUserToDiscardItemsWithSuccessBlock:^{
        [self doSwitchToMode:newMode];
    }];
}


- (IBAction)switchToPhotoMode:(id)sender
{
    if (_currentMode == PluginModePhoto) return;
    [self askUserToDiscardItemsWithSuccessBlock:^{
        [self doSwitchToMode:PluginModePhoto];
    }];
}


- (IBAction)switchToVideoMode:(id)sender
{
    if (_currentMode == PluginModeVideo) return;
    [self askUserToDiscardItemsWithSuccessBlock:^{
        [self doSwitchToMode:PluginModeVideo];
    }];
}

- (IBAction)switchToGalleryMode:(id)sender
{
    if (_currentMode == PluginModeGallery) return;
    [self askUserToDiscardItemsWithSuccessBlock:^{
        [self doSwitchToMode:PluginModeGallery];
    }];
}


- (IBAction)switchCamera:(id)sender {

    self.photoButton.enabled = NO;
    self.videoButton.enabled = NO;

    [self.camera switchCamera];

}

- (void)onCameraPreviewReady
{
    self.photoButton.enabled = YES;
    self.videoButton.enabled = YES;
    self.galleryButton.enabled = YES;
    //NSLog(@"onCameraPreviewReady");
}

-(void)onCameraPreviewPaused
{
    self.photoButton.enabled = NO;
    self.videoButton.enabled = NO;
    self.galleryButton.enabled = NO;
}

- (void)updateSwitchCameraButton:(CameraFacingType)cameraFacing
{
    NSString* btnIcon = nil;
    switch (cameraFacing)
    {
        case CameraFacingBack:
            btnIcon = @"camera_rear.png";
            break;

        case CameraFacingFront:
            btnIcon = @"camera_front.png";
            break;
    }
    [self.switchCameraButton setImage:[UIImage imageNamed:btnIcon] forState:UIControlStateNormal];
}

- (void)updateFlashButtonState:(FlashStateType)curMode
{
    self.flashModeButton.hidden = (curMode == FlashStateDisabled);
    if (curMode == FlashStateDisabled) return;

    NSString* btnIcon = nil;
    switch (curMode)
    {
        case FlashStateOff:
            btnIcon = @"flash_off.png";
            break;
        case FlashStateOn:
            btnIcon = @"flash_on.png";
            break;
        case FlashStateAuto:
            btnIcon = @"flash_auto.png";
            break;

        default:
            break;
    }

    if (btnIcon != nil)
    {
        [self.flashModeButton setImage:[UIImage imageNamed:btnIcon] forState:UIControlStateNormal];
    }

}

- (IBAction)selectFlashMode:(id)sender {
    [self.camera switchFlashMode];

}

- (void)dismissPicker:(BOOL)cancelled result:(NSDictionary*)result
{
    // If we dismiss the view controller while the camera preview is still active,
    // iOS shows red flashing bar, indicating that the video/audio recording is in progress
    // So we have to shut down the video preview here, although it takes some time, causing a slight delay
    [self.camera stopPreview:^ {
        [self.presentingViewController dismissViewControllerAnimated:YES completion:^{
            self.pickerCompletionBlock(cancelled, result);
        }];
    }];
}


- (IBAction)cancelPicker:(id)sender {

    CoolShutterViewController __weak *weakSelf = self;
    [self askUserToDiscardItemsWithSuccessBlock:^{
        //NSLog(@"CoolShutter::cancelPicker > dismissing ViewController");
        [weakSelf dismissPicker:YES result:nil];
    }];
}

- (void)preparePickerResult
{
    CoolShutterViewController __weak *weakSelf = self;
    [self.pluginResult prepareResult:self.containerView
                          completion:^(BOOL cancelled, NSDictionary* result) {
                              [weakSelf dismissPicker:cancelled result:result];
                          }];
}

- (IBAction)donePicking:(id)sender
{

    if ([self.pluginResult needToTrimLibraryVideo])
    {
        NSString* alert = [NSString stringWithFormat:self.pluginParams.localization.maxVideoTimeCutWarning, self.pluginParams.maxVideoTime];
        [UIAlertView showWithTitle:nil
                           message:alert
                 cancelButtonTitle:self.pluginParams.localization.alertCancelText
                 otherButtonTitles:@[self.pluginParams.localization.alertConfirmText]
                          tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                              if (buttonIndex != [alertView cancelButtonIndex]) {
                                  [self preparePickerResult];
                              }
                          }];

    }
    else
    {
        [self preparePickerResult];
    }

}

- (BOOL)canAddMorePhotos
{
    if (self.pluginResult.selectedItems.count >= self.pluginParams.maxPhotos)
    {
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:nil
                                                     message:self.pluginParams.localization.maxPhotosText
                                                    delegate:nil
                                           cancelButtonTitle:self.pluginParams.localization.alertOkText
                                           otherButtonTitles:nil];
        [av show];
        return NO;
    }
    return YES;
}

- (IBAction)takePicture:(id)sender
{
    if (_photoInProgress) return;
    if ([self canAddMorePhotos]) {
        _photoInProgress = YES;
        [self.camera takePicture:_currentScreenRotation];
    }
}

- (void)animateThumbAddition:(UIImage* )capturedImage
            forThumbnailItem:(SelectedItemInfo*)sii
                    fromRect:(CGRect *)pSrcRect
{
    CGRect previewFrame;
    BOOL flipImage = NO;
    if (pSrcRect == NULL)
    {
        previewFrame = self.cameraPreviewView.bounds;
        float barHeights = self.topBar.bounds.size.height + self.bottomBar.bounds.size.height;
        previewFrame.origin.y += self.topBar.bounds.size.height;
        previewFrame.size.height -= barHeights;
        if ([self.camera currentCameraFacing] == CameraFacingFront)
        {
            flipImage = YES;
        }
    }
    else
    {
        previewFrame = *pSrcRect;
    }

    UIView* animContainer = [[UIView alloc] initWithFrame:previewFrame];
    UIImageView* tmpView = [[UIImageView alloc] initWithFrame:animContainer.bounds];
    tmpView.contentMode = UIViewContentModeScaleAspectFill;
    tmpView.clipsToBounds = YES;
    if (flipImage) tmpView.transform = CGAffineTransformMakeScale(-1, 1);
    tmpView.image = capturedImage;
    [tmpView setRotationDegrees:_currentScreenRotation];
    [animContainer addSubview:tmpView];
    animContainer.clipsToBounds = YES;
    tmpView.frame = animContainer.bounds;


    tmpView.alpha = 0.8;
    [self.containerView addSubview:animContainer];
    CGRect targetFrame = [_thumbsBar lastThumbRect];
    [UIView animateWithDuration:0.3 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^ {
        animContainer.frame = targetFrame;
        tmpView.frame = animContainer.bounds;
        tmpView.alpha = 1;
    } completion: ^(BOOL completed) {
        [animContainer removeFromSuperview];
        if (!sii.imgView.image) sii.imgView.image = [capturedImage imageByCroppingImageToSize:targetFrame.size];
        sii.imgView.alpha = 1;
    }];
}

- (void)onPictureTaken:(UIImage *)image savingToFile:(NSString*)fileName
{
    SelectedItemInfo * sii = [[SelectedItemInfo alloc] initFromLibrary:NO isVideo:NO];
    sii.itemFilePath = fileName;
    sii.nativeOrientation = [LittleThings rotationDegreesFromImageOrientationFlag:image.imageOrientation];
    int currentDeviceRotation = 0;//[self.camera photoRotationFromDeviceRotation:_currentScreenRotation];
    sii.deviceOrientation = currentDeviceRotation;
    [self.thumbsBar addThumbWithImage:nil sii:sii];

    if (currentDeviceRotation != _currentScreenRotation)
    {
        [sii.imgView setRotationDegrees: _currentScreenRotation - currentDeviceRotation];
    }
    [self animateThumbAddition:image forThumbnailItem:sii fromRect:NULL];
    [self updateUploadButtonVisibility];
    _photoInProgress = NO;
}

- (void)onPhotoSavedToPath:(NSString *)file downscaledImage:(UIImage*)updatedImage
{
    SelectedItemInfo * sii = [self.pluginResult findItemWithPath:file];
    if (sii) {
        // if saved image was downscaled, then it was also rotated to correct orientation,
        // thus we have to reset device rotation flag for it here (otherwise generated thumbnails
        // would be incorrectly rotated)
//        if (updatedImage)
//        {
//            sii.deviceOrientation = 0;
//            sii.nativeOrientation = 0;
//            sii.imgView.image = [updatedImage imageByCroppingImageToSize:sii.imgView.bounds.size];
//            [sii.imgView setRotationDegrees: _currentScreenRotation];
//        }
        sii.fileDataSaved = YES;
        [self.pluginResult onFileSavedStateChanged];
    }
}

- (void)addSelectedItemWithPath:(NSString *)assetURI thumbnailImage:(UIImage *)thumbnail animateFromRect:(CGRect)srcRect
                        isVideo:(BOOL)isVideo
                    assetObject:(ALAsset *)asset
{
    //NSLog(@"Media item thumbnail = %@", thumbnail);
    SelectedItemInfo * sii = [[SelectedItemInfo alloc] initFromLibrary:YES isVideo:isVideo];
    sii.itemFilePath = assetURI;
    sii.assetObject = asset;
    NSNumber* val = [asset valueForProperty:ALAssetPropertyOrientation];
    ALAssetOrientation orientation = (ALAssetOrientation) [val integerValue];
    sii.nativeOrientation = [LittleThings rotationDegreesFromAssetOrientationFlag:orientation];
    sii.fileDataSaved = YES;
    [self.thumbsBar addThumbWithImage:nil sii:sii];
    [self animateThumbAddition:thumbnail forThumbnailItem:sii fromRect:&srcRect];
    [self updateUploadButtonVisibility];
}

- (void)removeSelectedItemWithPath:(NSString *)assetURI
{
    SelectedItemInfo * sii = [self.pluginResult findItemWithPath:assetURI];
    [self.pluginResult.selectedItems removeObject:sii];
    [self.thumbsBar deleteThumb:sii];
    [self updateUploadButtonVisibility];
}

- (void)discardAllSelectedItems
{
    [self.thumbsBar deleteAllThumbs];
    [self.pluginResult.selectedItems removeAllObjects];
    [self updateUploadButtonVisibility];
}

- (void)showCameraAndFlashButtons:(BOOL)show
{
    self.flashModeButton.userInteractionEnabled = show;
    self.switchCameraButton.userInteractionEnabled = show;

    [UIView animateWithDuration:0.3 animations:^ {
        self.switchCameraButton.alpha = show ? 1.0f : 0.0f;
        self.flashModeButton.alpha = show ? 1.0f : 0.0f;
    }];
}

- (void)updateUploadButtonVisibility
{
    BOOL visible = NO;
    if (self.pluginResult.selectedItems.count > 0) visible = YES;
    if ((_currentMode == PluginModeVideo) && self.camera.isRecording) visible = NO;

    //NSLog(@"updateUploadButtonVisibility, visible = %d", visible);

    self.uploadButton.userInteractionEnabled = visible;
    float newAlpha = visible ? 1.0f : 0.0f;
    if (self.uploadButton.alpha != newAlpha)
    {
        [UIView animateWithDuration:0.3 animations:^ {
            self.uploadButton.alpha = newAlpha;
        }];
    }

    if (_currentMode == PluginModeVideo)
    {
        BOOL isRecording = self.camera.isRecording;
        self.cancelButton.userInteractionEnabled = !isRecording;
        self.cancelButton.alpha = isRecording ? 0 : 1;
    }
}


- (void)onVideoRecordingStateChanged:(BOOL)recording
{
    UIImage* btnImg = [UIImage imageNamed:recording ? @"video_pause.png" : @"video_mode"];
    [self.videoButton setImage:btnImg forState:UIControlStateNormal];

    // disable switch camera & flash mode buttons as soon, as the recording of the first video part has started
    if (recording && self.flashModeButton.alpha != 0)
    {
        [self showCameraAndFlashButtons:NO];
    }

    if (recording)
    {
        _screenRotationSavedOnRecordingStart = _currentScreenRotation;
        self.recordingIndicatorTimer = [NSTimer scheduledTimerWithTimeInterval:RECORDING_INDICATOR_TIMER_RES target:self selector:@selector(updateRecordedVideoTime) userInfo:nil repeats:YES];
        _recordingIndicatorBlinkCounter = 0;
    }
    else
    {
        if (self.recordingIndicatorTimer != nil) {
            [self.recordingIndicatorTimer invalidate];
            self.recordingIndicatorTimer = nil;
        }

        self.recordingIndicator.alpha = 0;
    }

    [self updateUploadButtonVisibility];
}

- (void)onVideoPartRecorded
{
    if (self.pluginResult.videoParts.partsCount == 1)
    {
        // generate video thumbnail
        SelectedItemInfo * sii = [[SelectedItemInfo alloc] initFromLibrary:NO isVideo:YES];
        sii.itemFilePath = [FileUtils getTempFilePathWithExtension:@"mp4"];
        sii.deviceOrientation = [self.camera photoRotationFromDeviceRotation:_screenRotationSavedOnRecordingStart];
        ////NSLog(@"onVideoPartRecorded, savedScreenRotation = %d, deviceOrientation = %d", _screenRotationSavedOnRecordingStart, sii.deviceOrientation);
        [self.thumbsBar addThumbWithImage:nil sii:sii];

        ////NSLog(@"_currentScreenRotation = %d", _currentScreenRotation);
        if (sii.deviceOrientation != _currentScreenRotation)
        {
            [sii.imgView setRotationDegrees: _currentScreenRotation - sii.deviceOrientation];
        }

        NSString* partPath = [self.pluginResult.videoParts getPartPath:0];
        //NSLog(@"Load thumbnail for %@", partPath);
        UIImage* image = [VideoUtils loadVideoThumbnail:partPath];
        [self animateThumbAddition:image forThumbnailItem:sii fromRect:NULL];
    }
}

- (void)onVideoMaximumDurationReached
{
    [ToastView showToastInParentView:self.containerView
                            withText:self.pluginParams.localization.maxVideoText
                       withDuaration:3];
    [self updateRecordedVideoTime];
}


- (void)updateRecordedVideoTime
{
    float seconds = [self.camera getRemainingVideoTime];
    self.remainingTimeLabel.text = [LittleThings formatTime:(int)seconds];

    if (_recordingIndicatorBlinkCounter++ >= RECORDING_INDICATOR_BLINK_COUNT)
    {
        _recordingIndicatorBlinkCounter = 0;

        float alpha = (self.recordingIndicator.alpha < 1) ? 1 : 0;
        self.recordingIndicator.alpha = alpha;
    }

    float maxTime = (float)_pluginParams.maxVideoTime;
    float progress = 0;

    if (maxTime > 0)
    {
        progress = (maxTime - seconds) / maxTime;
    }
    self.progressView.progress = progress;

    if (![self.camera isRecording]) [self.recordingIndicatorTimer invalidate];
}

- (IBAction)recordVideo:(id)sender
{
    if ([self.camera isRecording]) {
        [self.recordingIndicatorTimer invalidate];
        self.recordingIndicatorTimer = nil;
    }

    [self.camera toggleVideoRecording];
}

- (void)runStillImageCaptureAnimation
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.cameraPreviewView.layer setOpacity:0.0];
        [UIView animateWithDuration:.25 animations:^{
            [self.cameraPreviewView.layer setOpacity:1.0];
        }];
    });
}

- (void)onThumbnailTap:(SelectedItemInfo *)sii
{
    BOOL isVideo = (_currentMode == PluginModeVideo);
    BOOL isPhoto = (_currentMode == PluginModePhoto);
    BOOL isGallery = (_currentMode == PluginModeGallery);

    if (isVideo || isPhoto)
    {
        NSString* alertMessage = isVideo ? self.pluginParams.localization.deleteVideoText : self.pluginParams.localization.deletePhotoText;
        [UIAlertView showWithTitle:nil
                           message:alertMessage
                 cancelButtonTitle:self.pluginParams.localization.alertCancelText
                 otherButtonTitles:@[self.pluginParams.localization.alertConfirmText]
                          tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                              if (buttonIndex == [alertView cancelButtonIndex]) {
                                  ////NSLog(@"Cancelled");
                              }
                              else
                              {
                                  if (isVideo) [self onDiscardRecordedVideo:sii];
                                  else [self deletePhoto:sii];
                              }
                          }];
    }
    else
    if (isGallery)
    {
        [self.pluginResult.selectedItems removeObject:sii];
        [self.mediaLibrary deselectItemWithURI:sii.itemFilePath];
        [self.thumbsBar deleteThumb:sii];
        [self updateUploadButtonVisibility];
    }
}

- (void)deletePhoto:(SelectedItemInfo *)info
{
    [self.pluginResult deleteItemAndFile:info];
    [self.thumbsBar deleteThumb:info];
    [self updateUploadButtonVisibility];
}

- (void)onDiscardRecordedVideo:(SelectedItemInfo *)info
{
    [self showCameraAndFlashButtons:YES];

    [self.pluginResult deleteItemAndFile:info];
    [self.thumbsBar deleteThumb:info];
    [self.pluginResult discardRecordedVideo];
    [self updateUploadButtonVisibility];
    [self updateRecordedVideoTime];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
