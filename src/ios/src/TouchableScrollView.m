//
// Created by Constantin on 27/04/15.
// Copyright (c) 2015 coolshutter. All rights reserved.
//

#import "TouchableScrollView.h"


@implementation TouchableScrollView
{

}

// fix for the UIScrollView to pass down touch event to subviews which are not subclasses of UIControl
- (BOOL)touchesShouldCancelInContentView:(UIView *)view
{
    return NO;
}


@end