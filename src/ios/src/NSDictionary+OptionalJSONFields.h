//
// Created by Constantin on 09/04/15.
// Copyright (c) 2015 coolshutter. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (OptionalJSONFields)

- (NSUInteger)optUIntValue:(NSString*)key defValue:(NSUInteger)val;

- (BOOL)optBoolValue:(NSString *)key defValue:(BOOL)value;

- (NSString *)optStringValue:(NSString *)key defValue:(NSString*)value;

@end