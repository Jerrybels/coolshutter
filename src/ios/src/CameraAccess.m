//
// Created by Constantin on 25/04/15.
// Copyright (c) 2015 coolshutter. All rights reserved.
//

#import "CameraAccess.h"
#import "FileUtils.h"
#import "VideoPreviewView.h"
#import "PluginResult.h"
#import "CameraPluginParams.h"
#import "VideoRecordingState.h"
#import "UIImage+ImageUtils.h"
#import "LittleThings.h"
#import "VideoUtils.h"

#import <AVFoundation/AVFoundation.h>


// funky way to introduce unique values
static void * CapturingStillImageContext = &CapturingStillImageContext;
static void * RecordingContext = &RecordingContext;
static void * SessionRunningAndDeviceAuthorizedContext = &SessionRunningAndDeviceAuthorizedContext;

@interface CameraAccess () <AVCaptureFileOutputRecordingDelegate> {
    BOOL _currentModeIsVideo;
}

@property AVCaptureSession* avCaptureSession;
// Communicate with the session and other session objects on this queue.
@property (nonatomic) dispatch_queue_t sessionQueue;
@property (nonatomic) AVCaptureDevice *avCaptureDevice;
@property (nonatomic) AVCaptureDeviceInput *videoDeviceInput;
@property (nonatomic) AVCaptureMovieFileOutput *movieFileOutput;
@property (nonatomic) AVCaptureStillImageOutput *stillImageOutput;

@property (nonatomic, getter = isDeviceAuthorized) BOOL deviceAuthorized;
@property (nonatomic) UIBackgroundTaskIdentifier backgroundRecordingID;
@property (nonatomic, readonly, getter = isSessionRunningAndDeviceAuthorized) BOOL sessionRunningAndDeviceAuthorized;
@property (nonatomic) id runtimeErrorHandlingObserver;

@property(nonatomic, weak) VideoPreviewView *cameraPreviewView;

@end

@implementation CameraAccess

- (void)setupCameraPreviewAsync:(VideoPreviewView*)cameraPreviewView
           withVideoOrientation:(UIInterfaceOrientation)orientation
       andInitialModeSetToVideo:(BOOL)initVideoMode
{
    _currentModeIsVideo = initVideoMode;

    self.cameraPreviewView = cameraPreviewView;
    // Create the AVCaptureSession
    AVCaptureSession *captureSession = [[AVCaptureSession alloc] init];
    self.avCaptureSession = captureSession;

    // Setup the preview view
    [cameraPreviewView setSession:captureSession];

    // Check for device authorization
    [self checkDeviceAuthorizationStatus];

    // In general it is not safe to mutate an AVCaptureSession or any of its inputs, outputs, or connections
    // from multiple threads at the same time.
    // Why not do all of this on the main queue?
    // -[AVCaptureSession startRunning] is a blocking call which can take a long time. We dispatch
    // captureSession setup to the sessionQueue so that the main queue isn't blocked (which keeps the UI responsive).

    self.sessionQueue = dispatch_queue_create("captureSession queue", DISPATCH_QUEUE_SERIAL);

    __weak CameraAccess* weakSelf = self;
    dispatch_async(_sessionQueue, ^{
        [weakSelf setBackgroundRecordingID:UIBackgroundTaskInvalid];

        NSError *error = nil;

        weakSelf.avCaptureDevice = [CameraAccess deviceWithMediaType:AVMediaTypeVideo preferringPosition:AVCaptureDevicePositionBack];
        AVCaptureDeviceInput *videoDeviceInput = [AVCaptureDeviceInput deviceInputWithDevice:weakSelf.avCaptureDevice error:&error];

        if (error)
        {
            NSLog(@"%@", error);
        }

        if ([captureSession canAddInput:videoDeviceInput])
        {
            [captureSession addInput:videoDeviceInput];
            [weakSelf setVideoDeviceInput:videoDeviceInput];

            dispatch_async(dispatch_get_main_queue(), ^{
                // Why are we dispatching this to the main queue?
                // Because AVCaptureVideoPreviewLayer is the backing layer for VideoPreviewView and UIView can
                // only be manipulated on main thread.
                // Note: As an exception to the above rule, it is not necessary to serialize video orientation
                // changes on the AVCaptureVideoPreviewLayer’s connection with other captureSession manipulation.

                [[(AVCaptureVideoPreviewLayer *)[cameraPreviewView layer] connection] setVideoOrientation:(AVCaptureVideoOrientation)orientation];

                [weakSelf updateFlashButtonState];
            });
        }

        AVCaptureDevice *audioDevice = [[AVCaptureDevice devicesWithMediaType:AVMediaTypeAudio] firstObject];
        AVCaptureDeviceInput *audioDeviceInput = [AVCaptureDeviceInput deviceInputWithDevice:audioDevice error:&error];

        if (error)
        {
            NSLog(@"%@", error);
        }

        if ([captureSession canAddInput:audioDeviceInput])
        {
            [captureSession addInput:audioDeviceInput];
        }

        AVCaptureMovieFileOutput *movieFileOutput = [[AVCaptureMovieFileOutput alloc] init];
        if ([captureSession canAddOutput:movieFileOutput])
        {
            [captureSession addOutput:movieFileOutput];
            AVCaptureConnection *connection = [movieFileOutput connectionWithMediaType:AVMediaTypeVideo];
            if ([connection isVideoStabilizationSupported])
            {
                if (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_7_1)
                {
                    [connection setPreferredVideoStabilizationMode:AVCaptureVideoStabilizationModeStandard];
                }
                else
                {
                    [connection setEnablesVideoStabilizationWhenAvailable:YES];
                }

            }
            [weakSelf setMovieFileOutput:movieFileOutput];
        }

        AVCaptureStillImageOutput *stillImageOutput = [[AVCaptureStillImageOutput alloc] init];
        if ([captureSession canAddOutput:stillImageOutput])
        {
            float jpegQuality = (float)weakSelf.pluginResult.pluginParams.quality / 100.0;
            [stillImageOutput setOutputSettings:@{
                    AVVideoCodecKey : AVVideoCodecJPEG,
                    AVVideoQualityKey: @(jpegQuality)
            }];
            [captureSession addOutput:stillImageOutput];
            [weakSelf setStillImageOutput:stillImageOutput];
        }
    });

}

- (void)checkDeviceAuthorizationStatus
{
    NSString *mediaType = AVMediaTypeVideo;

    __weak CameraAccess *weakSelf = self;
    if (NSFoundationVersionNumber >= NSFoundationVersionNumber_iOS_7_0) {
        [AVCaptureDevice requestAccessForMediaType:mediaType completionHandler:^(BOOL granted) {
            if (granted) {
                //Granted access to mediaType
                [weakSelf setDeviceAuthorized:YES];
            }
        }];
    }
    else {
        [self setDeviceAuthorized:YES];
    }
}


- (void)startPreview
{
    __weak CameraAccess *weakSelf = self;
    dispatch_async([self sessionQueue], ^{
        [weakSelf addObserver:weakSelf forKeyPath:@"sessionRunningAndDeviceAuthorized" options:(NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew) context:SessionRunningAndDeviceAuthorizedContext];
        [weakSelf addObserver:weakSelf forKeyPath:@"stillImageOutput.capturingStillImage" options:(NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew) context:CapturingStillImageContext];
        [weakSelf addObserver:weakSelf forKeyPath:@"movieFileOutput.recording" options:(NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew) context:RecordingContext];
        [[NSNotificationCenter defaultCenter] addObserver:weakSelf selector:@selector(subjectAreaDidChange:) name:AVCaptureDeviceSubjectAreaDidChangeNotification object:[[weakSelf videoDeviceInput] device]];

        weakSelf.runtimeErrorHandlingObserver = [[NSNotificationCenter defaultCenter] addObserverForName:AVCaptureSessionRuntimeErrorNotification
                                              object:weakSelf.avCaptureSession
                                               queue:nil
                                          usingBlock:^(NSNotification *note)
                                          {
                                              //NSLog(@"Camera runtime error handling observer, note = %@", note);
                                              dispatch_async(weakSelf.sessionQueue, ^{
                                                  // Manually restarting the session since it must have been stopped due to an error.
                                                  [weakSelf.avCaptureSession startRunning];
                                              });
                                          }];
        [weakSelf.avCaptureSession startRunning];
        [weakSelf configureForCurrentMode];
        [CameraAccess setFlashMode:AVCaptureFlashModeAuto forDevice:weakSelf.avCaptureDevice];

        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf updateFlashButtonState];
            [weakSelf updateSwitchCameraButton];
            [weakSelf.delegate onCameraPreviewReady];
        });
    });
}

- (void)stopPreview:(void (^)(void))completion {
    __weak CameraAccess * weakSelf = self;
    dispatch_async([self sessionQueue], ^{
        //NSLog(@"Stopping AVCaptureSession");
        [weakSelf.avCaptureSession stopRunning];

        [[NSNotificationCenter defaultCenter] removeObserver:weakSelf name:AVCaptureDeviceSubjectAreaDidChangeNotification object:weakSelf.videoDeviceInput.device];
        [[NSNotificationCenter defaultCenter] removeObserver:[weakSelf runtimeErrorHandlingObserver]];

        [weakSelf removeObserver:weakSelf forKeyPath:@"sessionRunningAndDeviceAuthorized" context:SessionRunningAndDeviceAuthorizedContext];
        [weakSelf removeObserver:weakSelf forKeyPath:@"stillImageOutput.capturingStillImage" context:CapturingStillImageContext];
        [weakSelf removeObserver:weakSelf forKeyPath:@"movieFileOutput.recording" context:RecordingContext];
        //NSLog(@"AVCaptureSession stopped");
        dispatch_async(dispatch_get_main_queue(), completion);
    });
}


- (void)configureForCurrentMode
{
    if (!self.avCaptureSession.isRunning) {
        //NSLog(@"CameraAccess - trying to configure while session is not running!");
        return;
    }
    if (_currentModeIsVideo)
    {
        NSLog(@"video -> configureForCurrentMode");
        CameraPluginParams *params = self.pluginResult.pluginParams;
        int maxW = (int)params.maxVideoWidth;
        int maxH = (int)params.maxVideoHeight;
        int minW = (int)params.minVideoWidth;
        int minH = (int)params.minVideoHeight;
        NSString* videoPreset = [VideoUtils findBestVideoCapturePresetMinWidth:minW maxWidth:maxW
                                                                     minHeight:minH maxHeight:maxH];
        self.avCaptureSession.sessionPreset = videoPreset;
        NSLog(@"Video capture preset = %@", videoPreset);
    }
    else
    {
        self.avCaptureSession.sessionPreset = AVCaptureSessionPresetPhoto;
    }
}

- (void)configureForPhotoMode
{
    _currentModeIsVideo = NO;
    __weak CameraAccess * weakSelf = self;
    dispatch_async([self sessionQueue], ^{
        //NSLog(@"Camera::configureForPhotoMode, isRunning = %d", self.avCaptureSession.isRunning);
        [weakSelf configureForCurrentMode];
    });
}

- (void)configureForVideoMode
{
    _currentModeIsVideo = YES;

    __weak CameraAccess * weakSelf = self;
    dispatch_async([self sessionQueue], ^{
        //NSLog(@"Camera::configureForVideoMode, isRunning = %d", self.avCaptureSession.isRunning);
        [weakSelf configureForCurrentMode];
    });
}

- (void)switchCamera {
    __weak CameraAccess * weakSelf = self;
    dispatch_async([self sessionQueue], ^{
        AVCaptureDevice *currentVideoDevice = weakSelf.videoDeviceInput.device;
        AVCaptureDevicePosition preferredPosition = AVCaptureDevicePositionUnspecified;
        AVCaptureDevicePosition currentPosition = [currentVideoDevice position];

        switch (currentPosition) {
            case AVCaptureDevicePositionUnspecified:
                preferredPosition = AVCaptureDevicePositionBack;
                break;
            case AVCaptureDevicePositionBack:
                preferredPosition = AVCaptureDevicePositionFront;
                break;
            case AVCaptureDevicePositionFront:
                preferredPosition = AVCaptureDevicePositionBack;
                break;
        }

        AVCaptureDevice *videoDevice = [CameraAccess deviceWithMediaType:AVMediaTypeVideo preferringPosition:preferredPosition];
        AVCaptureDeviceInput *videoDeviceInput = [AVCaptureDeviceInput deviceInputWithDevice:videoDevice error:nil];

        [weakSelf.avCaptureSession beginConfiguration];

        [weakSelf.avCaptureSession removeInput:weakSelf.videoDeviceInput];
        if ([weakSelf.avCaptureSession canAddInput:videoDeviceInput]) {
            [[NSNotificationCenter defaultCenter] removeObserver:weakSelf name:AVCaptureDeviceSubjectAreaDidChangeNotification object:currentVideoDevice];

            //[CameraAccess setFlashMode:AVCaptureFlashModeAuto forDevice:videoDevice];
            [[NSNotificationCenter defaultCenter] addObserver:weakSelf selector:@selector(subjectAreaDidChange:) name:AVCaptureDeviceSubjectAreaDidChangeNotification object:videoDevice];

            [weakSelf.avCaptureSession addInput:videoDeviceInput];
            [weakSelf setVideoDeviceInput:videoDeviceInput];
        }
        else {
            [weakSelf.avCaptureSession addInput:[weakSelf videoDeviceInput]];
        }

        [weakSelf configureForCurrentMode];

        [weakSelf.avCaptureSession commitConfiguration];

        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf updateFlashButtonState];
            [weakSelf updateSwitchCameraButton];
            [weakSelf.delegate onCameraPreviewReady];
        });
    });

}

- (void)subjectAreaDidChange:(NSNotification *)notification
{
    CGPoint devicePoint = CGPointMake(.5, .5);
    [self focusWithMode:AVCaptureFocusModeContinuousAutoFocus exposeWithMode:AVCaptureExposureModeContinuousAutoExposure atDevicePoint:devicePoint monitorSubjectAreaChange:NO];
}

- (CameraFacingType)currentCameraFacing
{
    AVCaptureDevice *currentVideoDevice = [[self videoDeviceInput] device];
    AVCaptureDevicePosition currentPosition = [currentVideoDevice position];
    CameraFacingType cur = CameraFacingBack;

    switch (currentPosition) {
        case AVCaptureDevicePositionUnspecified:
        case AVCaptureDevicePositionBack:
            cur = CameraFacingBack;
            break;
        case AVCaptureDevicePositionFront:
            cur = CameraFacingFront;
            break;
    }
    return cur;
}

- (void)updateSwitchCameraButton {

    CameraFacingType cur = [self currentCameraFacing];
    [self.delegate updateSwitchCameraButton:cur];

}

- (void)updateFlashButtonState {
    AVCaptureDevice *currentVideoDevice = self.videoDeviceInput.device;
    FlashStateType flashMode = FlashStateDisabled;
    if (currentVideoDevice.flashAvailable)
    {
        AVCaptureFlashMode curMode = currentVideoDevice.flashMode;
        switch (curMode)
        {
            case AVCaptureFlashModeOff: flashMode = FlashStateOff; break;
            case AVCaptureFlashModeOn: flashMode = FlashStateOn; break;
            case AVCaptureFlashModeAuto: flashMode = FlashStateAuto; break;
        }
    }


    [self.delegate updateFlashButtonState:flashMode];
}

- (void)switchFlashMode {
    NSError* err;
    AVCaptureDevice *currentVideoDevice = self.videoDeviceInput.device;
    if (!currentVideoDevice.flashAvailable) return;

    AVCaptureFlashMode curMode = currentVideoDevice.flashMode;
    int modes[] = { AVCaptureFlashModeOff, AVCaptureFlashModeOn, AVCaptureFlashModeAuto };
    int modesCount = sizeof(modes) / sizeof(modes[0]);
    int curIdx = -1;
    for (int ii = 0; ii < modesCount; ii++)
    {
        if (modes[ii] == curMode)
        {
            curIdx = ii;
            break;
        }
    }

    if (curIdx < 0) return;
    curIdx++;
    if (curIdx >= modesCount) curIdx = 0;

    [currentVideoDevice lockForConfiguration:&err];
    if (err != nil) return;
    currentVideoDevice.flashMode = (AVCaptureFlashMode) modes[curIdx];
    [currentVideoDevice unlockForConfiguration];

    [self updateFlashButtonState];
}

- (int)captureVideoOrientationFromRotation:(int)screenRotation
{
    switch (screenRotation)
    {
        case 0:
            return AVCaptureVideoOrientationPortrait;
        case 90:
            return AVCaptureVideoOrientationLandscapeRight;
        case 180:
            return AVCaptureVideoOrientationPortraitUpsideDown;
        case 270:
            return AVCaptureVideoOrientationLandscapeLeft;
    }
    return AVCaptureVideoOrientationPortrait;
}

- (void)takePicture:(int)currentScreenRotation {
    CameraAccess __weak *weakSelf = self;
    dispatch_async([self sessionQueue], ^{
        // Update the orientation on the still image output video connection before capturing.
        //[[self.stillImageOutput connectionWithMediaType:AVMediaTypeVideo] setVideoOrientation:[[(AVCaptureVideoPreviewLayer *)[self.cameraPreviewView layer] connection] videoOrientation]];
        [[weakSelf.stillImageOutput connectionWithMediaType:AVMediaTypeVideo] setVideoOrientation:[weakSelf captureVideoOrientationFromRotation:currentScreenRotation]];

        // Capture a still image.
        [weakSelf.stillImageOutput captureStillImageAsynchronouslyFromConnection:[weakSelf.stillImageOutput connectionWithMediaType:AVMediaTypeVideo]
                       completionHandler:^(CMSampleBufferRef imageDataSampleBuffer, NSError *error)
                       {
                           if (imageDataSampleBuffer)
                           {
                               NSData *imageData = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageDataSampleBuffer];
                               UIImage *image = [[UIImage alloc] initWithData:imageData];
                               NSString * tmpFile = [FileUtils getTempFilePathWithExtension:@"jpg"];
                               dispatch_async(dispatch_get_main_queue(), ^ {
                                   [weakSelf.delegate onPictureTaken:image savingToFile:tmpFile];
                               });

                               UIImage* updatedImage = nil;
                               NSData* resizedData = [weakSelf downscaleImage:image
                                                                 originalData:imageData
                                                                 updatedImage:&updatedImage
                                                               screenRotation:0];

                               NSError *err = nil;
                               [resizedData writeToFile:tmpFile options:0 error:&err];
                               if (err != nil)
                               {
                                   NSLog(@"Error saving photo to temp folder %@", err);
                               }
                               dispatch_async(dispatch_get_main_queue(), ^ {
                                   [weakSelf.delegate onPhotoSavedToPath:tmpFile downscaledImage:updatedImage];
                               });

                           }
                       }];
    });

}

- (NSData *)downscaleImage:(UIImage *)image originalData:(NSData*)originalData
              updatedImage:(UIImage**)outImage screenRotation:(int)currentScreenRotation
{
    CameraPluginParams *params = self.pluginResult.pluginParams;
    int maxW = (int)params.maxPhotoWidth;
    int maxH = (int)params.maxPhotoHeight;
    int minW = (int)params.minPhotoWidth;
    int minH = (int)params.minPhotoHeight;

    //return originalData;
    if (maxW <= 0) return originalData;

    int rotationDegrees = currentScreenRotation;
    //NSLog(@"downscaleImage, rotationDegress = %d", rotationDegrees);
    if (rotationDegrees % 180 != 0)
    {
        // swap width and height for rotated images
        int tmp = maxW;
        maxW = maxH;
        maxH = tmp;

        tmp = minW;
        minW = minH;
        minH = tmp;
    }

    // we don't want to upscale the image
    if ((image.size.width <= minW) || (image.size.height <= minH)) return originalData;

    UIImage* resized = [image imageByScalingWithSizeConstraintsMax:CGSizeMake(maxW, maxH) andMin:CGSizeMake(minW, minH)];
    if (rotationDegrees != 0) resized = [resized imageRotatedByDegrees: -rotationDegrees];
    NSData* data = UIImageJPEGRepresentation(resized, (CGFloat)((double)params.quality/100.0));
    *outImage = resized;
    return data;
}

- (void)toggleVideoRecording
{
    __weak CameraAccess* weakSelf = self;
    dispatch_async(self.sessionQueue, ^{
        if (![weakSelf.movieFileOutput isRecording])
        {
            if ([UIDevice.currentDevice isMultitaskingSupported])
            {
                // Setup background task. This is needed because the captureOutput:didFinishRecordingToOutputFileAtURL: callback
                // is not received until the app returns to the foreground unless you request background execution time.
                // This also ensures that there will be time to write the file to the assets library when app is backgrounded.
                // To conclude this background execution, -endBackgroundTask is called in
                // -recorder:recordingDidFinishToOutputFileURL:error: after the recorded file has been saved.
                [weakSelf setBackgroundRecordingID:[[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:nil]];
            }

            // Update the orientation on the movie file output video connection before starting recording.
            [[weakSelf.movieFileOutput connectionWithMediaType:AVMediaTypeVideo] setVideoOrientation:[[(AVCaptureVideoPreviewLayer *)[weakSelf.cameraPreviewView layer] connection] videoOrientation]];
            int64_t timeRemaining = (int64_t) (1000*[weakSelf.pluginResult getVideoTimeRemaining]);
            if (timeRemaining <= 0) timeRemaining = 1;
            weakSelf.movieFileOutput.maxRecordedDuration = CMTimeMake(timeRemaining, 1000);

            // Turning OFF flash for video recording
            [CameraAccess setFlashMode:AVCaptureFlashModeOff forDevice:weakSelf.videoDeviceInput.device];

            // Start recording to a temporary file.
            NSString *outputFilePath = [FileUtils getTempFilePathWithExtension:@"mp4"];
            [weakSelf.movieFileOutput startRecordingToOutputFileURL:[NSURL fileURLWithPath:outputFilePath] recordingDelegate:weakSelf];
        }
        else
        {
            [weakSelf.movieFileOutput stopRecording];
        }
    });
}

- (BOOL)isRecording
{
    return [self.movieFileOutput isRecording];
}


- (BOOL)isSessionRunningAndDeviceAuthorized
{
    return [self.avCaptureSession isRunning] && [self isDeviceAuthorized];
}

- (void)focusWithMode:(AVCaptureFocusMode)focusMode exposeWithMode:(AVCaptureExposureMode)exposureMode atDevicePoint:(CGPoint)point monitorSubjectAreaChange:(BOOL)monitorSubjectAreaChange
{
    __weak CameraAccess* weakSelf = self;
    dispatch_async([self sessionQueue], ^{
        AVCaptureDevice *device = [[weakSelf videoDeviceInput] device];
        NSError *error = nil;
        if ([device lockForConfiguration:&error])
        {
            if ([device isFocusPointOfInterestSupported] && [device isFocusModeSupported:focusMode])
            {
                [device setFocusMode:focusMode];
                [device setFocusPointOfInterest:point];
            }
            if ([device isExposurePointOfInterestSupported] && [device isExposureModeSupported:exposureMode])
            {
                [device setExposureMode:exposureMode];
                [device setExposurePointOfInterest:point];
            }
            [device setSubjectAreaChangeMonitoringEnabled:monitorSubjectAreaChange];
            [device unlockForConfiguration];
        }
        else
        {
            //NSLog(@"%@", error);
        }
    });
}

+ (void)setFlashMode:(AVCaptureFlashMode)flashMode forDevice:(AVCaptureDevice *)device
{
    if ([device hasFlash] && [device isFlashModeSupported:flashMode])
    {
        NSError *error = nil;
        if ([device lockForConfiguration:&error])
        {
            [device setFlashMode:flashMode];
            [device unlockForConfiguration];
        }
        else
        {
            //NSLog(@"%@", error);
        }
    }
}

+ (AVCaptureDevice *)deviceWithMediaType:(NSString *)mediaType preferringPosition:(AVCaptureDevicePosition)position
{
    NSArray *devices = [AVCaptureDevice devicesWithMediaType:mediaType];
    AVCaptureDevice *captureDevice = [devices firstObject];

    for (AVCaptureDevice *device in devices)
    {
        if ([device position] == position)
        {
            captureDevice = device;
            break;
        }
    }

    return captureDevice;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if (context == CapturingStillImageContext)
    {
        BOOL isCapturingStillImage = [change[NSKeyValueChangeNewKey] boolValue];

        if (isCapturingStillImage)
        {
            [self.delegate runStillImageCaptureAnimation];
        }
    }
    else if (context == RecordingContext)
    {
        BOOL isRecording = [change[NSKeyValueChangeNewKey] boolValue];
        //NSLog(@"Recording value changed: %d", isRecording);

        __weak CameraAccess* weakSelf = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            [weakSelf.delegate onVideoRecordingStateChanged:isRecording];
        });
    }
    else if (context == SessionRunningAndDeviceAuthorizedContext)
    {
        BOOL isRunning = [change[NSKeyValueChangeNewKey] boolValue];

        __weak CameraAccess* weakSelf = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            if (isRunning)
            {
                [weakSelf.delegate onCameraPreviewReady];
            }
            else
            {
                [weakSelf.delegate onCameraPreviewPaused];
            }
        });
    }
    else
    {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}


#pragma mark -
#pragma mark FILE OUTPUT DELEGATE

- (void)captureOutput:(AVCaptureFileOutput *)captureOutput didStartRecordingToOutputFileAtURL:(NSURL *)fileURL fromConnections:(NSArray *)connections
{
    [self.pluginResult beginVideoRecording:fileURL.path];
}

- (void)captureOutput:(AVCaptureFileOutput *)captureOutput didFinishRecordingToOutputFileAtURL:(NSURL *)outputFileURL
      fromConnections:(NSArray *)connections error:(NSError *)error
{
    float dur = (float) CMTimeGetSeconds(captureOutput.recordedDuration);
    //NSLog(@"Recorded part duration = %f", dur);
    [self.pluginResult.videoParts addVideoPart:outputFileURL.path withDuration:dur];

    if (error) {
        //NSLog(@"%@", error);
        if (error.code == AVErrorMaximumDurationReached) {
            [self.delegate onVideoMaximumDurationReached];
            [self.pluginResult.videoParts setRemainingTime:0];
        }
    }

    [self.delegate onVideoPartRecorded];


    // Note the backgroundRecordingID for use in the ALAssetsLibrary completion handler to end the background task associated with this recording. This allows a new recording to be started, associated with a new UIBackgroundTaskIdentifier, once the movie file output's -isRecording is back to NO — which happens sometime after this method returns.
    //UIBackgroundTaskIdentifier backgroundRecordingID = [self backgroundRecordingID];
    //[self setBackgroundRecordingID:UIBackgroundTaskInvalid];
    [[UIApplication sharedApplication] endBackgroundTask:self.backgroundRecordingID];

//    [[[ALAssetsLibrary alloc] init] writeVideoAtPathToSavedPhotosAlbum:outputFileURL completionBlock:^(NSURL *assetURL, NSError *error) {
//        if (error)
//            //NSLog(@"%@", error);
//
//        [[NSFileManager defaultManager] removeItemAtURL:outputFileURL error:nil];
//
//        if (backgroundRecordingID != UIBackgroundTaskInvalid)
//            [[UIApplication sharedApplication] endBackgroundTask:backgroundRecordingID];
//    }];
}


// in seconds
- (float)getRemainingVideoTime
{
    float seconds = [self.pluginResult getVideoTimeRemaining];
    if (![self isRecording]) return seconds;

    CMTime cur = self.movieFileOutput.recordedDuration;
    float recorded = (float) CMTimeGetSeconds(cur);
    return seconds - recorded;
}

- (int)photoRotationFromDeviceRotation:(int)deviceRotationDegrees
{
    CameraFacingType cur = [self currentCameraFacing];
    return (cur == CameraFacingBack) ? deviceRotationDegrees : -deviceRotationDegrees;
}

- (void)doFocusAndExposureAtPoint:(CGPoint)point
{
    AVCaptureDevice *device = self.videoDeviceInput.device;
    BOOL exposureSupported = (device.isExposurePointOfInterestSupported && [device isExposureModeSupported:AVCaptureExposureModeContinuousAutoExposure]);
    BOOL focusSupported = (device.isFocusPointOfInterestSupported && [device isFocusModeSupported:AVCaptureFocusModeAutoFocus]);
    if (focusSupported)
    {
        NSError *error;
        if ([device lockForConfiguration:&error]) {
            device.focusPointOfInterest = point;
            device.focusMode = AVCaptureFocusModeAutoFocus;
            if (exposureSupported)
            {
                device.exposurePointOfInterest = point;
                device.exposureMode = AVCaptureExposureModeContinuousAutoExposure;
            }
            [device unlockForConfiguration];
            [self.delegate showFocusingAnimation];
        } else {
            //NSLog(@"Cannot lock device: %@", error);
        }
    }
}


- (CGPoint) convertToPointOfInterestFrom:(CGRect)frame coordinates:(CGPoint)viewCoordinates
{
    CGPoint pointOfInterest = (CGPoint) {0.5f, 0.5f};
    CGSize frameSize = frame.size;

    AVCaptureVideoPreviewLayer *videoPreviewLayer = (AVCaptureVideoPreviewLayer *) [self.cameraPreviewView layer];

    if ([[videoPreviewLayer videoGravity] isEqualToString:AVLayerVideoGravityResize])
        pointOfInterest = (CGPoint) {viewCoordinates.y / frameSize.height, 1.0f - (viewCoordinates.x / frameSize.width)};
    else {
        CGRect cleanAperture;
        for (AVCaptureInputPort *port in self.videoDeviceInput.ports) {
            if ([port mediaType] == AVMediaTypeVideo) {
                cleanAperture = CMVideoFormatDescriptionGetCleanAperture([port formatDescription], YES);
                CGSize apertureSize = cleanAperture.size;
                CGPoint point = viewCoordinates;

                CGFloat apertureRatio = apertureSize.height / apertureSize.width;
                CGFloat viewRatio = frameSize.width / frameSize.height;
                CGFloat xc = 0.5f;
                CGFloat yc = 0.5f;

                if ([[videoPreviewLayer videoGravity] isEqualToString:AVLayerVideoGravityResizeAspect]) {
                    if (viewRatio > apertureRatio) {
                        CGFloat y2 = frameSize.height;
                        CGFloat x2 = frameSize.height * apertureRatio;
                        CGFloat x1 = frameSize.width;
                        CGFloat blackBar = (x1 - x2) / 2;
                        if (point.x >= blackBar && point.x <= blackBar + x2) {
                            xc = point.y / y2;
                            yc = 1.0f - ((point.x - blackBar) / x2);
                        }
                    } else {
                        CGFloat y2 = frameSize.width / apertureRatio;
                        CGFloat y1 = frameSize.height;
                        CGFloat x2 = frameSize.width;
                        CGFloat blackBar = (y1 - y2) / 2;
                        if (point.y >= blackBar && point.y <= blackBar + y2) {
                            xc = ((point.y - blackBar) / y2);
                            yc = 1.0f - (point.x / x2);
                        }
                    }
                } else if ([[videoPreviewLayer videoGravity] isEqualToString:AVLayerVideoGravityResizeAspectFill]) {
                    if (viewRatio > apertureRatio) {
                        CGFloat y2 = apertureSize.width * (frameSize.width / apertureSize.height);
                        xc = (point.y + ((y2 - frameSize.height) / 2.0f)) / y2;
                        yc = (frameSize.width - point.x) / frameSize.width;
                    } else {
                        CGFloat x2 = apertureSize.height * (frameSize.height / apertureSize.width);
                        yc = 1.0f - ((point.x + ((x2 - frameSize.width) / 2)) / x2);
                        xc = point.y / frameSize.height;
                    }
                }

                pointOfInterest = (CGPoint) {xc, yc};
                break;
            }
        }
    }

    return pointOfInterest;
}



@end