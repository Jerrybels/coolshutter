//
// Created by Constantin on 03/05/15.
// Copyright (c) 2015 coolshutter. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Cordova/CDVPlugin.h"



@interface CoolShutterPlugin : CDVPlugin

- (void)pickItems:(CDVInvokedUrlCommand*)command;
- (void)cleanup:(CDVInvokedUrlCommand*)command;

@end