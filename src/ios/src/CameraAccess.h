//
// Created by Constantin on 25/04/15.
// Copyright (c) 2015 coolshutter. All rights reserved.
//

#import <UIKit/UIKit.h>

@class VideoPreviewView;
@class CameraPluginParams;
@class PluginResult;

typedef NS_ENUM(NSInteger, FlashStateType) {
    FlashStateDisabled,
    FlashStateOff,
    FlashStateOn,
    FlashStateAuto
};

typedef NS_ENUM(NSInteger, CameraFacingType) {
    CameraFacingBack,
    CameraFacingFront
};

@protocol CameraAccessDelegate

- (void)onCameraPreviewReady;
- (void)onCameraPreviewPaused;
- (void)updateFlashButtonState:(FlashStateType)curMode;
- (void)updateSwitchCameraButton:(CameraFacingType)cameraFacing;
- (void)runStillImageCaptureAnimation;
- (void)onPictureTaken:(UIImage *)image savingToFile:(NSString*)fileName;
- (void)onVideoRecordingStateChanged:(BOOL)recording;
- (void)onVideoMaximumDurationReached;
- (void)onVideoPartRecorded;
- (void)onPhotoSavedToPath:(NSString *)file downscaledImage:(UIImage*)updatedImage;
- (void)showFocusingAnimation;

@end

@interface CameraAccess : NSObject

@property (weak, nonatomic) id<CameraAccessDelegate> delegate;
@property (weak, nonatomic) PluginResult* pluginResult;

- (void)setupCameraPreviewAsync:(VideoPreviewView*)cameraPreviewView
           withVideoOrientation:(UIInterfaceOrientation)orientation
       andInitialModeSetToVideo:(BOOL)initVideoMode;

- (void)startPreview;
- (void)stopPreview:(void (^)(void))completion;
- (void)configureForVideoMode;
- (void)configureForPhotoMode;
- (void)switchCamera;
- (CameraFacingType)currentCameraFacing;
- (void)switchFlashMode;
- (void)takePicture:(int)currentScreenRotation;
- (void)toggleVideoRecording;
- (BOOL)isRecording;
- (float)getRemainingVideoTime;
- (int)photoRotationFromDeviceRotation:(int)deviceRotationDegrees;
- (void)doFocusAndExposureAtPoint:(CGPoint)point;
- (CGPoint) convertToPointOfInterestFrom:(CGRect)frame coordinates:(CGPoint)viewCoordinates;

@end