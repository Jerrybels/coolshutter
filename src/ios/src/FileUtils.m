//
// Created by Constantin on 13/04/15.
// Copyright (c) 2015 coolshutter. All rights reserved.
//

#import "FileUtils.h"

#define PLUGIN_TEMP_FOLDER_NAME @"coolshutter"

@implementation FileUtils {

}

static char alphabet[]  = "ABCDEFGHIJKLMNOPQRSTUVWXZY0123456789";
static NSString* generateRandomString(int len)
{
	const int maxCharIndex = sizeof(alphabet)/sizeof(alphabet[0]) - 1;
	char str[129];
	if (len > 128) len = 128;
	memset(str, 0, sizeof(alphabet[0])*(len + 1));
	for (int ii = 0; ii < len; ii++)
	{
		str[ii] = alphabet[arc4random_uniform(maxCharIndex)];
	}
	return [NSString stringWithCString:str encoding:NSASCIIStringEncoding];
}

+ (NSString*)getTempDirPath
{
    NSString* dir = [NSTemporaryDirectory() stringByAppendingPathComponent:PLUGIN_TEMP_FOLDER_NAME];
    NSFileManager *mgr = [NSFileManager defaultManager];
    if (![mgr fileExistsAtPath:dir])
    {
        [mgr createDirectoryAtPath:dir withIntermediateDirectories:YES attributes:nil error:nil];
    }
    return dir;
}

+ (NSString *)getTempFilePathWithExtension:(NSString *)extension
{
    NSString *name = [generateRandomString(10) stringByAppendingPathExtension:extension];
    return [[FileUtils getTempDirPath] stringByAppendingPathComponent:name];
}

+ (void)deleteAllTemporaryFiles
{
    NSString* temp = [FileUtils getTempDirPath];
    NSFileManager *mgr = [NSFileManager defaultManager];
    NSError* err = nil;
    NSArray* files = [mgr contentsOfDirectoryAtPath:temp error:&err];
    if (err)
    {
        NSLog(@"Error getting directory contents: %@", err);
    }

    if (files)
    {
        for (NSString* file in files)
        {
            NSString* path = [temp stringByAppendingPathComponent:file];
            [mgr removeItemAtPath:path error:&err];
            if (err)
            {
                NSLog(@"Error removing temporary file: %@", err);
            }
        }
    }
}

@end