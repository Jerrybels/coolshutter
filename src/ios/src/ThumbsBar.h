//
// Created by Constantin on 12/04/15.
// Copyright (c) 2015 coolshutter. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SelectedItemInfo;
@class PluginResult;

@protocol ThumbsBarDelegate

@required
- (void)onThumbnailTap:(SelectedItemInfo *)sii;

@end


@interface ThumbsBar : NSObject <UIScrollViewDelegate>

@property (weak) PluginResult* pluginResult;
@property (weak) id<ThumbsBarDelegate> delegate;


- (id)initWithScroller:(UIScrollView *)scrollView leftEdge:(UIView *)leftEdge rightEdge:(UIView *)rightEdge fadeColor:(UIColor*)bkColor;

- (void)addThumbWithImage:(UIImage *)image sii:(SelectedItemInfo*)item;

- (CGRect)lastThumbRect;

- (void)rotateThumbsToAngle:(int)angle;

- (void)deleteThumb:(SelectedItemInfo *)info;

- (void)deleteAllThumbs;
@end