//
// Created by Constantin on 26/04/15.
// Copyright (c) 2015 coolshutter. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AssetsLibrary/ALAsset.h>

@interface LittleThings : NSObject

+ (NSString*)formatTime:(int)seconds;

+ (int)rotationDegreesFromImageOrientationFlag:(UIImageOrientation)orientation;
+ (UIImageOrientation)imageOrientationFlagFromRotationDegrees:(int)degrees;

+ (BOOL)moveFileFrom:(NSString*)srcName to:(NSString *)dstName;

+ (int)rotationDegreesFromAssetOrientationFlag:(ALAssetOrientation)orientation;

+ (BOOL)isBeforeiOS8;
@end