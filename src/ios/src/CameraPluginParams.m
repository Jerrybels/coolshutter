//
// Created by Constantin on 7/04/15.
// Copyright (c) 2015 coolshutter. All rights reserved.
//

#import "CameraPluginParams.h"
#import "NSDictionary+OptionalJSONFields.h"
#import "CameraPluginLocalization.h"

@interface CameraPluginParams ()

@property CameraAllowedSources allowedSources;

@end


@implementation CameraPluginParams {

}

- (id)init {
    self = [super init];

    self.quality = 50;
    self.maxPhotos = 5;
    self.maxVideoTime = 10;
    self.saveToGallery = NO;
    self.allowedSources = FLAG_ALLOW_ALL;
    self.generateThumbnailsRequested = true;
    self.thumbnailsAsFiles = true;
    self.maxThumbnailWidth = 200;
    self.maxThumbnailHeight = 200;
    self.minThumbnailWidth = 180;
    self.minThumbnailHeight = 180;

    self.maxPhotoWidth = 1400;
    self.minPhotoWidth = 1000;
    self.maxPhotoHeight = 1000;
    self.minPhotoHeight = 600;

    self.maxVideoWidth = 1400;
    self.minVideoWidth = 1000;
    self.maxVideoHeight = 1000;
    self.minVideoHeight = 600;

    self.downscalePhotosFromGallery = YES;
    self.downscaleVideosFromGallery = YES;


    self.localization = [[CameraPluginLocalization alloc] init];

    return self;
}


- (id)initWithJSON:(NSData *)json {
    self = [super init];
    NSError *err;
    NSDictionary * params = [NSJSONSerialization JSONObjectWithData:json options:0 error:&err];
    if (params != nil)
    {
        self.quality = [params optUIntValue:@"quality" defValue:50];
        self.maxPhotos = [params optUIntValue:@"maxPhotos" defValue: 5];
        self.maxVideoTime = [params optUIntValue:@"maxVideoTime" defValue:30];
        self.saveToGallery = [params optBoolValue:@"saveToGallery" defValue:false];
        self.allowedSources = (CameraAllowedSources)[params optUIntValue:@"allowedSourceFlags" defValue:FLAG_ALLOW_ALL];
        self.generateThumbnailsRequested = [params optBoolValue:@"generateThumbnails" defValue:false];
        self.thumbnailsAsFiles = [params optBoolValue:@"thumbnailsAsFiles" defValue:true];

        self.maxThumbnailWidth = [params optUIntValue:@"maxThumbnailWidth" defValue:200];
        self.maxThumbnailHeight = [params optUIntValue:@"maxThumbnailHeight" defValue:200];

        self.minThumbnailWidth = [params optUIntValue:@"minThumbnailWidth" defValue:150];
        self.minThumbnailHeight = [params optUIntValue:@"minThumbnailHeight" defValue:150];

        self.maxPhotoWidth = [params optUIntValue:@"maxPhotoWidth" defValue:1400];
        self.minPhotoWidth = [params optUIntValue:@"minPhotoWidth" defValue:1000];
        self.maxPhotoHeight = [params optUIntValue:@"maxPhotoHeight" defValue:1000];
        self.minPhotoHeight = [params optUIntValue:@"minPhotoHeight" defValue:600];

        self.maxVideoWidth = [params optUIntValue:@"maxVideoWidth" defValue:720];
        self.minVideoWidth = [params optUIntValue:@"minVideoWidth" defValue:500];
        self.maxVideoHeight = [params optUIntValue:@"maxVideoHeight" defValue:600];
        self.minVideoHeight = [params optUIntValue:@"minVideoHeight" defValue:300];

        self.downscalePhotosFromGallery = [params optBoolValue:@"downscalePhotosFromGallery" defValue:YES];
        self.downscaleVideosFromGallery = [params optBoolValue:@"downscaleVideosFromGallery" defValue:YES];

        NSDictionary * locJSON = [params valueForKey:@"localization"];
        self.localization = [[CameraPluginLocalization alloc] initWithJSON:locJSON];
    }
    else
    {
        self.localization = [[CameraPluginLocalization alloc] init];
    }
    return self;
}



- (BOOL)videoAllowed {
    return self.allowedSources & FLAG_ALLOW_VIDEO;
}

- (BOOL)photoAllowed {
    return self.allowedSources & FLAG_ALLOW_PHOTO;
}

- (BOOL)photoGalleryAllowed {
    return self.allowedSources & FLAG_ALLOW_GALLERY_PHOTO;
}

- (BOOL)someGallerySourceAllowed {
    return self.videoGalleryAllowed || self.photoGalleryAllowed;
}

- (BOOL)videoGalleryAllowed {
    return self.allowedSources & FLAG_ALLOW_GALLERY_VIDEO;
}

@end