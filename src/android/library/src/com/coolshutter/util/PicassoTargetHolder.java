package com.coolshutter.util;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Const on 4/8/2015.
 * Project: CoolShutter
 */
abstract public class PicassoTargetHolder implements Target
{
	abstract public void onImageReady(Bitmap bitmap);

	// Picasso uses WeakReference to hold Target objects, so we have to hold them strongly here
	static private final Set<Target> _refHolder = new HashSet<Target>();
	static private Activity _loadCompletionTarget;
	static private Runnable _loadCompletionRunnable;

	public PicassoTargetHolder()
	{
		synchronized (_refHolder)
		{
			_refHolder.add(this);
		}
	}

	public static int getThumbnailQueueSize()
	{
		synchronized (_refHolder)
		{
			return _refHolder.size();
		}
	}

	@Override
	public void onBitmapFailed(Drawable errorDrawable)
	{
		onLoadFinished();
	}

	@Override
	public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from)
	{
		onLoadFinished();
		onImageReady(bitmap);
	}

	@Override
	public void onPrepareLoad(Drawable placeHolderDrawable)
	{

	}

	private void onLoadFinished()
	{
		synchronized (_refHolder)
		{
			_refHolder.remove(this);
			if (_loadCompletionRunnable != null && getThumbnailQueueSize() == 0)
			{
				signalLoadComplete();
			}

		}
	}

	private static void signalLoadComplete()
	{
		_loadCompletionTarget.runOnUiThread(_loadCompletionRunnable);
	}

	public static void setLoadCompletionRunnable(Activity targetActivity, Runnable onDone)
	{
		_loadCompletionRunnable = onDone;
		_loadCompletionTarget = targetActivity;
	}
}
