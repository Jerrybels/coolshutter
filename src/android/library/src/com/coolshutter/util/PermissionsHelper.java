package com.coolshutter.util;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;


/**
 * Created by Const on 1/17/2016.
 * Project: dottyar
 */
public class PermissionsHelper
{
    // Permission Request code should be in range of [0..0xff]
    public static final int PERMISSIONS_REQUEST_CODE = 0xDA;
    public interface PermissionRequestResultListener
    {
        void onPermissionResult(boolean success);
    }
    private static PermissionRequestResultListener _callback;

    static public boolean canReadExternalStorage(Context ctx)
    {
        return hasPermission(ctx, Manifest.permission.READ_EXTERNAL_STORAGE);
    }

    static public boolean canAccessCamera(Context ctx)
    {
        return hasPermission(ctx, Manifest.permission.CAMERA);
    }

    static private boolean hasPermission(Context ctx, String permission)
    {
        int permissionCheck = ContextCompat.checkSelfPermission(ctx, permission);
        return permissionCheck == PackageManager.PERMISSION_GRANTED;
    }

    public static void requestReadExternalStorage(Activity activity, PermissionRequestResultListener cb,
                                                  String explanationMessage)
    {
        requestPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE, cb, explanationMessage);
    }

    public static void requestWriteExternalStorage(Activity activity, PermissionRequestResultListener cb,
                                                  String explanationMessage)
    {
        requestPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE, cb, explanationMessage);
    }



    public static void requestCamera(Activity activity, PermissionRequestResultListener cb,
                                     String explanationMessage)
    {
        requestPermission(activity, Manifest.permission.CAMERA, cb, explanationMessage);
    }

    public static void requestRecordAudio(Activity activity, PermissionRequestResultListener cb,
                                        String explanationMessage)
    {
        requestPermission(activity, Manifest.permission.RECORD_AUDIO, cb, explanationMessage);
    }


    static private void requestPermission(final Activity activity, final String permission,
                                          final PermissionRequestResultListener cb, String explanationMessage)
    {
        if (hasPermission(activity, permission))
        {
            cb.onPermissionResult(true);
            return;
        }

        // Should we show an explanation?
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission))
        {
            // Show an expanation to the user *asynchronously* -- don't block
            // this thread waiting for the user's response! After the user
            // sees the explanation, try again to request the permission.
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder.setMessage(explanationMessage)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialog, int which)
                        {
                            requestPermissionImmediately(activity, permission, cb);
                        }
                    });
            builder.show();
        }
        else
        {
            requestPermissionImmediately(activity, permission, cb);
        }

    }

    static private void requestPermissionImmediately(Activity activity, String permission, PermissionRequestResultListener cb)
    {
        _callback = cb;
        ActivityCompat.requestPermissions(activity, new String[]{permission}, PERMISSIONS_REQUEST_CODE);
    }

    static public boolean handlePermissionsResult(int requestCode, String permissions[], int[] grantResults)
    {
        if (requestCode != PERMISSIONS_REQUEST_CODE) return false;

        // If request is cancelled, the result arrays are empty.
        boolean granted = (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED);
        if (_callback != null) _callback.onPermissionResult(granted);

        return true;
    }


}
