package com.coolshutter.util;

import android.animation.ObjectAnimator;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;

import com.coolshutter.main.CameraAccess;

/**
 * Created by Const on 4/15/2015.
 * Project: CoolShutter
 */
public class FocusAreaView extends View
{
	private Paint _paint;
	private int _focusWidth;
	private int _focusHeight;
	private RectF _rc;
	private int _currentState = CameraAccess.FOCUS_AREA_HIDE;
	private int _color = 0;
	private int COLOR_STARTED;
	private int COLOR_FAILED;
	private int COLOR_SUCCEEDED;

	@SuppressWarnings("SuspiciousNameCombination")
	private void initialize()
	{
		float strokeWidth;
		if (isInEditMode())
		{
			strokeWidth = 2;
			_focusWidth = _focusHeight = 96;

			COLOR_STARTED = Color.YELLOW;
			COLOR_FAILED = Color.RED;
			COLOR_SUCCEEDED = Color.GREEN;
		}
		else
		{
			ResourceLookup res = new ResourceLookup(this.getContext());
			_focusWidth = res.getDimensionPixelSize("shutter_focus_area_width");
			_focusHeight = res.getDimensionPixelSize("shutter_focus_area_height");
			strokeWidth = res.getDimensionPixelSize("shutter_focus_area_stroke_width");

			COLOR_STARTED = res.getColor("focus_rect_started");
			COLOR_FAILED = res.getColor("focus_rect_failed");
			COLOR_SUCCEEDED = res.getColor("focus_rect_succeeded");


		}
		_paint = new Paint();
		_paint.setStrokeWidth(strokeWidth);
		_paint.setStyle(Paint.Style.STROKE);
		_paint.setColor(COLOR_STARTED);

		_rc = new RectF(strokeWidth, strokeWidth, _focusWidth - 2* strokeWidth, _focusHeight - 2* strokeWidth);
	}

	public FocusAreaView(Context context)
	{
		super(context);
		initialize();
	}

	public FocusAreaView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		initialize();
	}

	public FocusAreaView(Context context, AttributeSet attrs, int defStyleAttr)
	{
		super(context, attrs, defStyleAttr);
		initialize();
	}


	@TargetApi(Build.VERSION_CODES.LOLLIPOP)
	public FocusAreaView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes)
	{
		super(context, attrs, defStyleAttr, defStyleRes);
		initialize();
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
	{
		setMeasuredDimension(_focusWidth, _focusHeight);
	}

	@Override
	protected void onDraw(Canvas canvas)
	{
		float szw = (_rc.right - _rc.left) / 4;
		float szh = (_rc.bottom - _rc.top) / 4;
		canvas.drawLine(_rc.left, _rc.top, _rc.left + szw, _rc.top, _paint);
		canvas.drawLine(_rc.left, _rc.top, _rc.left, _rc.top + szh, _paint);

		canvas.drawLine(_rc.left, _rc.bottom, _rc.left + szw, _rc.bottom, _paint);
		canvas.drawLine(_rc.left, _rc.bottom, _rc.left, _rc.bottom - szh, _paint);

		canvas.drawLine(_rc.right, _rc.top, _rc.right - szw, _rc.top, _paint);
		canvas.drawLine(_rc.right, _rc.top, _rc.right, _rc.top + szh, _paint);

		canvas.drawLine(_rc.right, _rc.bottom, _rc.right, _rc.bottom - szh, _paint);
		canvas.drawLine(_rc.right, _rc.bottom, _rc.right - szw, _rc.bottom, _paint);
	}

	public void setCenter(int cx, int cy)
	{
		int left = cx - getWidth() / 2;
		int top = cy - getHeight() / 2;
		setTranslationX(left);
		setTranslationY(top);
	}

	public int getColor()
	{
		return _color;
	}

	public void setColor(int color)
	{
		_color = color;
		_paint.setColor(color);
		invalidate();
	}

	public void setFocusState(int state)
	{
		if (state == _currentState) return;

		if (state == CameraAccess.FOCUS_AREA_HIDE_NO_ANIM)
		{
			_currentState = state;
			setAlpha(0);
			return;
		}

		float alpha = 1;
		int color = _color;
		int duration = 200;
		switch (state)
		{
			case CameraAccess.FOCUS_AREA_HIDE:
				alpha = 0;
				duration = 600;
				break;

			case CameraAccess.FOCUS_AREA_STARTED:
				color = COLOR_STARTED;
				setColor(color);
				break;

			case CameraAccess.FOCUS_AREA_SUCCESS:
				color = COLOR_SUCCEEDED;
				break;

			case CameraAccess.FOCUS_AREA_FAIL:
				color = COLOR_FAILED;
				break;
		}
		//Log.d("FocusRect", "setFocusState " + state+" alpha = "+alpha+" color = "+String.format("%Xd", color));

		if (alpha != getAlpha())
		{
			animate().alpha(alpha).setDuration(duration).start();
		}

		if (_color != color)
		{
			ObjectAnimator clrAnim = ObjectAnimator.ofInt(this, "color", _color, color);
			clrAnim.setEvaluator(ArgbEvaluatorCompat.getInstance());
			clrAnim.setDuration(duration);
			clrAnim.start();
		}

		_currentState = state;
	}
}
