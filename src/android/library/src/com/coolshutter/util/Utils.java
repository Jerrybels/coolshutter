/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.coolshutter.util;

import android.content.Context;
import android.os.Build;
import android.os.Build.VERSION_CODES;
import android.os.Environment;
import android.view.Surface;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Class containing some static utility methods.
 */
public class Utils {
    private Utils() {};

    public static boolean hasJellyBean() {
        return Build.VERSION.SDK_INT >= VERSION_CODES.JELLY_BEAN;
    }

    public static boolean hasKitKat() {
        return Build.VERSION.SDK_INT >= VERSION_CODES.KITKAT;
    }

    public static String getTempDirectoryPath(Context ctx)
    {
        File cache;

        cache = ctx.getExternalFilesDir("media");
//        // SD Card Mounted
//        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED))
//        {
//            cache = new File(Environment.getExternalStorageDirectory().getAbsolutePath() +
//                    "/Android/data/" + ctx.getPackageName() + "/cache/coolshutter/");
//        }
//        // Use internal storage
//        else
//        {
//            cache = ctx.getCacheDir();
//        }

        // Create the cache directory if it doesn't exist
        //noinspection ResultOfMethodCallIgnored
        cache.mkdirs();
        return cache.getAbsolutePath();
    }

    public static String getTempFileName(Context ctx, String extension)
    {
        return getTempDirectoryPath(ctx) + "/"+System.currentTimeMillis() + extension;
    }

    public static String getCameraImagesFolder()
    {
        File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM);
        if (path.exists())
        {
            File test1 = new File(path, "Camera");
            if (test1.exists())
            {
                path = test1;
            }
            else
            {
                File test2 = new File(path, "100ANDRO");
                if (test2.exists())
                {
                    path = test2;
                }
                else
                {
                    File test3 = new File(path, "100MEDIA");
                    if (!test3.exists())
                    {
                        test3.mkdirs();
                    }
                    path = test3;
                }
            }
        }
        else
        {
            path = new File(path, "Camera");
            path.mkdirs();
        }
        return path.getAbsolutePath();
    }

    static public void copyFile(File src, File dst) throws IOException
    {
        InputStream in = new FileInputStream(src);
        copyStreamToFile(in, dst);
        in.close();
    }
    /////////////////////////////////////////////////////////////////////////////////
    static public void copyStreamToFile(InputStream in, File dst) throws IOException
    {
        OutputStream out = new FileOutputStream(dst);
        // Transfer bytes from in to out
        byte[] buf = new byte[128*1024];
        int len;
        while ((len = in.read(buf)) > 0)
        {
            out.write(buf, 0, len);
        }
        out.close();
    }

    public static boolean moveFile(File src, File dst)
    {
        // Try to just rename it first. If the file stays on the same mount point,
        // that will be the fastest way to move
        if (src.renameTo(dst)) return true;

        try
        {
            copyFile(src, dst);
            return true;
        }
        catch (IOException e)
        {
            e.printStackTrace();
            return false;
        }
    }

    public static String formatTimeCounter(long t)
    {
        long sec = t / 1000;
        long mm = sec / 60;
        long hh = mm / 60;
        if (hh > 0)
        {
            return String.format("%d:%02d:%02d", hh, mm % 60, sec % 60);
        }
        else
        {
            return String.format("%d:%02d", mm % 60, sec % 60);
        }
    }


    public static int degreesFromSurfaceRotation(int surfaceRotationFlag)
    {
        switch (surfaceRotationFlag)
        {
            case Surface.ROTATION_0:
                return 0;

            case Surface.ROTATION_90:
                return 90;

            case Surface.ROTATION_180:
                return 180;

            case Surface.ROTATION_270:
                return 270;

            default:
                return 0;
        }
    }
}
