package com.coolshutter.util;

import android.content.Context;
import android.view.GestureDetector;
import android.view.MotionEvent;

/**
 * Created by Const on 4/5/2015.
 * Project: CoolShutter
 */
public class SwipeGestureRecognizer
{
	private final GestureDetector gestureDetector;
	private final int _swipeDistanceThreshold;
	private final int _swipeVelocityThreshold;

	public SwipeGestureRecognizer(Context context)
	{
		gestureDetector = new GestureDetector(context, new GestureListener());
		float density = context.getResources().getDisplayMetrics().density;
		_swipeDistanceThreshold = (int) (30*density);
		_swipeVelocityThreshold = (int)(50*density);
	}

	public void onTapGesture(float x, float y)
	{

	}

	public void onSwipeLeft()
	{
	}

	public void onSwipeRight()
	{
	}

	public boolean onTouch(MotionEvent event)
	{
		return gestureDetector.onTouchEvent(event);
	}

	private final class GestureListener extends GestureDetector.SimpleOnGestureListener
	{


		@Override
		public boolean onDown(MotionEvent e)
		{
			return true;
		}

		@Override
		public boolean onSingleTapConfirmed(MotionEvent e)
		{
			onTapGesture(e.getX(), e.getY());
			return true;
		}

		@Override
		public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY)
		{
			// sometimes, while fiddling with the status bar hiding/showing by pulling  from top in
			// a full screen mode, one of the received events was null
			if (e2 == null || e1 == null) return false;

			float distanceX = e2.getX() - e1.getX();
			float distanceY = e2.getY() - e1.getY();
			if (Math.abs(distanceX) > Math.abs(distanceY) &&
					Math.abs(distanceX) > _swipeDistanceThreshold &&
					Math.abs(velocityX) > _swipeVelocityThreshold)
			{
				if (distanceX > 0)
				{
					onSwipeRight();
				}
				else
				{
					onSwipeLeft();
				}
				return true;
			}
			return false;
		}
	}
}
