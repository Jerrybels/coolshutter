package com.coolshutter.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.CamcorderProfile;
import android.media.MediaCodecInfo;
import android.media.MediaFormat;
import android.os.Build;
import android.util.Log;

import com.coolshutter.main.PluginConfig;

import net.ypresto.androidtranscoder.MediaTranscoder;
import net.ypresto.androidtranscoder.format.MediaFormatStrategy;
import net.ypresto.androidtranscoder.format.OutputFormatUnavailableException;

import java.io.IOException;
import java.util.concurrent.Future;

/**
 * Created by Const on 12/17/2016.
 * Project: android
 */
public class MediaResizeUtils
{
    private final static boolean DEBUG = PluginConfig.DEBUG;
    private final static String TAG = MediaResizeUtils.class.getSimpleName();

    public interface VideoTranscoderListener
    {
        void onTranscodingCompleted(String outPath);
        void onTranscodingProgress(double progress);
        void onTranscodingFailed();
    }

    public static boolean isVideoLargerThanRequested(String videoPath, int maxW, int maxH)
    {
        // check if resize is enabled
        if (maxW <= 0) return false;

        Bitmap bmp = VideoFileHelper.createVideoThumbnail(videoPath);
        if (bmp == null)
        {
            Log.e(TAG, "Can't get video frame: "+videoPath);
            return false;
        }

        int w = bmp.getWidth();
        int h = bmp.getHeight();
        int srcW = Math.max(w, h);
        int srcH = Math.min(w, h);
        return srcW > maxW || srcH > maxH;
    }

    public static boolean isPhotoLargerThanRequested(String photoPath, int maxW, int maxH)
    {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(photoPath, options);

        if (options.outWidth == 0 || options.outHeight == 0) return false;

        int w = Math.max(options.outWidth, options.outHeight);
        int h = Math.min(options.outWidth, options.outHeight);
        return w > maxW || h > maxH;
    }

    public static boolean downscalePhoto(String outPath, String srcPath, int orientation,
                                        int maxW, int maxH, int minW, int minH,
                                        int JPEGQuality)
    {
        ImageProcessorPipeline.Pipeline pipe = ImageProcessorPipeline.loadFrom(srcPath, false, orientation);

        pipe.addGenerateThumbnailJob(0xD0D0, null, true,
                outPath,
                maxW, maxH,
                minW, minH,
                JPEGQuality);

        return pipe.runSync();
    }

    public static boolean downscaleVideo(final String dstPath, String srcPath,
                                         int maxW, int maxH, int minW, int minH,
                                         final VideoTranscoderListener cb)
    {
        // MediaTranscoder is only supported on API >= 18
        if (Build.VERSION.SDK_INT < 18) return false;

        MediaTranscoder.Listener listener = new MediaTranscoder.Listener() {
            double lastProgress = -100;

            @Override
            public void onTranscodeProgress(double progress) {
                if (progress - lastProgress > 0.05)
                {
                    if (DEBUG) Log.d(TAG, "Transcode progress: "+progress);
                    lastProgress = progress;
                }
                cb.onTranscodingProgress(progress);
            }

            @Override
            public void onTranscodeCompleted() {
                if (DEBUG) Log.d(TAG, "Video transcode completed successfully");
                cb.onTranscodingCompleted(dstPath);
            }

            @Override
            public void onTranscodeCanceled() {
                if (DEBUG) Log.d(TAG, "Transcode cancelled");
                cb.onTranscodingFailed();
            }

            @Override
            public void onTranscodeFailed(Exception exception) {
                if (DEBUG) Log.e(TAG, "Video transcode failed", exception);
                cb.onTranscodingFailed();
            }
        };
        try
        {
            if (DEBUG) Log.d(TAG, "Transcoding video "+srcPath);
            MediaTranscoder.getInstance().transcodeVideo(srcPath, dstPath,
                    new CompatibleMediaFormatStrategy(maxW, maxH, minW, minH), listener);

            return true;
        }
        catch (IOException | RuntimeException e)
        {
            if (DEBUG) Log.e(TAG, "Error while transcoding video", e);
            return false;
        }
    }

    private static class CompatibleMediaFormatStrategy implements MediaFormatStrategy
    {
        final private int _maxW;
        final private int _maxH;
        final private int _minW;
        final private int _minH;

        public CompatibleMediaFormatStrategy(int maxW, int maxH, int minW, int minH)
        {
            _maxH = maxH;
            _maxW = maxW;
            _minH = minH;
            _minW = minW;
        }

        private CamcorderProfile findMatchingVideoRecorderProfile(double aspectRatio)
        {
            int maxSize = 0;
            CamcorderProfile match = null;
            for (int profile = CamcorderProfile.QUALITY_QCIF; profile <= CamcorderProfile.QUALITY_2160P; profile++)
            {
                if (!CamcorderProfile.hasProfile(profile)) continue;

                CamcorderProfile description = CamcorderProfile.get(profile);
                if (description.videoFrameWidth > _maxW || description.videoFrameWidth < _minW ||
                        description.videoFrameHeight > _maxH || description.videoFrameHeight < _minH) continue;

                // check that aspect ratio is close enough to the requested aspect
                double aspect = (double)description.videoFrameWidth / (double)description.videoFrameHeight;
                if (Math.abs(aspectRatio - aspect) > 0.1) continue;

                int thisSize = description.videoFrameWidth * description.videoFrameHeight;
                if (thisSize > maxSize)
                {
                    match = description;
                    maxSize = thisSize;
                }
            }

            return match;
        }

        @Override
        public MediaFormat createVideoOutputFormat(MediaFormat inputFormat)
        {
            int dim1 = inputFormat.getInteger(MediaFormat.KEY_WIDTH);
            int dim2 = inputFormat.getInteger(MediaFormat.KEY_HEIGHT);
            int srcW = Math.max(dim1, dim2);
            int srcH = Math.min(dim1, dim2);
            if (DEBUG) Log.d(TAG, "createVideoOutputFormat, srcW = "+dim1+", srcH = "+dim2);
            // Try to find existing CamcorderProfile, matching all requirements
            // My guess here is that if MediaRecorder supports a profile, it will most
            // likely be supported in MediaEncoder framework too
            CamcorderProfile prof = findMatchingVideoRecorderProfile((double)srcW/(double)srcH);
            if (prof != null) return mediaFormatFromCamcorderProfile(prof, dim1 > dim2);

            throw new OutputFormatUnavailableException("Can't find compatible video encoder format for video size: "+dim1+", "+dim2);
        }

        private MediaFormat mediaFormatFromCamcorderProfile(CamcorderProfile prof, boolean landscape)
        {
            int dstW = landscape ? prof.videoFrameWidth : prof.videoFrameHeight;
            int dstH = landscape ? prof.videoFrameHeight : prof.videoFrameWidth;

            if (DEBUG) Log.d(TAG, "mediaFormatFromCamcorderProfile, dstW = "+dstW+", dstH = "+dstH);
            MediaFormat format = MediaFormat.createVideoFormat(MediaFormat.MIMETYPE_VIDEO_AVC, dstW, dstH);

            format.setInteger(MediaFormat.KEY_BIT_RATE, prof.videoBitRate);
            format.setInteger(MediaFormat.KEY_FRAME_RATE, prof.videoFrameRate);
            format.setInteger(MediaFormat.KEY_I_FRAME_INTERVAL, 3);
            format.setInteger(MediaFormat.KEY_COLOR_FORMAT, MediaCodecInfo.CodecCapabilities.COLOR_FormatSurface);
            return format;
        }

        @Override
        public MediaFormat createAudioOutputFormat(MediaFormat inputFormat)
        {
            // returning null for pass-through copying
            return null;
        }
    }
}
