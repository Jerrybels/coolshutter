package com.coolshutter.util;

import android.graphics.Bitmap;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Request;
import com.squareup.picasso.RequestHandler;

import java.io.IOException;

/**
 * Created by Const on 4/9/2015.
 * Project: CoolShutter
 */
public class PicassoVideoThumbnailRequestHandler extends RequestHandler
{
	public static final String VIDEO_THUMBNAIL_SCHEME = "vthumb";
	@Override
	public boolean canHandleRequest(Request data)
	{
		return VIDEO_THUMBNAIL_SCHEME.equals(data.uri.getScheme());
	}

	@Override
	public Result load(Request request, int networkPolicy) throws IOException
	{
		String path = request.uri.getPath();
		Bitmap bmp = VideoFileHelper.createVideoThumbnail(path);

		return new Result(bmp, Picasso.LoadedFrom.DISK);
	}
}
