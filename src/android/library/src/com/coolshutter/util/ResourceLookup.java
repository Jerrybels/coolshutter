package com.coolshutter.util;

import android.content.Context;
import android.content.res.Resources;

/**
 * Created by Const on 4/3/2015.
 * Project: CoolShutter
 */

// Cordova generates R.java files for all resoures as part of the main app package
// To make the plugin app package agnostic, we have to lookup resource ids by their names
public class ResourceLookup
{
	private Resources _res;
	private String _packageName;

	public ResourceLookup(Context ctx)
	{
		_res = ctx.getResources();
		_packageName = ctx.getPackageName();
	}

	private int resourceId(String name, String type)
	{
		return _res.getIdentifier(name, type, _packageName);
	}

	public int drawable(String name)
	{
		return resourceId(name, "drawable");
	}

	public int layout(String name)
	{
		return resourceId(name, "layout");
	}

	public int color(String name)
	{
		return resourceId(name, "color");
	}

	public int dimen(String name)
	{
		return resourceId(name, "dimen");
	}

	public int id(String name)
	{
		return resourceId(name, "id");
	}

	public int getDimensionPixelSize(String name)
	{
		return _res.getDimensionPixelSize(dimen(name));
	}

	public int getColor(String name)
	{
		return _res.getColor(color(name));
	}
}
