package com.coolshutter.util;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Created by Const on 4/22/2015.
 * Project: CoolShutter
 */
public class HookedImageView extends ImageView
{
	public interface ImageChangeListener
	{
		void onImageViewDrawableSet(Drawable newDrawable);
	}

	private ImageChangeListener _listener;

	public void setImageChangeListener(ImageChangeListener listener)
	{
		_listener = listener;
	}

	public HookedImageView(Context context)
	{
		super(context);
	}

	public HookedImageView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
	}

	public HookedImageView(Context context, AttributeSet attrs, int defStyleAttr)
	{
		super(context, attrs, defStyleAttr);
	}

	@TargetApi(Build.VERSION_CODES.LOLLIPOP)
	public HookedImageView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes)
	{
		super(context, attrs, defStyleAttr, defStyleRes);
	}

	@Override
	public void setImageDrawable(Drawable drawable)
	{
		super.setImageDrawable(drawable);
		if (_listener != null) _listener.onImageViewDrawableSet(getDrawable());
	}

	@Override
	public void setImageBitmap(Bitmap bm)
	{
		super.setImageBitmap(bm);
		if (_listener != null) _listener.onImageViewDrawableSet(getDrawable());
	}
}
