package com.coolshutter.util;

import android.animation.Animator;
import android.os.Handler;

/**
 * Created by const on 27/03/15.
 * Project: CoolShutter
 */
public class AnimatorListenerWrapper implements Animator.AnimatorListener
{
	final private Runnable _endRunnable;
	final static Handler _handler = new Handler();

	public AnimatorListenerWrapper(Runnable endRunnable)
	{
		_endRunnable = endRunnable;
	}

	@Override
	public void onAnimationStart(Animator animation)
	{

	}

	@Override
	public void onAnimationEnd(Animator animation)
	{
		_handler.post(_endRunnable);
	}

	@Override
	public void onAnimationCancel(Animator animation)
	{
		_handler.post(_endRunnable);
	}

	@Override
	public void onAnimationRepeat(Animator animation)
	{

	}
}
