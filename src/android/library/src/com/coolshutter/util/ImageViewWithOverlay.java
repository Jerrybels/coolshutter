package com.coolshutter.util;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Created by Const on 4/5/2015.
 * Project: CoolShutter
 */
public class ImageViewWithOverlay extends ImageView
{
	private Drawable _overlay;

	public ImageViewWithOverlay(Context context)
	{
		super(context);
	}

	public ImageViewWithOverlay(Context context, AttributeSet attrs)
	{
		super(context, attrs);
	}

	public ImageViewWithOverlay(Context context, AttributeSet attrs, int defStyleAttr)
	{
		super(context, attrs, defStyleAttr);
	}

	@TargetApi(Build.VERSION_CODES.LOLLIPOP)
	public ImageViewWithOverlay(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes)
	{
		super(context, attrs, defStyleAttr, defStyleRes);
	}

	public void setOverlayResource(int drawableId)
	{
		_overlay = getContext().getResources().getDrawable(drawableId);
		updateDrawableBounds();
	}

	@Override
	protected void onLayout(boolean changed, int left, int top, int right, int bottom)
	{
		super.onLayout(changed, left, top, right, bottom);

		updateDrawableBounds();
	}

	private void updateDrawableBounds()
	{
		if (_overlay == null) return;

		int w = getWidth();
		int h = getHeight();
		if (w == 0 || h == 0) return;

		int drW = _overlay.getIntrinsicWidth();
		int drH = _overlay.getIntrinsicHeight();
		if (drW <= 0 || drH <= 0)
		{
			drW = w;
			drH  = h;
		}
		int x = w / 2 - drW / 2;
		int y = h / 2 - drH / 2;
		_overlay.setBounds(x, y, x + drW, y + drH);
	}

	@Override
	protected void dispatchDraw(Canvas canvas)
	{
		super.dispatchDraw(canvas);
		if (_overlay != null && getDrawable() != null) _overlay.draw(canvas);
	}
}
