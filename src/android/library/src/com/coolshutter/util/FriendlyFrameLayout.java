package com.coolshutter.util;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;

/**
 * Created by const on 27/03/15.
 * Project: CoolShutter
 */

// Simple subclass to allow local layout change listener to be set
public class FriendlyFrameLayout extends FrameLayout
{
	private OnLayoutChangeListener _layoutChangeListener = null;

	public FriendlyFrameLayout(Context context)
	{
		super(context);
	}

	public FriendlyFrameLayout(Context context, AttributeSet attrs)
	{
		super(context, attrs);
	}

	public FriendlyFrameLayout(Context context, AttributeSet attrs, int defStyleAttr)
	{
		super(context, attrs, defStyleAttr);
	}

	public void setOnLayoutChangeListener(OnLayoutChangeListener layoutChangeListener)
	{
		_layoutChangeListener = layoutChangeListener;
	}

	@Override
	protected void onLayout(boolean changed, int left, int top, int right, int bottom)
	{
		super.onLayout(changed, left, top, right, bottom);

		if (_layoutChangeListener != null)
		{
			_layoutChangeListener.onLayoutChange(this, left, top, right, bottom, 0, 0, 0, 0);
		}
	}


}
