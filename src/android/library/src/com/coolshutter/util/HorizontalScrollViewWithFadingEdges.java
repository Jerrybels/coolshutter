package com.coolshutter.util;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.widget.HorizontalScrollView;

/**
 * Created by const on 26/03/15.
 * Project: CoolShutter
 */
public class HorizontalScrollViewWithFadingEdges extends HorizontalScrollView
{
	private View _leftEdge;
	private View _rightEdge;
	private boolean _leftEdgeVisible = false;
	private boolean _rightEdgeVisible = false;

	public HorizontalScrollViewWithFadingEdges(Context context)
	{
		super(context);
	}

	public HorizontalScrollViewWithFadingEdges(Context context, AttributeSet attrs)
	{
		super(context, attrs);
	}

	public HorizontalScrollViewWithFadingEdges(Context context, AttributeSet attrs, int defStyleAttr)
	{
		super(context, attrs, defStyleAttr);
	}

	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b)
	{
		super.onLayout(changed, l, t, r, b);

		updateFadingEdgesVisibility();
	}

	private void updateFadingEdgesVisibility()
	{
		int l = getScrollX();

		if (_leftEdge != null)
		{
			int leftEdgeThreshold = _leftEdge.getWidth();
			//boolean shouldShowLeftEdge = l > leftEdgeThreshold;
			float alpha = leftEdgeThreshold != 0 ? (float)l / (float)leftEdgeThreshold : 0;
			if (alpha < 1)
			{
				_leftEdgeVisible = false;
				_leftEdge.setAlpha(alpha);
			}
			else
			{
				if (!_leftEdgeVisible)
				{
					_leftEdge.setAlpha(1);
					_leftEdgeVisible = true;
				}
			}

		}

		if (_rightEdge != null && getChildCount() > 0)
		{
			int viewW = getWidth();
			int contentW = getChildAt(0).getWidth();
			int rightEdgeThreshold = _rightEdge.getWidth();
			float alpha = rightEdgeThreshold != 0 ? (float)(contentW - viewW - l)/(float)(rightEdgeThreshold) : 0;

			//boolean shouldShowRightEdge = contentW - viewW - l > rightEdgeThreshold;

			if (alpha < 1)
			{
				_rightEdge.setAlpha(alpha);
				_rightEdgeVisible = false;
			}
			else
			{
				if (!_rightEdgeVisible)
				{
					_rightEdge.setAlpha(1);
					_rightEdgeVisible = true;
				}
			}

		}


	}

	@Override
	protected void onScrollChanged(int l, int t, int oldl, int oldt)
	{
		super.onScrollChanged(l, t, oldl, oldt);

		updateFadingEdgesVisibility();
	}

	public void setLeftEdgeView(View leftEdge)
	{
		_leftEdge = leftEdge;
	}

	public void setRightEdgeView(View rightEdge)
	{
		_rightEdge = rightEdge;
	}
}
