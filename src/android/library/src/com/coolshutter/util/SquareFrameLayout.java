package com.coolshutter.util;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;

/**
 * Created by const on 28/03/15.
 * Project: CoolShutter
 */

// The FrameLayout which is always square - the height is set to be equal to width after measure
// Hack to be used in GridView to produce nice square elements
public class SquareFrameLayout extends FrameLayout
{
	public SquareFrameLayout(Context context)
	{
		super(context);
	}

	public SquareFrameLayout(Context context, AttributeSet attrs)
	{
		super(context, attrs);
	}

	public SquareFrameLayout(Context context, AttributeSet attrs, int defStyleAttr)
	{
		super(context, attrs, defStyleAttr);
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
	{
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		int w = getMeasuredWidth();
		//setMeasuredDimension(w, w);
		super.onMeasure(w | MeasureSpec.EXACTLY, w | MeasureSpec.EXACTLY);


	}

}
