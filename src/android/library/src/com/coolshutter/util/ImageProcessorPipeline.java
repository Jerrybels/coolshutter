package com.coolshutter.util;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;

import com.coolshutter.exif.ExifInterface;
import com.coolshutter.exif.ExifTag;
import com.coolshutter.exif.IfdId;
import com.coolshutter.main.PluginConfig;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Const on 4/19/2015.
 * Project: CoolShutter
 */
public class ImageProcessorPipeline
{
	private static final boolean DEBUG = PluginConfig.DEBUG;
	private static final String TAG = "MultiResizer";

	public interface ImageProcessorCallback
	{
		void onTransformationJobDone(int jobId, Object tag, Bitmap bmp);
		void onSaveJobDone(int jobId, Object tag, String destPath, boolean success);
		void onBase64ThumbnailReady(int jobId, Object tag, String thumbData, int thumbW, int thumbH);
		void onThumbnailFileSaved(int jobId, Object tag, String thumbFile, int thumbW, int thumbH, boolean success);
        void onJobFailed(int jobId, Object tag);
	}

	public static Pipeline loadFrom(byte[] rawJPEGdata, int orientation)
	{
		return new Pipeline(rawJPEGdata, null, false, orientation);
	}

	public static Pipeline loadFrom(String fileUri, boolean isVideo, int orientation)
	{
		return new Pipeline(null, fileUri, isVideo, orientation);
	}

	private static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight)
	{
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth)
		{

			final int halfHeight = height / 2;
			final int halfWidth = width / 2;

			// Calculate the largest inSampleSize value that is a power of 2 and keeps both
			// height and width larger than the requested height and width.
			while ((halfHeight / inSampleSize) > reqHeight
					&& (halfWidth / inSampleSize) > reqWidth)
			{
				inSampleSize *= 2;
			}
		}

		return inSampleSize;
	}

	private static Bitmap loadBitmapFromDisk(String filePath, Pipeline r)
	{
		// figure out the original width and height of the image
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(filePath, options);

		if (options.outWidth == 0 || options.outHeight == 0)
		{
			return null;
		}

		Size sz = r.getRequiredImageSize(options.outWidth, options.outHeight);

		// Load in the smallest bitmap possible that is closest to the size we want
		options.inJustDecodeBounds = false;
		options.inSampleSize = calculateInSampleSize(options, sz.width, sz.height);
		return BitmapFactory.decodeFile(filePath, options);
	}

	private static Bitmap loadVideoThumbnail(String sourceFileUri)
	{
		return VideoFileHelper.createVideoThumbnail(sourceFileUri);
	}

	private static Bitmap loadBitmapFromMemory(byte[] data, Pipeline r)
	{
		// figure out the original width and height of the image
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		BitmapFactory.decodeByteArray(data, 0, data.length, options);

		//CB-2292: WTF? Why is the width null?
		if (options.outWidth == 0 || options.outHeight == 0)
		{
			return null;
		}

		Size sz = r.getRequiredImageSize(options.outWidth, options.outHeight);

		// Load in the smallest bitmap possible that is closest to the size we want
		options.inJustDecodeBounds = false;
		options.inSampleSize = calculateInSampleSize(options, sz.width, sz.height);
		return BitmapFactory.decodeByteArray(data, 0, data.length, options);
	}

	private static class Size
	{
		public int width;
		public int height;

//		public int getMaxDimension()
//		{
//			return width > height ? width : height;
//		}
//
//		public int getMinDimension()
//		{
//			return width > height ? height : width;
//		}
	}

	private static abstract class PipelineJob
	{
		private final int _id;
		private final Object _tag;

		protected PipelineJob(int id, Object tag)
		{
			_id = id;
			_tag = tag;
		}

		// get the source image size required to produce result bitmap
		// params: srcWidth, srcHeight = size of the original image
		// sz = [out] required image size
		public abstract void getRequiredSize(int srcWidth, int srcHeight, int orientation, Size sz);
		// returns true if the source bitmap should not be recycled (i.e. some result bitmap makes hold
		// of the source bitmap data)
		public abstract boolean execute(Pipeline owner);

		public int getId()
		{
			return _id;
		}

		public Object getTag()
		{
			return _tag;
		}

		public abstract void reportResult(ImageProcessorCallback cb);

		protected static boolean shouldResize(int inWidth, int inHeight, int targetWidth, int targetHeight)
		{
			return inWidth > targetWidth || inHeight > targetHeight;
		}
	}

	private static class CropCenterJob extends PipelineJob
	{
		private final int _thumbSize;
		private Bitmap _resultBitmap;

		public CropCenterJob(int id, Object tag, int targetSize)
		{
			super(id, tag);
			_thumbSize = targetSize;
		}

		@Override
		public void getRequiredSize(int srcWidth, int srcHeight, int orientation, Size sz)
		{
			float scale = (float)_thumbSize / (float)Math.min(srcWidth, srcHeight);
			sz.width = (int)(scale*srcWidth);
			sz.height = (int)(scale*srcHeight);
		}

		@Override
		public boolean execute(Pipeline owner)
		{
			Bitmap source = owner.getSourceBitmap();
			Matrix matrix = new Matrix();
			int inWidth = source.getWidth();
			int inHeight = source.getHeight();
			int drawY = 0, drawX = 0;
			int drawHeight = inHeight;
			int drawWidth = inWidth;

			float widthRatio = _thumbSize / (float) inWidth;
			float heightRatio = _thumbSize / (float) inHeight;
			float scaleX, scaleY;
			if (widthRatio > heightRatio)
			{
				int newSize = (int) Math.ceil(inHeight * (heightRatio / widthRatio));
				drawY = (inHeight - newSize) / 2;
				drawHeight = newSize;
				scaleX = widthRatio;
				scaleY = _thumbSize / (float) drawHeight;
			}
			else
			{
				int newSize = (int) Math.ceil(inWidth * (widthRatio / heightRatio));
				drawX = (inWidth - newSize) / 2;
				drawWidth = newSize;
				scaleX = _thumbSize / (float) drawWidth;
				scaleY = heightRatio;
			}
			if (shouldResize(inWidth, inHeight, _thumbSize, _thumbSize))
			{
				matrix.preScale(scaleX, scaleY);
			}

			if (DEBUG) Log.d(TAG, "CropCenterJob, srcOrientation = "+owner.getSourceOrientation());
			if (owner.getSourceOrientation() != 0)
			{
				matrix.preRotate(owner.getSourceOrientation());
			}

			_resultBitmap = Bitmap.createBitmap(source, drawX, drawY, drawWidth, drawHeight, matrix, true);
			return _resultBitmap == source;
		}

		@Override
		public void reportResult(ImageProcessorCallback cb)
		{
			cb.onTransformationJobDone(getId(), getTag(), _resultBitmap);
		}

		@Override
		public String toString()
		{
			return "CropCenterJob{" +
					"_resultBitmap=" + _resultBitmap +
					", _thumbSize=" + _thumbSize +
					'}';
		}
	}

	private static class AspectScaleFitJob extends PipelineJob
	{
		private final int _maxW;
		private final int _maxH;
		private final int _minW;
		private final int _minH;
		private Bitmap _resultBitmap;

		public AspectScaleFitJob(int id, Object tag, int maxWidth, int maxHeight, int minWidth, int minHeight)
		{
			super(id, tag);
			_maxW = maxWidth;
			_maxH = maxHeight;
			_minW = minWidth;
			_minH = minHeight;
		}

		public int getFitWidth()
		{
			return _maxW;
		}

		public int getFitHeight()
		{
			return _maxH;
		}

		public Bitmap getResultBitmap()
		{
			return _resultBitmap;
		}

		private float calcScale(int srcWidth, int srcHeight, int orientation)
		{
			if (srcWidth == 0 || srcHeight == 0) return 0;
			int maxW, maxH, minW, minH;
			if (orientation % 180 == 0)
			{
				maxW = _maxW;
				maxH = _maxH;
				minW = _minW;
				minH = _minH;
			}
			else
			{
				maxW = _maxH;
				maxH = _maxW;
				minW = _minH;
				minH = _minW;
			}
			float maxScaleW = maxW > 0 ? maxW / (float)srcWidth : 1E10f;
			float maxScaleH = maxH > 0 ? maxH / (float)srcHeight : 1E10f;

			float minScaleW = minW > 0 ? minW / (float)srcWidth : 0;
			float minScaleH = minH > 0 ? minH / (float)srcHeight : 0;
			float maxScale = Math.min(maxScaleW, maxScaleH);
			if (maxScale > 0.9e10f) maxScale = 0; // avoid large scale if maxW and maxH are 0
			float minScale = Math.max(minScaleW, minScaleH);
			return Math.max(maxScale, minScale);
		}

		@Override
		public void getRequiredSize(int srcWidth, int srcHeight, int orientation, Size sz)
		{
			float scale = calcScale(srcWidth, srcHeight, orientation);
			sz.width = (int)(scale*srcWidth);
			sz.height = (int)(scale*srcHeight);
		}

		@Override
		public boolean execute(Pipeline owner)
		{
			Bitmap source = owner.getSourceBitmap();
			Matrix matrix = new Matrix();
			int inWidth = source.getWidth();
			int inHeight = source.getHeight();

			int drawY = 0, drawX = 0;

			float scale = calcScale(inWidth, inHeight, owner.getSourceOrientation());
			if (shouldResize(inWidth, inHeight, _maxW, _maxH))
			{
				matrix.preScale(scale, scale);
			}

			if (owner.getSourceOrientation() != 0)
			{
				matrix.preRotate(owner.getSourceOrientation());
			}

			_resultBitmap =	Bitmap.createBitmap(source, drawX, drawY, inWidth, inHeight, matrix, true);
			return _resultBitmap == source;
		}

		@Override
		public void reportResult(ImageProcessorCallback cb)
		{
			cb.onTransformationJobDone(getId(), getTag(), _resultBitmap);
		}

		@Override
		public String toString()
		{
			return "AspectScaleFitJob{" +
					"_fitSize = (" + _maxW + ", " + _maxH + ")" +
					", _resultBitmap=" + _resultBitmap +
					'}';
		}
	}

	private static class GenerateBase64ThumbnailJob extends AspectScaleFitJob
	{
		private String _thumbData;
		private int _thumbW;
		private int _thumbH;
		final private int _jpegQuality;

		public GenerateBase64ThumbnailJob(int id, Object tag, int maxW, int maxH, int minW, int minH, int jpegQuality)
		{
			super(id, tag, maxW, maxH, minW, minH);
			_jpegQuality = jpegQuality;
		}

		@Override
		public boolean execute(Pipeline owner)
		{
			// generate thumbnail bitmap fit into [fitSize x fitSize] square
			super.execute(owner);

			Bitmap res = getResultBitmap();
			_thumbW = res.getWidth();
			_thumbH = res.getHeight();

			if (DEBUG) Log.d(TAG, "prepareBase64Thumbnail > thumbW = "+_thumbW+" thumbH = "+_thumbH);
			ByteArrayOutputStream os = new ByteArrayOutputStream();
			res.compress(Bitmap.CompressFormat.JPEG, _jpegQuality, os);
			byte[] jpeg = os.toByteArray();

			_thumbData = "data:image/jpeg;base64," + Base64.encodeToString(jpeg, Base64.NO_WRAP);
			return false;
		}

		@Override
		public void reportResult(ImageProcessorCallback cb)
		{
			cb.onBase64ThumbnailReady(getId(), getTag(), _thumbData, _thumbW, _thumbH);
		}

	}

	private static class GenerateFileThumbnailJob extends AspectScaleFitJob
	{
		private String _thumbFile;
		private int _thumbW;
		private int _thumbH;
		final private int _jpegQuality;
		private boolean _success = false;

		public GenerateFileThumbnailJob(int id, Object tag, String outFile, int maxW, int maxH, int minW, int minH, int jpegQuality)
		{
			super(id, tag, maxW, maxH, minW, minH);
			_thumbFile = outFile;
			_jpegQuality = jpegQuality;
		}

		@Override
		public boolean execute(Pipeline owner)
		{
			// generate thumbnail bitmap fit into [fitW x fitH] square
			super.execute(owner);

			Bitmap res = getResultBitmap();
			_thumbW = res.getWidth();
			_thumbH = res.getHeight();

			if (DEBUG) Log.d(TAG, "prepareFileThumbnail > thumbW = "+_thumbW+" thumbH = "+_thumbH);
			ByteArrayOutputStream os = new ByteArrayOutputStream();
			res.compress(Bitmap.CompressFormat.JPEG, _jpegQuality, os);
			byte[] jpeg = os.toByteArray();
			try
			{
				FileOutputStream fos = new FileOutputStream(_thumbFile);
				fos.write(jpeg);
				fos.close();
				_success = true;
			}
			catch (IOException e)
			{
				if (DEBUG) Log.e(TAG, "Error saving image thumbnail to "+_thumbFile, e);
				else e.printStackTrace();
			}
			return false;
		}

		@Override
		public void reportResult(ImageProcessorCallback cb)
		{
			cb.onThumbnailFileSaved(getId(), getTag(), _thumbFile, _thumbW, _thumbH, _success);
		}

		@Override
		public String toString()
		{
			return "GenerateFileThumbnailJob{" +
					"fitSize = (" + getFitWidth() + ", "+ getFitHeight() + ")" +
					", outFile = "+_thumbFile+
					'}';

		}
	}

	private static class SaveToFileJob extends PipelineJob
	{
		private final int _srcOrientation;
		private final boolean _fixOrientation;
		private final int _destQuality;
		private final String _destPath;
		private boolean _resultSuccess = false;

		public SaveToFileJob(int id, Object tag, int srcOrientation, String destPath, int destQuality, boolean fixOrientation)
		{
			super(id, tag);
			_srcOrientation = srcOrientation;
			_destPath = destPath;
			_destQuality = destQuality;
			_fixOrientation = fixOrientation;
		}

		@Override
		public void getRequiredSize(int srcWidth, int srcHeight, int orientation, Size sz)
		{
			if (_fixOrientation)
			{
				sz.width = srcWidth;
				sz.height = srcHeight;
			}
			else
			{
				sz.width = 0;
				sz.height = 0;
			}
		}

		private ExifInterface loadExif(Pipeline owner)
		{
			ExifInterface exif = new ExifInterface();
			try
			{
				if (owner.getRawJPEGData() != null)
				{
					exif.readExif(owner.getRawJPEGData());
				}
				else
				{
					exif.readExif(owner.getSourceFileUri());
				}
				return exif;
			}
			catch (IOException e)
			{
				if (DEBUG) Log.e(TAG, "Error parsing EXIF from camera JPEG", e);
				return null;
			}
		}

		private byte[] prepareFixedImageData(Pipeline owner)
		{
			byte[] jpegData = owner.getRawJPEGData();
			if (_fixOrientation && _srcOrientation != 0)
			{
				Bitmap rotated = rotateBitmap(owner.getSourceBitmap());
				if (rotated != null)
				{
					_resultSuccess = false;
					ByteArrayOutputStream os = new ByteArrayOutputStream();
					rotated.compress(Bitmap.CompressFormat.JPEG, _destQuality, os);
					jpegData = os.toByteArray();
					try
					{
						os.close();
					}
					catch (IOException e)
					{
						if (DEBUG) Log.e(TAG, "Error compressing bitmap", e);
					}
				}
			}
			return jpegData;
		}

		private static int getExifRotationFlagFromAngle(int degrees)
		{
			switch (degrees % 360)
			{
				default:
				case 0: return ExifInterface.Orientation.TOP_LEFT;
				case 90: return ExifInterface.Orientation.RIGHT_TOP;
				case 180: return ExifInterface.Orientation.BOTTOM_LEFT;
				case 270: return ExifInterface.Orientation.RIGHT_BOTTOM;
			}
		}

		@Override
		public boolean execute(Pipeline owner)
		{
			_resultSuccess = true;
			byte[] jpegData = prepareFixedImageData(owner);

			int dstOrientation = _srcOrientation;
			if (_fixOrientation) dstOrientation = 0;

			ExifInterface exif = loadExif(owner);
			try
			{
				if (exif != null)
				{
					if (DEBUG) Log.d(TAG, "Updating EXIF photo orientation to " + dstOrientation);
					int orientationFlag = getExifRotationFlagFromAngle(dstOrientation);
					ExifTag tag = exif.buildTag(ExifInterface.TAG_ORIENTATION, IfdId.TYPE_IFD_0, (short) (orientationFlag));
					exif.setTag(tag);

					exif.writeExif(jpegData, _destPath);
				}
				else
				{
					// do not update EXIF - write RAW data
					if (DEBUG) Log.d(TAG, "Don't have EXIF - writing raw data as is");
					FileOutputStream fos = new FileOutputStream(_destPath);
					fos.write(jpegData);
					fos.close();
				}
			}
			catch (Exception x)
			{
				if (DEBUG) Log.e(TAG, "Error while saving updated image", x);
			}
			return false;
		}

		private Bitmap rotateBitmap(Bitmap source)
		{
			Matrix matrix = new Matrix();
			if (_srcOrientation == 180)
			{
				matrix.setRotate(_srcOrientation);
			}
			else
			{
				matrix.setRotate(_srcOrientation, (float) source.getWidth() / 2, (float) source.getHeight() / 2);
			}

			try
			{
				return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
			}
			catch (OutOfMemoryError oom)
			{
				return null;
			}
		}

		@Override
		public void reportResult(ImageProcessorCallback cb)
		{
			cb.onSaveJobDone(getId(), getTag(), _destPath, _resultSuccess);
		}

		@Override
		public String toString()
		{
			return "FixOrientationJob{" +
					"_srcOrientation=" + _srcOrientation +
					", _destQuality=" + _destQuality +
					", _destPath='" + _destPath + '\'' +
					'}';
		}
	}

	public static class Pipeline
	{
		private final byte[] _rawJPEGdata;
		private final String _sourceFileUri;
		private final boolean _isVideo;
		private final int _sourceOrientation;
		private final List<PipelineJob> _jobs = new ArrayList<PipelineJob>();
		private Bitmap _sourceBmp;
		private int _jobsDone = 0;
        private boolean _operationFailed;

		private ImageProcessorCallback _callback;
		private boolean _canRecycleSourceBitmap = true;

		public Pipeline(byte[] rawJPEGdata, String sourceFileUri, boolean isVideo, int orientation)
		{
			_rawJPEGdata = rawJPEGdata;
			_sourceFileUri = sourceFileUri;
			_isVideo = isVideo;
			_sourceOrientation = orientation;
		}

        public boolean isOperationFailed()
        {
            return _operationFailed;
        }

        public void setOperationFailed(boolean operationFailed)
        {
            _operationFailed = operationFailed;
        }

        public Pipeline setCallback(ImageProcessorCallback callback)
		{
			_callback = callback;
			return this;
		}

		private byte[] getRawJPEGData()
		{
			return _rawJPEGdata;
		}

		public Pipeline addSaveToFileJob(int jobId, Object tag, int srcOrientation, String destPath, int jpegQuality, boolean fixOrientation)
		{
			if (DEBUG) Log.d(TAG, "addSaveToFileJob jobId = "+jobId+" orientation = "+srcOrientation+" destPath = "+destPath);
			_jobs.add(new SaveToFileJob(jobId, tag, srcOrientation, destPath, jpegQuality, fixOrientation));
			return this;
		}

//		public Pipeline addAspectScaledToFitJob(int jobId, Object tag, int fitW, int fitH)
//		{
//			if (DEBUG) Log.d(TAG, "addAspectScaledToFitJob jobId = "+jobId+" fitSize = ("+fitW + ", "+fitH+")");
//			_jobs.add(new AspectScaleFitJob(jobId, tag, fitW, fitH));
//			return this;
//		}

		public Pipeline addGenerateThumbnailJob(int jobId, Object tag,
												boolean saveAsFile,
												String outFileName,
												int maxThumbnailWidth, int maxThumbnailHeight,
												int minThumbnailWidth, int minThumbnailHeight,
												int jpegQuality)
		{
			if (DEBUG) Log.d(TAG, "addGenerateThumbnailJob jobId = "+jobId +
					" fitSize = ("+maxThumbnailWidth + ", " + maxThumbnailHeight + ")");
			if (saveAsFile)
			{
				_jobs.add(new GenerateFileThumbnailJob(jobId, tag, outFileName,
										maxThumbnailWidth, maxThumbnailHeight,
										minThumbnailWidth, minThumbnailHeight,
										jpegQuality));
			}
			else
			{
				_jobs.add(new GenerateBase64ThumbnailJob(jobId, tag,
						maxThumbnailWidth, maxThumbnailHeight,
						minThumbnailWidth, minThumbnailHeight,
						jpegQuality));
			}
			return this;
		}

		public Pipeline addCenterCropJob(int jobId, Object tag, int targetSz)
		{
			if (DEBUG) Log.d(TAG, "addCenterCropJob jobId = "+jobId+" targetSz = " + targetSz);
			_jobs.add(new CropCenterJob(jobId, tag, targetSz));
			return this;
		}

		public void runAsync()
		{
			if (DEBUG) Log.d(TAG, "runAsync()");
			new AsyncJobRunner().execute(this);
		}

		public boolean runSync()
		{
			if (DEBUG) Log.d(TAG, "runSync()");

            loadImage();
            if (getSourceBitmap() == null)
            {
                if (DEBUG) Log.e(TAG, "Source bitmap is null, stopping pipeline");
                setOperationFailed(true);
                return false;
            }
            if (DEBUG) Log.d(TAG, "Source image loaded > "+_sourceBmp);
            for (int ii = 0; ii < _jobs.size(); ii++)
            {
                PipelineJob job = doJob(ii);
                if (_callback != null) job.reportResult(_callback);
            }
            onAllJobsDone();
            return true;
		}

		private void loadImage()
		{
			if (_rawJPEGdata != null)
			{
				_sourceBmp = loadBitmapFromMemory(_rawJPEGdata, this);
			}
			else
			if (_isVideo)
			{
				_sourceBmp = loadVideoThumbnail(_sourceFileUri);
			}
			else
			{
				_sourceBmp = loadBitmapFromDisk(_sourceFileUri, this);
			}
		}

		public Bitmap getSourceBitmap()
		{
			return _sourceBmp;
		}

		private Size getRequiredImageSize(int srcWidth, int srcHeight)
		{
			Size sz = new Size();
			int maxW = 0, maxH = 0;
			for (PipelineJob job : _jobs)
			{
				job.getRequiredSize(srcWidth, srcHeight, _sourceOrientation, sz);
				if (sz.width > maxW) maxW = sz.width;
				if (sz.height > maxH) maxH = sz.height;
			}
			sz.width = maxW;
			sz.height = maxH;
			return sz;
		}

		public void reportProgress()
		{
			if (DEBUG) Log.d(TAG, "Report progress, jobs done = "+_jobsDone);

			if (_callback != null)
			{
                if (isOperationFailed())
                {
                    _callback.onJobFailed(0, _jobs.get(0).getTag());
                    return;
                }

                for (int ii = 0; ii < _jobsDone; ii++)
                {
                    PipelineJob job = _jobs.get(ii);
                    _jobs.set(ii, null);
                    if (job != null) {
                        if (DEBUG) Log.d(TAG, "reporting job "+ii+" progress");

                        job.reportResult(_callback);
                    }
                }
			}
		}

		public PipelineJob doJob(int ii)
		{
			PipelineJob job = _jobs.get(ii);
			if (DEBUG) Log.d(TAG, "Do job "+ii+" job = "+job);
			if (job.execute(this))
			{
				if (DEBUG) Log.d(TAG, "Source bitmap locked!");
				_canRecycleSourceBitmap = false;
			}
			if (DEBUG) Log.d(TAG, "Job "+ii+" done = "+job);
			_jobsDone = ii + 1;
            return job;
		}

		public void onAllJobsDone()
		{
			if (_canRecycleSourceBitmap) {
				_sourceBmp.recycle();
				_sourceBmp = null;
			}
		}

		public int getSourceOrientation()
		{
			return _sourceOrientation;
		}

		public String getSourceFileUri()
		{
			return _sourceFileUri;
		}

	}

	private static class AsyncJobRunner extends AsyncTask<Pipeline, Pipeline, Pipeline>
	{
		@Override
		protected Pipeline doInBackground(Pipeline... params)
		{
			Pipeline r = params[0];
			if (DEBUG) Log.d(TAG, "doInBackground()");
			r.loadImage();
			if (r.getSourceBitmap() == null)
			{
				if (DEBUG) Log.e(TAG, "Source bitmap is null, stopping pipeline");
                r.setOperationFailed(true);
				return r;
			}
			if (DEBUG) Log.d(TAG, "Image loaded > "+r._sourceBmp);
			for (int ii = 0; ii < r._jobs.size(); ii++)
			{
				r.doJob(ii);
				publishProgress(r);
			}
			r.onAllJobsDone();
			return r;
		}

		@Override
		protected void onProgressUpdate(Pipeline... values)
		{
			Pipeline r = values[0];

			r.reportProgress();
		}

		@Override
		protected void onPostExecute(Pipeline r)
		{
            r.reportProgress();
		}
	}
}
