package com.coolshutter.util;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.SystemClock;


public class TickTockDrawable extends Drawable
{
    static final private int UPDATE_INTERVAL = 100;

    private int BORDER_OFFSET = 2;
    private int DOT_SPACING = 2;
    private int DOT_SIZE = 4;

    final private Paint _paint = new Paint();

    private int _color = Color.rgb(192, 192, 192);
    private int _dotsCount = 10;
    private int _emptyDots = _dotsCount - 1;
    private int _curDotPos = -1;
    private boolean _invert = false;
    private boolean _ticking = false;

    public TickTockDrawable(Context ctx)
    {
        super();
        float density = ctx.getResources().getDisplayMetrics().density;
        BORDER_OFFSET = Math.round(density * BORDER_OFFSET);
        DOT_SPACING = Math.round(density * DOT_SPACING);
        DOT_SIZE = Math.round(density * DOT_SIZE);
    }

    public void resetCounter()
    {
        _curDotPos = -1;
        _invert = false;
        _emptyDots = _dotsCount - 1;
    }

    private Runnable _onTick = new Runnable()
    {
        @Override
        public void run()
        {
            _curDotPos++;
            if (_invert)
            {
                if (_curDotPos >= _emptyDots)
                {
                    _emptyDots++;
                    _curDotPos = 0;
                    if (_emptyDots >= _dotsCount)
                    {
                        _invert = !_invert;
                    }
                }
            }
            else
            {
                if (_curDotPos >= _emptyDots)
                {
                    _emptyDots--;
                    _curDotPos = -1;
                    if (_emptyDots <= 0)
                    {
                        _invert = !_invert;
                    }
                }
            }
            invalidateSelf();
            scheduleSelf(_onTick, SystemClock.uptimeMillis() + UPDATE_INTERVAL);
        }
    };

    @Override
    public void draw(Canvas canvas)
    {
        Rect bounds = getBounds();
        _paint.setColor(_color);
        _paint.setStrokeWidth(1);

        _paint.setStyle(Paint.Style.STROKE);
        canvas.drawRect(bounds.left + BORDER_OFFSET, bounds.top + BORDER_OFFSET,
                bounds.right - BORDER_OFFSET, bounds.bottom - BORDER_OFFSET, _paint);
        int w = bounds.right - bounds.left;
        int start = (w - ((_dotsCount * DOT_SIZE) + (DOT_SPACING * (_dotsCount - 1)))) / 2;

        int yOffset = bounds.bottom - BORDER_OFFSET - 1 - DOT_SPACING;
        for (int ii = 0; ii < _dotsCount; ii++)
        {
            int y = yOffset - DOT_SIZE;
            boolean empty;
            if (_invert)
            {
                empty = (ii >= _dotsCount - _emptyDots) && (ii != (_dotsCount - _emptyDots + _curDotPos));
            }
            else
            {
                empty = (ii <= _emptyDots) && (ii != _curDotPos);
            }
            _paint.setStyle(empty ? Paint.Style.STROKE : Paint.Style.FILL_AND_STROKE);
            canvas.drawRect(start, y, start + DOT_SIZE, y + DOT_SIZE, _paint);
            start += DOT_SPACING + DOT_SIZE;
        }

        if (!_ticking) scheduleSelf(_onTick, 0);
    }

    @Override
    public boolean setVisible(boolean visible, boolean restart)
    {
        unscheduleSelf(_onTick);
        if (visible) scheduleSelf(_onTick, 0);

        return super.setVisible(visible, restart);
    }

    @Override
    public void scheduleSelf(Runnable what, long when)
    {
        _ticking = true;
        super.scheduleSelf(what, when);
    }

    @Override
    public void unscheduleSelf(Runnable what)
    {
        super.unscheduleSelf(what);
        _ticking = false;
    }

    @Override
    public void setAlpha(int alpha)
    {

    }

    @Override
    public void setColorFilter(ColorFilter cf)
    {

    }

    @Override
    public int getOpacity()
    {
        return PixelFormat.OPAQUE;
    }

}
