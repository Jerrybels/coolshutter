package com.coolshutter.util;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;


/**
 * Created by const on 30/03/15.
 * Project: CoolShutter
 */

/////////////////////////////////////////////////////////////////////////////
// This class helps to build more complex layout for ToggleButton.
// Usage pattern: create CustomToggleButtonContainer with ToggleButton inside
// ToggleButton should not contain text nor background images, but it will receive
// touch events, update its checked state and call listener if present.
// All visuals should be recreated by adding other controls to the container.
// For example: TextView, ImageView, etc.
// CustomToggleButtonContainer provides marshalling of drawable states between
// the ToggleButton and other controls and the container itself.
// Container copies state changes from ToggleButton and then propagates them
// to other children
// As a result, all child controls could have drawables with state_checked to
// correctly reflect checked state changes
/////////////////////////////////////////////////////////////////////////////
public class CustomToggleButtonContainer extends RelativeLayout
{
	public CustomToggleButtonContainer(Context context)
	{
		super(context);
	}

	public CustomToggleButtonContainer(Context context, AttributeSet attrs)
	{
		super(context, attrs);
	}

	public CustomToggleButtonContainer(Context context, AttributeSet attrs, int defStyleAttr)
	{
		super(context, attrs, defStyleAttr);
	}

	private int[] _curStates = null;

	@Override
	protected int[] onCreateDrawableState(int extraSpace)
	{
		// return drawable state copied from the CompoundButton child
		// so it will propagate to all other children
		if (_curStates != null)
		{
			if (extraSpace == 0)
			{
				return _curStates;
			}

			final int[] fullState;
			fullState = new int[_curStates.length + extraSpace];
			System.arraycopy(_curStates, 0, fullState, 0, _curStates.length);

			return fullState;
		}

		return super.onCreateDrawableState(extraSpace);
	}



	@Override
	public void childDrawableStateChanged(View child)
	{
		// always copy drawable state from descendants of CompoundButton
		if (child instanceof CompoundButton)
		{
			_curStates = child.getDrawableState();

			refreshDrawableState();

			final int count = getChildCount();

			for (int i = 0; i < count; i++)
			{
				final View v = getChildAt(i);
				// Update drawable states for all children except the CompoundButtons
				if (!(v instanceof CompoundButton))
				{
					// duplicate parent state flag should be set to force child to
					// use parent's drawable state
					v.setDuplicateParentStateEnabled(true);
					v.refreshDrawableState();
				}
			}
		}
	}

}
