package com.coolshutter.main;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.OrientationEventListener;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.coolshutter.util.ArgbEvaluatorCompat;
import com.coolshutter.util.FocusAreaView;
import com.coolshutter.util.FriendlyFrameLayout;
import com.coolshutter.util.ImageProcessorPipeline;
import com.coolshutter.util.PermissionsHelper;
import com.coolshutter.util.PicassoVideoThumbnailRequestHandler;
import com.coolshutter.util.ResourceLookup;
import com.coolshutter.util.SwipeGestureRecognizer;
import com.coolshutter.util.Utils;
import com.coolshutter.util.progress.CircularProgressDrawable;
import com.coolshutter.util.progress.IndeterminateProgressDrawable;
import com.squareup.picasso.LruCache;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.UrlConnectionDownloader;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by const on 24/03/15.
 * Project: CoolShutter
 */

@SuppressWarnings("deprecation")
public class ShutterActivity extends FragmentActivity implements
				   GalleryPickerFragment.SelectedItemsController,
				   CameraAccess.CameraAccessCallbacks,
				   ImageProcessorPipeline.ImageProcessorCallback
{
	static final private String TAG = "ShutterActivity";
	static final private boolean DEBUG = PluginConfig.DEBUG;

	static final public String EXTRA_PLUGIN_PARAMS = "pluginParams";
	static final public String EXTRA_PLUGIN_RESULT = "pluginResult";

	static final private int FIXED_UI_ORIENTATION_ANGLE = 0;

	static final private int MODE_NOT_SET = 0;
	static final public int MODE_PHOTO = 1;
	static final public int MODE_VIDEO = 2;
	static final public int MODE_GALLERY = 3;

	private static final long RECORDING_DOT_BLINK_PERIOD = 500;
	private static final long VIDEO_PROGRESS_TIMER_TIMEOUT = 100;

	private static boolean _picasso_initialized = false;
	private static final float PICASSO_MEM_CACHE_SIZE_PERCENT = 0.5f;

	private static float TAP_JITTER_THRESHOLD;

	private static final int IMAGE_JOB_ID_CROP_FOR_CAPTURE_ANIMATION = 1;
	private static final int IMAGE_JOB_ID_CROP_FOR_THUMB_BAR = 2;
	private static final int IMAGE_JOB_ID_GENERATE_THUMBNAIL = 3;
	private static final int IMAGE_JOB_ID_SAVE_PHOTO_TO_FILE = 4;

	private int _currentMode = MODE_NOT_SET;
	private CameraPluginParams _pluginParams;
	private SurfaceView _surfaceView;
    private boolean _activityResumed = false;

	private CameraAccess _cam;

	private ViewGroup _layoutRoot;
	private View _flashModeButton;
	private View _switchCameraButton;
	private View _cancelButton;
	private View _uploadButton;

	private FriendlyFrameLayout _bottomBlock;
	private View _cameraButton;
	private View _galleryButton;
	private View _videoButton;


	static final private int BUTTON_STATE_NORMAL = 0;
	static final private int BUTTON_STATE_RECORDING = 1;
	// array index = current mode - 1 (MODE_PHOTO, MODE_VIDEO, MODE_GALLERY)
	static private int[][] BUTTON_IMAGES = {
			{0, 0},
			{0, 0},
			{0, 0},
	};
	final private View[] BUTTON_LOOKUP = {
			_cameraButton,
			_videoButton,
			_galleryButton
	};

	private List<View> _activeButtons = new ArrayList<>(3);
	private List<TextView> _activeLabels = new ArrayList<>(3);

	private OrientationEventListener _orientationEventListener;
	// device orientation in degrees. 4 possible positions:
	// 0 = PORTRAIT, 90 = LANDSCAPE, LEFT EDGE UP
	// 180 = PORTRAIT UPSIDE DOWN, 270 = LANDSCAPE, RIGHT EDGE UP
	private int _lastDeviceOrientation = 0;
	private int _currentControlsRotationAngle = 0;
	// some devices have their natural orientation set to landscape. We need to take that into account
	private int _naturalDisplayRotation = 0;


	private TextView _timeTicker;

	private GalleryPickerFragment _pickerFragment = null;
	private SelectedItemInfo.ThumbnailClickListener _thumbnailClickListener = new SelectedItemInfo.ThumbnailClickListener()
	{
		@Override
		public void onThumbnailClicked(SelectedItemInfo tbi)
		{
			ShutterActivity.this.onThumbnailClicked(tbi);
		}
	};

	private View _videoProgressFrame;
	private View _videoRecordingIndicatorDot;
	private ProgressBar _videoTimeProgress;

	private PluginResult _pluginResult;
	private ThumbsBarControl _thumbsBar;
	private SelectedItemInfo _photoInProcessing = null;

	private ResourceLookup _res;
	private int MODE_LABELS_TEXT_COLOR_ACTIVE;
	private int MODE_LABELS_TEXT_COLOR_INACTIVE;

	private SwipeGestureRecognizer _gestureRecognizer;
	private int SCREEN_WIDTH;
	//private int SCREEN_HEIGHT;

	private ProgressDialog _progressDialog;
	private int _topBlockHeight;

	private FocusAreaView _focusOverlay;

	private boolean _canBeTap;
	private float _tapX;
	private float _tapY;
    private boolean _showGalleryPickerFragmentOnResume;
    private boolean _hideGalleryPickerFragmentOnResume;

    @Override
	public void onCreate(Bundle savedInstanceState)
	{
		initializePicasso();

		DisplayMetrics metrics = new DisplayMetrics();
		Display display = getWindowManager().getDefaultDisplay();
		display.getMetrics(metrics);
		SCREEN_WIDTH = metrics.widthPixels;
		//SCREEN_HEIGHT = metrics.heightPixels;

		// since ShutterActivity is locked to the portrait mode in the manifest,
		// we can query the difference between portrait and natural display orientation here
		_naturalDisplayRotation = Utils.degreesFromSurfaceRotation(display.getRotation());
		if (DEBUG) Log.d(TAG, "Natural display rotation = "+_naturalDisplayRotation);

		TAP_JITTER_THRESHOLD = 10*metrics.density;

		_res = new ResourceLookup(this);
		_topBlockHeight = _res.getDimensionPixelSize("shutter_top_block_height");

		MODE_LABELS_TEXT_COLOR_ACTIVE = _res.getColor("shutter_mode_button_text_active");
		MODE_LABELS_TEXT_COLOR_INACTIVE = _res.getColor("shutter_mode_button_text_inactive");

		BUTTON_IMAGES[MODE_PHOTO - 1][BUTTON_STATE_RECORDING] = BUTTON_IMAGES[MODE_PHOTO - 1][BUTTON_STATE_NORMAL] = _res.drawable("photo_mode");

		BUTTON_IMAGES[MODE_VIDEO - 1][BUTTON_STATE_NORMAL] = _res.drawable("video_mode");
		BUTTON_IMAGES[MODE_VIDEO - 1][BUTTON_STATE_RECORDING] = _res.drawable("video_pause");

		BUTTON_IMAGES[MODE_GALLERY - 1][BUTTON_STATE_RECORDING] = BUTTON_IMAGES[MODE_GALLERY - 1][BUTTON_STATE_NORMAL] = _res.drawable("gallery_mode");

		Intent intent = getIntent();
		String paramsString = null;
		if (intent != null)
		{
			paramsString = intent.getStringExtra(EXTRA_PLUGIN_PARAMS);
		}
		_pluginParams = new CameraPluginParams(paramsString);
		_pluginResult = new PluginResult(this, _pluginParams);

		this.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);

		// set screen to max brightness - see http://stackoverflow.com/questions/11978042/android-screen-brightness-max-value
		// done here rather than onCreate, so that changing it in preferences takes effect without restarting app
		{
			WindowManager.LayoutParams layout = getWindow().getAttributes();
			layout.screenBrightness = WindowManager.LayoutParams.BRIGHTNESS_OVERRIDE_FULL;
			//layout.screenBrightness = WindowManager.LayoutParams.BRIGHTNESS_OVERRIDE_NONE;
			getWindow().setAttributes(layout);
		}


		super.onCreate(savedInstanceState);

		setContentView(_res.layout("shutter_layout"));

		_orientationEventListener = new OrientationEventListener(this)
		{
			@Override
			public void onOrientationChanged(int orientation)
			{
				orientationChangeHandler(orientation);
			}
		};

		_gestureRecognizer = new SwipeGestureRecognizer(this)
		{
			@Override
			public void onSwipeLeft()
			{
				onHorizontalSwipe(false);
			}

			@Override
			public void onSwipeRight()
			{
				onHorizontalSwipe(true);
			}
		};

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        _cam = new CameraAccess(this, _pluginParams, this);
        _surfaceView = (SurfaceView) findViewById(_res.id("shutter_surface"));
        _surfaceView.getHolder().addCallback(_cam);

        setupControls();

        PermissionsHelper.requestCamera(this, new PermissionsHelper.PermissionRequestResultListener()
        {
            @Override
            public void onPermissionResult(boolean success)
            {
                if (success)
                {
                    onCameraAccessGranted();
                }
                else
                {
                    onCameraAccessError(_pluginParams.loc().getCameraLockedErrorMessage());
                }
            }
        }, _pluginParams.loc().getCameraPermissionExplanation());

	}

    private void onCameraAccessGranted()
    {
        if (DEBUG) Log.d(TAG, "onCameraAccessGranted()");

        _cam.setCameraPermissionGranted(true);
        if (_activityResumed) _cam.onResume(); // calling onResume() here to start camera preview
    }

    private void requestFullScreenMode()
	{
		if (android.os.Build.VERSION.SDK_INT >= 19)
		{
			getWindow().getDecorView().setSystemUiVisibility(
					View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
							View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION |
							View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN |
							View.SYSTEM_UI_FLAG_HIDE_NAVIGATION |
							View.SYSTEM_UI_FLAG_FULLSCREEN |
							View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
		}
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus)
	{
		super.onWindowFocusChanged(hasFocus);
		if (DEBUG) Log.d(TAG, "onWindowFocusChanged > hasFocus = "+hasFocus);
		if (hasFocus)
		{
			requestFullScreenMode();
		}
	}

	private boolean isTapOnButton(View v, float x, float y)
	{
		if (v.getVisibility() != View.VISIBLE) return false;

		int left = v.getLeft();
		int top = v.getTop() + _topBlockHeight;

		Rect rc = new Rect(left, top, left + v.getWidth(), top + v.getHeight());
		return rc.contains((int)x, (int)y);
	}

	private void onPreviewTapped(float tapX, float tapY)
	{
		if (isGalleryMode()) return;
		if (DEBUG) Log.d(TAG, "onPreviewTapped x = "+tapX+" tapY = "+tapY);
		if (tapY >= _bottomBlock.getTop() + _topBlockHeight) return;

		if (isTapOnButton(_switchCameraButton, tapX, tapY)) return;
		if (isTapOnButton(_flashModeButton, tapX, tapY)) return;

		float x = tapX / (float)_layoutRoot.getWidth();
		float y = tapY / (float)_layoutRoot.getHeight();
		_cam.doAutoFocusAtPoint(x, y);
	}

	@Override
	public boolean dispatchTouchEvent(@NonNull MotionEvent ev)
	{
		// we should prevent gesture recognition in the top-bar area (there is a HorizontalScrollView
		// which can be swiped left or right
		if (ev.getY() > _topBlockHeight)
		{
			_gestureRecognizer.onTouch(ev);
			switch (ev.getActionMasked())
			{
				case MotionEvent.ACTION_DOWN:
					_canBeTap = true;
					_tapX = ev.getX();
					_tapY = ev.getY();
					break;

				case MotionEvent.ACTION_UP:
					if (_canBeTap)
					{
						onPreviewTapped(_tapX, _tapY);
					}
					break;

				case MotionEvent.ACTION_CANCEL:
				case MotionEvent.ACTION_OUTSIDE:
					_canBeTap = false;
					break;

				case MotionEvent.ACTION_MOVE:
					if (Math.abs(_tapX - ev.getX()) > TAP_JITTER_THRESHOLD ||
							Math.abs(_tapY - ev.getY()) > TAP_JITTER_THRESHOLD)
					{
						_canBeTap = false;
					}
					break;
			}
		}
		return super.dispatchTouchEvent(ev);
	}

	private void initializePicasso()
	{
		if (_picasso_initialized) return;
		_picasso_initialized = true;
		Picasso.Builder builder = new Picasso.Builder(this);
		int memCacheSize = Math.round(PICASSO_MEM_CACHE_SIZE_PERCENT * Runtime.getRuntime().maxMemory());
		if (DEBUG) Log.d(TAG, "Picasso memory cache size is "+memCacheSize);
		builder.memoryCache(new LruCache(memCacheSize));
		builder.downloader(new UrlConnectionDownloader(this));
		builder.addRequestHandler(new PicassoVideoThumbnailRequestHandler());
		builder.listener(new Picasso.Listener()
		{
			@Override
			public void onImageLoadFailed(Picasso picasso, Uri uri, Exception exception)
			{
				if (DEBUG) Log.e(TAG, "Picasso: error loading image, uri = "+uri, exception);
			}
		});
		Picasso.setSingletonInstance(builder.build());
	}

	final private View.OnClickListener _galleryModeClick = new View.OnClickListener()
	{
		@Override
		public void onClick(View v)
		{
			onGalleryModeButtonClicked();
		}
	};
	final private View.OnClickListener _photoModeClick = new View.OnClickListener()
	{
		@Override
		public void onClick(View v)
		{
			if (v instanceof TextView) onPhotoModeButtonClicked();
			else onTakePictureClicked();
		}
	};
	final private View.OnClickListener _videoModeClick = new View.OnClickListener()
	{
		@Override
		public void onClick(View v)
		{
			if (v instanceof TextView) onVideoModeButtonClicked();
			else onRecordVideoClicked();
		}
	};


	private void setupControls()
	{
        if (DEBUG) Log.d(TAG, "setupControls()");
		_layoutRoot = (ViewGroup) findViewById(_res.id("shutter_layout_root"));

		_switchCameraButton = findViewById(_res.id("shutter_switch_camera"));
		if (CameraAccess.getCamerasCount() <= 1) _switchCameraButton.setVisibility(View.GONE);
		else
		{
			_switchCameraButton.setOnClickListener(new View.OnClickListener()
			{
				@Override
				public void onClick(View v)
				{
					_cam.switchCamera();
				}
			});
		}

		_flashModeButton = findViewById(_res.id("shutter_flash_mode"));
		_flashModeButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				_cam.switchFlashMode();
			}
		});

		_uploadButton = findViewById(_res.id("shutter_done"));
		_uploadButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				onUploadButtonClicked();
			}
		});
		_cancelButton = findViewById(_res.id("shutter_cancel"));
		_cancelButton.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				onCancelButtonClicked();
			}
		});


		_bottomBlock = (FriendlyFrameLayout) findViewById(_res.id("shutter_bottom_block"));
		_bottomBlock.setOnLayoutChangeListener(new View.OnLayoutChangeListener()
		{
			@Override
			public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom)
			{
				_bottomBlock.setOnLayoutChangeListener(null);
				layoutActiveModeButtons();
			}
		});

		_videoProgressFrame = findViewById(_res.id("shutter_video_progress_frame"));
		_videoRecordingIndicatorDot = findViewById(_res.id("shutter_video_recording_indicator"));
		_videoTimeProgress = (ProgressBar) findViewById(_res.id("shutter_video_progress_bar"));

		_timeTicker = (TextView) findViewById(_res.id("shutter_video_time_ticker"));

		updateCameraProgressControls(true);

		_thumbsBar = new ThumbsBarControl(this, _pluginResult);

		updateUploadButtonVisibility();

		setupModeControls();

		_focusOverlay = (FocusAreaView) _layoutRoot.findViewById(_res.id("shutter_focus_area"));
	}

	private void setupModeControls()
	{
		int newMode = MODE_NOT_SET;
        _activeButtons.clear();
        _activeLabels.clear();

		TextView galleryLabel = (TextView) findViewById(_res.id("shutter_show_gallery_label"));
		galleryLabel.setText(_pluginParams.loc().getGalleryLabel());
		galleryLabel.setOnClickListener(_galleryModeClick);
		_galleryButton = findViewById(_res.id("shutter_show_gallery"));
		_galleryButton.setTag(MODE_GALLERY);
		BUTTON_LOOKUP[MODE_GALLERY - 1] = _galleryButton;
		//_galleryButton.setOnClickListener(_galleryModeClick);
		if (_pluginParams.isGalleryAllowed())
		{
			_activeButtons.add(_galleryButton);
			_activeLabels.add(galleryLabel);
			newMode = MODE_GALLERY;
		}
		else
		{
			_galleryButton.setVisibility(View.GONE);
			galleryLabel.setVisibility(View.GONE);
		}

		TextView cameraLabel = (TextView) findViewById(_res.id("shutter_take_picture_label"));
		cameraLabel.setText(_pluginParams.loc().getPhotoLabel());
		cameraLabel.setOnClickListener(_photoModeClick);
		_cameraButton = findViewById(_res.id("shutter_take_picture"));
		_cameraButton.setTag(MODE_PHOTO);
		BUTTON_LOOKUP[MODE_PHOTO - 1] = _cameraButton;
		_cameraButton.setOnClickListener(_photoModeClick);
		if (_pluginParams.isPhotoAllowed())
		{
			_activeButtons.add(_cameraButton);
			_activeLabels.add(cameraLabel);
			newMode = MODE_PHOTO;
		}
		else
		{
			_cameraButton.setVisibility(View.GONE);
			cameraLabel.setVisibility(View.GONE);
		}

		TextView videoLabel = (TextView) findViewById(_res.id("shutter_record_video_label"));
		videoLabel.setText(_pluginParams.loc().getVideoLabel());
		videoLabel.setOnClickListener(_videoModeClick);
		_videoButton = findViewById(_res.id("shutter_record_video"));
		_videoButton.setTag(MODE_VIDEO);
		BUTTON_LOOKUP[MODE_VIDEO - 1] = _videoButton;
		_videoButton.setOnClickListener(_videoModeClick);
		if (_pluginParams.isVideoAllowed())
		{
			_activeButtons.add(_videoButton);
			_activeLabels.add(videoLabel);
			if (newMode != MODE_PHOTO) newMode = MODE_VIDEO;
		}
		else
		{
			_videoButton.setVisibility(View.GONE);
			videoLabel.setVisibility(View.GONE);
		}

		switchToMode(newMode);

	}

	private boolean isPhotoMode()
	{
		return _currentMode == MODE_PHOTO;
	}

	private boolean isVideoMode()
	{
		return _currentMode == MODE_VIDEO;
	}

	private boolean isGalleryMode()
	{
		return _currentMode == MODE_GALLERY;
	}

	private void setButtonTransform(View btn, TextView label, int left, int top, boolean active, boolean initialSetup)
	{
		btn.setClickable(active);

		int clrFrom = label.getCurrentTextColor();
		int clrTo = active ? MODE_LABELS_TEXT_COLOR_ACTIVE : MODE_LABELS_TEXT_COLOR_INACTIVE;
		ObjectAnimator color = ObjectAnimator.ofInt(label, "textColor", clrFrom, clrTo);
		color.setEvaluator(ArgbEvaluatorCompat.getInstance());
		color.start();

		final float inactiveBtnScale = 0.7f;
		final float activeBtnScale = 1.0f;
		float newScale = active ? 1.0f : inactiveBtnScale;
		if (initialSetup)
		{
			btn.setScaleX(newScale);
			btn.setScaleY(newScale);
			btn.setTranslationX(left);
			btn.setTranslationY(top);
			btn.setAlpha(active ? 1.0f : 0f);
			setModeButtonImageState(_currentMode, BUTTON_STATE_NORMAL);
		}
		else
		{
			final int dur = 400;
			if (active)
			{
				Interpolator ip = new LinearInterpolator();
				float curX = btn.getTranslationX();
				ObjectAnimator alpha = ObjectAnimator.ofFloat(btn, "alpha", 0f, 1f);
				alpha.setInterpolator(new DecelerateInterpolator(3));
				ObjectAnimator transX = ObjectAnimator.ofFloat(btn, "translationX", curX, (float)left, (float)left, (float)left);
				float curScale = btn.getScaleX();
				ObjectAnimator scaleX = ObjectAnimator.ofFloat(btn, "scaleX", curScale, 1.3f, activeBtnScale);
				ObjectAnimator scaleY = ObjectAnimator.ofFloat(btn, "scaleY", curScale, 1.3f, activeBtnScale);
				alpha.setDuration(dur).setInterpolator(ip);
				transX.setDuration(dur).setInterpolator(ip);
				scaleX.setDuration(dur).setInterpolator(ip);
				scaleY.setDuration(dur).setInterpolator(ip);
				alpha.start();
				transX.start();
				scaleX.start();
				scaleY.start();
			}
			else
			{
				ViewPropertyAnimator anim = btn.animate().translationX(left).translationY(top)
						.alpha(0f)
						.setInterpolator(new DecelerateInterpolator(3))
						.setDuration(dur)
						.setListener(null);
				anim.scaleX(newScale).scaleY(newScale);
				anim.start();
			}
		}
	}

	private int findActiveButtonIdx()
	{
		for (int ii = 0; ii < _activeButtons.size(); ii++)
		{
			int mode = (Integer) _activeButtons.get(ii).getTag();
			if (mode == _currentMode)
			{
				return ii;
			}
		}
		return -1;
	}

	private void onHorizontalSwipe(boolean swipeRight)
	{
		int activeIdx = findActiveButtonIdx();
		if (activeIdx < 0) return;
		activeIdx += swipeRight ? -1 : 1;
		if (activeIdx > _activeLabels.size() - 1) activeIdx = 0;
		if (activeIdx < 0) activeIdx = _activeLabels.size() - 1;
		View newActive = _activeLabels.get(activeIdx);
		newActive.performClick();
	}

	// That function should only be called when all views have been laid out
	private void layoutActiveModeButtons()
	{
		if (_activeButtons.isEmpty()) return;

		int activeIdx = findActiveButtonIdx();
		if (activeIdx < 0) return;

		View active = _activeButtons.get(activeIdx);
		TextView activeLabel = _activeLabels.get(activeIdx);
		final int btnW = active.getWidth();
		final int btnH = active.getHeight();
		final int wrapW = _bottomBlock.getWidth();
		final int wrapH = _bottomBlock.getHeight();
		// active button is always placed in the center
		final int activeLeft = wrapW / 2 - btnW / 2;
		final int activeRight = activeLeft + btnW;
		int labelsPadding = _res.getDimensionPixelSize("shutter_mode_button_label_padding");
		final int labelsH = activeLabel.getHeight() - labelsPadding;
		final int top = labelsH + (wrapH  - labelsH) / 2 - btnH / 2;

		boolean buttonsNeedInitialization = false;
		if (active.getTranslationX() == 0) buttonsNeedInitialization = true;

		if (buttonsNeedInitialization && _activeLabels.size() == 3)
		{
			// center the middle button, not the whole labels block
			View modeLabelsWrap = findViewById(_res.id("shutter_mode_labels_wrap"));
			View middle = _activeLabels.get(1);
			int centerX = modeLabelsWrap.getLeft() + middle.getLeft() + middle.getWidth() / 2;
			int realCenterX = wrapW / 2;
			modeLabelsWrap.setTranslationX(realCenterX - centerX);
		}

		setButtonTransform(active, activeLabel, activeLeft, top, true, buttonsNeedInitialization);

		// layout other buttons
		final int buttonsHorSpacing = _res.getDimensionPixelSize("shutter_mode_buttons_hor_spacing");
		int left = activeLeft - btnW + buttonsHorSpacing;
		for (int ii = activeIdx - 1; ii >= 0; ii--)
		{
			View btn = _activeButtons.get(ii);
			//left -= btnW + buttonsHorSpacing;
			TextView label = _activeLabels.get(ii);
			setButtonTransform(btn, label, left, top, false, buttonsNeedInitialization);
		}

		left = activeRight + buttonsHorSpacing;
		for (int ii = activeIdx + 1; ii < _activeButtons.size(); ii++)
		{
			View btn = _activeButtons.get(ii);
			TextView label = _activeLabels.get(ii);
			setButtonTransform(btn, label, left, top, false, buttonsNeedInitialization);
			//left += btnW + buttonsHorSpacing;
		}

	}

	private int getFixedOrientationAngles(int angle)
	{
		if (angle < 45 || angle >= 315) return 0;
		if (angle >= 45 && angle < 135) return 90;
		if (angle >= 135 && angle < 225) return 180;
		if (angle >= 225 && angle < 315) return 270;
		return 0;
	}

	private void orientationChangeHandler(int orientation)
	{
		//if (DEBUG) Log.d(TAG, "Orientations: "+orientation);
		if (orientation == OrientationEventListener.ORIENTATION_UNKNOWN) return;


		if (_currentMode == MODE_GALLERY)
		{
			// gallery uses fixed screen orientation
			orientation = FIXED_UI_ORIENTATION_ANGLE;
		}

		//if (DEBUG) Log.d(TAG, "Sensor orientation > " + orientation);
		orientation = (orientation + _naturalDisplayRotation) % 360;

		int fixed = getFixedOrientationAngles(orientation);
		if (fixed == _lastDeviceOrientation) return;
		_lastDeviceOrientation = fixed;

		// rotate all controls to the new orientation
		int rot = 360 - _lastDeviceOrientation;
		if (rot > 180) rot = rot - 360;
		_currentControlsRotationAngle = rot;

		if (DEBUG) Log.d(TAG, "Rotation > " + rot);
		_flashModeButton.animate().rotation(rot).start();
		_switchCameraButton.animate().rotation(rot).start();
		_cameraButton.animate().rotation(rot).start();
		_videoButton.animate().rotation(rot).start();
		_galleryButton.animate().rotation(rot).start();
		_uploadButton.animate().rotation(rot).start();
		_cancelButton.animate().rotation(rot).start();

		_thumbsBar.rotateThumbs(rot);

		int absRot = Math.abs(rot);
		int barLeft, barTop;
		if (absRot == 0 || absRot == 180)
		{
			// portrait mode - no need to offset progress bar position
			barLeft = 0;
			barTop = 0;
		}
		else
		{
			// landscape mode, push the bar to the side of the screen
			int barW = _videoProgressFrame.getWidth();
			int barMargin = _res.getDimensionPixelSize("shutter_progress_bar_landscape_left_margin");
			barLeft = barW / 2 - barMargin;
			if (rot > 0) barLeft = -barLeft;
			// barTop should point to the center of the visible preview frame
			int verticalSpace = _layoutRoot.getHeight() - _topBlockHeight
					- _res.getDimensionPixelSize("shutter_bottom_block_height") - _res.getDimensionPixelSize("shutter_progress_bar_landscape_top_margin");
			barTop = -verticalSpace / 2;
		}
		_videoProgressFrame.animate().rotation(rot).translationX(barLeft).translationY(barTop).start();
	}

	@Override
	protected void onPause()
	{
        if (DEBUG) Log.d(TAG, "onPause()");
        _activityResumed = false;
		super.onPause();
		_orientationEventListener.disable();
        if (_cam != null) _cam.onPause();
	}

	@Override
	protected void onResume()
	{
        if (DEBUG) Log.d(TAG, "onResume()");
		super.onResume();
        _activityResumed = true;

		_orientationEventListener.enable();
		if (_cam != null) _cam.onResume();
		requestFullScreenMode();

        if (_showGalleryPickerFragmentOnResume) showGalleryPickerFragment();
        if (_hideGalleryPickerFragmentOnResume) hideGalleryPickerFragment();
	}

	@Override
	public void onCameraSwitched(boolean backFacing)
	{
		@DrawableRes int resId = backFacing ? _res.drawable("camera_rear") : _res.drawable("camera_front");
		((ImageView) _switchCameraButton).setImageResource(resId);
	}

	@Override
	public void onFlashModeChanged(int newFlashMode)
	{
		int resId;
		switch (newFlashMode)
		{
			case CameraAccess.FLASH_AUTO:
				resId = _res.drawable("flash_auto");
				break;
			case CameraAccess.FLASH_OFF:
				resId = _res.drawable("flash_off");
				break;
			case CameraAccess.FLASH_ON:
				resId = _res.drawable("flash_on");
				break;

			default:
				_flashModeButton.setVisibility(View.GONE);
				return;
		}

		_flashModeButton.setVisibility(View.VISIBLE);
		((ImageView) _flashModeButton).setImageResource(resId);
	}

	private void onPhotoModeButtonClicked()
	{
		if (!isPhotoMode()) tryToSwitchToMode(MODE_PHOTO);
	}

	private void onTakePictureClicked()
	{

		if (_pluginResult.getItemCount() >= _pluginParams.getMaxAllowedPhotosCount())
		{
			showMaxPhotosReachedAlert();
			return;
		}

		if (!_cam.readyToTakePicture()) return;

		_cameraButton.setClickable(false);

		String tmpPath = Utils.getTempFileName(this, ".jpg");
		_photoInProcessing = new SelectedItemInfo(tmpPath, _thumbnailClickListener);

		int orientation = _cam.getPhotoOrientation(_lastDeviceOrientation);
		_photoInProcessing.setPhotoOrientation(orientation);
		_photoInProcessing.setThumbOrientation(0);

		_thumbsBar.createNewBarThumbnail(_photoInProcessing);

		_cam.takePicture();

	}


	private void onGalleryModeButtonClicked()
	{
		if (_currentMode != MODE_GALLERY)
		{
			tryToSwitchToMode(MODE_GALLERY);
		}
	}

	private void updateCameraProgressControls(boolean updateText)
	{
		long t;
		CapturedVideoParts parts = _pluginResult.getVideoParts();
		if (parts != null) t = Math.max(0, parts.getRemainingTime() - parts.getCurrentVideoPartDuration());
		else t = _pluginParams.getMaxVideoTimeMS();

		if (updateText)	_timeTicker.setText(Utils.formatTimeCounter(t));
		long maxTime = _pluginParams.getMaxVideoTimeMS();
		final int maxProgress = _videoTimeProgress.getMax();
		int progress = maxProgress;
		if (maxTime != 0) progress = (int) Math.round((double)maxProgress * (maxTime - t) / (double) maxTime);
		if (progress > maxProgress) progress = maxProgress;
		_videoTimeProgress.setProgress(progress);
	}

	private long _lastProgressUpdaterTick;

	private Runnable _videoProgressUpdaterRunnable = new Runnable()
	{
		@Override
		public void run()
		{
			long curTick = SystemClock.uptimeMillis();
			boolean updateDot = curTick - _lastProgressUpdaterTick > RECORDING_DOT_BLINK_PERIOD;
			if (updateDot) _lastProgressUpdaterTick = curTick;

			_pluginResult.getVideoParts().updateCurrentVideoDuration();
			updateCameraProgressControls(updateDot);

			if (updateDot)
			{
				boolean dotVisible = _videoRecordingIndicatorDot.getAlpha() != 0;
				_videoRecordingIndicatorDot.setAlpha(dotVisible ? 0f : 1f);
			}

			_layoutRoot.postDelayed(_videoProgressUpdaterRunnable, VIDEO_PROGRESS_TIMER_TIMEOUT);
		}
	};

	private void startVideoRecording()
	{
		CapturedVideoParts parts = _pluginResult.getVideoParts();
		if (parts != null && parts.getRemainingTime() < 900)
		{
			Toast.makeText(this, _pluginParams.loc().getMaxVideoText(), Toast.LENGTH_LONG).show();
			return;
		}
		// start video recording
		String resultPath = null;
		if (parts == null)
		{
			resultPath = Utils.getTempFileName(this, "123.mp4");
			int orientation = _cam.getPhotoOrientation(_lastDeviceOrientation);
			parts = new CapturedVideoParts(resultPath, _pluginParams.getMaxVideoTimeMS());
			parts.setVideoOrientation(orientation);
			_pluginResult.setVideoParts(parts);
		}

		String partPath = Utils.getTempFileName(this, ".mp4");
		parts.addVideoPart(partPath);

		if (resultPath != null)
		{
			_photoInProcessing = new SelectedItemInfo(resultPath, _thumbnailClickListener);
			_photoInProcessing.setThumbOrientation(0);
			_photoInProcessing.setPhotoOrientation(parts.getVideoOrientation());
			_thumbsBar.createNewBarThumbnail(_photoInProcessing);
		}

		if (_cam.startVideoRecording(partPath, (int) parts.getRemainingTime(), parts.getVideoOrientation()))
		{
			_lastProgressUpdaterTick = SystemClock.uptimeMillis();
			parts.setLastPartRecordingStartedTime(_lastProgressUpdaterTick + 100);
			parts.setCurrentVideoPartDuration(0);
			_layoutRoot.postDelayed(_videoProgressUpdaterRunnable, VIDEO_PROGRESS_TIMER_TIMEOUT);
			setModeButtonImageState(MODE_VIDEO, BUTTON_STATE_RECORDING);

			updateUploadButtonVisibility();
			showCameraModeButtons(false);
		}
	}

	private void showCameraModeButtons(boolean show)
	{
		_flashModeButton.animate().alpha(show ? 1f : 0f).start();
		_switchCameraButton.animate().alpha(show ? 1f : 0f).start();
	}

	@Override
	public void onVideoRecordingStopped(boolean errorHappened)
	{
		_layoutRoot.removeCallbacks(_videoProgressUpdaterRunnable);
		_videoRecordingIndicatorDot.setAlpha(0f);

		CapturedVideoParts parts = _pluginResult.getVideoParts();
		if (parts != null)
		{
			if (parts.getPartsCount() == 1)
			{
				if (errorHappened)
				{
					discardRecordedVideo();
				}
				else
				{
					_photoInProcessing.setPhotoDataSaved(true);
					int cropSize = Math.min(_layoutRoot.getWidth(), _layoutRoot.getHeight());
					ImageProcessorPipeline.Pipeline pipe = ImageProcessorPipeline.loadFrom(parts.getPartPath(0), true, 0)
							.addCenterCropJob(IMAGE_JOB_ID_CROP_FOR_CAPTURE_ANIMATION, _photoInProcessing, cropSize);
					if (_pluginParams.isGenerateThumbnailsRequested())
					{
						pipe.addGenerateThumbnailJob(IMAGE_JOB_ID_GENERATE_THUMBNAIL,
								_photoInProcessing,
								_pluginParams.shouldGenerateThumbnailsAsFiles(),
								Utils.getTempFileName(this, ".jpg"),
								_pluginParams.getMaxThumbnailWidth(), _pluginParams.getMaxThumbnailHeight(),
								_pluginParams.getMinThumbnailWidth(), _pluginParams.getMinThumbnailHeight(),
								_pluginParams.getQuality());
					}
					pipe.setCallback(this)
					.runAsync();

					parts.onLastPartCompleted();
				}
			}
			else
			{
				// that is not the first video part, so on error just discard last recorded part
				if (errorHappened) parts.discardLastVideoPart();
				else parts.onLastPartCompleted();
			}
		}
		setModeButtonImageState(MODE_VIDEO, BUTTON_STATE_NORMAL);
		updateUploadButtonVisibility();
	}

	private void onVideoModeButtonClicked()
	{
		if (!isVideoMode()) tryToSwitchToMode(MODE_VIDEO);
	}

	private void onRecordVideoClicked()
	{
		if (!_cam.readyToTakePicture()) return;

		if (_cam.isRecording())
		{
			_cam.stopVideoRecording();
		}
		else
		{
			startVideoRecording();
		}
	}

	private void updateUploadButtonVisibility()
	{
		boolean show = false;
		if (isVideoMode())
		{
			CapturedVideoParts parts = _pluginResult.getVideoParts();
			if (!_cam.isRecording() && parts != null && parts.getPartsCount() > 0) show = true;
		}
		else
		{
			show = _pluginResult.getItemCount() > 0;
		}

		_uploadButton.setEnabled(show);
		_uploadButton.animate().alpha(show ? 1f : 0f).start();
	}

	private void onUploadButtonClicked()
	{
		_progressDialog = null;
		_pluginResult.prepareResult(_currentMode, new PluginResult.OnPluginResultReadyListener() {
            private ProgressBar _progressBar;
            private CircularProgressDrawable _progressDrawable;

            @Override
			public void showWaitDialog()
			{
				_progressDialog = ProgressDialog.show(ShutterActivity.this, null, null, true);
                // to remove default dialog background in 5.0+
                _progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
				_progressDialog.setContentView(_res.layout("simple_square_wait_dialog"));
                _progressBar = (ProgressBar)_progressDialog.getWindow().getDecorView().findViewById(_res.id("result_progress_bar"));
                _progressDrawable = new CircularProgressDrawable(ShutterActivity.this);
                _progressBar.setIndeterminateDrawable(_progressDrawable);
			}

            @Override
            public void setProgress(double progress)
            {
                if (_progressBar != null)
                {
                    boolean needProgress = progress >= 0;
                    if (needProgress) _progressDrawable.setProgress(progress * 0.95 + 0.05);
                    _progressDrawable.setIndeterminate(!needProgress);
                }
            }

            @Override
			public void onResultReady(boolean cancelled)
			{
				if (_progressDialog != null) _progressDialog.dismiss();
				if (!cancelled) submitResult();
			}
		});
	}

	private void submitResult()
	{
		Intent intent = new Intent();
		String tmp = Utils.getTempFileName(this, ".tmp");
		_pluginResult.saveResultToFile(tmp);
		intent.putExtra(EXTRA_PLUGIN_RESULT, tmp);
		setResult(Activity.RESULT_OK, intent);
		finish();
	}

	private void onCancelButtonClicked()
	{
		this.onBackPressed();
	}

	@Override
	public void onBackPressed()
	{
		confirmDiscardOfCurrentItemsIfNeeded(new Runnable()
		{
			@Override
			public void run()
			{
				setResult(Activity.RESULT_CANCELED);
				ShutterActivity.super.onBackPressed();
			}
		}, null);
	}

	private void setModeButtonImageState(int mode, int state)
	{
		if (mode == MODE_NOT_SET) return;

		ImageView btn = (ImageView) BUTTON_LOOKUP[mode - 1];
		btn.setImageResource(BUTTON_IMAGES[mode - 1][state]);
	}

	private void switchToMode(int newMode)
	{
		// 1. Set current buttons' image state to INACTIVE
		setModeButtonImageState(_currentMode, BUTTON_STATE_NORMAL);
		_currentMode = newMode;

		layoutActiveModeButtons();
		cleanupCurrentItems();

        if (_cam != null)
        {
            switch (newMode)
            {
                case MODE_GALLERY:
                    startGalleryPickerMode();
                    break;

                case MODE_PHOTO:
                    startPhotoCaptureMode();
                    break;

                case MODE_VIDEO:
                    startVideoCaptureMode();
                    break;
            }
        }

		updateUploadButtonVisibility();
	}

	private void cleanupCurrentItems()
	{
		cleanupTempFiles(this);
		clearSelectedItems();
		CapturedVideoParts parts = _pluginResult.getVideoParts();
		if (parts != null)
		{
			parts.deletePartsFiles();
			_pluginResult.setVideoParts(null);
			_videoTimeProgress.setProgress(0);
		}
	}

	@SuppressWarnings("ResultOfMethodCallIgnored")
	public static void cleanupTempFiles(Context ctx)
	{
		String temp = Utils.getTempDirectoryPath(ctx);
		File tmp = new File(temp);
		File[] files = tmp.listFiles();
		if (files == null) return;
		for (File fl : files)
		{
			if (!fl.isDirectory()) fl.delete();
		}

	}

	private void startVideoCaptureMode()
	{
        PermissionsHelper.requestRecordAudio(this, new PermissionsHelper.PermissionRequestResultListener()
        {
            @Override
            public void onPermissionResult(boolean success)
            {
                // Allow to record view without audio or fail here?
                _cam.setVideoModeActive(true);
                hideGalleryPickerFragment();
                showVideoProgress(true);
            }
        }, _pluginParams.loc().getRecordAudioExplanation());
	}

	private void showVideoProgress(boolean show)
	{
		_videoProgressFrame.animate().alpha(show ? 1f : 0f).start();
	}

	private void startPhotoCaptureMode()
	{
		_cam.setVideoModeActive(false);
		showVideoProgress(false);
		hideGalleryPickerFragment();
	}

	private void hideGalleryPickerFragment()
	{
		if (_pickerFragment == null || !_pickerFragment.isAdded() || _pickerFragment.isHidden()) return;

		_pickerFragment.cleanup();

        _hideGalleryPickerFragmentOnResume = true;
		try
		{
			FragmentManager fm = getSupportFragmentManager();
			fm.beginTransaction()
					.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
					.hide(_pickerFragment).commit();
            _hideGalleryPickerFragmentOnResume = false;
		}
		catch (RuntimeException e)
		{
			Log.e(TAG, "Error in hideGalleryPickerFragment()", e);
		}
	}

	private void startGalleryPickerMode()
	{
		showVideoProgress(false);
        //stopPreview();
        clearSelectedItems();

        PermissionsHelper.requestWriteExternalStorage(this, new PermissionsHelper.PermissionRequestResultListener()
        {
            @Override
            public void onPermissionResult(boolean success)
            {
                if (success)
                {
                    showGalleryPickerFragment();
                }
            }
        }, _pluginParams.loc().getStorageAccessExplanation());

	}

    private void showGalleryPickerFragment()
    {
        if (!_activityResumed)
        {
            if (DEBUG) Log.e(TAG, "Activity is not resumed yet to manipulate fragments");
            _showGalleryPickerFragmentOnResume = true;
            return;
        }

        _showGalleryPickerFragmentOnResume = false;

        FragmentManager fm = getSupportFragmentManager();
        if (_pickerFragment == null)
        {
            _pickerFragment = new GalleryPickerFragment();
            _pickerFragment.setThumbnailBarCallback(ShutterActivity.this);
            _pickerFragment.setPluginParams(_pluginParams);
            fm.beginTransaction()
                    .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                    .add(_res.id("shutter_gallery_content_frame"), _pickerFragment)
                    .commit();
        }
        else
        {
            fm.beginTransaction()
                    .setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out)
                    .show(_pickerFragment).commit();
        }
    }

    private void confirmDiscardOfCurrentItemsIfNeeded(@Nullable final Runnable yesAction, @Nullable final Runnable noAction)
	{
		if ((isPhotoMode() || isVideoMode()) && _pluginResult.getItemCount() > 0)
		{
			// show move away warning message
			new AlertDialog.Builder(this, AlertDialog.THEME_HOLO_LIGHT)
					.setMessage(isPhotoMode() ? _pluginParams.loc().getMoveFromPhotosText() :
							_pluginParams.loc().getMoveFromVideosText())
					.setPositiveButton(_pluginParams.loc().getAlertConfirmText(), new DialogInterface.OnClickListener()
					{
						public void onClick(DialogInterface dialog, int which)
						{
							if (yesAction != null) yesAction.run();
						}
					})
					.setNegativeButton(_pluginParams.loc().getAlertCancelText(), new DialogInterface.OnClickListener()
					{
						public void onClick(DialogInterface dialog, int which)
						{
							if (noAction != null) noAction.run();
						}
					})
					.show();

		}
		else
		{
			if (yesAction != null) yesAction.run();
		}

	}

	private void tryToSwitchToMode(final int newMode)
	{
		// prevent switches to other modes while recording video
		if (_cam.isRecording()) return;

		confirmDiscardOfCurrentItemsIfNeeded(new Runnable()
		{
			@Override
			public void run()
			{
				switchToMode(newMode);
			}
		}, null);
	}

	@Override
	public int addNewSelectedItem(final View imageOriginView, final Bitmap thumbBmp,
								  final SelectedItemInfo sii, boolean isVideo)
	{
		_thumbsBar.createNewBarThumbnail(sii);

		if (_pluginParams.isGenerateThumbnailsRequested())
		{
			ImageProcessorPipeline.loadFrom(sii.getOriginalPath(), isVideo, sii.getPhotoOrientation())
					.addGenerateThumbnailJob(IMAGE_JOB_ID_GENERATE_THUMBNAIL, sii,
							_pluginParams.shouldGenerateThumbnailsAsFiles(),
							Utils.getTempFileName(this, ".jpg"),
							_pluginParams.getMaxThumbnailWidth(), _pluginParams.getMaxThumbnailHeight(),
							_pluginParams.getMinThumbnailWidth(), _pluginParams.getMinThumbnailHeight(),
							_pluginParams.getQuality())
					.setCallback(this)
					.runAsync();
		}

		_layoutRoot.post(new Runnable()
		{
			@Override
			public void run()
			{
				_thumbsBar.animateThumbnailAddition(imageOriginView, false, sii.getThumbnailView(),
						thumbBmp, thumbBmp, sii.getThumbOrientation(), sii);
			}
		});

		updateUploadButtonVisibility();

		return _pluginResult.getItemCount() - 1;
	}

	@Override
	public void removeSelectedItem(String itemPath)
	{
		if (itemPath == null) return;

		_thumbsBar.removeItem(itemPath);
		updateUploadButtonVisibility();
	}

	@Override
	public void clearSelectedItems()
	{
		_thumbsBar.clearItems();
		updateUploadButtonVisibility();
	}

	@Override
	public void onShutter()
	{
		//showCaptureAnimationFromPreviewFrame();
	}

	@Override
	public void onMaxVideoDurationReached()
	{
		Toast.makeText(this, _pluginParams.loc().getMaxVideoText(), Toast.LENGTH_LONG).show();
	}

	@Override
	public void resizeCameraPreview(float cameraAspectRatio)
	{
		FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) _surfaceView.getLayoutParams();
		// try to fully occupy the screen width (in portrait)
		int previewH = (int)(SCREEN_WIDTH * cameraAspectRatio);
		lp.width = SCREEN_WIDTH;
		lp.height = previewH;
		lp.bottomMargin = 0;
		_surfaceView.setLayoutParams(lp);
	}

	@Override
	public void updateFocusArea(int state, float x, float y)
	{
		int vw = _layoutRoot.getWidth();
		int vh = _layoutRoot.getHeight();
		int cx = (int) (x * vw);
		int cy = (int) (y * vh);
		int fw = _focusOverlay.getWidth();
		int fh = _focusOverlay.getHeight();
		int left = cx - fw / 2;
		int right = left + fw;
		int top = cy - fh / 2;
		int bottom = top + fh;
		if (left < 0)
		{
			 cx += -left;
		}
		if (right > vw)
		{
			cx -= right - vw;
		}
		if (top < 0)
		{
			cy += -top;
		}
		if (bottom > vh)
		{
			cy -= bottom - vh;
		}
		_focusOverlay.setCenter(cx, cy);
		_focusOverlay.setFocusState(state);
	}

	@Override
	public void onCameraAccessError(String errMessage)
	{
		new AlertDialog.Builder(this)
				.setMessage(errMessage)
				.setPositiveButton(_pluginParams.loc().getAlertOkText(), new DialogInterface.OnClickListener()
				{
					public void onClick(DialogInterface dialog, int which)
					{
						onCancelButtonClicked();
					}
				})
				.show();

	}

	@Override
	public void onTransformationJobDone(int jobId, Object tag, Bitmap bmp)
	{
		if (jobId == IMAGE_JOB_ID_CROP_FOR_CAPTURE_ANIMATION)
		{
			SelectedItemInfo sii = (SelectedItemInfo)tag;

			sii.setThumbOrientation(0);
			ImageView iv = sii.getThumbnailView();
			iv.setRotation(_currentControlsRotationAngle);
			iv.setScaleType(ImageView.ScaleType.CENTER_CROP);
			_thumbsBar.animateThumbnailAddition(_surfaceView, true, iv, bmp, bmp, 0, sii);
		}
		else
		if (jobId == IMAGE_JOB_ID_CROP_FOR_THUMB_BAR)
		{
			SelectedItemInfo sii = (SelectedItemInfo)tag;
			sii.setThumbOrientation(0);
			ImageView iv = sii.getThumbnailView();
			iv.setRotation(_currentControlsRotationAngle);
			if (iv.getDrawable() == null)
			{
				// if the thumbnail image is not set, then capture animation is still running,
				// so we should not set the thumbnail here. Instead save it for capture animation
				// end listener to use (see _thumbsBar.animateThumbnailAddition() )
				sii.setThumbnailBitmap(bmp);
			}
			else
			{
				// if the thumbnail is set, that means that capture animation was finished
				// and larger capture bitmap was set in place of the thumbnail bitmap
				// just replace it with actual small thumbnail to free up memory, occupied by
				// the capture animation image
				iv.setImageBitmap(bmp);
			}

		}

	}

	@Override
	public void onSaveJobDone(int jobId, Object tag, String destPath, boolean success)
	{
		if (DEBUG) Log.d(TAG, "onSaveJobDone > success = " + success + " path = " + destPath);

		if (jobId == IMAGE_JOB_ID_SAVE_PHOTO_TO_FILE)
		{
			// send plugin result, if waiting for the file save complete event
			SelectedItemInfo sii = (SelectedItemInfo)tag;
			sii.setPhotoDataSaved(true);
			_pluginResult.onBackgroundTaskStateUpdated();
		}
	}

	@Override
	public void onBase64ThumbnailReady(int jobId, Object tag, String thumbData, int thumbW, int thumbH)
	{
		if (jobId == IMAGE_JOB_ID_GENERATE_THUMBNAIL)
		{
			SelectedItemInfo sii = (SelectedItemInfo)tag;

			sii.setThumbData(thumbData);
			sii.setThumbWidth(thumbW);
			sii.setThumbHeight(thumbH);
		}
	}

	@Override
	public void onThumbnailFileSaved(int jobId, Object tag, String thumbFile, int thumbW, int thumbH, boolean success)
	{
		if (jobId == IMAGE_JOB_ID_GENERATE_THUMBNAIL && success)
		{
			SelectedItemInfo sii = (SelectedItemInfo)tag;

			sii.setThumbData(null);
			sii.setThumbFilePath(thumbFile);
			sii.setThumbWidth(thumbW);
			sii.setThumbHeight(thumbH);
		}

	}

    @Override
    public void onJobFailed(int jobId, Object tag)
    {
        SelectedItemInfo sii = (SelectedItemInfo)tag;
        _thumbsBar.removeItem(sii);
    }

    @Override
	public void onPictureTaken(@NonNull byte[] jpegData)
	{
		_cameraButton.setClickable(true);
		if (_photoInProcessing == null) return;


		int cropSize = Math.min(_layoutRoot.getWidth(), _layoutRoot.getHeight());
		ImageProcessorPipeline.Pipeline pipe = ImageProcessorPipeline.loadFrom(jpegData, _photoInProcessing.getPhotoOrientation());
		pipe.addCenterCropJob(IMAGE_JOB_ID_CROP_FOR_CAPTURE_ANIMATION, _photoInProcessing, cropSize)
				.addSaveToFileJob(IMAGE_JOB_ID_SAVE_PHOTO_TO_FILE, _photoInProcessing,
						_photoInProcessing.getPhotoOrientation(),
						_photoInProcessing.getOriginalPath(),
						_pluginParams.getQuality(), false);
		if (_pluginParams.isGenerateThumbnailsRequested())
		{
			pipe.addGenerateThumbnailJob(IMAGE_JOB_ID_GENERATE_THUMBNAIL, _photoInProcessing,
					_pluginParams.shouldGenerateThumbnailsAsFiles(),
					Utils.getTempFileName(this, ".jpg"),
					_pluginParams.getMaxThumbnailWidth(), _pluginParams.getMaxThumbnailHeight(),
					_pluginParams.getMinThumbnailWidth(), _pluginParams.getMinThumbnailHeight(),
					_pluginParams.getQuality());
		}
		pipe.addCenterCropJob(IMAGE_JOB_ID_CROP_FOR_THUMB_BAR, _photoInProcessing, _thumbsBar.getThumbnailSize())
				.setCallback(this)
				.runAsync();

		updateUploadButtonVisibility();
	}

	private void discardRecordedVideo()
	{
		_pluginResult.getVideoParts().deletePartsFiles();
		_pluginResult.setVideoParts(null);
		_photoInProcessing = null;
		updateCameraProgressControls(true);
		showCameraModeButtons(true);
	}

	private void onDeleteItem(SelectedItemInfo ph)
	{
		_thumbsBar.removeItem(ph);

		if (isPhotoMode())
		{
			File f = new File(ph.getOriginalPath());
			//noinspection ResultOfMethodCallIgnored
			f.delete();
		}
		else
		{
			discardRecordedVideo();
		}
		updateUploadButtonVisibility();
	}


	private void onThumbnailClicked(final SelectedItemInfo sii)
	{
		_thumbsBar.highlightSelectedPhotoThumb(sii, true);
		new AlertDialog.Builder(this, AlertDialog.THEME_HOLO_LIGHT)
				.setMessage(isPhotoMode() ? _pluginParams.loc().getDeletePhotoText() : _pluginParams.loc().getDeleteVideoText())
				.setPositiveButton(_pluginParams.loc().getAlertConfirmText(), new DialogInterface.OnClickListener()
				{
					public void onClick(DialogInterface dialog, int which)
					{
						_thumbsBar.highlightSelectedPhotoThumb(sii, false);
						onDeleteItem(sii);
					}
				})
				.setNegativeButton(_pluginParams.loc().getAlertCancelText(), new DialogInterface.OnClickListener()
				{
					public void onClick(DialogInterface dialog, int which)
					{
						_thumbsBar.highlightSelectedPhotoThumb(sii, false);
					}
				})
				.setOnCancelListener(new DialogInterface.OnCancelListener()
				{
					@Override
					public void onCancel(DialogInterface dialog)
					{
						_thumbsBar.highlightSelectedPhotoThumb(sii, false);
					}
				})
				.show();

	}


	private void showMaxPhotosReachedAlert()
	{
		new AlertDialog.Builder(this, AlertDialog.THEME_HOLO_LIGHT)
				.setMessage(_pluginParams.loc().getMaxPhotosText())
				.setPositiveButton(_pluginParams.loc().getAlertOkText(), new DialogInterface.OnClickListener()
				{
					public void onClick(DialogInterface dialog, int which)
					{
					}
				})
				.show();
	}

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        if (PermissionsHelper.handlePermissionsResult(requestCode, permissions, grantResults)) return;
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}
