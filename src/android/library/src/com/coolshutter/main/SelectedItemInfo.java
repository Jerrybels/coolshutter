package com.coolshutter.main;

import android.graphics.Bitmap;
import android.util.Log;
import android.widget.ImageView;

/**
 * Created by const on 26/03/15.
 * Project: CoolShutter
 */
class SelectedItemInfo
{
	private static final boolean DEBUG = PluginConfig.DEBUG;
	private static final String TAG = "SelectedItemInfo";

	public interface ThumbnailClickListener
	{
		void onThumbnailClicked(SelectedItemInfo tbi);
	}

	private String _originalPath;
	// flag should be set to true after the photo has been saved to disk
	private boolean _photoDataSaved = false;
	private final ThumbnailClickListener _onClickListener;
	private ImageView _thumbnailView;
	private Bitmap _thumbnailBitmap;
	private int _thumbOrientation = 0; // used for displaying correctly rotated thumbnail on the thumb bar
	private int _photoOrientation = 0; // used to pass along with the result to JS code
	private String _thumbData; // base64 encoded thumbnail in Data URI format
	private String _thumbFilePath; // thumbnail file path (if requested)
	private int _thumbWidth;
	private int _thumbHeight;
	private long _videoDuration = -1;

	public SelectedItemInfo(String photoPath, ThumbnailClickListener onClickListener)
	{
		_originalPath = photoPath;
		_onClickListener = onClickListener;
	}

	public String getOriginalPath()
	{
		return _originalPath;
	}

	public ImageView getThumbnailView()
	{
		return _thumbnailView;
	}

	public void setThumbnailView(ImageView thumbnailView)
	{
		_thumbnailView = thumbnailView;
	}

	public int getThumbOrientation()
	{
		return _thumbOrientation;
	}

	public void setThumbOrientation(int orientation)
	{
		_thumbOrientation = orientation;
	}

	public void callOnClickListener()
	{
		if (_onClickListener != null) _onClickListener.onThumbnailClicked(this);
	}

	public void setOriginalPath(String originalPath)
	{
		_originalPath = originalPath;
	}

	public void setThumbData(String thumbData)
	{
		if (DEBUG) Log.d(TAG, "setThumbData: " + thumbData);
		_thumbData = thumbData;
	}

	public String getThumbData()
	{
		if (DEBUG) Log.d(TAG, "getThumbData: " + _thumbData);

		return _thumbData;
	}

	public int getThumbHeight()
	{
		return _thumbHeight;
	}

	public void setThumbHeight(int thumbHeight)
	{
		_thumbHeight = thumbHeight;
	}

	public int getThumbWidth()
	{
		return _thumbWidth;
	}

	public void setThumbWidth(int thumbWidth)
	{
		_thumbWidth = thumbWidth;
	}

	public int getPhotoOrientation()
	{
		return _photoOrientation;
	}

	public void setPhotoOrientation(int photoOrientation)
	{
		_photoOrientation = photoOrientation;
	}

	public long getVideoDuration()
	{
		return _videoDuration;
	}

	public void setVideoDuration(long videoDuration)
	{
		_videoDuration = videoDuration;
	}

	public boolean isPhotoDataSaved()
	{
		return _photoDataSaved;
	}

	public void setPhotoDataSaved(boolean photoDataSaved)
	{
		_photoDataSaved = photoDataSaved;
	}

	public Bitmap getThumbnailBitmap()
	{
		return _thumbnailBitmap;
	}

	public void setThumbnailBitmap(Bitmap thumbnailBitmap)
	{
		_thumbnailBitmap = thumbnailBitmap;
	}

	public void setThumbFilePath(String thumbFilePath)
	{
		_thumbFilePath = thumbFilePath;
	}

	public String getThumbFilePath()
	{
		return _thumbFilePath;
	}
}
