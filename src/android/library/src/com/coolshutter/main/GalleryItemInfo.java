package com.coolshutter.main;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.util.LongSparseArray;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.coolshutter.util.HookedImageView;
import com.coolshutter.util.ResourceLookup;
import com.coolshutter.util.TickTockDrawable;
import com.coolshutter.util.Utils;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

/**
 * Created by Const on 11/6/2016.
 * Project: android
 */
class GalleryItemInfo extends RecyclerView.ViewHolder implements Target
{
    private final static boolean DEBUG = PluginConfig.DEBUG;
    private final static String TAG = GalleryItemInfo.class.getSimpleName();

    public interface GalleryItemInfoHost
    {
        void onThumbnailClicked(GalleryItemInfo gii);
    }

    public interface BitmapEventsListener
    {
        void onBitmapFailed(GalleryAdapter.MediaItem item);
    }

    final private GalleryItemInfoHost _callback;
    private final BitmapEventsListener _bitmapEventsListener;

    final private FrameLayout _rootView;
    final private HookedImageView _thumbnailView;
    final private ImageView _placeholderImg;
    final private ImageView _checkMarkView;
    final private TextView _videoDurationTV;
    final private View _tintOverlay;

    private GalleryAdapter.MediaItem _mediaItem;
    private boolean _isChecked = false;
    private boolean _thumbnailSet = false;

    private View.OnClickListener _onImageClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            GalleryItemInfo ti = (GalleryItemInfo) v.getTag();
            _callback.onThumbnailClicked(ti);
        }
    };

    public GalleryItemInfo(GalleryItemInfoHost host, BitmapEventsListener bitmapEventsListener,
                           View root, ResourceLookup res)
    {
        super(root);
        root.setTag(this);
        _callback = host;
        _bitmapEventsListener = bitmapEventsListener;
        Context ctx = root.getContext();
        _rootView = (FrameLayout) root;
        _thumbnailView = (HookedImageView) root.findViewById(res.id("gallery_item_image"));
        _checkMarkView = (ImageView) root.findViewById(res.id("gallery_item_selection_mark"));
        _videoDurationTV = (TextView) root.findViewById(res.id("gallery_item_video_duration"));
        _tintOverlay = root.findViewById(res.id("gallery_item_tint_overlay"));
        _placeholderImg = (ImageView) root.findViewById(res.id("gallery_item_placeholder"));
        _placeholderImg.setImageDrawable(new TickTockDrawable(ctx));

        root.setTag(this);
        _thumbnailView.setTag(this);
        _thumbnailView.setOnClickListener(_onImageClickListener);
    }

    public ImageView getThumbnailView()
    {
        return _thumbnailView;
    }

    public long getThumbnailId()
    {
        return _mediaItem == null ? -1 : _mediaItem.getId();
    }

    public String getSourceFilePath()
    {
        return _mediaItem == null ? null : _mediaItem.getData();
    }

    public void setChecked(boolean set, boolean animate)
    {
        if (set == _isChecked) return;
        _isChecked = set;
        float a = set ? 1.0f : 0f;
        if (animate)
        {
            _checkMarkView.animate().alpha(a).start();
            _tintOverlay.animate().alpha(a).start();
        }
        else
        {
            _checkMarkView.setAlpha(a);
            _tintOverlay.setAlpha(a);
        }
    }

    public boolean isChecked()
    {
        return _isChecked;
    }

    public boolean isThumbnailSet()
    {
        return _thumbnailSet;
    }

    private void setOrientation(int angle)
    {
        if (DEBUG) Log.d(TAG, "setOrientation, angle = "+angle);
        if (_thumbnailView != null) _thumbnailView.setRotation(angle);
    }

    public int getOrientation()
    {
        return _mediaItem == null ? 0 : _mediaItem.getOrientation();
    }

    private void showVideoDurationText(boolean show)
    {
        if (show) _videoDurationTV.setAlpha(0.0f);
        _videoDurationTV.setVisibility(show ? View.VISIBLE : View.INVISIBLE);
    }

    private void setVideoDuration(long duration)
    {
        if (duration >= 0) _videoDurationTV.setText(Utils.formatTimeCounter(duration));
    }

    @Override
    public String toString()
    {
        return "GalleryItemInfo{mi = " +_mediaItem + '}';
    }

    public FrameLayout getRootView()
    {
        return _rootView;
    }

    public long getVideoDurationTime()
    {
        return _mediaItem == null ? -1 : _mediaItem.getVideoDuration();
    }

    private void showPlaceholder(boolean show)
    {
        if (!show)
        {
            Drawable dr = _placeholderImg.getDrawable();
            if (dr != null && dr instanceof TickTockDrawable)
            {
                TickTockDrawable waitDrawable = (TickTockDrawable) dr;
                waitDrawable.resetCounter();
            }
        }
        else
        {
            _thumbnailView.setVisibility(View.INVISIBLE);
        }
        _placeholderImg.setVisibility(show ? View.VISIBLE : View.INVISIBLE);
    }

    public void onRecycled()
    {
        // reset previous media item state
        _thumbnailView.setImageDrawable(null);
        _mediaItem = null;
    }

    void bindData(Context ctx, GalleryAdapter.MediaItem mi, LongSparseArray<String> selectedItems, int thumbSize)
    {
        onRecycled();

        _mediaItem = mi;
        if (mi == null)
        {
            showPlaceholder(true);
            return;
        }

        boolean checked = selectedItems.get(mi.getId()) != null;

        setVideoDuration(mi.getVideoDuration());
        // Picasso handles thumbnail orientation itself, we don't need it here
        //setOrientation(mi.getOrientation());
        showVideoDurationText(mi.isVideo());

        setChecked(checked, false);

        // Picasso automatically resolves request for video preview to
        // request to a thumbnail
        Uri imageUri = Uri.withAppendedPath(mi.isVideo() ? MediaStore.Video.Media.EXTERNAL_CONTENT_URI :
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, String.valueOf(mi.getId()));

        // lets fit our thumbnail into MINI_KIND thumbnail size (MediaStore.Images.Thumbnails.MINI_KIND)
        // otherwise, Picasso will try to load from original JPG, which is much slower and hogs CPU
        // MINI_KIND: 512 x 384 thumbnail
        int loadSize = thumbSize > 384 ? 384 : thumbSize;
        Picasso.with(ctx)
                .load(imageUri)
                .resize(loadSize, loadSize)
                .centerCrop()
                .into(this);
    }

    @Override
    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from)
    {
        //if (DEBUG) Log.d(TAG, "onBitmapLoaded(), _id = "+getThumbnailId());
        if (_mediaItem == null) return;

        showPlaceholder(false);
        _thumbnailView.setImageBitmap(bitmap);
        _thumbnailView.setVisibility(View.VISIBLE);
        _thumbnailSet = true;
        if (_thumbnailView.getAlpha() < 0.9999f) _thumbnailView.animate().alpha(1.0f).start();

        if (_videoDurationTV.getVisibility() == View.VISIBLE)
        {
            _videoDurationTV.setAlpha(1.0f);
        }
    }

    @Override
    public void onBitmapFailed(Drawable errorDrawable)
    {
        //if (DEBUG) Log.e(TAG, "onBitmapFailed(), item = "+_mediaItem);
        if (_mediaItem == null) return;
        _bitmapEventsListener.onBitmapFailed(_mediaItem);
    }

    @Override
    public void onPrepareLoad(Drawable placeHolderDrawable)
    {
        //if (DEBUG) Log.d(TAG, "onPrepareLoad(), _id = "+getThumbnailId());

        showPlaceholder(true);
        _thumbnailSet = false;
        _thumbnailView.setImageDrawable(null);
        _thumbnailView.setAlpha(0.0f);
    }

}
