package com.coolshutter.main;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.coolshutter.util.MediaResizeUtils;
import com.coolshutter.util.Utils;
import com.coolshutter.util.VideoFileHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Const on 4/22/2015.
 * Project: CoolShutter
 */
public class PluginResult
{
    public interface OnPluginResultReadyListener
	{
		void showWaitDialog();
        void setProgress(double progress);
		void onResultReady(boolean cancelled);
	}


	private static final boolean DEBUG = PluginConfig.DEBUG;
	private static final String TAG = "PluginResult";

	static final public String RESULT_ITEMS = "items";

	private final CameraPluginParams _pluginParams;
	private CapturedVideoParts _videoParts = null;
	private final Context _context;

	private List<SelectedItemInfo> _selectedItems = new ArrayList<SelectedItemInfo>();
	private boolean _waitingForBackgroundTasks = false;
	private boolean _mediaFilesScanInProgress = false;
    private boolean _transcodeInProgress = false;

    private OnPluginResultReadyListener _resultCompletionListener;

	public PluginResult(Context ctx, CameraPluginParams pluginParams)
	{
		_context = ctx;
		_pluginParams = pluginParams;
	}

	public List<SelectedItemInfo> getItems()
	{
		return _selectedItems;
	}

	public int getItemCount()
	{
		return _selectedItems.size();
	}

	public void addItem(SelectedItemInfo selectedItemInfo)
	{
		_selectedItems.add(selectedItemInfo);
	}

	public void removeItem(SelectedItemInfo sii)
	{
		_selectedItems.remove(sii);
	}

	public SelectedItemInfo findItemByPath(String itemPath)
	{
		for (SelectedItemInfo sii : _selectedItems)
		{
			if (itemPath.equals(sii.getOriginalPath()))
			{
				return sii;
			}
		}
		return null;
	}

	public void clear()
	{
		_selectedItems.clear();
	}

	private List<String> getListOfSelectedItemsPaths()
	{
		List<String> res = new ArrayList<String>();
		for (SelectedItemInfo sii : _selectedItems)
		{
			res.add(sii.getOriginalPath());
		}
		return res;
	}

	public void saveResultToFile(String outFile)
	{
		JSONObject json = new JSONObject();
		JSONArray items = new JSONArray();
		try
		{
			for (SelectedItemInfo sii : _selectedItems)
			{
				JSONObject entry = new JSONObject();
				if (DEBUG) Log.d(TAG, "Result entry");
				String thumbData = sii.getThumbData();
				String thumbFilePath = sii.getThumbFilePath();
				if (thumbData != null || thumbFilePath != null)
				{
					if (DEBUG) Log.d(TAG, "Adding thumbnail data");
					if (thumbData != null) entry.put("thumbnailData", thumbData);
					if (thumbFilePath != null) entry.put("thumbnailUri", thumbFilePath);
					entry.put("thumbWidth", sii.getThumbWidth());
					entry.put("thumbHeight", sii.getThumbHeight());
				}
				entry.put("fileUri", sii.getOriginalPath());
				entry.put("orientation", sii.getPhotoOrientation());
				items.put(entry);
			}
			json.put(RESULT_ITEMS, items);
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}
		writeResultToFile(outFile, json);
	}

	private void writeResultToFile(String fileName, JSONObject json)
	{
		try
		{
			String jsonString = json.toString();
			OutputStream os = new FileOutputStream(fileName);
			byte[] data = jsonString.getBytes();
			os.write(data);
			os.close();
			if (DEBUG) {
				try
				{
					Log.d(TAG, "Result: "+json.toString(1));
				}
				catch (JSONException e)
				{

				}
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	private boolean needToCutPickedVideo()
	{
		return getItemCount() > 0 && _selectedItems.get(0).getVideoDuration() > _pluginParams.getMaxVideoTimeMS();
	}

    // this is called on UI thread, so we need to decide quickly
    // its ok even if the decision is not 100% correct -
    // the prorgess dialog would flick for a short time as a side effect of wrong decision
    private boolean needTimeToDownsizeMediaFiles()
    {
        if (!_pluginParams.shouldDownscalePhotosFromGallery() && !_pluginParams.shouldDownscaleVideosFromGallery()) return false;

        int phMaxW = _pluginParams.getMaxPhotoWidth();
        int vMaxW = _pluginParams.getMaxVideoWidth();

        for (SelectedItemInfo sii : _selectedItems)
        {
            boolean isVideo = (sii.getVideoDuration() >= 0);
            if (isVideo)
            {
                if (_pluginParams.shouldDownscaleVideosFromGallery() && vMaxW > 0) return true;
            }
            else
            {
                if (_pluginParams.shouldDownscalePhotosFromGallery() && phMaxW > 0) return true;
            }
        }
        return false;
    }

	private boolean needTimeToPrepareResult(int pickerMode)
	{
		switch (pickerMode)
		{
			case ShutterActivity.MODE_GALLERY:
				// check if we need to cut long video picked from gallery
				if (needToCutPickedVideo()) return true;
                if (needTimeToDownsizeMediaFiles()) return true;
				break;

			case ShutterActivity.MODE_VIDEO:
				// always return true for video mode: we need to process recorded video(s) to
				// produce single file
				return true;

			case ShutterActivity.MODE_PHOTO:
				if (_pluginParams.isAddToGalleryEnabled()) return true;
				break;
		}

		// check if we are still waiting for captired photo(s) to be saved to disk
		// or if we are waiting for base64 thumbnails to be generated
		return waitingForBackgroundTasksToComplete();
	}

	private boolean waitingForBackgroundTasksToComplete()
	{
        if (_transcodeInProgress)
        {
            if (DEBUG) Log.d(TAG, "waitingForBackgroundTasksToComplete > transcodeInProgress");
            return true;
        }

		if (_mediaFilesScanInProgress) {
			if (DEBUG) Log.d(TAG, "waitingForBackgroundTasksToComplete > mediaFilesScanInProgress");
			return true;
		}

		boolean needThumbnails = _pluginParams.isGenerateThumbnailsRequested();
		for (SelectedItemInfo sii : _selectedItems)
		{
			if (!sii.isPhotoDataSaved()) {
				if (DEBUG) Log.d(TAG, "waitingForBackgroundTasksToComplete > waiting for photo to be saved");
				return true;
			}
			if (needThumbnails && (sii.getThumbData() == null) && (sii.getThumbFilePath() == null)) {
				if (DEBUG) Log.d(TAG, "waitingForBackgroundTasksToComplete > waiting for thumbnail to be generated");
				return true;
			}
		}
		return false;
	}

	// this is called if new photo has been saved or base64 thumbnail generated
	public void onBackgroundTaskStateUpdated()
	{
		// update can get called even if we are not going to end activity right now
		// (i.e. user did not press the upload (->) button yet)
		if (!_waitingForBackgroundTasks) {
			if (DEBUG) Log.d(TAG, "onBackgroundTaskStateUpdated > not waiting for completion");
			return;
		}

		// We need to check if all photos are saved & all thumbnails generated
		// If so, plugin result should be sent to the calling activity
		if (!waitingForBackgroundTasksToComplete())
			_resultCompletionListener.onResultReady(false);
	}

	private void startAsyncResultTask(int pickerMode)
	{
		_resultCompletionListener.showWaitDialog();
		new AsyncResultTask().execute(pickerMode);
	}

	public void prepareResult(final int pickerMode, OnPluginResultReadyListener completionListener)
	{
		if (needTimeToPrepareResult(pickerMode))
		{
			_resultCompletionListener = completionListener;
			if (needToCutPickedVideo())
			{
				new AlertDialog.Builder(_context, android.R.style.Theme_DeviceDefault_Light_Dialog_Alert)
						.setMessage(_pluginParams.loc().formatVideoCutText((int) (_pluginParams.getMaxVideoTimeMS()/1000)))
						.setPositiveButton(_pluginParams.loc().getAlertConfirmText(), new DialogInterface.OnClickListener()
						{
							public void onClick(DialogInterface dialog, int which)
							{
								startAsyncResultTask(pickerMode);
							}
						})
						.setNegativeButton(_pluginParams.loc().getAlertCancelText(), new DialogInterface.OnClickListener()
						{
							public void onClick(DialogInterface dialog, int which)
							{
								_resultCompletionListener.onResultReady(true);
							}
						})
						.show();
			}
			else
			{
				startAsyncResultTask(pickerMode);
			}
		}
		else
		{
			completionListener.onResultReady(false);
		}
	}

	private class AsyncResultTask extends AsyncTask<Integer, Integer, Integer>
	{
		@Override
		protected Integer doInBackground(Integer... params)
		{
			int pickerMode = params[0];
			preparePluginResult(pickerMode);
			return null;
		}

		@Override
		protected void onPostExecute(Integer integer)
		{
			// try if everything is done already and we are good to go
			onBackgroundTaskStateUpdated();
		}
	}

	private void preparePluginResult(int pickerMode)
	{
		_waitingForBackgroundTasks = true;
		switch (pickerMode)
		{
			case ShutterActivity.MODE_GALLERY:
				// check if we need to cut long video picked from gallery
				cutLongVideoIfNeeded();
                _transcodeInProgress = downscaleMediaFiles();
				break;

			case ShutterActivity.MODE_VIDEO:
				if (_videoParts == null) throw new RuntimeException("No video parts captured yet");
				_videoParts.buildJoinedVideoFile();
				break;

			case ShutterActivity.MODE_PHOTO:
				break;
		}

		if (_pluginParams.isAddToGalleryEnabled() && pickerMode != ShutterActivity.MODE_GALLERY)
		{
			// move all files to the gallery folder
			String dcimPath = Utils.getCameraImagesFolder();
			for (SelectedItemInfo sii : _selectedItems)
			{
				String path = sii.getOriginalPath();
				File src = new File(path);
				File dst = new File(dcimPath, src.getName());
				if (Utils.moveFile(src, dst))
				{
					sii.setOriginalPath(dst.getAbsolutePath());
				}
			}
			final List<String> pathsToScan = getListOfSelectedItemsPaths();
			String pathsArray[] = new String[pathsToScan.size()];
			pathsToScan.toArray(pathsArray);
			_mediaFilesScanInProgress = true;
			MediaScannerConnection.scanFile(_context, pathsArray, null, new MediaScannerConnection.OnScanCompletedListener()
			{
				@Override
				public void onScanCompleted(String path, Uri uri)
				{
					pathsToScan.remove(path);
					int remainingItems = pathsToScan.size();
					if (DEBUG) Log.d(TAG, "Completed media scan of " + path + " remaining " + remainingItems);
					if (remainingItems == 0)
					{
						_mediaFilesScanInProgress = false;
						onBackgroundTaskStateUpdated();
					}
				}
			});
		}
	}

    private void cutLongVideoIfNeeded()
	{
		SelectedItemInfo sii = _selectedItems.get(0);
		if (sii.getVideoDuration() <= _pluginParams.getMaxVideoTimeMS()) return;

		String tmpFile = Utils.getTempFileName(_context, ".mp4");
		double endTime = _pluginParams.getMaxVideoTimeMS();
		try
		{
			if (DEBUG) Log.d(TAG, "Cutting video " + sii.getOriginalPath());
			VideoFileHelper.cutVideoFile(sii.getOriginalPath(), tmpFile, 0, endTime/1000.0);
			if (DEBUG) Log.d(TAG, "Video cut > output = " + tmpFile);
			sii.setOriginalPath(tmpFile);
		}
		catch (IOException e)
		{
			if (DEBUG) Log.e(TAG, "Error cutting video to time", e);
		}
	}

    private void onVideoTranscodingFinished()
    {
        _transcodeInProgress = false;
        onBackgroundTaskStateUpdated();
    }

    private boolean transcodeVideo(final SelectedItemInfo sii, int vMaxW, int vMaxH, int vMinW, int vMinH)
    {
        String resizedPath = Utils.getTempFileName(_context, ".mp4");
        return MediaResizeUtils.downscaleVideo(resizedPath, sii.getOriginalPath(), vMaxW, vMaxH, vMinW, vMinH,
                new MediaResizeUtils.VideoTranscoderListener()
        {
            @Override
            public void onTranscodingCompleted(String outPath)
            {
                sii.setOriginalPath(outPath);
                onVideoTranscodingFinished();
                _resultCompletionListener.setProgress(-1);
            }

            @Override
            public void onTranscodingProgress(double progress)
            {
                _resultCompletionListener.setProgress(progress);
            }

            @Override
            public void onTranscodingFailed()
            {
                onVideoTranscodingFinished();
            }
        });
    }

    // Returns true if long async operation was started
    private boolean downscaleMediaFiles()
    {
        if (!_pluginParams.shouldDownscalePhotosFromGallery() && !_pluginParams.shouldDownscaleVideosFromGallery()) return false;

        int phMaxW = _pluginParams.getMaxPhotoWidth();
        int phMaxH = _pluginParams.getMaxPhotoHeight();
        int phMinW = _pluginParams.getMinPhotoWidth();
        int phMinH = _pluginParams.getMinPhotoHeight();

        int vMaxW = _pluginParams.getMaxVideoWidth();
        int vMaxH = _pluginParams.getMaxVideoHeight();
        int vMinW = _pluginParams.getMinVideoWidth();
        int vMinH = _pluginParams.getMinVideoHeight();

        boolean asyncOpStarted = false;

        for (SelectedItemInfo sii : _selectedItems)
        {
            boolean isVideo = (sii.getVideoDuration() >= 0);
            String itemPath = sii.getOriginalPath();
            if (isVideo)
            {
                if (_pluginParams.shouldDownscaleVideosFromGallery()
						&& MediaResizeUtils.isVideoLargerThanRequested(itemPath, vMaxW, vMaxH))
                {
                    // resize video
                    asyncOpStarted |= transcodeVideo(sii, vMaxW, vMaxH, vMinW, vMinH);
                }
            }
            else
            {
                if (_pluginParams.shouldDownscalePhotosFromGallery()
						&& MediaResizeUtils.isPhotoLargerThanRequested(itemPath, phMaxW, phMaxH))
                {
                    String resizedPath = Utils.getTempFileName(_context, ".jpg");
                    boolean success = MediaResizeUtils.downscalePhoto(resizedPath, itemPath, sii.getPhotoOrientation(),
                            phMaxW, phMaxH, phMinW, phMinH, _pluginParams.getQuality());

                    if (success)
                    {
                        sii.setOriginalPath(resizedPath);
                    }
                    else
                    {
                        if (DEBUG) Log.e(TAG, "Couldn't downscale image: "+itemPath);
                    }
                }
            }
        }

        return asyncOpStarted;
    }

    public void setVideoParts(CapturedVideoParts videoParts)
	{
		_videoParts = videoParts;
	}

	public CapturedVideoParts getVideoParts()
	{
		return _videoParts;
	}
}
