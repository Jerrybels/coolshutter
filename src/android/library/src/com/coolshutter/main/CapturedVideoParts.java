package com.coolshutter.main;

import android.os.SystemClock;

import com.coolshutter.util.VideoFileHelper;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by const on 27/03/15.
 * Project: CoolShutter
 * The Android's MediaRecorder can't pause/resume video recording
 * Instead, we have to record each piece of the video as a separate file
 * and then glue those pieces together
 */

public class CapturedVideoParts
{
	// the name of the file with joined video parts
	final private String _resultFileName;

	List<String> _fileNames = new ArrayList<String>();
	private int _videoOrientation;
	private long _remainingTime;

	private long _lastPartRecordingStartedTime;
	private long _currentVideoPartDuration = 0;

	public CapturedVideoParts(String resultFileName, long maxVideoTime)
	{
		_resultFileName = resultFileName;
		_remainingTime = maxVideoTime;
	}

	public long getRemainingTime()
	{
		return _remainingTime;
	}

	public void onLastPartCompleted()
	{
		/*
		String path = getPartPath(getPartsCount() - 1);

		try
		{
			dur = VideoFileHelper.getVideoDuration(path);
			Log.e("VideoDuration", "Last part duration is " + dur +" ms, approximate duration = "+timerDuration);
			if (dur == 0) dur = timerDuration;
		}
		catch (IOException e)
		{
			e.printStackTrace();
			dur = timerDuration;
		} */

		_remainingTime -= _currentVideoPartDuration;
	}

	@SuppressWarnings("ResultOfMethodCallIgnored")
	public void discardLastVideoPart()
	{
		int sz = _fileNames.size();
		if (sz <= 0) return;
		String lastFileName = _fileNames.get(sz - 1);
		_fileNames.remove(sz - 1);

		File file = new File(lastFileName);
		if (file.exists()) file.delete();
	}


	public String getResultFileName()
	{
		return _resultFileName;
	}

	public void addVideoPart(String path)
	{
		_fileNames.add(path);
	}

	public int getPartsCount()
	{
		return _fileNames.size();
	}

	public String getPartPath(int idx)
	{
		return _fileNames.get(idx);
	}

	public void setVideoOrientation(int videoOrientation)
	{
		_videoOrientation = videoOrientation;
	}

	public int getVideoOrientation()
	{
		return _videoOrientation;
	}

	@SuppressWarnings("ResultOfMethodCallIgnored")
	public void deletePartsFiles()
	{
		for (String nm : _fileNames)
		{
			File file = new File(nm);
			if (file.exists()) file.delete();
		}
	}

	@SuppressWarnings("ResultOfMethodCallIgnored")
	public boolean buildJoinedVideoFile()
	{
		if (getPartsCount() == 0) return true;
		if (getPartsCount() == 1)
		{
			String part = getPartPath(0);
			File src = new File(part);
			src.renameTo(new File(_resultFileName));
			return true;
		}
		else
		{
			try
			{
				//long startTime = System.currentTimeMillis();
				VideoFileHelper.joinVideos(this);
				for (int ii = 0; ii < getPartsCount(); ii++)
				{
					File part = new File(getPartPath(ii));
					part.delete();
				}
				//long joinTime = System.currentTimeMillis() - startTime;
				//Log.e("JoinVideo", "Video join took " + joinTime +" ms");
				return true;
			}
			catch (IOException e)
			{
				e.printStackTrace();
				return false;
			}
		}
	}

	public long getCurrentVideoPartDuration()
	{
		return _currentVideoPartDuration;
	}

	public void setCurrentVideoPartDuration(long currentVideoPartDuration)
	{
		_currentVideoPartDuration = currentVideoPartDuration;
	}

//	public long getLastPartRecordingStartedTime()
//	{
//		return _lastPartRecordingStartedTime;
//	}
//
	public void setLastPartRecordingStartedTime(long lastPartRecordingStartedTime)
	{
		_lastPartRecordingStartedTime = lastPartRecordingStartedTime;
	}

	public void updateCurrentVideoDuration()
	{
		long cur = SystemClock.uptimeMillis();
		_currentVideoPartDuration = cur - _lastPartRecordingStartedTime;
	}


}
