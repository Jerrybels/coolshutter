package com.coolshutter.main;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.coolshutter.util.AnimatorListenerWrapper;
import com.coolshutter.util.HorizontalScrollViewWithFadingEdges;
import com.coolshutter.util.ImageViewWithOverlay;
import com.coolshutter.util.ResourceLookup;

/**
 * Created by Const on 4/1/2015.
 * Project: CoolShutter
 */
public class ThumbsBarControl
{
	private static final long THUMB_ADDITION_ANIMATION_DURATION = 300;
	final private static String TAG = "ThumbsBar";
	final private static boolean DEBUG = PluginConfig.DEBUG;

	final private Activity _host;

	private PluginResult _pluginResult;

	final private int _thumbSize;
	final private int _thumbsHorSpacing;

	private int _currentButtonRotationAngle = 0;
	private HorizontalScrollViewWithFadingEdges _thumbsScroller;
	private ViewGroup _thumbsContainer;
	private ViewGroup _layoutRoot;
	private int _thumbOverlayImageId;

	public ThumbsBarControl(Activity host, PluginResult pluginResult)
	{
		_host = host;
		_pluginResult = pluginResult;
		ResourceLookup res = new ResourceLookup(host);

		_thumbOverlayImageId = res.drawable("delete_thumbnail");

		_thumbSize = res.getDimensionPixelSize("shutter_thumbnail_size");
		_thumbsHorSpacing = res.getDimensionPixelSize("shutter_horizontal_gap_between_photo_thumbs");

		_layoutRoot = (ViewGroup) _host.findViewById(res.id("shutter_layout_root"));

		_thumbsScroller = (HorizontalScrollViewWithFadingEdges)host.findViewById(res.id("shutter_thumbs_scroller"));
		_thumbsContainer = (ViewGroup)host.findViewById(res.id("shutter_thumbs_container"));
		View thumbsLeftFadeOutEdgeView = host.findViewById(res.id("shutter_thumbs_left_fade_edge"));
		View thumbsRightFadeOutEdgeView = host.findViewById(res.id("shutter_thumbs_right_fade_edge"));
		_thumbsScroller.setLeftEdgeView(thumbsLeftFadeOutEdgeView);
		_thumbsScroller.setRightEdgeView(thumbsRightFadeOutEdgeView);
		_thumbsScroller.setOverScrollMode(View.OVER_SCROLL_NEVER);
		_thumbsScroller.setSmoothScrollingEnabled(true);
	}


	public void rotateThumbs(int rot)
	{
		_currentButtonRotationAngle = rot;
		for (SelectedItemInfo ph : _pluginResult.getItems())
		{
			View v = ph.getThumbnailView();
			if (v != null) v.animate().rotation(rot + ph.getThumbOrientation()).start();
		}
	}

	private Runnable _scrollThumbsRightRunnable = new Runnable()
	{
		@Override
		public void run()
		{
			_thumbsScroller.fullScroll(View.FOCUS_RIGHT);
		}
	};


	public void createNewBarThumbnail(SelectedItemInfo selectedItemInfo)
	{
		_pluginResult.addItem(selectedItemInfo);
		// add photo preview
		ImageViewWithOverlay iv = new ImageViewWithOverlay(_host);
		iv.setOverlayResource(_thumbOverlayImageId);
		iv.setTag(selectedItemInfo);
		iv.setScaleType(ImageView.ScaleType.CENTER_CROP);
		int rot = _currentButtonRotationAngle + selectedItemInfo.getThumbOrientation();
		iv.setRotation(rot);
		_thumbsContainer.addView(iv, _thumbSize, _thumbSize);

		// scroll only after the layout pass with the new content added
		_thumbsScroller.post(_scrollThumbsRightRunnable);

		LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) iv.getLayoutParams();
		lp.gravity = Gravity.CENTER_VERTICAL;
		lp.rightMargin = _thumbsHorSpacing;
		iv.setLayoutParams(lp);

		selectedItemInfo.setThumbnailView(iv);
		iv.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				SelectedItemInfo tbi = (SelectedItemInfo) v.getTag();
				if (tbi != null) tbi.callOnClickListener();
			}
		});
	}

	public void animateThumbnailAddition(View imageOriginView,
										 boolean useCroppedOrigin,
										 final ImageView thumbnailView,
										 final Bitmap srcImage,
										 final Bitmap thumbnailBmp,
										 int previewOrientation,
										 final SelectedItemInfo sii)
	{
		final ImageView preview = new ImageView(_host);
		preview.setScaleType(ImageView.ScaleType.FIT_CENTER);
		preview.setImageBitmap(srcImage);
		previewOrientation += _currentButtonRotationAngle;
		int previewSz = Math.min(imageOriginView.getWidth(), imageOriginView.getHeight());
		if (previewOrientation != 0)
		{
			preview.setScaleType(ImageView.ScaleType.MATRIX);
			Matrix rot = new Matrix();
		 	rot.postRotate(previewOrientation, previewSz/2, previewSz/2);
			preview.setImageMatrix(rot);
		}

		_layoutRoot.addView(preview, previewSz, previewSz);

		int rootPos[] = {0, 0};
		_layoutRoot.getLocationOnScreen(rootPos);

		int scrollerPos[] = {0, 0};
		_thumbsScroller.getLocationOnScreen(scrollerPos);

		int originView[] = {0, 0};
		imageOriginView.getLocationOnScreen(originView);

		int offsetY = useCroppedOrigin ? (_layoutRoot.getHeight() - previewSz) / 2 : 0;
		preview.setTranslationX(originView[0] - rootPos[0]);
		preview.setTranslationY(originView[1] - rootPos[1] + offsetY);


		int innerThumbLeft = thumbnailView.getLeft();
		// if the thumbnailView was just added to the scroller and the scroller didn't scroll completely to the right,
		// the thumbnailView coordinates can be outside of the scroller
		// We are going to fix that case and set the thumbnail position to be inside the scroller
		if (innerThumbLeft - _thumbsScroller.getScrollX() > _thumbsScroller.getWidth() - thumbnailView.getWidth() - _thumbsHorSpacing)
		{
			innerThumbLeft = _thumbsScroller.getWidth() + _thumbsScroller.getScrollX() - thumbnailView.getWidth() - _thumbsHorSpacing;
		}

		int thumbLeft = scrollerPos[0] - rootPos[0] + innerThumbLeft - _thumbsScroller.getScrollX();
		int thumbTop = scrollerPos[1] - rootPos[1] + thumbnailView.getTop();

		float scaleX = (float)_thumbSize / (float)previewSz;
		float scaleY = (float)_thumbSize / (float)previewSz;

		preview.setPivotX(0);
		preview.setPivotY(0);

		preview.setAlpha(0.5f);
		//final int thumbOrientation = previewOrientation;
		preview.animate().translationX(thumbLeft).translationY(thumbTop)
				.scaleX(scaleX).scaleY(scaleY).alpha(1)
				.setInterpolator(new DecelerateInterpolator())
				.setListener(new AnimatorListenerWrapper(new Runnable()
						{
							@Override
							public void run()
							{
								Bitmap finalBitmap = sii.getThumbnailBitmap();
								if (finalBitmap == null) {
									if (DEBUG) Log.e(TAG, "Capture animation > using capture bitmap for thumbnail!");
									finalBitmap = thumbnailBmp;
								}
								thumbnailView.setRotation(_currentButtonRotationAngle + sii.getThumbOrientation());
								thumbnailView.setImageBitmap(finalBitmap);
								_layoutRoot.removeView(preview);
								preview.setImageBitmap(null);
							}
						})
				).setDuration(THUMB_ADDITION_ANIMATION_DURATION).start();
	}

	public void highlightSelectedPhotoThumb(SelectedItemInfo selected, boolean turnHighlightOn)
	{
		for (SelectedItemInfo ph : _pluginResult.getItems())
		{
			if (ph != selected)
			{
				View v = ph.getThumbnailView();
				v.animate().alpha(turnHighlightOn ? 0.3f : 1.0f).start();
			}
		}
	}


	public void removeItem(SelectedItemInfo sii)
	{
		_pluginResult.removeItem(sii);
		removeSingleThumbView(sii.getThumbnailView());
		sii.setThumbnailView(null);
	}

	public void removeItem(String itemPath)
	{
		SelectedItemInfo sii = _pluginResult.findItemByPath(itemPath);
		if (sii == null) return;
		removeItem(sii);
	}

	private int getViewIndexInContainer(ViewGroup container, View view)
	{
		final int count = container.getChildCount();
		for (int ii = 0; ii < count; ii++)
		{
			View v = container.getChildAt(ii);
			if (v == view) return ii;
		}
		return -1;
	}

	private void removeSingleThumbView(final View thumbView)
	{
		if (thumbView == null) return;

		final int leftEdge = _thumbsScroller.getLeft();
		final int rightEdge = leftEdge + _thumbsScroller.getWidth();

		final int[] pos = {0, 0};
		thumbView.getLocationInWindow(pos);
		if (thumbView.getRotation() != 0)
		{
			final float[] offset = { 0, 0 };
			thumbView.getMatrix().mapPoints(offset);
			pos[0] -= offset[0];
			pos[1] -= offset[1];
		}
		final int thumbTop = pos[1];
		final int thumbLeft = _thumbsScroller.getLeft() + thumbView.getLeft() - _thumbsScroller.getScrollX();

		if ((thumbLeft + _thumbSize < leftEdge) || (thumbLeft > rightEdge))
		{
			_thumbsContainer.removeView(thumbView);
			return;
		}

		int thumbIdx = getViewIndexInContainer(_thumbsContainer, thumbView);
		View emptyView = null;
		if (thumbIdx >= 0)
		{
			final int thumbW = _thumbSize + _thumbsHorSpacing;
			final int childrenCount = _thumbsContainer.getChildCount();
			for (int ii = thumbIdx + 1; ii < childrenCount; ii++)
			{
				final View v = _thumbsContainer.getChildAt(ii);
				v.animate().translationX(-thumbW).setListener(new AnimatorListenerWrapper(new Runnable()
				{
					@Override
					public void run()
					{
						v.setTranslationX(0);
					}
				})).start();
			}
			LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(_thumbSize, _thumbSize);
			lp.rightMargin = _thumbsHorSpacing;
			emptyView = new View(_host);
			_thumbsContainer.addView(emptyView, thumbIdx, lp);
		}
		_thumbsContainer.removeView(thumbView);


		thumbView.setTranslationX(thumbLeft);
		thumbView.setTranslationY(thumbTop);

		_layoutRoot.addView(thumbView);

		final View tempView = emptyView;
		thumbView.animate().translationY(-2*_thumbSize)
				.setListener(new AnimatorListenerWrapper(new Runnable()
				{
					@Override
					public void run()
					{
						_layoutRoot.removeView(thumbView);
						if (tempView != null) _thumbsContainer.removeView(tempView);
					}
				}))
				.start();
	}

	public void clearItems()
	{
		_pluginResult.clear();
		if (_thumbsContainer.getChildCount() == 0) return;

		final int thumbW = _thumbSize + _thumbsHorSpacing;
		final int scrollX = _thumbsScroller.getScrollX();
		final int firstVisibleThumb = (scrollX + _thumbsHorSpacing) / thumbW;
		final int visibleCount = _thumbsScroller.getWidth() / thumbW;
		final int scrollerLeft = _thumbsScroller.getLeft();
		final int[] pos = {0, 0};
		_thumbsContainer.getChildAt(0).getLocationInWindow(pos);
		final int thumbTop = pos[1];

		if (DEBUG) Log.d(TAG, "clearItems animation: firstVisibleThumb = "+firstVisibleThumb+" visibleCount = "+visibleCount);

		int delay = 0;
		// we need to be able to calculate thumbs positions in the current state,
		// otherwise they could be changed when some items are removed from the thumbs scroller
		for (int ii = firstVisibleThumb + visibleCount - 1; ii >= firstVisibleThumb; ii--)
		{
			final View thumb = _thumbsContainer.getChildAt(ii);
			if (thumb == null) continue;

			int thumbLeft = scrollerLeft + ii*thumbW - scrollX;
			_thumbsContainer.removeView(thumb);

			thumb.setTranslationX(thumbLeft);
			thumb.setTranslationY(thumbTop);

			_layoutRoot.addView(thumb);

			thumb.animate().translationY(-2 * _thumbSize)
					.setListener(new AnimatorListenerWrapper(new Runnable()
					{
						@Override
						public void run()
						{
							_layoutRoot.removeView(thumb);
						}
					}))
					.setStartDelay(50*delay++)
					.start();
		}
		// remove remaining children (not animated)
		_thumbsContainer.removeAllViews();

	}

	public int getThumbnailSize()
	{
		return _thumbSize;
	}

}
