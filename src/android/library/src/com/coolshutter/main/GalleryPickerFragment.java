package com.coolshutter.main;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.util.LongSparseArray;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.coolshutter.util.GridItemOffsetDecoration;
import com.coolshutter.util.ResourceLookup;

import java.io.File;

import static com.coolshutter.main.GalleryAdapter.MODE_PHOTO;

/**
 * Created by const on 28/03/15.
 * Project: CoolShutter
 */
public class GalleryPickerFragment extends Fragment implements CompoundButton.OnCheckedChangeListener,
															   LoaderManager.LoaderCallbacks<Cursor>,
                                                                GalleryItemInfo.GalleryItemInfoHost
{
	private static final String TAG = "GalleryPickerFragment";
	private static final boolean DEBUG = PluginConfig.DEBUG;

	// callback used to
	public interface SelectedItemsController
	{
		// returns thumbnail idx
		int addNewSelectedItem(View imageOriginView, Bitmap thumbBmp, SelectedItemInfo ti, boolean isVideo);
		void removeSelectedItem(String itemPath);
		void clearSelectedItems();
	}

	private Context _ctx;

	private ResourceLookup _res;

	private SelectedItemsController _selectedItemsController;
	private CameraPluginParams _pluginParams;
	private ToggleButton _filterPhotos;
	private ToggleButton _filterVideos;
	private RecyclerView _grid;
	private GalleryAdapter _adapter;

	// maps thumbId -> full media file path
	private LongSparseArray<String> _selectedItems = new LongSparseArray<>();

	private SelectedItemInfo.ThumbnailClickListener _thumbnailClickListener = new SelectedItemInfo.ThumbnailClickListener()
	{
		@Override
		public void onThumbnailClicked(SelectedItemInfo tbi)
		{
			onThumbBarThumbnailClicked(tbi);
		}
	};


	public GalleryPickerFragment()
	{

	}

	public void setPluginParams(CameraPluginParams params)
	{
		_pluginParams = params;
	}

	public void setThumbnailBarCallback(SelectedItemsController selectedItemsHost)
	{
		_selectedItemsController = selectedItemsHost;
	}

	@SuppressWarnings("deprecation")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
	{
		_ctx = container.getContext();
		_res = new ResourceLookup(_ctx);

		View v = inflater.inflate(_res.layout("gallery_picker_fragment"), container, false);
		_filterPhotos = (ToggleButton)v.findViewById(_res.id("shutter_gallery_filter_photos"));
		_filterPhotos.setOnCheckedChangeListener(this);
		_filterVideos = (ToggleButton)v.findViewById(_res.id("shutter_gallery_filter_videos"));
		_filterVideos.setOnCheckedChangeListener(this);

		boolean selectVideoMode = false;
		if (!_pluginParams.isPhotoAllowed())
		{
			_filterPhotos.setEnabled(false);
            selectVideoMode = true;
			v.findViewById(_res.id("shutter_gallery_filter_photos_button")).setVisibility(View.GONE);
		}
		if (!_pluginParams.isVideoAllowed())
		{
			_filterVideos.setEnabled(false);
			if (selectVideoMode) throw new RuntimeException("At least one of photo or video gallery mode should be allowed to use this fragment");
			v.findViewById(_res.id("shutter_gallery_filter_videos_button")).setVisibility(View.GONE);
		}

		TextView videos = (TextView)v.findViewById(_res.id("shutter_gallery_filter_videos_text"));
		videos.setText(_pluginParams.loc().getVideosLabel());
		TextView photos = (TextView)v.findViewById(_res.id("shutter_gallery_filter_photos_text"));
		photos.setText(_pluginParams.loc().getPhotosLabel());

		_grid = (RecyclerView) v.findViewById(_res.id("shutter_gallery_grid"));

		_grid.setHasFixedSize(true);
		int columns = 3;
        int gridSpacing = _res.getDimensionPixelSize("shutter_gallery_grid_spacing");
		RecyclerView.LayoutManager gridLayoutManager = new GridLayoutManager(this.getContext(), columns, LinearLayoutManager.VERTICAL, false);

		_grid.setLayoutManager(gridLayoutManager);
		_grid.addItemDecoration(new GridItemOffsetDecoration(this.getContext(), _res.dimen("shutter_gallery_grid_spacing")));

		_adapter = new GalleryAdapter(_ctx, _res, this, _selectedItems);
        // Add footer to allow selection of items when grid is scrolled to the very bottom
        // Without footer the last row of items would be under the bottom buttons block
        int footerH = _ctx.getResources().getDimensionPixelOffset(_res.dimen("shutter_bottom_block_height"));
        _adapter.setFooterHeight(footerH);

        int scrWidth = _ctx.getResources().getDisplayMetrics().widthPixels;
        int colWidth = (scrWidth - (2 + (columns - 1)) * gridSpacing) / columns;
        _adapter.setColumnInfo(columns, colWidth);

		_grid.setAdapter(_adapter);
        _adapter.setCurrentMode(selectVideoMode ? GalleryAdapter.MODE_VIDEO : MODE_PHOTO);
        updateFilterButtonsState();

		return v;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState)
	{
		super.onActivityCreated(savedInstanceState);
		getLoaderManager().initLoader(0, null, this);
	}

	private void updateFilterButtonsState()
	{
		boolean photo = _adapter.isPhotoGalleryActive();
		_filterPhotos.setChecked(photo);
		_filterVideos.setChecked(!photo);
	}

	private boolean _allowCheckedChange = false;
	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
	{
		if (!isChecked) {
			if (!_allowCheckedChange) buttonView.setChecked(true);
			return;
		}
		int newMode = GalleryAdapter.MODE_NOT_SET;
		if (buttonView == _filterPhotos)
		{
			_allowCheckedChange = true;
			_filterVideos.setChecked(false);
			_allowCheckedChange = false;
			newMode = MODE_PHOTO;
		}
		if (buttonView == _filterVideos)
		{
			_allowCheckedChange = true;
			_filterPhotos.setChecked(false);
			_allowCheckedChange = false;
			newMode = GalleryAdapter.MODE_VIDEO;
		}

		switchFilterMode(newMode);
	}

	public void cleanup()
	{
		_selectedItems.clear();
		if (_adapter != null) _adapter.notifyDataSetChanged();
	}


	private void switchFilterMode(int newMode)
	{
		if (newMode == _adapter.getCurrentMode()) return;

		_adapter.setCurrentMode(newMode);

		_selectedItems.clear();
		_selectedItemsController.clearSelectedItems();
		getLoaderManager().restartLoader(0, null, this);
        _grid.smoothScrollToPosition(0);
	}

	private void onThumbBarThumbnailClicked(SelectedItemInfo tbi)
	{
		_selectedItemsController.removeSelectedItem(tbi.getOriginalPath());

		for (int ii = 0; ii < _selectedItems.size(); ii++)
		{
			String path = _selectedItems.valueAt(ii);
			if (path != null && path.equals(tbi.getOriginalPath()))
			{
				_selectedItems.removeAt(ii);
				//_adapter.notifyDataSetChanged();
				updateSelectedStateForVisibleGridItem(tbi.getOriginalPath(), false);
				break;
			}
		}
	}

	private void updateSelectedStateForVisibleGridItem(String originalPath, boolean setCheck)
	{
		final int childCount = _grid.getChildCount();
		for (int ii = 0; ii < childCount; ii++)
		{
			View item = _grid.getChildAt(ii);
			GalleryItemInfo gii = (GalleryItemInfo)item.getTag();
			if (gii != null)
			{
				if (originalPath.equals(gii.getSourceFilePath()))
				{
					gii.setChecked(setCheck, true);
				}
			}
		}
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args)
	{
		if (_adapter.isPhotoGalleryActive())
		{
			final String[] projection = {
					MediaStore.Images.Media._ID,
					MediaStore.Images.Media.DATA,
					MediaStore.Images.Media.ORIENTATION
			};
			return new CursorLoader(getActivity(), MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
					projection, MediaStore.Images.ImageColumns.SIZE + " > 0", null, MediaStore.Images.Media.DATE_TAKEN+" DESC"
			);
		}
		else
		if (_adapter.isVideoGalleryActive())
		{
			final String[] projection = {
					MediaStore.Video.Media._ID,
					MediaStore.Video.Media.DATA,
					MediaStore.Video.Media.DURATION
			};
			return new CursorLoader(getActivity(), MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
					projection, MediaStore.Images.ImageColumns.SIZE + " > 0", null, MediaStore.Video.Media.DATE_TAKEN+" DESC"
			);
		}

		return null;
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor data)
	{
		_adapter.setNewCursor(data);
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader)
	{
		_adapter.setNewCursor(null);
	}


    private void showMaxPhotosReachedAlert()
	{
		new AlertDialog.Builder(this.getActivity())
				.setMessage(_pluginParams.loc().getMaxPhotosText())
				.setPositiveButton(_pluginParams.loc().getAlertOkText(), new DialogInterface.OnClickListener()
				{
					public void onClick(DialogInterface dialog, int which) {}
				})
				.show();
	}

    public void clearCheckedItems()
    {
        for (int ii = 0, ll = _grid.getChildCount(); ii < ll; ii++)
        {
            View ch = _grid.getChildAt(ii);
            Object tag = ch.getTag();
            if (tag instanceof GalleryItemInfo)
            {
                GalleryItemInfo gii = (GalleryItemInfo)tag;
                if (gii.isChecked()) gii.setChecked(false, true);
            }
        }
    }

    // handle grid thumbnails clicks here - add/remove photos/videos
    @Override
    public void onThumbnailClicked(GalleryItemInfo gii)
	{
		// does not allow selection of files which are not available any more
		// references to those files can still be present in Media Library DB
		boolean fileExists = new File(gii.getSourceFilePath()).canRead();
		if (!fileExists) return;

		boolean checked = !gii.isChecked();

		if (checked && _selectedItems.size() >= _pluginParams.getMaxAllowedPhotosCount())
		{
			showMaxPhotosReachedAlert();
			return;
		}


		if (checked)
		{
			if (_adapter.isVideoGalleryActive())
			{
				// remove previously picked video
                clearCheckedItems();
                _selectedItems.clear();
				_selectedItemsController.clearSelectedItems();
			}
            gii.setChecked(checked, true);
			_selectedItems.append(gii.getThumbnailId(), gii.getSourceFilePath());
			ImageView thumbView = gii.getThumbnailView();
			Drawable dr = thumbView.getDrawable();
			Bitmap thumbBmp = null;
			if (dr instanceof BitmapDrawable)
			{
				thumbBmp = ((BitmapDrawable) dr).getBitmap();
			}
			SelectedItemInfo sii = new SelectedItemInfo(gii.getSourceFilePath(), _thumbnailClickListener);
			sii.setThumbOrientation(gii.getOrientation());
			sii.setPhotoOrientation(gii.getOrientation());
			sii.setVideoDuration(gii.getVideoDurationTime());
			sii.setPhotoDataSaved(true);
			_selectedItemsController.addNewSelectedItem(gii.getRootView(), thumbBmp, sii, _adapter.isVideoGalleryActive());
		}
		else
		{
            gii.setChecked(checked, true);
			_selectedItems.remove(gii.getThumbnailId());
			_selectedItemsController.removeSelectedItem(gii.getSourceFilePath());
		}
	}
}
