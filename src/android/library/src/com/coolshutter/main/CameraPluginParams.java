package com.coolshutter.main;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.InvalidParameterException;

/**
 * Created by const on 25/03/15.
 * Project: CoolShutter
 */
public class CameraPluginParams
{
	final private static int FLAG_ALLOW_PHOTO = 1;
	final private static int FLAG_ALLOW_VIDEO = 2;
	final private static int FLAG_ALLOW_GALLERY_PHOTO = 4;
	final private static int FLAG_ALLOW_GALLERY_VIDEO = 8;
	final private static int FLAG_ALLOW_ALL = FLAG_ALLOW_PHOTO | FLAG_ALLOW_VIDEO |
			FLAG_ALLOW_GALLERY_PHOTO | FLAG_ALLOW_GALLERY_VIDEO;

	final private String _originalParamsJSON;


	// 	quality: Quality of the saved image or video, expressed as a range of 0-100, where 100 is
	// typically full resolution with no loss from file compression. The default is 50. (Number)
	private int _quality = 90;

	// saveToGallery is the image saved also to the device or only returned to the app’s handler.
	// Default to false. (Boolean)
	private boolean _saveToGallery = true;

	// allowedSource : object containing between 1 to 4 elements,
	// that can be “photo”, “video”, “gallery photo”, “gallery video”.
	// Used to define what source would be available when the camera is launched
	// (other options would be greyed / not displayed).
	// Default to all 3 elements. (Object). Camera opens to the default available in this order :
	// photo > video > gallery photo > gallery video.
	private int _allowedSourceFlags = FLAG_ALLOW_ALL;

	// maxPhotos : max number of pics to take before asking the user to upload.
	// Default to 5. (Number)
	private int _maxPhotos = 15;

	// maxVideoTime : max time (in milliseconds) a user can use the video mode before upload.
	// Default to 30000. (Number)
	private long _maxVideoTimeMS = 30*1000;

	// generateThumbnails: boolean. Set this to true to generate thumbs for selected media items
	// thumbnails would be in JPEG format
	private boolean _generateThumbnailsRequested = true;

	// If this parameter is set to true, thumbnails would be saved to temporary files
	// otherwise, the JPEG data will be converted to BASE64 and passed to the JS side as strings
	private boolean _generateThumbnailsAsFiles = true;

	// Resulting thumbnails will be downscaled to fit inside
	// maxThumbnailWidth x maxThumbnailHeight pixels rectangle (default: 128)
	private int _maxThumbnailWidth = 320;
	private int _maxThumbnailHeight = 480;
	// if min width or height specified, then it takes precedence over max width and height
	private int _minThumbnailWidth = 240;
	private int _minThumbnailHeight = 240;

    // To specify desired images and video output resolution, use following set of parameters.
    // camera resolution would be selected to be between minMediaWidth x minMediaHeight and
    // maxMediaWidth x maxMediaHeight
    // set maxMediaWidth to 0 to use maximum available hardware resolution
    // limits for photo camera:
	private int _maxPhotoWidth = 1400;
	private int _maxPhotoHeight = 1000;
	private int _minPhotoWidth = 1000;
	private int _minPhotoHeight = 600;
    // limits for video camera:
	private int _maxVideoWidth = 1400;
	private int _maxVideoHeight = 1000;
	private int _minVideoWidth = 1000;
	private int _minVideoHeight = 600;

    private boolean _downscalePhotosFromGallery = true;
    private boolean _downscaleVideosFromGallery = true;

	private CameraPluginLocalization _loc = new CameraPluginLocalization();


    public CameraPluginParams(String params)
	{
		_originalParamsJSON = params;
		if (params == null) return;

		try
		{
			//Log.i("CameraPluginParams", params);
			JSONObject json = new JSONObject(params);
			_quality = json.optInt("quality", 90);
			_maxPhotos = json.optInt("maxPhotos", 5);
			_maxVideoTimeMS = json.optInt("maxVideoTime", 30)*1000;
			_saveToGallery = json.optBoolean("saveToGallery", false);
			_allowedSourceFlags = json.optInt("allowedSourceFlags", FLAG_ALLOW_ALL);
			_generateThumbnailsRequested = json.optBoolean("generateThumbnails", false);
			_generateThumbnailsAsFiles = json.optBoolean("thumbnailsAsFiles", true);
			_maxThumbnailWidth = json.optInt("maxThumbnailWidth", 240);
			_maxThumbnailHeight = json.optInt("maxThumbnailHeight", 320);
			_minThumbnailWidth = json.optInt("minThumbnailWidth", 120);
			_minThumbnailHeight = json.optInt("minThumbnailHeight", 120);

            _maxPhotoWidth = json.optInt("maxPhotoWidth", _maxPhotoWidth);
            _maxPhotoHeight = json.optInt("maxPhotoHeight", _maxPhotoHeight);
            _minPhotoWidth = json.optInt("minPhotoWidth", _minPhotoWidth);
            _minPhotoHeight = json.optInt("minPhotoHeight", _minPhotoHeight);

            _maxVideoWidth = json.optInt("maxVideoWidth", _maxVideoWidth);
            _maxVideoHeight = json.optInt("maxVideoHeight", _maxVideoHeight);
            _minVideoWidth = json.optInt("minVideoWidth", _minVideoWidth);
            _minVideoHeight = json.optInt("minVideoHeight", _minVideoHeight);

            if (_maxPhotoWidth != 0 && (_maxPhotoWidth < _minPhotoWidth && _maxPhotoHeight < _minPhotoHeight))
            {
                throw new InvalidParameterException("maxPhotoWidth(Height) should be > minPhotoWidth(Height)");
            }
            if (_maxVideoWidth != 0 && (_maxVideoWidth < _minVideoWidth && _maxVideoHeight < _minVideoHeight))
            {
                throw new InvalidParameterException("maxVideoWidth(Height) should be > minVideoWidth(Height)");
            }

            _downscalePhotosFromGallery = json.optBoolean("downscalePhotosFromGallery", _downscalePhotosFromGallery);
            _downscaleVideosFromGallery = json.optBoolean("downscaleVideosFromGallery", _downscaleVideosFromGallery);

			JSONObject locData = json.optJSONObject("localization");
			_loc.parseData(locData);
		}
		catch (JSONException e)
		{
			e.printStackTrace();
		}
	}

	public boolean isPhotoAllowed()
	{
		return (_allowedSourceFlags & FLAG_ALLOW_PHOTO) != 0;
	}

	public boolean isVideoAllowed()
	{
		return (_allowedSourceFlags & FLAG_ALLOW_VIDEO) != 0;
	}

	public boolean isPhotoGalleryAllowed()
	{
		return (_allowedSourceFlags & FLAG_ALLOW_GALLERY_PHOTO) != 0;
	}

	public boolean isVideoGalleryAllowed()
	{
		return (_allowedSourceFlags & FLAG_ALLOW_GALLERY_VIDEO) != 0;
	}

	public boolean isGalleryAllowed()
	{
		return isVideoGalleryAllowed() || isPhotoGalleryAllowed();
	}



	public int getMaxAllowedPhotosCount()
	{
		return _maxPhotos;
	}

	public long getMaxVideoTimeMS()
	{
		return _maxVideoTimeMS;
	}

	public int getQuality()
	{
		return _quality;
	}

	public boolean isAddToGalleryEnabled()
	{
		return _saveToGallery;
	}

	public int getMaxThumbnailWidth()
	{
		return _maxThumbnailWidth;
	}

	public int getMaxThumbnailHeight()
	{
		return _maxThumbnailHeight;
	}
	public int getMinThumbnailWidth()
	{
		return _minThumbnailWidth;
	}

	public int getMinThumbnailHeight()
	{
		return _minThumbnailHeight;
	}

	public boolean isGenerateThumbnailsRequested()
	{
		return _generateThumbnailsRequested;
	}

	public CameraPluginLocalization loc()
	{
		return _loc;
	}

	public boolean shouldGenerateThumbnailsAsFiles()
	{
		return _generateThumbnailsAsFiles;
	}

    public int getMaxPhotoWidth()
    {
        return _maxPhotoWidth;
    }

    public int getMaxPhotoHeight()
    {
        return _maxPhotoHeight;
    }

    public int getMaxVideoHeight()
    {
        return _maxVideoHeight;
    }

    public int getMaxVideoWidth()
    {
        return _maxVideoWidth;
    }

    public int getMinPhotoHeight()
    {
        return _minPhotoHeight;
    }

    public int getMinPhotoWidth()
    {
        return _minPhotoWidth;
    }

    public int getMinVideoHeight()
    {
        return _minVideoHeight;
    }

    public int getMinVideoWidth()
    {
        return _minVideoWidth;
    }

    public boolean shouldDownscalePhotosFromGallery()
    {
        return _downscalePhotosFromGallery;

	}
    public boolean shouldDownscaleVideosFromGallery()
    {
        return _downscaleVideosFromGallery;
    }
}
