package com.coolshutter.main;

import org.json.JSONObject;

/**
 * Created by Const on 4/2/2015.
 * Project: CoolShutter
 */
public class CameraPluginLocalization
{
	// maxPhotosText : the message that appears when the user try to take more than the
	// limited number of pictures. Default : “You have reached the limit number of photos!”. (String)
	private String _maxPhotosText = "You have reached the maximum number of photos!";

	// deletePhotoText : the message that appears when the user clicks on one of
	// the thumbnails in order to delete a picture.
	// Default : “Are you sure that you want to delete this photo ?”. (String)
	private String _deletePhotoText = "Are you sure that you want to delete this photo?";

	// deleteVideoText : the message to be displayed when user taps on the recorded video thumbnail
	private String _deleteVideoText = "Discard recorded video?";

	// moveFromPhotosText : the message that appears when the user tries to switch out from
	// photo mode and has pictures waiting for upload.
	// Default : “Are you sure that you want to move away and lose the pictures you took ?”. (String)
	private String _moveFromPhotosText = "Are you sure that you want to move away and lose the pictures you took?";

	// moveFromVideosText : the message that appears when the user tries to switch out
	// from video mode and has a video waiting for upload.
	// Default : “Are you sure that you want to move away and lose the video you took ?”. (String)
	private String _moveFromVideosText = "Are you sure that you want to move away and lose the video you took?";

	// maxVideoText : the message that appears when maxVideoTime is reached.
	// Default : “Maximum video length reached !”. (String)
	private String _maxVideoText = "Maximum video length reached!";
	private String _maxVideoTimeCutWarning = "This video is too long. Only the first %d seconds will be used.";

	// Text for alerts buttons
	private String _alertCancelText = "Cancel"; // = no
	private String _alertConfirmText = "OK";    //  = yes
	private String _alertOkText = "OK";         //  = OK

	// Mode buttons text
	private String _photoLabel = "PHOTO";
	private String _galleryLabel = "GALLERY";
	private String _videoLabel = "VIDEO";
	private String _videosLabel = "VIDEOS";
	private String _photosLabel = "PHOTOS";
	private String _cameraLockedErrorMessage = "Cannot initialize camera. Probably permission to use camera wasn't granted or it is in use by another app.";
	private String _cameraPermissionExplanation = "Camera access permission is required to capture photos or videos";
    private String _storageAccessExplanation = "The app needs access to the external storage folders to be able to save new photos and videos";
    private String _recordAudioExplanation = "The app needs access to the microphone to record video with audio";

	public CameraPluginLocalization()
	{
	}

	public void parseData(JSONObject locData)
	{
		if (locData == null) return;
		_maxPhotosText = locData.optString("maxPhotosText", _maxPhotosText);
		_deletePhotoText = locData.optString("deletePhotoText", _deletePhotoText);
		_deleteVideoText = locData.optString("deleteVideoText", _deletePhotoText);
		_moveFromPhotosText = locData.optString("moveFromPhotosText", _moveFromPhotosText);
		_moveFromVideosText = locData.optString("moveFromVideosText", _moveFromVideosText);
		_maxVideoText = locData.optString("maxVideoText", _maxVideoText);
		_maxVideoTimeCutWarning = locData.optString("videoCutWarning", _maxVideoTimeCutWarning);
		_alertCancelText = locData.optString("alertCancelText", _alertCancelText);
		_alertConfirmText = locData.optString("alertConfirmText", _alertConfirmText);
		_alertOkText = locData.optString("alertOkText", _alertOkText);

		_photoLabel = locData.optString("photoLabel", _photoLabel);
		_photosLabel = locData.optString("photosLabel", _photosLabel);
		_videoLabel = locData.optString("videoLabel", _videoLabel);
		_videosLabel = locData.optString("videosLabel", _videosLabel);
		_galleryLabel = locData.optString("galleryLabel", _galleryLabel);

		_cameraLockedErrorMessage = locData.optString("cameraLockedErrorMessage", _cameraLockedErrorMessage);
		_cameraPermissionExplanation = locData.optString("cameraPermissionExplanation", _cameraPermissionExplanation);
        _storageAccessExplanation = locData.optString("storagePermissionExplanation", _storageAccessExplanation);
        _recordAudioExplanation = locData.optString("recordAudioPermissionExplanation", _recordAudioExplanation);
	}

	public String getDeletePhotoText()
	{
		return _deletePhotoText;
	}

	public String getMaxPhotosText()
	{
		return _maxPhotosText;
	}

	public String getMaxVideoText()
	{
		return _maxVideoText;
	}


	public String getMoveFromPhotosText()
	{
		return _moveFromPhotosText;
	}

	public String getMoveFromVideosText()
	{
		return _moveFromVideosText;
	}


	public String getAlertCancelText()
	{
		return _alertCancelText;
	}

	public String getAlertConfirmText()
	{
		return _alertConfirmText;
	}

	public String getDeleteVideoText()
	{
		return _deleteVideoText;
	}

	public String getAlertOkText()
	{
		return _alertOkText;
	}

	public String getGalleryLabel()
	{
		return _galleryLabel;
	}

	public String getPhotoLabel()
	{
		return _photoLabel;
	}

	public String getPhotosLabel()
	{
		return _photosLabel;
	}

	public String getVideoLabel()
	{
		return _videoLabel;
	}

	public String getVideosLabel()
	{
		return _videosLabel;
	}

	public String formatVideoCutText(int secondsToCut)
	{
		return String.format(_maxVideoTimeCutWarning, secondsToCut);
	}

	public String getCameraLockedErrorMessage()
	{
		return _cameraLockedErrorMessage;
	}

	public String getCameraPermissionExplanation()
	{
		return _cameraPermissionExplanation;
	}

    public String getStorageAccessExplanation()
    {
        return _storageAccessExplanation;
    }

    public String getRecordAudioExplanation()
    {
        return _recordAudioExplanation;
    }
}
