package com.coolshutter.main;

import android.content.Context;
import android.database.Cursor;
import android.os.Handler;
import android.support.v4.util.LongSparseArray;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.coolshutter.util.ResourceLookup;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Const on 11/7/2016.
 * Project: android
 */
class GalleryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements GalleryItemInfo.BitmapEventsListener
{
    private static final boolean DEBUG = PluginConfig.DEBUG;
    private static final String TAG = GalleryAdapter.class.getSimpleName();

    private static final int CONTENT_VIEW_TYPE = 2;
    private static final int FOOTER_VIEW_TYPE = 3;

    static final int MODE_NOT_SET = 0;
    static final int MODE_PHOTO = 1;
    static final int MODE_VIDEO = 2;

    private int _currentMode = MODE_NOT_SET;

    private LayoutInflater _inflater;
    private Context _context;
    final private ResourceLookup _res;
    final private GalleryItemInfo.GalleryItemInfoHost _callback;

    private final LongSparseArray<String> _selectedItems;

    private int _goodItemsCount = 0;
    private int _loadedItems = 0;
    private Cursor _cursor;
    private ArrayList<MediaItem> _items = new ArrayList<>();
    private int _footerHeight;
    private int _columnWidth;
    private int _columnCount;

    private Handler _handler = new Handler();

    void setCurrentMode(int mode)
    {
        if (DEBUG) Log.d(TAG, "setCurrentMode "+mode);
        if (_currentMode != mode)
        {
            resetContent(true);
            _currentMode = mode;
        }
    }

    int getCurrentMode()
    {
        return _currentMode;
    }

    boolean isVideoGalleryActive()
    {
        return _currentMode == MODE_VIDEO;
    }

    boolean isPhotoGalleryActive()
    {
        return _currentMode == MODE_PHOTO;
    }

    GalleryAdapter(Context context, ResourceLookup res,
                          GalleryItemInfo.GalleryItemInfoHost hostCallback,
                          LongSparseArray<String> selectedItems)
    {
        _res = res;
        _selectedItems = selectedItems;
        _context = context;
        _callback = hostCallback;
        _inflater = LayoutInflater.from(context);

        setHasStableIds(true);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        switch (viewType)
        {
            case FOOTER_VIEW_TYPE:
            {
                View v = new View(_context);
                return new SpacerViewHolder(v);
            }

            case CONTENT_VIEW_TYPE:
            {
                View v = _inflater.inflate(_res.layout("gallery_grid_element"), parent, false);
                return new GalleryItemInfo(_callback, this, v, _res);
            }
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position)
    {
        int type = getItemViewType(position);
        //if (DEBUG) Log.d(TAG, "onBindViewHolder(), position = " + position + ", type = "+type + ", goodCount = "+_goodItemsCount);
        if (type == FOOTER_VIEW_TYPE)
        {
            ((SpacerViewHolder)holder).setHeight(_footerHeight);
        }
        else
        {
            bindContentView((GalleryItemInfo)holder, position);
        }
    }

    @Override
    public int getItemCount()
    {
        return _goodItemsCount + _columnCount;
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @Override
    public int getItemViewType(int position)
    {
        if (position < _goodItemsCount) return CONTENT_VIEW_TYPE;
        return FOOTER_VIEW_TYPE;
    }

    private void resetContent(boolean reloadData)
    {
        if (DEBUG) Log.d(TAG, "resetContent(), current goodCount = "+_goodItemsCount+", items count = "+_items.size());
        _goodItemsCount = 0;
        _loadedItems = 0;
        _items.clear();
        if (reloadData) notifyDataSetChanged();
    }

    void setNewCursor(Cursor cur)
    {
        if (DEBUG) Log.d(TAG, "setNewCursor(), cur = "+cur);
        _cursor = cur;
        resetContent(false);
        if (_cursor != null)
        {
            _goodItemsCount = _cursor.getCount();
            if (DEBUG) Log.i(TAG, "Count of items in cursor: "+_goodItemsCount);
            _items.ensureCapacity(_goodItemsCount);
        }
        notifyDataSetChanged();
    }

    private void loadItems(int lastIndex)
    {
        //if (DEBUG) Log.d(TAG, "loadItems, lastIndex = "+lastIndex);

        if (_items.size() > lastIndex) return;
        // Cursor has N items
        // Some of them (X) can be broken
        // thus, _items only contains N - X items
        // _goodItemsCount = N - X
        // position is in [0.._goodItemsCount) range
        boolean isVideo = isVideoGalleryActive();
        while (_items.size() <= lastIndex)
        {
            if (!_cursor.moveToPosition(_loadedItems++)) break;
            MediaItem item = new MediaItem(_cursor, isVideo);
            boolean fileExists = new File(item.getData()).canRead();
            if (fileExists) _items.add(item);
            else
            {
                if (DEBUG) Log.e(TAG, "Item's file does not exist: "+item);
            }
        }

        if (_items.size() <= lastIndex)
        {
            // we still need some items at the end of the list, but cursor doesn't have any more
            // we're going to add nulls instead of real items and then compact them away
            int curSize = _items.size();
            //if (DEBUG) Log.d(TAG, "Adding nulls, curSize = "+curSize+", need = "+lastIndex);
            for (int ii = curSize; ii <= lastIndex; ii++) _items.add(null);
            _handler.post(new Runnable()
            {
                @Override
                public void run()
                {
                    removeNullItems();
                }
            });
        }
        //if (DEBUG) Log.d(TAG, "Loaded items, count = "+_items.size());
    }

    private void removeNullItems()
    {
        for (int ii = _items.size() - 1; ii >= 0; ii--)
        {
            if (_items.get(ii) == null)
            {
                _items.remove(ii);
            }
        }
        _goodItemsCount = _items.size();
        notifyDataSetChanged();
    }

    private MediaItem getMediaItem(int position)
    {
        if (position >= _items.size())
        {
            loadItems(position);
        }

        return _items.get(position);
    }

    @Override
    public void onBitmapFailed(MediaItem item)
    {
        //if (DEBUG) Log.d(TAG, "Removing item: "+item);

        int itemIdx = -1;
        int ll = _items.size();
        for (int ii = 0; ii < ll; ii++)
        {
            if (_items.get(ii) == item)
            {
                itemIdx = ii;
                break;
            }
        }
        if (itemIdx < 0)
        {
            if (DEBUG) Log.e(TAG, "Can't find item to delete: "+item);
            return;
        }


        _items.remove(itemIdx);
        _goodItemsCount--;

        notifyItemRemoved(getItemCount() - 1);
        notifyItemRangeChanged(itemIdx, getItemCount() - itemIdx + 1);
        //if (DEBUG) Log.d(TAG, "Removed, remaining items = "+getItemCount());
    }

    private void bindContentView(GalleryItemInfo gii, int position)
    {
        MediaItem mi = getMediaItem(position);
        gii.bindData(_context, mi, _selectedItems, _columnWidth);
    }

    void setFooterHeight(int footerHeight)
    {
        _footerHeight = footerHeight;
    }

    void setColumnInfo(int columnCount, int columnWidth)
    {
        _columnCount = columnCount;
        _columnWidth = columnWidth;
    }

    @Override
    public void onViewRecycled(RecyclerView.ViewHolder holder)
    {
        super.onViewRecycled(holder);

        if (holder instanceof GalleryItemInfo)
        {
            GalleryItemInfo gii = (GalleryItemInfo) holder;
            gii.onRecycled();
        }
    }

    static private class SpacerViewHolder extends RecyclerView.ViewHolder
    {
        private View _view;

        SpacerViewHolder(View v)
        {
            super(v);
            _view = v;
        }

        public void setHeight(int h)
        {
            ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(1, h | View.MeasureSpec.EXACTLY);
            _view.setLayoutParams(params);
        }
    }

    static class MediaItem
    {
        final private long _id;
        final private boolean _isVideo;
        final private String _data;
        private int _orientation = 0;
        private long _videoDuration = -1;

        MediaItem(Cursor cursor, boolean isVideo)
        {
            _isVideo = isVideo;
            String id = cursor.getString(0);
            _id = Long.valueOf(id);
            _data = cursor.getString(1);

            if (!isVideo)
            {
                String orientation = cursor.getString(2);
                if (orientation != null) _orientation = Integer.valueOf(orientation);
            }
            else
            {
                _videoDuration = cursor.getLong(2);
            }
        }

        public String getData()
        {
            return _data;
        }

        public long getId()
        {
            return _id;
        }

        long getVideoDuration()
        {
            return _videoDuration;
        }

        public int getOrientation()
        {
            return _orientation;
        }

        public boolean isVideo()
        {
            return _isVideo;
        }


        @Override
        public String toString()
        {
            return "MediaItem{" +
                    "_data='" + _data + '\'' +
                    ", _id=" + _id +
                    ", _isVideo=" + _isVideo +
                    ", _orientation=" + _orientation +
                    ", _videoDuration=" + _videoDuration +
                    '}';
        }
    }

}
