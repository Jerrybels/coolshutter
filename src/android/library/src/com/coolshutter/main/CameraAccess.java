package com.coolshutter.main;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.WindowManager;

import com.coolshutter.util.Utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Const on 4/16/2015.
 * Project: CoolShutter
 */

@SuppressWarnings("deprecation")
@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
public class CameraAccess implements SurfaceHolder.Callback,
										Camera.ShutterCallback,
										Camera.PictureCallback,
										Camera.AutoFocusMoveCallback,
										Camera.AutoFocusCallback,
										MediaRecorder.OnInfoListener,
										MediaRecorder.OnErrorListener
{
	private static final String TAG = "CameraAccess";
	private static final boolean DEBUG = PluginConfig.DEBUG;

	public static final int FLASH_NOT_SUPPORTED = -1;
	public static final int FLASH_OFF = 0;
	public static final int FLASH_ON = 1;
	public static final int FLASH_AUTO = 2;
	private static final int FLASH_MODES_COUNT = 3;

	public static final int FOCUS_AREA_HIDE = 0;
	public static final int FOCUS_AREA_STARTED = 1;
	public static final int FOCUS_AREA_FAIL = 2;
	public static final int FOCUS_AREA_SUCCESS = 3;
	public static final int FOCUS_AREA_HIDE_NO_ANIM = 4;

	// When user taps on the screen, we are trying to ask camera to focus on the area under the tap
	// (if such functionality is supported by the camera). Then, focus stays locked in the requested position
	// for AUTOFOCUS_LOCK_TIME milliseconds to allow the user to take a picture.
	// After the timeout, focus lock is cancelled and camera autofocuses in continuous mode
	private static final long AUTOFOCUS_LOCK_TIME = 5000;
	private static final long AUTOFOCUS_FAIL_DISMISS_DELAY = 500;

	// in relative camera frame coords in the range of [-1000..1000],[-1000,1000]
	private static final int FOCUS_AREA_SIZE = 300;
	private static final int METERING_AREA_SIZE = 500;

	public interface CameraAccessCallbacks
	{
		void onFlashModeChanged(int newFlashMode);
		void onCameraSwitched(boolean backFacing);
		void onPictureTaken(@NonNull byte[] jpegData);
		void onShutter();
		void onMaxVideoDurationReached();
		void resizeCameraPreview(float cameraAspectRatio);
		void onVideoRecordingStopped(boolean errorHappened);

		// x, y - relative coords in the preview in range of [0..1],[0..1]
		void updateFocusArea(int mode, float x, float y);
		void onCameraAccessError(String errMessage);
	}


	static final private String[] FLASH_MODE_IDS = {
			Camera.Parameters.FLASH_MODE_OFF,
			Camera.Parameters.FLASH_MODE_ON,
			Camera.Parameters.FLASH_MODE_AUTO
	};

	private boolean _focusAreasSupported;
	private boolean _meteringAreasSupported;

	private final int SCREEN_WIDTH;
	private final int SCREEN_HEIGHT;

	private final Handler _handler = new Handler();
	private float _tapFocusX, _tapFocusY;
	private boolean _autoFocusInProgress = false;

	private SurfaceHolder _surfaceHolder;
	private final CameraPluginParams _pluginParams;
	private Camera _camera;
	private boolean _previewing = false;
	private boolean _videoModeActive = false;
	private int _currentCameraId = 0;
	private int _camerasCount = 0;
	private int _currentFlashMode = -1;
	private int _cameraDisplayOrientation = 0;

	private MediaRecorder _mediaRecorder = null;
	private boolean _mediaRecorderStarted = false;

	private Camera.Parameters _activeCameraParameters;
	private final Object _paramsSync = new Object();

	private final CameraAccessCallbacks _callbacks;

	private final Context _context;
    private boolean _cameraPermissionGranted = false;

	public CameraAccess(Context ctx, CameraPluginParams pluginParams, CameraAccessCallbacks cb)
	{
		_context = ctx;

		_pluginParams = pluginParams;
		_camerasCount = Camera.getNumberOfCameras();
		_callbacks = cb;

		DisplayMetrics metrics = ctx.getResources().getDisplayMetrics();
		SCREEN_WIDTH = metrics.widthPixels;
		SCREEN_HEIGHT = metrics.heightPixels;
	}

    public void setCameraPermissionGranted(boolean cameraPermissionGranted)
    {
        _cameraPermissionGranted = cameraPermissionGranted;
    }

    public boolean isRecording()
	{
		return _mediaRecorderStarted;
	}


	static public int getCamerasCount()
	{
		return Camera.getNumberOfCameras();
	}

	private int getCameraOrientation()
	{
		return _cameraDisplayOrientation;
	}

	public boolean readyToTakePicture()
	{
		return _previewing && _camera != null;
	}

	public void takePicture()
	{
		if (DEBUG) {
			if (_camera != null)
			{
				Camera.Parameters params = _camera.getParameters();
				Log.d(TAG, "Camera params: " + params.flatten());
			}
		}
		if (_camera != null) _camera.takePicture(this, null, null, this);
	}

	private int findDefaultFlashMode(Camera.Parameters params)
	{
		List<String> supportedModes = params.getSupportedFlashModes();
		if (supportedModes == null) return FLASH_NOT_SUPPORTED;

		// if there are no flash modes or only one mode, then just hide the flash button
		if (supportedModes.size() <= 0) return FLASH_NOT_SUPPORTED;

		if (supportedModes.contains(Camera.Parameters.FLASH_MODE_AUTO)) return FLASH_AUTO;

		// Make sure that if camera supports flash ON then it also supports OFF mode
		// We don't want to turn on the flash right now
		if (supportedModes.contains(Camera.Parameters.FLASH_MODE_ON) &&
				supportedModes.contains(Camera.Parameters.FLASH_MODE_OFF)) return FLASH_OFF;

		return FLASH_NOT_SUPPORTED;
	}

	private boolean isFlashModeSupported(String mode)
	{
		List<String> supportedModes = _activeCameraParameters.getSupportedFlashModes();
		if (supportedModes == null)
		{
			return false;
		}

		for (String flashMode : supportedModes)
		{
			if (mode.equals(flashMode)) return true;
		}

		return false;
	}

	private void activateFlashMode(String mode)
	{
		if (_camera == null) return;
		synchronized (_paramsSync)
		{
			_activeCameraParameters.setFlashMode(mode);
		}
		_camera.setParameters(_activeCameraParameters);
	}

	public void switchFlashMode()
	{
		int oldMode = _currentFlashMode;
		do
		{
			// try if any of the possible flash modes is supported
			_currentFlashMode++;
			if (_currentFlashMode >= FLASH_MODES_COUNT) _currentFlashMode = 0;

			// we tried all of them, but none is supported -> bail out
			if (_currentFlashMode == oldMode) return;
		}
		while (!isFlashModeSupported(FLASH_MODE_IDS[_currentFlashMode]));

		activateFlashMode(FLASH_MODE_IDS[_currentFlashMode]);
		_callbacks.onFlashModeChanged(_currentFlashMode);
	}

	private boolean isCurrentCameraBackFacing()
	{
		Camera.CameraInfo info = new Camera.CameraInfo();
		Camera.getCameraInfo(_currentCameraId, info);
		return info.facing == Camera.CameraInfo.CAMERA_FACING_BACK;
	}

	public void switchCamera()
	{
		if (_camerasCount <= 1) return;

		stopPreview();
		_currentCameraId++;
		if (_currentCameraId >= _camerasCount)
		{
			_currentCameraId = 0;
		}
		startPreview();

		_callbacks.onCameraSwitched(isCurrentCameraBackFacing());
	}

	public int getPhotoOrientation(int phoneOrientation)
	{
		boolean isBack = isCurrentCameraBackFacing();
		int orientation;
		if (DEBUG) Log.d(TAG, "Camera::getPhotoOrientation > camera is back facing = " + isBack);
		if (DEBUG) Log.d(TAG, "cameraDisplayOrientation = "+_cameraDisplayOrientation+" phoneOrientation = "+phoneOrientation);
		if (isBack)
		{
			orientation = _cameraDisplayOrientation + phoneOrientation;
		}
		else
		{
			orientation = _cameraDisplayOrientation - phoneOrientation;
		}
		orientation = orientation % 360;
		if (DEBUG) Log.d(TAG, "EXIF orientation = "+orientation);
		return orientation;
	}

	public void onPause()
	{
		if (isVideoModeActive() && isRecording())
		{
			stopVideoRecording();
		}
		stopPreview();
	}

	public void onResume()
	{
		if (DEBUG) Log.d(TAG, "onResume()");
		startPreview();
	}

	private void startPreview()
	{
		if (_surfaceHolder == null)
		{
			if (DEBUG) Log.d(TAG, "startPreview() -> _surfaceHolder == null");
			return;
		}

        if (!_cameraPermissionGranted)
        {
            if (DEBUG) Log.d(TAG, "startPreview() -> no camera permission");
            return;
        }

		if (_previewing)
        {
			// camera preview is active > just update parameters
			if (DEBUG) Log.d(TAG, "startPreview() -> _preview is already active, updating params");
			setupCameraParameters();
			setupCameraAutofocus();
			return;
		}

		if (DEBUG) Log.d(TAG, "Starting preview, camera = " + _camera);

		new AsyncCameraInitializer().execute();
	}

	private class AsyncCameraInitializer extends AsyncTask<Integer, Integer, Integer>
	{
		@Override
		protected Integer doInBackground(Integer... params)
		{
			try
			{
				if (_camera == null) _camera = Camera.open(_currentCameraId);

				setupCameraParameters();
				_camera.setPreviewDisplay(_surfaceHolder);
				updateCameraDisplayOrientation();
				_camera.startPreview();
				_previewing = true;
			}
			catch (IOException | RuntimeException e)
			{
				e.printStackTrace();
				_previewing = false;
				if (_camera != null) _camera.release();
				_camera = null;
			}
            return null;
		}

		@Override
		protected void onPostExecute(Integer integer)
		{
			if (_previewing)
			{
				_callbacks.onFlashModeChanged(_currentFlashMode);
				updatePreviewSize();
				setupCameraAutofocus();
				_callbacks.onCameraSwitched(isCurrentCameraBackFacing());
			}
			else
			{
				_callbacks.onCameraAccessError(_pluginParams.loc().getCameraLockedErrorMessage());
			}
		}

	}

	private void stopPreview()
	{
		_handler.removeCallbacks(_cancelAutoFocusRunnable);
		_callbacks.updateFocusArea(FOCUS_AREA_HIDE_NO_ANIM, 0, 0);
		if (_camera == null || !_previewing) return;

		if (DEBUG) Log.d(TAG, "Stopping preview");

		_previewing = false;
		_activeCameraParameters = null;
		_camera.stopPreview();
		_camera.release();
		_camera = null;
	}

	private void setupCameraParameters()
	{
		if (_camera == null) return;

		synchronized (_paramsSync)
		{
			final boolean firstCall = _activeCameraParameters == null;
			if (!firstCall) return;

			_activeCameraParameters = _camera.getParameters();
			_currentFlashMode = findDefaultFlashMode(_activeCameraParameters);
			if (_currentFlashMode != FLASH_NOT_SUPPORTED)
			{
				_activeCameraParameters.setFlashMode(FLASH_MODE_IDS[_currentFlashMode]);
			}

			_activeCameraParameters.setJpegQuality(_pluginParams.getQuality());

			_meteringAreasSupported = (_activeCameraParameters.getMaxNumMeteringAreas() > 0);

			Camera.Size previewSz = findBestPreviewSize(_activeCameraParameters, SCREEN_WIDTH, SCREEN_HEIGHT);
			_activeCameraParameters.setPreviewSize(previewSz.width, previewSz.height);


			// Setup default parameters
			if (_activeCameraParameters.getMinExposureCompensation() != 0 || _activeCameraParameters.getMaxExposureCompensation() != 0)
			{
				_activeCameraParameters.setExposureCompensation(0);
			}
			if (_activeCameraParameters.getSupportedSceneModes() != null)
			{
				_activeCameraParameters.setSceneMode(Camera.Parameters.SCENE_MODE_AUTO);
			}
			if (_activeCameraParameters.getSupportedColorEffects() != null)
			{
				_activeCameraParameters.setColorEffect(Camera.Parameters.EFFECT_NONE);
			}
			if (_activeCameraParameters.getSupportedWhiteBalance() != null)
			{
				_activeCameraParameters.setWhiteBalance(Camera.Parameters.WHITE_BALANCE_AUTO);
			}
			if (_activeCameraParameters.isZoomSupported())
			{
				_activeCameraParameters.setZoom(0);
			}

            Camera.Size photoSz = findBestMediaResolution(false);
            if (photoSz != null) _activeCameraParameters.setPictureSize(photoSz.width, photoSz.height);


            preparePreviewParameters(_activeCameraParameters);
			_camera.setParameters(_activeCameraParameters);
		}
	}

    private static Camera.Size findLargestResolutionInRange(List<Camera.Size> sizes,
                                                     int minW, int minH, int maxW, int maxH)
    {
        Camera.Size savedSz = null;
        // find highest resolution which fits inside requested parameters
        int maxFoundW = 0;
        for (Camera.Size sz : sizes)
        {
            if (sz.width < minW || sz.height < minH || sz.width > maxW || sz.height > maxH) continue;

            if (sz.width > maxFoundW)
            {
                maxFoundW = sz.width;
                savedSz = sz;
            }
        }
        return savedSz;
    }

    private Camera.Size findBestMediaResolution(boolean isVideo)
    {
        if (DEBUG) Log.d(TAG, "findBestMediaResolution, for video = "+isVideo);

        int maxW = isVideo ? _pluginParams.getMaxVideoWidth() : _pluginParams.getMaxPhotoWidth();
        int maxH = isVideo ? _pluginParams.getMaxVideoHeight() : _pluginParams.getMaxPhotoHeight();
        int minW = isVideo ? _pluginParams.getMinVideoWidth() : _pluginParams.getMinPhotoWidth();
        int minH = isVideo ? _pluginParams.getMinVideoHeight() : _pluginParams.getMinPhotoHeight();

        if (DEBUG) Log.d(TAG, "min = ("+minW+", "+minH+"), max = ("+maxW+", "+maxH+")");

        if (maxW == 0)
        {
            if (isVideo) return _activeCameraParameters.getPreferredPreviewSizeForVideo();

            maxH = maxW = Integer.MAX_VALUE;
            minH = minW = 0;
        }

        List<Camera.Size> sizes = isVideo ? _activeCameraParameters.getSupportedVideoSizes() : _activeCameraParameters.getSupportedPictureSizes();
        Camera.Size savedSz = findLargestResolutionInRange(sizes, minW, minH, maxW, maxH);

        if (savedSz == null)
        {
            // None of the existing resolutions is inside the requested range
            // Lets try to find resolution, which is larger than the requested
            int minFoundSz = Integer.MAX_VALUE;
            for (Camera.Size sz : sizes)
            {
                // we're only interested in resolutions higher than the upper requested limit
                if (sz.width < maxW || sz.height < maxH) continue;

                int newSz = sz.width * sz.height;
                if (newSz < minFoundSz)
                {
                    minFoundSz = newSz;
                    savedSz = sz;
                }
            }
        }

        if (savedSz == null)
        {
            // We couldn't find res a bit larger than the requested size (probably we have a low res camera?)
            // Just use the best res available
            savedSz = findLargestResolutionInRange(sizes, 0, 0, Integer.MAX_VALUE, Integer.MAX_VALUE);
        }
        if (DEBUG)
        {
            if (savedSz != null) Log.d(TAG, "Best matching media resolution is: ("+savedSz.width+", "+savedSz.height+")");
            else Log.d(TAG, "Didn't find any matching resolution");
        }
        return savedSz;
    }

    private void setupCameraAutofocus()
	{
		String mode = isVideoModeActive() ? Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO : Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE;
		_focusAreasSupported = false;
		if (!isFocusModeSupported(mode)) {
			mode = Camera.Parameters.FOCUS_MODE_AUTO;
			if (!isFocusModeSupported(mode)) mode = null;
		}

		if (mode != null)
		{
			// continuous focus mode is supported, set focus area
			int numFocusAreas = _activeCameraParameters.getMaxNumFocusAreas();
			if (numFocusAreas > 0) _focusAreasSupported = true;
			synchronized (_paramsSync)
			{
				_activeCameraParameters.setFocusMode(mode);
			}
		}


		if (DEBUG) Log.d(TAG, "Set focus mode to " + mode);
		_camera.setParameters(_activeCameraParameters);

//		if (Utils.hasJellyBean() && _focusAreasSupported)
//		{
//			_camera.setAutoFocusMoveCallback(this);
//		}
	}


	private void preparePreviewParameters(Camera.Parameters params)
	{
		int previewFormat = 0;
		for (int format : params.getSupportedPreviewFormats())
		{
			if (format == ImageFormat.NV21)
			{
				previewFormat = ImageFormat.NV21;
			}
			else
			if (previewFormat == 0 && (format == ImageFormat.JPEG || format == ImageFormat.RGB_565))
			{
				previewFormat = format;
			}
		}

		if (previewFormat != 0)
		{
			if (DEBUG) Log.d(TAG, "Preview format = "+previewFormat);
			params.setPreviewFormat(previewFormat);
		}
	}

	private boolean isFocusModeSupported(String mode)
	{
		List<String> supportedModes = _activeCameraParameters.getSupportedFocusModes();
		if (supportedModes == null)
		{
			return false;
		}

		for (String focusMode : supportedModes)
		{
			if (mode.equals(focusMode)) return true;
		}
		return false;
	}

	private void chooseBestPreviewFPS()
	{
		List<int[]> fpsRanges = _activeCameraParameters.getSupportedPreviewFpsRange();
		int count = fpsRanges.size();
		int min = fpsRanges.get(count - 1)[0];
		int max = fpsRanges.get(count - 1)[1];
		// The list is sorted from small to large (first by maximum fps and then minimum fps).

		// Find the range with the largest max fps and the lowest min fps
		for (int ii = count - 2; ii >= 0; ii--)
		{
			int[] range = fpsRanges.get(ii);
			if (range[1] != max) break;
			min = range[0];
		}

		_activeCameraParameters.setPreviewFpsRange(min, max);
	}

	// Make camera preview surface aspect ratio equal to that of the camera frame size
	private void updatePreviewSize()
	{
		if (_camera == null) return;

		chooseBestPreviewFPS();

		_camera.setParameters(_activeCameraParameters);

		if (DEBUG) Log.d(TAG, "Camera params: " + _activeCameraParameters.flatten());

		// adjust the camera size so it is always in portrait mode
		Camera.Size previewSz = _activeCameraParameters.getPreviewSize();
		if (previewSz.width > previewSz.height)
		{
			int tmp = previewSz.width;
			//noinspection SuspiciousNameCombination
			previewSz.width = previewSz.height;
			previewSz.height = tmp;
		}
		float camAspect = (float)previewSz.height / (float)previewSz.width;

		_callbacks.resizeCameraPreview(camAspect);

	}

	private Camera.Size findBestPreviewSize(Camera.Parameters params, int screenW, int screenH)
	{
		List<Camera.Size> sizes = params.getSupportedPreviewSizes();
		Camera.Size res = null;
		if (screenW > screenH)
		{
			int tmp = screenH;
			screenH = screenW;
			screenW = tmp;
		}
		if (DEBUG) Log.d(TAG, "Screen size: ("+screenW+", "+screenH+")");

		for (Camera.Size sz : sizes)
		{
			if (DEBUG) Log.d(TAG, "Preview candidate: ("+sz.width+", "+sz.height+")");
			if (Math.min(sz.width, sz.height) > screenW) {
				if (DEBUG) Log.d(TAG, "Size is larger than screen ");
				continue;
			}
			if (res == null) res = sz;
			else
			{
				if (sz.width*sz.height > res.width*res.height) res = sz;
			}
		}

		//if (DEBUG && res != null) Log.d(TAG, "Preview size selected: ("+res.width+", "+res.height+")");

		if (res == null) res = sizes.get(0);
		return res;
	}


	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width, int height)
	{
		if (DEBUG) Log.d(TAG, "surfaceChanged, w = "+width+" h = "+height+" isCreating = "+holder.isCreating());
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder)
	{
		_surfaceHolder = holder;
		startPreview();
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder)
	{
		stopPreview();
	}

	private void updateCameraDisplayOrientation()
	{
		if (_camera == null) return;

		Camera.CameraInfo info = new Camera.CameraInfo();
		Camera.getCameraInfo(_currentCameraId, info);
		WindowManager wmgr = (WindowManager) _context.getSystemService(Context.WINDOW_SERVICE);
		int rotation = wmgr.getDefaultDisplay().getRotation();
		int degrees = Utils.degreesFromSurfaceRotation(rotation);

		int result;
		int thumbOrientation;
		if (info.facing == Camera.CameraInfo.CAMERA_FACING_FRONT)
		{
			result = (info.orientation + degrees) % 360;
			thumbOrientation = result;
			result = (360 - result) % 360;  // compensate the mirror
		}
		else
		{  // back-facing
			result = (info.orientation - degrees + 360) % 360;
			thumbOrientation = result;
		}
		_camera.setDisplayOrientation(result);
		_cameraDisplayOrientation = thumbOrientation;
	}

	@Override
	public void onPictureTaken(byte[] data, Camera camera)
	{
		_callbacks.onPictureTaken(data);
		_camera.startPreview();
	}

	@Override
	public void onShutter()
	{
		_callbacks.onShutter();
	}

	// MediaRecorder.OnInfoListener
	@Override
	public void onInfo(MediaRecorder mr, int what, int extra)
	{
		if (what == MediaRecorder.MEDIA_RECORDER_INFO_MAX_DURATION_REACHED)
		{
			_callbacks.onMaxVideoDurationReached();

		}
		stopVideoRecording(false);
	}

	// MediaRecorder.OnErrorListener
	@Override
	public void onError(MediaRecorder mr, int what, int extra)
	{
		stopVideoRecording(true);
	}

	private boolean isVideoModeActive()
	{
		return _videoModeActive;
	}

	void setVideoModeActive(boolean videoModeActive)
	{
		_videoModeActive = videoModeActive;
		// call startPreview() to start preview with updated parameters for the new mode
		startPreview();
	}

	@Override
	public void onAutoFocusMoving(boolean start, Camera camera)
	{
		if (DEBUG) Log.d(TAG, "onAutoFocusMoving, start = "+start);
		if (!start)
		{
			_callbacks.updateFocusArea(FOCUS_AREA_HIDE, 0, 0);
			return;
		}
		List<Camera.Area> areas = camera.getParameters().getFocusAreas();
		float cx, cy;
		if (areas == null || areas.size() == 0)
		{
			cx = 0.5f;
			cy = 0.5f;
			if (DEBUG) Log.d(TAG, "NO focus area - show centered");
		}
		else
		{
			Camera.Area f = areas.get(0);
			if (DEBUG) Log.d(TAG, "focus area = "+f.rect.toString());
			Rect rc = f.rect;

			float x = rc.left + (rc.right - rc.left) / 2;
			float y = rc.top + (rc.bottom - rc.top) / 2;
			cx = (float) ((1000.0 + x) / 2000.0);
			cy = (float) ((1000.0 + y) / 2000.0);
			if (DEBUG) Log.d(TAG, "x = "+x+" y = "+y+" cx = "+ cx+" cy = "+cy);
		}

		_callbacks.updateFocusArea(FOCUS_AREA_STARTED, cx, cy);
	}

	@Override
	public void onAutoFocus(boolean success, Camera camera)
	{
		if (DEBUG) Log.d(TAG, "onAutoFocus, success = "+success);
		_autoFocusInProgress = false;
		_handler.postDelayed(_cancelAutoFocusRunnable, success ? AUTOFOCUS_LOCK_TIME : AUTOFOCUS_FAIL_DISMISS_DELAY);
		_callbacks.updateFocusArea(success ? FOCUS_AREA_SUCCESS : FOCUS_AREA_FAIL, _tapFocusX, _tapFocusY);
	}

	private Runnable _cancelAutoFocusRunnable = new Runnable()
	{
		@Override
		public void run()
		{
			synchronized (_paramsSync)
			{
				_activeCameraParameters.setFocusAreas(null);
				if (_meteringAreasSupported) _activeCameraParameters.setMeteringAreas(null);
			}
			_camera.setParameters(_activeCameraParameters);
			cancelAutoFocus();
		}
	};

	private void cancelAutoFocus()
	{
		_handler.removeCallbacks(_cancelAutoFocusRunnable);

		_camera.cancelAutoFocus();
		setupCameraAutofocus();
		_callbacks.updateFocusArea(FOCUS_AREA_HIDE, _tapFocusX, _tapFocusY);
		if (DEBUG) Log.d(TAG, "autoFocus cancelled");
	}

	// x, y - relative coordinates of the tap in [0..1],[0..1] range
	public void doAutoFocusAtPoint(float x, float y)
	{
		if (!_focusAreasSupported || !_previewing || _camera == null) return;

		if (_autoFocusInProgress)
		{
			cancelAutoFocus();
		}
		else
		{
			_handler.removeCallbacks(_cancelAutoFocusRunnable);
		}

		_tapFocusX = x;
		_tapFocusY = y;
		_callbacks.updateFocusArea(FOCUS_AREA_STARTED, _tapFocusX, _tapFocusY);

		if (DEBUG) Log.d(TAG, "Tap at coordinates x = "+x+" y = "+y);

		float[] coords = {x, y};
		Matrix previewToCamera = createPreviewToCameraTranslationMatrix();
		previewToCamera.mapPoints(coords);
		x = coords[0];
		y = coords[1];
		if (DEBUG) Log.d(TAG, "Mapped coordinates: x = "+x+" y = "+y);

		List<Camera.Area> focusAreas = new ArrayList<Camera.Area>(1);
		Rect focusRect = new Rect((int)(x - FOCUS_AREA_SIZE/2), (int)(y - FOCUS_AREA_SIZE/2),
									(int)(x + FOCUS_AREA_SIZE/2), (int)(y + FOCUS_AREA_SIZE/2));
		validateFocusAreaRect(focusRect);
		Camera.Area area = new Camera.Area(focusRect, 1000);
		focusAreas.add(area);

		List<Camera.Area> meteringAreas = null;
		if (_meteringAreasSupported)
		{
			Rect meteringRect = new Rect((int) (x - METERING_AREA_SIZE / 2), (int) (y - METERING_AREA_SIZE / 2),
					(int) (x + METERING_AREA_SIZE / 2), (int) (y + METERING_AREA_SIZE / 2));
			Camera.Area mArea = new Camera.Area(meteringRect, 1000);
			meteringAreas = new ArrayList<Camera.Area>(1);
			meteringAreas.add(mArea);
		}

		synchronized (_paramsSync)
		{
			_activeCameraParameters.setFocusAreas(focusAreas);
			_activeCameraParameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);

			if (meteringAreas != null) _activeCameraParameters.setMeteringAreas(meteringAreas);

			try
			{
				_camera.setParameters(_activeCameraParameters);
				if (DEBUG) Log.d(TAG, "Focusing at point x = "+x+" y = "+y+" rect = "+focusRect);
				_autoFocusInProgress = true;
				_camera.autoFocus(this);
			}
			catch (RuntimeException e)
			{
				_autoFocusInProgress = false;
				if (DEBUG) Log.e(TAG, "Error applying new autofocus area params", e);
			}
		}
	}

	private static void validateFocusAreaRect(Rect focusRect)
	{
		// all coords should be in the [-1000..1000],[-1000..1000] range
		if (focusRect.left < -1000)
		{
			focusRect.right += -1000 - focusRect.left;
			focusRect.left = -1000;
		}
		if (focusRect.top < -1000)
		{
			focusRect.bottom += -1000 - focusRect.top;
			focusRect.top = -1000;
		}
		if (focusRect.bottom > 1000)
		{
			focusRect.top -= focusRect.bottom - 1000;
			focusRect.bottom = 1000;
		}
		if (focusRect.right > 1000)
		{
			focusRect.left -= focusRect.right - 1000;
			focusRect.right = 1000;
		}
	}

	private Matrix createPreviewToCameraTranslationMatrix()
	{
		Matrix m = new Matrix();
		boolean mirror = !isCurrentCameraBackFacing();
		m.setScale(mirror ? -1 : 1, 1);

		m.postRotate(-getCameraOrientation(), 0.5f, 0.5f);
		m.postScale(2000f, 2000f);
		m.postTranslate(-1000f, -1000f);
		return m;
	}

    private int getMatchingVideoRecorderProfile(Camera.Size videoSize)
    {
        if (videoSize == null) return CamcorderProfile.QUALITY_HIGH;

        int targetSize = videoSize.width * videoSize.height;
        int minDist = Integer.MAX_VALUE;
        int bestProfile = -1;
        for (int profile = CamcorderProfile.QUALITY_QCIF; profile <= CamcorderProfile.QUALITY_2160P; profile++)
        {
            if (CamcorderProfile.hasProfile(_currentCameraId, profile))
            {
                CamcorderProfile description = CamcorderProfile.get(_currentCameraId, profile);
                int thisSize = description.videoFrameWidth * description.videoFrameHeight;
                int distance = Math.abs(targetSize - thisSize);
                if (distance < minDist)
                {
                    bestProfile = profile;
                    minDist = distance;
                }
            }
        }

        // QUALTITY_HIGH and QUALITY_LOW are guaranteed to exist, so we shouldn't be here
        return bestProfile >= 0 ? bestProfile : CamcorderProfile.QUALITY_HIGH;
    }

    public boolean startVideoRecording(String outputPath, int maxDuration, int orientationHint)
	{
		if (!readyToTakePicture()) return false;

		_camera.unlock();
		_mediaRecorder = new MediaRecorder();
		_mediaRecorder.setCamera(_camera);
		_mediaRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
		_mediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);

        Camera.Size videoSize = findBestMediaResolution(true);
        int videoProfileId = getMatchingVideoRecorderProfile(videoSize);
        CamcorderProfile videoProfile = CamcorderProfile.get(_currentCameraId, videoProfileId);
		_mediaRecorder.setProfile(videoProfile);

		_mediaRecorder.setOutputFile(outputPath);

		_mediaRecorder.setPreviewDisplay(_surfaceHolder.getSurface());

		_mediaRecorder.setOrientationHint(orientationHint);
		_mediaRecorder.setMaxDuration(maxDuration);
		_mediaRecorder.setOnInfoListener(this);
		_mediaRecorder.setOnErrorListener(this);
		try
		{
			_mediaRecorder.prepare();
			_mediaRecorder.start();
			_mediaRecorderStarted = true;
            Log.d(TAG, "Video recording started");
			return true;
		}
		catch (IOException e)
		{
			Log.e(TAG, "Cannot start video recording", e);
			stopVideoRecording(true);
			return false;
		}
		catch (IllegalStateException e)
		{
			Log.e(TAG, "Cannot start video recording", e);
			stopVideoRecording(true);
			return false;
		}
	}

    public void stopVideoRecording()
	{
		stopVideoRecording(false);
	}

	private void stopVideoRecording(boolean errorHappened)
	{
		if (_mediaRecorder != null)
		{
			try
			{
				if (_mediaRecorderStarted) _mediaRecorder.stop();
			}
			catch (RuntimeException e)
			{
				Log.e(TAG, "Error stopping MediaRecorder - probably it wasn't started before?", e);
				// Note that a RuntimeException is intentionally thrown to the application, if no valid audio/video
				// data has been received when stop() is called. This happens if stop() is called immediately after
				// start(). The failure lets the application take action accordingly to clean up the output file
				// (delete the output file, for instance), since the output file is not properly constructed when this happens.
				// Clean up last media part
				errorHappened = true;
			}
			finally
			{
				_mediaRecorder.reset();
				_mediaRecorder.release();
				_mediaRecorder = null;
			}
		}

		_mediaRecorderStarted = false;
		_callbacks.onVideoRecordingStopped(errorHappened);
	}


}
