package com.coolshutter.main;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Const on 4/2/2015.
 * Project: CoolShutter
 */
public class CoolShutterPlugin extends CordovaPlugin
{
	static final private int ACTIVITY_REQUEST_CODE = 123;

	private CallbackContext _callbackContext;

	/**
	 * Executes the request and returns PluginResult.
	 *
	 * @param action            The action to execute.
	 * @param args              JSONArry of arguments for the plugin.
	 * @param callbackContext   The callback id used when calling back into JavaScript.
	 * @return                  A PluginResult object with a status and message.
	 */
	@Override
	public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException
	{
		this._callbackContext = callbackContext;

		if (action.equals("pickItems"))
		{
			// parameters are in the JS object, encoded as a string
			String params = args.getString(0);

			Intent intent = new Intent(cordova.getActivity(), ShutterActivity.class);
			intent.putExtra(ShutterActivity.EXTRA_PLUGIN_PARAMS, params);
			cordova.startActivityForResult(this, intent, ACTIVITY_REQUEST_CODE);

//			callbackContext.error("Illegal Argument Exception");
//			PluginResult r = new PluginResult(PluginResult.Status.ERROR);
//			callbackContext.sendPluginResult(r);
//			return true;

			PluginResult r = new PluginResult(PluginResult.Status.NO_RESULT);
			r.setKeepCallback(true);
			callbackContext.sendPluginResult(r);

			return true;
		}
		else
		if (action.equals("cleanup"))
		{
			PluginResult r = new PluginResult(PluginResult.Status.NO_RESULT);
			r.setKeepCallback(true);
			callbackContext.sendPluginResult(r);
			new AsyncCleanup().execute(cordova.getActivity());
		}
		return false;
	}

	private class AsyncCleanup extends AsyncTask<Context, Integer, Integer>
	{
		@Override
		protected Integer doInBackground(Context... params)
		{
			Context ctx = params[0];
			ShutterActivity.cleanupTempFiles(ctx);
			return null;
		}

		@Override
		protected void onPostExecute(Integer integer)
		{
			_callbackContext.success("Cleanup done");
		}
	}

	/**
	 * Called when the camera view exits.
	 *
	 * @param requestCode       The request code originally supplied to startActivityForResult(),
	 *                          allowing you to identify who this result came from.
	 * @param resultCode        The integer result code returned by the child activity through its setResult().
	 * @param intent            An Intent, which can return result data to the caller (various data can be attached to Intent "extras").
	 */
	public void onActivityResult(int requestCode, int resultCode, Intent intent)
	{
		if (requestCode != ACTIVITY_REQUEST_CODE) return;

		// If cancelled
		if (resultCode == Activity.RESULT_CANCELED)
		{
			failPicture("Camera cancelled");
			return;
		}

		if (resultCode == Activity.RESULT_OK)
		{
			String resultFile = intent.getStringExtra(ShutterActivity.EXTRA_PLUGIN_RESULT);
			String result = readResultFromFile(resultFile);
			if (result == null)
			{
				_callbackContext.error("Got empty result string");
				return;
			}

			try
			{
				JSONObject json = new JSONObject(result);
				_callbackContext.success(json);
			}
			catch (JSONException e)
			{
				e.printStackTrace();
				_callbackContext.error("Failed to parse JSON result");
			}
		}
	}

	private String readResultFromFile(String tmpFile)
	{
		if (tmpFile == null) return null;

		File fin = new File(tmpFile);
		int sz = (int) fin.length();
		byte[] buf = new byte[sz];
		InputStream is = null;
		try
		{
			is = new FileInputStream(fin);
			is.read(buf);
			is.close();
			fin.delete();
			return new String(buf);
		}
		catch (IOException e)
		{
			e.printStackTrace();
			return null;
		}
	}


	/**
	 * Send error message to JavaScript.
	 *
	 * @param err error message
	 */
	public void failPicture(String err) {
		this._callbackContext.error(err);
	}



}
