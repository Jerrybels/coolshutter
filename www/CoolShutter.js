var argscheck = require('cordova/argscheck'),
    exec = require('cordova/exec');

var moduleExport = {};

/**
 * @param {Function} successCallback
 * @param {Function} errorCallback
 * @param {Object} options
 * Use CoolShutter.OptionsBuilder and CoolShutter.LocalizationBuilder to prepare options object
 */
moduleExport.pickItems = function(successCallback, errorCallback, options) {
    argscheck.checkArgs('fFO', 'CoolShutter.getPicture', arguments);
    options = options || {};

    var args = [JSON.stringify(options)];
    exec(successCallback, errorCallback, "CoolShutter", "pickItems", args);
};

moduleExport.cleanup = function(successCallback, errorCallback) {
    exec(successCallback, errorCallback, "CoolShutter", "cleanup", []);
};

module.exports = moduleExport;
