

var CoolShutterConstants = {
	AllowedSourceType : {
		PHOTO: 1,
		VIDEO: 2,
		GALLERY_PHOTO: 4,
		GALLERY_VIDEO: 8,
		ALL: 1 | 2 | 4 | 8
	}
};

function OptionsBuilder()
{
	return {
		options: {
			quality: 50,
			saveToGallery: false,
			allowedSourceFlags: CoolShutterConstants.AllowedSourceType.ALL,
			maxPhotos: 5,
			maxVideoTime: 30,
			generateThumbnails: false,
			thumbnailsAsFiles: true,
			maxThumbnailWidth: 240,
			maxThumbnailHeight: 320,
			minThumbnailWidth: 0,
			minThumbnailHeight: 0,

            maxPhotoWidth: 1400,
			minPhotoWidth: 1000,
            maxPhotoHeight: 1000,
            minPhotoHeight: 600,

            maxVideoWidth: 1400,
			minVideoWidth: 1000,
            maxVideoHeight: 1000,
            minVideoHeight: 600,

            downscalePhotosFromGallery: true,
            downscaleVideosFromGallery: true,

			localization: {}
		},

		sourcesToAllow: 0,

		build: function () {
			if (this.sourcesToAllow) this.options.allowedSourceFlags = this.sourcesToAllow;
			return this.options;
		},

		setQuality: function (quality) {
			this.options.quality = quality;
			return this;
		},

		setMaxPhotos: function (maxPhotos) {
			this.options.maxPhotos = maxPhotos;
			return this;
		},

		setMaxVideoTime: function (maxVideoDuration) {
			this.options.maxVideoTime = maxVideoDuration;
			return this;
		},

		setSaveToGallery: function (save)
		{
			this.options.saveToGallery = save;
			return this;
		},

		setAllSourcesAllowed: function ()
		{
			this.sourcesToAllow = CoolShutterConstants.AllowedSourceType.ALL;
			return this;
		},

		setPhotoSourceAllowed: function ()
		{
			this.sourcesToAllow |= CoolShutterConstants.AllowedSourceType.PHOTO;
			return this;
		},

		setVideoSourceAllowed: function ()
		{
			this.sourcesToAllow |= CoolShutterConstants.AllowedSourceType.VIDEO;
			return this;
		},

		setGalleryVideoSourceAllowed: function ()
		{
			this.sourcesToAllow |= CoolShutterConstants.AllowedSourceType.GALLERY_PHOTO;
			return this;
		},

		setGalleryPhotoSourceAllowed: function ()
		{
			this.sourcesToAllow |= CoolShutterConstants.AllowedSourceType.GALLERY_VIDEO;
			return this;
		},

		setGenerateThumbnails: function (set)
		{
			this.options.generateThumbnails = set;
			return this;
		},

		setThumbnailsAsFiles: function (set)
		{
			this.options.thumbnailsAsFiles = set;
			return this;
		},

		setMaxThumbnailWidth: function (size)
		{
			this.options.maxThumbnailWidth = size;
			return this;
		},

		setMaxThumbnailHeight: function (size)
		{
			this.options.maxThumbnailHeight = size;
			return this;
		},

		setMinThumbnailWidth: function (size)
		{
			this.options.minThumbnailWidth = size;
			return this;
		},

		setMinThumbnailHeight: function (size)
		{
			this.options.minThumbnailHeight = size;
			return this;
		},

        // Photo resolution bucket
        setDesiredPhotoResolution: function (minW, minH, maxW, maxH)
        {
            this.options.maxPhotoWidth = maxW;
            this.options.maxPhotoHeight = maxH;
            this.options.minPhotoWidth = minW;
            this.options.minPhotoHeight = minH;
            return this;
        },

        // Video resolution bucket
        setDesiredVideoResolution: function (minW, minH, maxW, maxH)
        {
            this.options.maxVideoWidth = maxW;
            this.options.maxVideoHeight = maxH;
            this.options.minVideoWidth = minW;
            this.options.minVideoHeight = minH;
            return this;
        },

        setDownscalePhotosFromGallery: function (set)
        {
            this.options.downscalePhotosFromGallery = set;
            return this;
        },

        setDownscaleVideosFromGallery: function (set)
        {
            this.options.downscaleVideosFromGallery = set;
            return this;
        },

        setLocalization: function (loc)
		{
			this.options.localization = loc;
			return this;
		}
	}
}


function LocalizationBuilder()
{
	return {
		loc: {
			maxPhotosText:"You have reached the maximum number of photos!",
			maxVideoText:"Maximum video length reached!",
			videoCutWarning:"This video is too long. Only the first %d seconds will be used.",
			deletePhotoText:"Are you sure that you want to delete this photo?",
			deleteVideoText:"Discard recorded video?",
			moveFromPhotosText:"Are you sure that you want to move away and lose the pictures you took?",
			moveFromVideosText:"Are you sure that you want to move away and lose the video you took?",
			alertCancelText:"Cancel",
			alertConfirmText:"Yes",
			alertOkText:"OK",
			photoLabel:"PHOTO",
			photosLabel:"PHOTOS",
			videoLabel:"VIDEO",
			videosLabel:"VIDEOS",
			galleryLabel:"GALLERY",
			cameraLockedErrorMessage:"Cannot initialize camera. Probably permission to use camera wasn't granted or it is in use by another app.",
			cameraPermissionExplanation:"Camera access permission is required to capture photos or videos",
			storagePermissionExplanation:"The app needs access to the external storage folders to be able to save new photos and videos",
			recordAudioPermissionExplanation:"The app needs access to the microphone to record video with audio"
		},

		build: function() {
			return this.loc;
		},

		setMaxPhotosText: function(txt) { this.loc.maxPhotosText = txt; return this; },
		setMaxVideoText: function(txt) { this.loc.maxVideoText = txt; return this;  },
		setVideoCutWarningText: function(txt) { this.loc.videoCutWarning = txt; return this; },
		setDeletePhotoText: function(txt) { this.loc.deletePhotoText = txt; return this;  },
		setDeleteVideoText: function(txt) { this.loc.deleteVideoText = txt; return this;  },
		setMoveFromPhotosText: function(txt) { this.loc.moveFromPhotosText = txt; return this;  },
		setMoveFromVideosText: function(txt) { this.loc.moveFromVideosText = txt; return this;  },

		setAlertCancelText: function(txt) { this.loc.alertCancelText = txt; return this;  },
		setAlertConfirmText: function(txt) { this.loc.alertConfirmText = txt; return this;  },
		setAlertOkText: function(txt) { this.loc.alertOkText = txt; return this;  },

		setPhotoLabelText: function(txt) { this.loc.photoLabel = txt; return this;  },
		setPhotosLabelText: function(txt) { this.loc.photosLabel = txt; return this;  },
		setVideoLabelText: function(txt) { this.loc.videoLabel = txt; return this;  },
		setVideosLabelText: function(txt) { this.loc.videosLabel = txt; return this;  },
		setGalleryLabelText: function(txt) { this.loc.galleryLabel = txt; return this;  },

		setCameraLockedErrorMessage: function(txt) { this.loc.cameraLockedErrorMessage = txt; return this; },
		setCameraPermissionExplanation: function(txt) { this.loc.cameraPermissionExplanation = txt; return this; },
		setStoragePermissionExplanation: function(txt) { this.loc.storagePermissionExplanation = txt; return this; },
		setRecordAudioPermissionExplanation: function(txt) { this.loc.recordAudioPermissionExplanation = txt; return this; }
	}
}
	
module.exports = {
	OptionsBuilder: OptionsBuilder,
	LocalizationBuilder: LocalizationBuilder
};
