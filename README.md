
# com.coolshutter.camera

This plugin defines a global `navigator.coolshutter` object, which provides an API for taking pictures, videos or for choosing images from the system's image library.

Although the object is attached to the global scoped `navigator`, it is not available until after the `deviceready` event.

    document.addEventListener("deviceready", onDeviceReady, false);
    function onDeviceReady() {
        console.log(navigator.coolshutter);
    }

## Installation
Currently, installation is only supported from a local folder:

    cordova plugin add /path/to/plugin/com.coolshutter.camera

## Build Troubleshooting
 
* __Android:__ Plugin depends on several external libraries, including Android Support v4, Recycler View and Picasso. Potentially there could be conflicts if other Cordova plugins in the project use different versions of these libraries. To resolve version conflicts you may edit ./src/android/library/build-extras.gradle and change library versions to be the same across all plugins. 
     

## navigator.coolshutter.pickItems

Shows the plugin UI, allowing to take photos, videos or select items from the device gallery.  Selected media items are passed to the success callback as an
array of URIs for the files.

    navigator.coolshutter.pickItems( success, error, pickerOptions );

### Description

Selected items are passed to the success callback in the result object:	


	function onSuccess(result) {
		for (var ii = 0, ll = result.items.length; ii < ll; ii++)
		{
				var item = result.items[ii];
				// If thumbnail generation was requested in pickerOptions, you can use picked media thumbnails, encoded in BASE64:
				app.addPhotoPreview(item.thumbnailData, item.thumbWidth, item.thumbHeight);

				// or, for thumbnails saved to temporary files:
				app.addPhotoPreview(item.thumbnailUri, item.thumbWidth, item.thumbHeight);

				// To access original files, use item.fileUri:
				app.uploadMedia(item.fileUri);				
		}
	}

Result object looks like that:

	{
		"items":[
			{
				fileUri:"/storage/path/to/the/selecteditem.jpg",
				orientation:90,
				thumbnailData:"data:image/jpeg;base64,/9j/4AAQSkZJRgA...", // if BASE64 encoded thumbnails requested
				thumbnailUri: "/storage/.../cache/coolshutter/123423423.jpg", // if file based thumbnails requested  
				thumbWidth:256,
				thumbHeight:144
			},
			..
		]
	}
- __fileUri__: local file path to the selected media item.
- __orientation__: photo or video orientation in degrees (can be 0, 90, 180 or 270). Usually, photos and videos are saved in native camera matrix orientation (mostly landscape). To correctly display these media files, EXIF or video metadata headers with correct orientation are used. Since WebView does not read those headers at all, you'll need to adjust photo orientation for correct display manually. One of the possible solutions is to apply CSS `transform: rotate(orientation)` style to the photo container.
- __thumbnailData__: present only if `generateThumbnails = true` and `thumbnailsAsFiles = false`. Contains generated thumbnail in data URI scheme. Can be used for previews of picked items: `img.src = item.thumbnailData;`
- __thumbnailUri__: present only if `generateThumbnails = true` and `thumbnailsAsFiles = true`. Contains URI to a file with a generated thumbnail in JPEG format.
- __thumbWidth__, __thumbHeight__: actual dimensions of the thumbnail image in pixels.  

Error message is passed to the error callback:

    function(message) {
        // Handle error
    }
 


### Supported Platforms

- Android API level >= 14 (Android 4.0 and up)
- iOS minimum target os version is 6.0, build target SDK = 8.x and up

### Plugin Options

Optional parameters to customize the plugin appearance and settings.

    { 
		quality: 90,
		saveToGallery: false,
		allowedSourceFlags: CoolShutterConstants.AllowedSourceType.PHOTO | CoolShutterConstants.AllowedSourceType.VIDEO,
		maxPhotos: 10,
		maxVideoTime: 120,
		generateThumbnails: false,
		thumbnailsAsFiles: true,
		maxThumbnailWidth: 240,
		maxThumbnailHeight: 320,
		minThumbnailWidth: 100,
		minThumbnailHeight: 100,

        maxPhotoWidth: 1400,
		minPhotoWidth: 1000,
        maxPhotoHeight: 1000,
        minPhotoHeight: 600,

        maxVideoWidth: 1400,
		minVideoWidth: 1000,
        maxVideoHeight: 1000,
        minVideoHeight: 600,

        downscalePhotosFromGallery: true,
        downscaleVideosFromGallery: true,

		localization : { ... }
	 };


- __quality__: Quality of the saved image, expressed as a range of 0-100, where 100 is typically full resolution with no loss from file compression. The default is 50. _(Number)_ (Note that information about the camera's resolution is unavailable.)

- __saveToGallery__: is the image saved also to the device photo library or only returned to the app’s handler. Default is false.

- __allowedSourceFlags__: one or bitwise combination of any of the following flags (Default is `CoolShutterConstants.AllowedSourceType.ALL`)

  * `CoolShutterConstants.AllowedSourceType.PHOTO` - allow to take photos with the camera.
  * `CoolShutterConstants.AllowedSourceType.VIDEO` - allow to record video with the camera.
  * `CoolShutterConstants.AllowedSourceType.GALLERY_PHOTO` - allow to choose photos from the photo library.
  * `CoolShutterConstants.AllowedSourceType.GALLERY_VIDEO` - allow to choose videos from the photo library.
  * `CoolShutterConstants.AllowedSourceType.ALL` - allow all media sources.

- __maxPhotos__: max number of pics to take or select before asking the user to upload. Default is `5`.

- __maxVideoTime__: Maximum video duration user can record before uploading (in seconds). Default is `30`.

- __generateThumbnails__: If set to true, plugin will generate base64 encoded thumbnails for every picked media item and pass them to the success callback. (Default is false).
- __thumbnailsAsFiles__: If set to true, generated thumbnails will be saved as temporary files in JPEG format. URI of that file will be added to the plugin result object. 
 
- __maxThumbnailWidth__, __maxThumbnailHeight__: Generated thumbnails will be scaled down to fit `maxThumbnailWidth x maxThumbnailHeight`. If one of the `maxThumbnailXXX` parameters is set to 0, it will not be used for limiting thumbnail size. For example, if `maxThumbnailHeight = 0`, the image will always have width of `maxThumbnailWidth` and proportionally scaled height.  __Use with care with BASE64 encoded thumbnails__ - large images may require a lot of memory to be passed from the native code to JS. Multiplied by `maxPhotos` parameter, that could potentially result in Out of Memory crashes. For a hi-resolution thumbnails it is recommended to save thumbnails as files.
- __minThumbnailWidth__, __minThumbnailHeight__: If set to anything but 0, will not allow thumbnail to have corresponding dimension less than the specified value. These parameters take precedence over maxThumbnailWidth and maxThumbnailHeight (i.e. if maxWidth and maxHeight are 200 and minWidth and minHeight are 100, and original image is 1000x100 pixels in size, the resulting thumbnail will have 1000x100 pixels, since minHeight is limited to 100 pixels).              

- __maxPhotoWidth, minPhotoWidth, maxPhotoHeight, minPhotoHeight:__ sets desired photo resolution. Plugin tries to select camera resolution inside specified range. If it is not possible, it selects resolution as close to the requested range as possible. If maxPhotoWidth is 0, best available hardware resolution is selected.

- __maxVideoWidth, minVideoWidth, maxVideoHeight, minVideoHeight:__ desired video resolution. If maxVideoWidth is 0, recommended hardware video resolution is selected.   

- __downscalePhotosFromGallery:__ if set to true, photo size constraints are also applied to the photos selected from media gallery (i.e. photos are resized before being returned to the Cordova app).
 
- __downscaleVideosFromGallery:__ if set to true, video size constraints are also applied to the videos selected from media gallery. __Note__: Video transcoder is only supported on Android 4.3+ and target resolution support is OEM dependent. If plugin fails to transcode the video, it provides it in original resolution.   

- __localization__:
Object containing localized strings for the UI
 
		{
			maxPhotosText:"You have reached the maximum number of photos!",
			maxVideoText:"Maximum video length reached!",
			videoCutWarning:"This video is too long. Only the first %d seconds will be used.",
			deletePhotoText:"Are you sure that you want to delete this photo?",
			deleteVideoText:"Discard recorded video?",
			moveFromPhotosText:"Are you sure that you want to move away and lose the pictures you took?",
			moveFromVideosText:"Are you sure that you want to move away and lose the video you took?",
			alertCancelText:"Cancel",
			alertConfirmText:"Yes",
			alertOkText:"OK",
			photoLabel:"PHOTO", // Camera mode button
			photosLabel:"PHOTOS", // Gallery filter label
			videoLabel:"VIDEO", // Video mode button 
			videosLabel:"VIDEOS", // Gallery filter label
			galleryLabel:"GALLERY", // Gallery mode button
			cameraLockedErrorMessage:"Cannot initialize camera. Probably permission to use camera wasn't granted or it is in use by another app.",
			cameraPermissionExplanation:"Camera access permission is required to capture photos or videos",
			storagePermissionExplanation:"The app needs access to the external storage folders to be able to save new photos and videos", // Android only
			recordAudioPermissionExplanation:"The app needs access to the microphone to record video with audio"			 
		}


### Options Builder
You can also use `CoolShutter.LocalizationBuilder` and `CoolShutter.OptionsBuilder` classes to setup plugin options object in a more convenient way.



### Example

	// Prepare plugin options object
	function buildCoolShutterOptions() {
		var loc = CoolShutter.LocalizationBuilder()
			.setMaxPhotosText("You took maximum allowed number of photos")
			.setMaxVideoText("Maximum video duration reached")
			.setVideoCutWarningText("This video is too long. Only the first %d seconds will be used.")
			.setDeletePhotoText("Delete photo?")
			.setDeleteVideoText("Discard recorded video?")
			.setMoveFromPhotosText("Switch away from camera and discard the photos you took?")
			.setMoveFromVideosText("Switch away from video recorder and discard recorded video?")
			.setAlertCancelText("Cancel")
			.setAlertConfirmText("OK")
			.setAlertOkText("OK")
			.setPhotoLabelText("Photo")
			.setPhotosLabelText("Photos")
			.setVideoLabelText("Video")
			.setVideosLabelText("Videos")
			.setGalleryLabelText("Gallery")
		    .setCameraLockedErrorMessage("...")
		    .setCameraPermissionExplanation("...")
		    .setStoragePermissionExplanation("...")
		    .setRecordAudioPermissionExplanation("...")
			.build();

		return CoolShutter.OptionsBuilder()
			.setLocalization(loc)
			.setMaxPhotos(5)
			.setQuality(90)
			.setSaveToGallery(false)
			.setAllSourcesAllowed() 
			// or .setPhotoSourceAllowed(), .setVideoSourceAllowed(), 
			// .setGalleryVideoSourceAllowed(), .setGalleryPhotoSourceAllowed()
			.setMaxVideoTime(60)
			.setGenerateThumbnails(true)
			.setThumbnailsAsFiles(true)
			.setMaxThumbnailWidth(240)
			.setMaxThumbnailHeight(320)
			.setMinThumbnailWidth(100)
			.setMinThumbnailHeight(100)
			.setDesiredPhotoResolution(minW, minH, maxW, maxH)
			.setDesiredVideoResolution(minW, minH, maxW, maxH)
			.setDownscalePhotosFromGallery(true)
			.setDownscaleVideosFromGallery(false)
			.build();
	}

	// Request plugin UI
	function pickMediaItems() {
		navigator.coolshutter.pickItems(onSuccess, onFail, buildCoolShutterOptions());

		function onSuccess(result) {
			for (var ii = 0, ll = result.items.length; ii < ll; ii++)
			{
				app.addPhotoPreview(result.items[ii]);
			}

		function onFail(message) {
			alert('Failed because: ' + message);
		}
	}

## navigator.coolshutter.cleanup

Removes intermediate photos taken by the camera from temporary
storage.

    navigator.camera.cleanup( cameraSuccess, cameraError );

### Description

Removes intermediate image files that are kept in temporary storage
after calling `coolshutter.pickItems`.

### Example

    navigator.camera.cleanup(onSuccess, onFail);

    function onSuccess() {
        console.log("Camera cleanup success.")
    }

    function onFail(message) {
        alert('Failed because: ' + message);
    }

